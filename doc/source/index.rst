******************************
  COMODO Tools Documentation
******************************

Release: |version| -- |today|

.. only:: html

	Foreword
	--------

.. only:: latex

	.. rubric:: Foreword

If you want to know more about the COMODO projet, please visit its official page: `<http://www.comodo-ocean.fr/>`_.

You can download the COMODO tools package on its development page `[:HERE:] <https://forge.imag.fr/scm/?group_id=398>`_.

On this pages, we will find:

 #. the specification of the COMODO file format for NetCDF oceanic model outputs.
 #. the reference documentation of the `pycomodo <https://forge.imag.fr/projects/pycomodo/>`_
    library designed to write Python scripts for producing model-independent diagnostics, either
    in the form of graphics or raw data in text format or NetCDF.
 #. tables of standard names for the univocal description of the variables found in the model output
    NetCDF files.

.. only:: html

	Table of contents
	-----------------

.. toctree::
	:maxdepth: 2
	:numbered:

	norm.rst
	api/index.rst
	examples.rst
	stdnames.rst
	budget.rst

.. only:: html

	* :ref:`genindex`
	* :ref:`modindex`
	* :ref:`search`
