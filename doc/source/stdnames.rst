.. _stdnames_label:

Standard Names
==============

The ``standard_name`` values used in the COMODO norm must conform either to the
`CF Standard Name convention <http://cf-pcmdi.llnl.gov/documents/cf-standard-names/>`_
or to the COMODO extensions.

.. include:: table_stdnames.inc
