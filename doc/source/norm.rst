.. _norm-comodo:

====================================
 COMODO file format: specifications
====================================

:Revisions:
    - 2013-12-13: M. Honnorat
    - 2012-05-03: V. Garnier

Introduction
============

This document intends to standardize the NetCDF file format of the output from the ocean models involved in the COMODO ANR project (COmmunauté de MODélisation Océanographique). It will be denoted below as the *COMODO file format*.
The aim is to facilitate the exchange of data and the development of interoperable diagnostics and post-processing tools.
This synthesis is based on the following material:

- discussions from the MARMO group (Toulouse, july 2010)
- the NetCDF Climate and Forecast (CF) Metadata Conventions [CF]_:
	- `<http://cf-pcmdi.llnl.gov/documents/cf-conventions/1.6/cf-conventions.html/>`_
	- `<http://cf-pcmdi.llnl.gov/documents/cf-standard-names>`_
- the CMIP5 Coupled Model Intercomparison Project:
	- `<http://cmip-pcmdi.llnl.gov/cmip5/data_description.html>`_
- several meetings and discussion within the COMODO ANR project.

Requirements
------------
- model outputs must be files in the `NetCDF format <http://www.unidata.ucar.edu/software/netcdf/>`_.
- the data is stored in structured rectangular grids (any type of Arakawa grid).

Overview
--------
The COMODO file format is based on the NetCDF Climate and Forecast (CF) Metadata Conventions [CF]_. A NetCDF file in the COMODO format should be fully compliant with the CF conventions.

The name of variables and dimensions can be chosen freely, as well as the type of the variables. Therefore, in order to precisely document the dimensions of the physical domain, the relative position of staggered variables on the grid and the physical content of each variable, we request the use of two main techniques following the [CF]_ conventions:

- for dimensions: their corresponding :ref:`coordinate variables <coordvar-label>`.
- for variables: their ``standard_name`` :ref:`attribute <at_location-label>`.

Dimensions
==========

Any NetCDF file in the COMODO format has to satisfy the following statements:

- the number and the names of the dimensions are free ;
- the time dimension is defined as unlimited (*ie.* a record dimension) ;
- time and every space dimension is associated with a corresponding coordinate variables ;
- each spatial axis (``X``, ``Y`` or ``Z``) should be associated with as many dimensions as there
  are positional shifts.

.. _arakawa-label:

.. admonition:: Arakawa staggered grids

	To explain the last statement, let's consider an Arakawa C-grid. Cell-centered variables such as
	temperature, salinity or other tracer are said to be located on ``t``-points. The ``x`` component
	of the velocity (*u*) is staggered with the ``X`` axis ; it is located on a ``u``-point.

	.. figure:: figures/grid2d_hv.*
		:width: 70%
		:align: center
		:alt: Arakawa C-grid

	Let us denote by ``u``, ``v`` and ``w`` the points in a C-grid cell that are staggered
	respectively with the ``X``, ``Y``, ``Z`` axis. Also, ``f`` denotes the vorticity point that
	is staggered by both ``X`` and ``Y`` axes.
	We can define ``uw``, ``vw`` and ``fw``-points staggered by ``Z`` axis in addition to another one.

On a three-dimensional C-grid, there should be at least two dimensions for each axis: one for centered variables, one for staggered variables. For example for the ``X`` axis, the two guys can be called ``xi_rho`` and ``xi_u`` in ROMS, ``ni_t`` and ``ni_u`` in S, ``ni`` and ``ni_u`` in MARS, *etc.*

Therefore there should be at least 6 space dimensions.
This minimal choice has been made by ROMS: ::

	netcdf roms_his {
	    dimensions:
	        xi_rho  = 43 ;
	        xi_u    = 42 ;
	        eta_rho = 44 ;
	        eta_v   = 43 ;
	        s_rho   = 32 ;
	        s_w     = 33 ;
	        time    = UNLIMITED ; // (13 currently)
	    ...
	}

Here, a 2D variable defined on a vorticity point ``f`` can be declared with dimensions ``(eta_v, xi_u)``.

Of course, it is still possible to define more dimensions : ::

	netcdf bcvortex_mars {
	    dimensions:
	        nj   = 181 ;
	        ni   = 181 ;
	        nj_u = 181 ;
	        ni_u = 181 ;
	        nj_v = 181 ;
	        ni_v = 181 ;
	        nj_f = 181 ;
	        ni_f = 181 ;
	        level   = 10 ;
	        level_w = 10 ;
	        time = UNLIMITED ; // (3 currently)
	    ...
	}

.. _coordvar-label:

Coordinate variables
--------------------

A coordinate variable is a variable with the same name as a dimension. Refer to
:netcdf:`§2.3.1 <Variables>` of [NUG]_ and the :cf:`terminology <terminology>` section of [CF]_.
It is necessarily a 1D vector.

Coordinate variables are required for time and space dimensions. Their content is not imposed.
According to the CF convention, they can contain dimensional data (latitude or longitude, height, pressure,
*etc*.) a non-dimensional data. Cf. :cf:`[CF: Chap.4] <coordinate-types>` and :cf:`[CF: Chap.5]<coordinate-system>`.

However, the use of some variable attributes is required.

.. _grid1d-label:

.. admonition :: Example : 1d grid with staggered variables

	Here is an example that recapitulates all that will be explained afterwards :

	.. figure:: figures/grid1d.*
		:width: 90%
		:align: center

	Let's consider this 1D grid. It is designed to host two variables: one cell-centered, the other
	shifted to the left (on a ``u``-point). For this, two dimensions are defined, ``ni`` and ``ni_u``,
	with respective sizes of 10 and 9, as well as the two corresponding coordinated variables. ::

		netcdf example {
			dimensions:
				ni = 9 ;
				ni_u = 10 ;
			variables:
				float ni(ni) ;
					ni:axis = "X" ;
					ni:standard_name = "x_grid_index" ;
					ni:long_name = "x-dimension of the grid" ;
					ni:c_grid_dynamic_range = "2:8" ;
				float ni_u(ni_u) ;
					ni_u:axis = "X" ;
					ni_u:standard_name = "x_grid_index_at_u_location" ;
					ni_u:long_name = "x-dimension of the grid" ;
					ni_u:c_grid_dynamic_range = "3:8" ;
					ni_u:c_grid_axis_shift = -0.5 ;
			data:
				ni = 1, 2, 3, 4, 5, 6, 7, 8, 9 ;
				ni_u = 0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5 ;
		}


Attribute ``axis``
""""""""""""""""""

A coordinate variable must be fitted with the attribute ``axis``, which contains one of the characters
``X``, ``Y``, ``Z`` or ``T``, respectively to denote the longitudes, latitudes, vertical or time.


Attribute ``standard_name``
"""""""""""""""""""""""""""

The CF convention recommends to use, with no obligation, the attributes ``standard_name`` and ``long_name``.

If the variable is dimensional, the attribute ``units`` is mandatory. In this case, the CF convention provides
specific values for the ``standard_name`` attribute. Please refer to the examples given for
:ref:`horizontal <horizontal_axis-label>` and :ref:`vertical <vertical_axis-label>` axes.

The COMODO file format **requires** the ``standard_name`` attribute for coordinate variables.

.. note::

	In the case where a coordinate variable cannot be fitted with any kind of relevant dimensional data,
	or if the model does not want to give such information, it is possible to assign a default value
	to the ``standard_name`` attribute, which is specific to the COMODO file format:

		- ``[xyz]_grid_index`` with ``x``, ``y``, ``z`` according the value of the ``axis`` attribute ;
		- ``[xyz]_grid_index_at_[uvfw]_location`` to specify the position of variable on the grid
		  (see :ref:`at_location-label`).

	In this case, the value of the coordinate variable elements should correspond to the coordinate of
	the data along the given axis, with the convention that the first tracer (cell-centered) point
	is located at ``(1,1)``.

	In the above example, the coordinate variable ``ni`` and ``ni_u`` should contain : ::

		data:
		    ni = 1, 2, 3, 4, 5, 6, 7, 8, 9 ;
		    ni_u = 0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5 ;

.. _norm-c_grid_axis_shift:

Attribute ``c_grid_axis_shift``
"""""""""""""""""""""""""""""""

In the :ref:`given example<grid1d-label>`, the variable ``ni_u`` is shifted to the left by a half cell
with respect to the variable ``ni``.

In order to document this property, the COMODO file format provides a new attribute named ``c_grid_axis_shift``. It *must* be used for shifted coordinate variables and should be equal
to the value of the shift with respect to the cell center. Here, it is *-0.5*. ::

	variables:
	    float ni_u(ni_u) ;
	        [...]
	        ni_u:c_grid_axis_shift = -0.5 ;

.. _norm-c_grid_dynamic_range:

Attribute ``c_grid_dynamic_range``
""""""""""""""""""""""""""""""""""

A variable stored in a NetCDF file can contain dynamic data, that is actually computed by the model,
as well as boundary data (*ghost cells*, climatology, *etc.*). The COMODO file format promotes the
concept of *dynamic domain*. It consists of the section of the data array that is really evaluated by
the model equations.

To define a dynamic domain, the coordinate variables shall be fitted with an attribute
``c_grid_dynamic_range`` containing the corresponding array indices (Matlab/Fortran notation,
beginning with 1). In the above example, we have : ::

	variables:
	    float ni(ni) ;
	        ni:axis = "X" ;
	        ni:c_grid_dynamic_range = "2:8" ;
	    float ni_u(ni_u) ;
	        ni_u:axis = "X" ;
	        ni_u:c_grid_dynamic_range = "3:8" ;

It means that the dynamic domain for the axis ``ni_u`` is comprised between third and eighth indices, *ie.* for ``ni_u = 2.5 .. 7.5``.

If the attribute ``c_grid_dynamic_range`` is missing, its default value is ``1:dim`` where ``dim`` is the
value of the associated dimension. In other words, the whole array is considered as part of the dynamic domain.

.. _horizontal_axis-label:

Horizontal axes
---------------

The CF convention defines two kinds of dimensional horizontal coordinate variables, according
to their ``standard_name`` attribute :

- ``latitude`` or ``longitude`` : cf. :cf:`[CF: §4.1] <latitude-coordinate>` and
  :cf:`[CF: §4.2]<longitude-coordinate>`.
- ``projection_x_coordinate`` or ``projection_y_coordinate`` : cf.
  :cf:`[CF: §5.6]<grid-mappings-and-projections>` and :cf:`[CF: Appendix F]<appendix-grid-mappings>`.

Example for a basic lat-lon grid : ::

	float lat(lat) ;
	    lat:standard_name = "latitude" ;
	    lat:long_name = "latitude" ;
	    lat:units = "degrees_north" ;

However, for ideal test cases that are mapped on a :math:`\beta`-plan or an *f*-plan, the CF
convention says nothing. Therefore, the COMODO file format introduces two new standard names for
horizontal coordinate variables :

- ``plane_x_coordinate`` and ``plane_y_coordinate`` : Plane coordinates are distances in the x- and
  y-directions on a cartesian plane which is not a projection of the surface of the Earth.

.. _vertical_axis-label:

Vertical axis
-------------

A coordinate variable must define an attribute ``positive`` equal to ``up`` or ``down`` according to
whether the ``Z`` axis is oriented upwards or downwards.

The COMODO file format **requires** that it is possible to compute a dimensional value of the depth.

If the vertical axis is already dimensional (for example in geopotential system, the vertical coordinate
variable contains the depth in meters), then the ``units`` attribute is mandatory. ::

	float deptht(deptht) ;
	    deptht:axis = "Z" ;
	    deptht:standard_name = "depth" ;
	    deptht:long_name = "Vertical T levels" ;
	    deptht:units = "m" ;
	    deptht:positive = "down" ;
	    deptht:valid_min = 0. ;
	    deptht:valid_max = 10000. ;

If the vertical axis contains non-dimensional data, the CF convention provides a way to recompute a
physical vertical coordinated ``z(i,j,k)`` as a function of other variables found in the NetCDF file.
For that purpose, the ``formula_terms`` attribute must be used.

.. admonition :: Example : Sigma vertical coordinates

	The ``standard_name`` attribute of a vertical coordinate variable should be set to the value
	``ocean_sigma_coordinate``. In this case, the ``formula_terms`` attribute shall contain
	a string that define the association between the terms of the formula and the variables
	in the NetCDF file. ::

		netcdf example_sigma {
		    dimensions:
		        ni = 181 ;
		        nj = 181 ;
		        level = 10 ;
		        time = UNLIMITED ; // (10 currently)
		    variables:
		        float level(level) ;
		            level:standard_name = "ocean_sigma_coordinate" ;
		            level:long_name = "sigma level" ;
		            level:axis = "Z" ;
		            level:positive = "up" ;
		            level:valid_min = -1.f ;
		            level:valid_max = 0.f ;
		            level:formula_terms = "sigma: level eta: xe depth: h0" ;
		            level:formula_definition = "z(n,k,j,i) = eta(n,j,i) + sigma(k)*(depth(j,i)+eta(n,j,i))" ;
		        float xe(time, nj, ni) ;
		            xe:standard_name = "sea_surface_height_above_geoid" ;
		            xe:long_name = "sea surface height" ;
		            xe:units = "m" ;
		            xe:coordinates = "latitude longitude" ;
		        float h0(nj, ni) ;
		            h0:long_name = "bathymetry relative to the mean level" ;
		            h0:standard_name = "model_sea_floor_depth_below_geoid" ;
		            h0:units = "m" ;
		            h0:coordinates = "latitude longitude" ;
		    data:
		        level = -0.95, -0.85, -0.75, -0.65, -0.55, -0.45, -0.35, -0.25, -0.15, -0.05 ;
 		}

For generalized vertical s-coordinate, the official CF standard name table

.. _vertical_s_coordinate_g1_index:

.. admonition:: Ocean s-coordinate (ROMS original) `source <https://www.myroms.org/wiki/index.php/Vertical_S-coordinate#transform1>`_

	::

		standard_name = "ocean_s_coordinate_g1" ;

	**Definition :** ::

		z(n,k,j,i) = S1(k,j,i) + eta(n,j,i)*(1+S1(k,j,i)/depth(j,i))

		with :  S1(k,j,i) = depth_c*s(k) + (depth(j,i)-depth_c)*C(k)

	- ``z(n,k,j,i)`` : grid point elevation, increasing upwards, wrt. the reference level
	  (eg. the mean sea level).
	- ``eta(n,j,i)`` : water level above the reference level (positive upwards).
	- ``s(k)``       : adimensional vertical coordinate (0 <= k <= 1).
	- ``depth(j,i)`` : bottom depth (positive value) wrt. the reference level.
	- ``depth_c``    : a stretching parameter.
	- ``C(k)``       : vertical stretching function.

	The corresponding ``formula_terms`` attribute should be defined as follows : ::

		formula_terms = "s: var1 C: var2 eta: var4 depth: var4 depth_c: var5" ;

	where ``var1``, ``var2``, *etc.* are the name of the actual variables in the NetCDF file.

.. _vertical_s_coordinate_g2_index:

.. admonition:: Ocean s-coordinate (UCLA-ROMS 2005) `source <https://www.myroms.org/wiki/index.php/Vertical_S-coordinate#transform2>`_

	::

		standard_name = "ocean_s_coordinate_g2" ;

	**Definition :** ::

		z(n,k,j,i) = eta(n,j,i) + (eta(n,j,i)+depth(j,i))*S2(k,j,i)

		with :  S2(k,j,i) = ( depth_c*s(k) + depth(j,i)*C(k) ) / ( depth_c + depth(j,i) )

	The corresponding ``formula_terms`` attribute should be defined as follows : ::

		formula_terms = "s: var1 C: var2 eta: var4 depth: var4 depth_c: var5" ;

For a complete documentation, refer to :cf:`[CF: §4.3.2] <dimensionless-vertical-coordinate>` and
:cf:`[CF: Appendix D]<dimensionless-v-coord>`.

.. todo::

	Handle the case of *partial step* Z coordinate variables.


Time axis
---------

For a time coordinate variable, both ``axis`` and ``units`` attributes must be specified : ::

	double time(time) ;
	    time:long_name = "time" ;
	    time:units = "days since 1990-1-1 0:0:0" ;
	    time:axis = "T" ;

In addition, the COMODO file format requires that the time axis is the unique **UNLIMITED** (or **record**) dimension.

Concerning the ``units`` attribute, it is strongly recommended to use the
`ISO-8601 norm <http://en.wikipedia.org/wiki/ISO_8601>`_.

.. note::

	If the timezone is not specified, we use UTC by default.

	If both the time and timezone are omitted, we use 00:00:00 UTC by default.

::

	float time_counter(time_counter) ;
	    time_counter:standard_name = "time" ;
	    time_counter:long_name = "Time axis" ;
	    time_counter:axis = "T" ;
	    time_counter:units = "seconds since 2006-10-11 00:00:00" ;
	    time_counter:time_origin = "2006-10-11T02:00:00+02:00" ;
	    time_counter:calendar = "gregorian" ;

.. _variables-comodo:

Variables
==========

Any variable can be fitted with attributes, which can help to specify or clarify some aspects
of the corresponding data. Following the CF convention, for each variable :

- ``standard_name`` is **required** to uniquely and non-ambiguously identify the physical nature of
  the variable's content.
- ``long_name`` is recommended ; it may contain a short description of the variable's content
- ``units`` is required for all variables which represent a dimensional quantity.

Besides, the following attributes are recommended :

- ``valid_min`` and ``valid_max`` (cf. :nug:`NUG, Appendix B<Attribute-Conventions>`)
- ``_FillValue`` (preferred to ``missing_value``)
- ``coordinates`` to define the longitude and latitude on any point of the domain
  (cf. :cf:`CF Convention, chapter 5. <coordinate-system>`).

The type of the variables is free. Double precision is recommended for longitude and latitude of high
resolution domains.

Standard names
==============

The ``standard_name`` attribute for variables plays an important role in the COMODO file format,
since by design it is the only way to know precisely what kind of physical data is contained in
a variable.

Recognized names
----------------

The values of this attribute should be chosen either among the CF Standard Name Table or its COMODO
extension. See the :ref:`dedicated pages <stdnames_label>` for an exhaustive list of recognized standard
names. A separate list is dedicated to the :ref:`budget terms <budget-label>` in an
energy equation.

The construction of new standard names is possible and should follow the
`official guidelines <http://cf-pcmdi.llnl.gov/documents/cf-standard-names/guidelines>`_.

.. _at_location-label:

Grid placement
--------------

A prominent feature of the COMODO file format is the **systematic** use of grid placement suffixes in
standard names. For each standard name listed in the authorized tables, the value of the
``standard_name`` attribute should be supplemented by a suffix documenting the position of the variable
on the staggered grid.

This suffix has the form ``_at_[uvfw]_location`` where the character ``u``, ``v``, ``f`` or ``w`` is
chosen according to the point where the data is precisely defined on the :ref:`Arakawa C grid<arakawa-label>`.
The omission of this suffix means that the variables is cell-centered. ::

	double longitude_v(nj_v, ni_v) ;
	    longitude_v:standard_name = "longitude_at_v_location" ;
	    longitude_v:long_name = "longitude" ;
	    longitude_v:units = "degrees_east" ;
	    longitude_v:valid_min = -180. ;
	    longitude_v:valid_max = 180. ;
	    longitude_v:_FillValue = 1.7e+38 ;

	float vz(time, nk, nj_v, ni_v) ;
	    vz:standard_name = "sea_water_y_velocity_at_v_location" ;
	    vz:long_name = "3d meridional velocity" ;
	    vz:units = "m s-1" ;
	    vz:coordinates = "latitude_v longitude_v" ;
	    vz:valid_min = -100.f ;
	    vz:valid_max = 100.f ;
	    vz:_FillValue = 999.f ;

Another example : ::

	float u(time, s_rho, eta_rho, xi_u) ;
	    u:standard_name = "sea_water_x_velocity_at_u_location" ;
	    u:long_name = "u-momentum component" ;
	    u:units = "meter second-1" ;
	    u:field = "u-velocity, scalar, series" ;
	    u:coordinates = "lat_u lon_u" ;
	    u:_FillValue = -9999.f ;


Global attributes
=================

The only mandatory global attribute is ``Conventions``. It is a character string which shall contain
a white-spaced list of convention names that the NetCDF file is supposed to follow.
The convention name for the COMODO file format is "COMODO-1.4". ::

	// global attributes:
	    :Conventions = "CF-1.6 COMODO-1.4" ;


.. only:: html

	.. rubric:: Bibliography

.. [NUG] | The NetCDF Users' Guide :
		 | `<http://www.unidata.ucar.edu/software/netcdf/docs/netcdf.html>`_
.. [CF]  | NetCDF Climate and Forecast (CF) Metadata Conventions :
		 | `<http://cf-pcmdi.llnl.gov/documents/cf-conventions/1.6/cf-conventions.html>`_
.. [CFT] | Climate and Forcast (CF) Standard Name Table :
		 | `<http://cf-pcmdi.llnl.gov/documents/cf-standard-names>`_
