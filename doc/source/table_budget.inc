comodo-budget-terms.xml
-----------------------

- **Contact :** `marc.honnorat@gmail.com`
- **Version :** 1
- **Last modified :** 2013-11-21T06:00:00Z

Tendency terms
""""""""""""""

.. tabularcolumns:: |L|c|c| 

+-----------------------------------------------------------------------------------------------------------+----------+----------+
|Standard Name                                                                                              |Unit      |Staggering|
+===========================================================================================================+==========+==========+
|``tendency_of_sea_water_x_velocity``                                                                       |m s-2     |u         |
|                                                                                                           |          |          |
|**Description :** Derivative of :math:`u` with respect to time: :math:`\frac{\partial u}{\partial t}`.     |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``tendency_of_sea_water_y_velocity``                                                                       |m s-2     |v         |
|                                                                                                           |          |          |
|**Description :** Derivative of :math:`v` with respect to time: :math:`\frac{\partial v}{\partial t}`.     |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+

Advection terms
"""""""""""""""

.. tabularcolumns:: |L|c|c| 

+-----------------------------------------------------------------------------------------------------------+----------+----------+
|Standard Name                                                                                              |Unit      |Staggering|
+===========================================================================================================+==========+==========+
|``non_linear_advection_of_sea_water_x_velocity``                                                           |m s-2     |u         |
|                                                                                                           |          |          |
|**Description :** Non linear advection terms for :math:`u` in momentum equation: :math:`\ (\vec{\bf        |          |          |
|u}\cdot\nabla)\,u\ =\ u\,\partial_x u + v\,\partial_y u + w\,\partial_z u`                                 |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``x_non_linear_advection_of_sea_water_x_velocity``                                                         |m s-2     |u         |
|                                                                                                           |          |          |
|**Description :** Non linear advection term for :math:`u` along *x* axis in momentum equation: :math:`\    |          |          |
|u\,\partial_x u`                                                                                           |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``y_non_linear_advection_of_sea_water_x_velocity``                                                         |m s-2     |u         |
|                                                                                                           |          |          |
|**Description :** Non linear advection term for :math:`u` along *y* axis in momentum equation: :math:`\    |          |          |
|v\,\partial_y u`                                                                                           |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``upward_non_linear_advection_of_sea_water_x_velocity``                                                    |m s-2     |u         |
|                                                                                                           |          |          |
|**Description :** Non linear upward advection term for :math:`u` in momentum equation: :math:`\            |          |          |
|w\,\partial_z u`                                                                                           |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``non_linear_advection_of_sea_water_y_velocity``                                                           |m s-2     |v         |
|                                                                                                           |          |          |
|**Description :** Non linear advection terms for :math:`v` in momentum equation: :math:`\ (\vec{\bf        |          |          |
|u}\cdot\nabla)\,v\ =\ u\,\partial_x v + v\,\partial_y v + w\,\partial_z v`                                 |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``x_non_linear_advection_of_sea_water_y_velocity``                                                         |m s-2     |v         |
|                                                                                                           |          |          |
|**Description :** Non linear advection term for :math:`v` along *x* axis in momentum equation: :math:`\    |          |          |
|u\,\partial_x v`                                                                                           |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``y_non_linear_advection_of_sea_water_y_velocity``                                                         |m s-2     |v         |
|                                                                                                           |          |          |
|**Description :** Non linear advection term for :math:`v` along *y* axis in momentum equation: :math:`\    |          |          |
|v\,\partial_y v`                                                                                           |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``upward_non_linear_advection_of_sea_water_y_velocity``                                                    |m s-2     |v         |
|                                                                                                           |          |          |
|**Description :** Non linear upward advection term for :math:`v` in momentum equation: :math:`\            |          |          |
|w\,\partial_z v`                                                                                           |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+

Pressure gradient terms
"""""""""""""""""""""""

.. tabularcolumns:: |L|c|c| 

+-----------------------------------------------------------------------------------------------------------+----------+----------+
|Standard Name                                                                                              |Unit      |Staggering|
+===========================================================================================================+==========+==========+
|``x_pressure_gradient``                                                                                    |m s-2     |u         |
|                                                                                                           |          |          |
|**Description :** grid x-axis component of total pressure gradient: :math:`\ \frac{1}{\rho_0}\partial_xp`  |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``x_external_pressure_gradient``                                                                           |m s-2     |u         |
|                                                                                                           |          |          |
|**Description :** grid x-axis component of surface or external pressure gradient.                          |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``x_internal_pressure_gradient``                                                                           |m s-2     |u         |
|                                                                                                           |          |          |
|**Description :** grid x-axis component of surface or internal pressure gradient.                          |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``y_pressure_gradient``                                                                                    |m s-2     |v         |
|                                                                                                           |          |          |
|**Description :** grid v-axis component of total pressure gradient: :math:`\ \frac{1}{\rho_0}\partial_vp`  |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``y_external_pressure_gradient``                                                                           |m s-2     |v         |
|                                                                                                           |          |          |
|**Description :** grid v-axis component of surface or external pressure gradient.                          |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``y_internal_pressure_gradient``                                                                           |m s-2     |v         |
|                                                                                                           |          |          |
|**Description :** grid v-axis component of surface or internal pressure gradient.                          |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+

Mixing terms
""""""""""""

.. tabularcolumns:: |L|c|c| 

+-----------------------------------------------------------------------------------------------------------+----------+----------+
|Standard Name                                                                                              |Unit      |Staggering|
+===========================================================================================================+==========+==========+
|``horizontal_diffusion_of_sea_water_x_velocity``                                                           |m s-2     |u         |
|                                                                                                           |          |          |
|**Description :** Horizontal diffusion term for :math:`u` in momentum equation: :math:`\                   |          |          |
|\nabla_{\!h}\cdot\left(\kappa_h\nabla_{\!h}u\right)\ =\ ` :math:`\frac{\partial}{\partial                  |          |          |
|x}\left(\kappa_h\frac{\partial u}{\partial x}\right)+` :math:`\frac{\partial}{\partial                     |          |          |
|y}\left(\kappa_h\frac{\partial u}{\partial y}\right)`                                                      |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``x_diffusion_of_sea_water_x_velocity``                                                                    |m s-2     |u         |
|                                                                                                           |          |          |
|**Description :** Diffusion term for :math:`u` along *x* axis in momentum equation: :math:`\               |          |          |
|\frac{\partial}{\partial x}\left(\kappa_h\frac{\partial u}{\partial x}\right)`                             |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``y_diffusion_of_sea_water_x_velocity``                                                                    |m s-2     |u         |
|                                                                                                           |          |          |
|**Description :** Diffusion term for :math:`u` along *y* axis in momentum equation: :math:`\               |          |          |
|\frac{\partial}{\partial y}\left(\kappa_h\frac{\partial u}{\partial y}\right)`                             |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``upward_diffusion_of_sea_water_x_velocity``                                                               |m s-2     |u         |
|                                                                                                           |          |          |
|**Description :** Vertical mixing term for :math:`u` in momentum equation: :math:`\                        |          |          |
|\frac{\partial}{\partial z}\left(\kappa_v\frac{\partial u}{\partial z}\right)`                             |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``horizontal_diffusion_of_sea_water_y_velocity``                                                           |m s-2     |v         |
|                                                                                                           |          |          |
|**Description :** Horizontal diffusion term for :math:`v` in momentum equation: :math:`\                   |          |          |
|\nabla_{\!h}\cdot\left(\kappa_h\nabla_{\!h}v\right)\ =\ ` :math:`\frac{\partial}{\partial                  |          |          |
|x}\left(\kappa_h\frac{\partial v}{\partial x}\right)+` :math:`\frac{\partial}{\partial                     |          |          |
|y}\left(\kappa_h\frac{\partial v}{\partial y}\right)`                                                      |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``x_diffusion_of_sea_water_y_velocity``                                                                    |m s-2     |v         |
|                                                                                                           |          |          |
|**Description :** Diffusion term for :math:`v` along *x* axis in momentum equation: :math:`\               |          |          |
|\frac{\partial}{\partial x}\left(\kappa_h\frac{\partial v}{\partial x}\right)`                             |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``y_diffusion_of_sea_water_y_velocity``                                                                    |m s-2     |v         |
|                                                                                                           |          |          |
|**Description :** Diffusion term for :math:`v` along *y* axis in momentum equation: :math:`\               |          |          |
|\frac{\partial}{\partial y}\left(\kappa_h\frac{\partial v}{\partial y}\right)`                             |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``upward_diffusion_of_sea_water_y_velocity``                                                               |m s-2     |v         |
|                                                                                                           |          |          |
|**Description :** Vertical mixing term for :math:`v` in momentum equation: :math:`\                        |          |          |
|\frac{\partial}{\partial z}\left(\kappa_v\frac{\partial v}{\partial z}\right)`                             |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+

Coriolis terms
""""""""""""""

.. tabularcolumns:: |L|c|c| 

+-----------------------------------------------------------------------------------------------------------+----------+----------+
|Standard Name                                                                                              |Unit      |Staggering|
+===========================================================================================================+==========+==========+
|``x_coriolis_acceleration``                                                                                |m s-2     |u         |
|                                                                                                           |          |          |
|**Description :** Coriolis term: :math:`\ -f\cdot v`                                                       |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+
|``y_coriolis_acceleration``                                                                                |m s-2     |v         |
|                                                                                                           |          |          |
|**Description :** Coriolis term: :math:`\ f\cdot u`                                                        |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+

Other terms
"""""""""""

.. tabularcolumns:: |L|c|c| 

+-----------------------------------------------------------------------------------------------------------+----------+----------+
|Standard Name                                                                                              |Unit      |Staggering|
+===========================================================================================================+==========+==========+
|``sea_water_buoyancy``                                                                                     |m s-2     |t         |
|                                                                                                           |          |          |
|**Description :** buoyancy of sea water: :math:`\ -\frac{g}{\rho_0}\rho`                                   |          |          |
+-----------------------------------------------------------------------------------------------------------+----------+----------+

