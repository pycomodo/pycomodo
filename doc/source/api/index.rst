==================
 pycomodo library
==================

This chapter describes pycomodo library API. All classes and functions are defined in sub packages
such as :mod:`pycomodo.core`, but for ease of use, the most important ones are also placed in the
:mod:`pycomodo` package scope.

.. automodule:: pycomodo
    :members:
    :undoc-members:

.. toctree::

	installation.rst
	overview.rst
	pycomodo.core.rst
	pycomodo.io.rst
	pycomodo.operators.rst
	pycomodo.stdnames.rst
	pycomodo.util.rst

