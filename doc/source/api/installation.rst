Installation
============

The source code of `pycomodo` is hosted here :
https://forge.imag.fr/projects/pycomodo/

Requirements:
	* python >= 2.6
	* numpy >= 1.3

Optional:
	* `matplotlib <http://matplotlib.org/>`_ >= 0.99 (only for visualization scripts)
	* `python-netcdf4 <http://code.google.com/p/netcdf4-python/>`_ (for reading NetCDF4 files)
	* `numexpr <http://code.google.com/p/numexpr/>`_
	  (for efficient calls to :func:`pycomodo.operators.simple_ops.operation`).
	* `cython <http://cython.org/>`_ (for much more efficient loops in some algorithms).

The recommended way to install the library is to clone the git repository:

.. code-block:: bash

	$ cd /path/to/somewhere
	$ export GIT_SSL_NO_VERIFY=true
	$ git clone https://forge.imag.fr/anonscm/git/pycomodo/pycomodo.git

You should write the following line in your ``~/.profile`` configuration file.

.. code-block:: bash

	$ export PYTHONPATH=/path/to/somewhere/pycomodo:$PYTHONPATH

You can now use the library.

.. code-block:: bash

	$ cd pycomodo/examples
	$ ../bin/check_comodo.py nc_files/roms_vortex.nc
	$ ./plot_ssh.py   nc_files/roms_vortex.nc
	$ ./calc_pvort.py nc_files/roms_vortex.nc

In order to improve the performance of some modules (especially :mod:`pycomodo.util.internal`),
it is strongly recommended to install `cython <http://cython.org/>`_ and build a binary version
of the modules :

.. code-block:: bash

	$ cd /path/to/somewhere/pycomodo
	$ ./build_pyx.py
