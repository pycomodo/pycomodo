**pycomodo.operators**
======================

.. automodule:: pycomodo.operators
    :members:

.. autosummary::

	pycomodo.operators.indices
	pycomodo.operators.interpolation
	pycomodo.operators.operations
	pycomodo.operators.differentiate

:mod:`pycomodo.operators.indices`
----------------------------------------

.. automodule:: pycomodo.operators.indices
    :members:
    :undoc-members:

:mod:`pycomodo.operators.interpolation`
---------------------------------------

.. automodule:: pycomodo.operators.interpolation
    :members:
    :undoc-members:

:mod:`pycomodo.operators.operations`
------------------------------------

.. automodule:: pycomodo.operators.operations
    :members:
    :undoc-members:

:mod:`pycomodo.operators.differentiate`
---------------------------------------

.. automodule:: pycomodo.operators.differentiate
    :members:
    :undoc-members:
