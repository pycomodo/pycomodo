.. _overview:

Overview
========

Create a new Archive :

>>> import pycomodo
>>> archive = pycomodo.Archive("examples/nc_files/roms_vortex.nc")
>>> print archive.info(1)
Archive[ ('examples/nc_files/roms_vortex.nc') ]:
 - Dimensions : s_rho, s_w, eta_rho, eta_v, xi_rho, time, auxil, xi_u
 - Variables  : Cs_r, Cs_w, Vtransform, el, eta_rho, eta_v, f, h, hc, pm, pn, rho, s_rho, s_w, sc_r, sc_w,
                spherical, temp, time, time_step, u, ubar, v, vbar, w, x_rho, xi_rho, xi_u, xl, y_rho, zeta

Get a variable by its name in the file :

>>> ubar = archive.get_variable("ubar")
>>> print ubar.info()
    float32 ubar(time, eta_rho, xi_u)
        ubar:long_name = "vertically integrated u-momentum component"
        ubar:units = "meter second-1"
        ubar:standard_name = "barotropic_sea_water_x_velocity_at_u_location"

Get a variable by its standard name :

>>> ssh = archive.get_variable(stdname='sea_surface_height')
>>> print ssh.info()
    float32 zeta(time, eta_rho, xi_rho)
        zeta:long_name = "free-surface"
        zeta:units = "meter"
        zeta:standard_name = "sea_surface_height"
        zeta:coordinates = "x_rho y_rho"

or :

>>> u = archive.get_variable(stdname='sea_water_x_velocity', location='u')
>>> u.check(True)
u | [OK]     - standard_name: sea_water_x_velocity [at u location] (m s-1) [CF v.26]
  | [OK]     - has position 'u' on C grid.
  | [OK]     - has a checked coordinate variable 'time' for axis 'T'
  | [OK]     - is a correctly directed (positive='up')
  | [OK]     - has a checked coordinate variable 's_rho' for axis 'Z'
  | [OK]     - has a checked coordinate variable 'eta_rho' for axis 'Y'
  | [OK]     - has a checked coordinate variable 'xi_u' for axis 'X'

Compute derivatives :

>>> ssh_x = ssh.diff_x()
>>> ssh_y = ssh.diff_y()
>>> for v in (ssh, ssh_x, ssh_y):
...     print v.position, map(str, v.dimensions), v
...
t ['time', 'eta_rho', 'xi_rho'] zeta
u ['time', 'eta_rho', 'xi_u'] d_zeta_dx
v ['time', 'eta_v', 'xi_rho'] d_zeta_dy

Carry out interpolations :

>>> ssh_x_on_t = ssh_x.interpolate('t')
>>> ssh_y_on_t = ssh_y.interpolate('t')
>>> for v in (ssh, ssh_x_on_t, ssh_y_on_t):
...     print v.position, map(str, v.dimensions), v
...
t ['time', 'eta_rho', 'xi_rho'] zeta
t ['time', 'eta_rho', 'xi_rho'] d_zeta_dx_at_t_location
t ['time', 'eta_rho', 'xi_rho'] d_zeta_dy_at_t_location

Some calculus :

>>> div_ssh = ssh_x + ssh_y
Exception: _simple_op(+) error: both variables are not defined on the same grid.
 - Names  : 'd_zeta_dx' and 'd_zeta_dy'
 - Shapes : (3, 62, 61) and (3, 61, 62)
 - Grids  : 'u' and 'v'
>>> div_ssh = ssh_x_on_t + ssh_y_on_t
>>> div_ssh[:].min(), div_ssh[:].max()
(-1.1752767e-05, 1.1752767e-05)

Write variables to file :

>>> pycomodo.write_netcdf("div_ssh.nc", (ssh_x, ssh_y, div_ssh))

.. code-block:: bash

	$ ncview div_ssh.nc &

.. figure :: figures/div_ssh_ncview.*
	:width: 100%
	:align: center

Compute Z levels :

>>> z_at_t = archive.get_zlevels("t")
>>> print z_at_t.position, z_at_t.dimensions, z_at_t[0, :, 0, 0]
t ['time', 's_rho', 'eta_rho', 'xi_rho']
[-4750.0 -4250.0 -3750.0 -3250.0 -2750.0 -2250.0 -1750.0 -1250.0 -750.0 -250.0]

>>> z_at_w = archive.get_zlevels("w", axes="Z", tpoint=0, hpoint=(0, 0))
>>> print z_at_w.position, z_at_w.dimensions, z_at_w[:]
w ['s_w']
[-5000.0 -4500.0 -4000.0 -3500.0 -3000.0 -2500.0 -2000.0 -1500.0 -1000.0 -500.0 0.0]

Get some metrics :

>>> # Cell x size at cell center
>>> dx_t = archive.get_metric('x', 't')
>>> print dx_t.info()
	float32 inverse_of_pm(eta_rho, xi_rho)
		inverse_of_pm:standard_name = "cell_x_size"

>>> # Cell y size at vertically staggered vorticity points
>>> dy_fw = archive.get_metric('y', 'fw')
>>> print dy_fw.position, dy_fw.dimensions
fw ['eta_v', 'xi_u']

>>> # Cell thickness in 1d
>>> dz_t = archive.get_metric('z', 't', axes='Z', tpoint=0, hpoint=(1, 1))
>>> print dz_t.position, dz_t.dimensions, dz_t[:]
t ['s_rho']
[500.0 500.0 500.0 500.0 500.0 500.0 500.0 500.0 500.0 500.0]
