**pycomodo.util**
=================

.. automodule:: pycomodo.util
    :members:

.. autosummary::

	pycomodo.util.plots
	pycomodo.util.stdnames
	pycomodo.util.variables
	pycomodo.util.vertical
	pycomodo.util.courant
	pycomodo.util.internal

:mod:`pycomodo.util.plots`
--------------------------

.. automodule:: pycomodo.util.plots
    :members:
    :undoc-members:

:mod:`pycomodo.util.stdnames`
-----------------------------

.. automodule:: pycomodo.util.stdnames
    :members:
    :undoc-members:

:mod:`pycomodo.util.variables`
------------------------------

.. automodule:: pycomodo.util.variables
    :members:
    :undoc-members:

:mod:`pycomodo.util.vertical`
-----------------------------

.. automodule:: pycomodo.util.vertical
    :members:
    :undoc-members:

:mod:`pycomodo.util.courant`
----------------------------

.. automodule:: pycomodo.util.courant
    :members:
    :undoc-members:

:mod:`pycomodo.util.internal`
-----------------------------

.. automodule:: pycomodo.util.internal
    :members:
    :undoc-members:

