**pycomodo.io**
===============

.. automodule:: pycomodo.io
    :members:

.. autosummary::

	pycomodo.io.write_netcdf
	pycomodo.io.customlogger
	pycomodo.io.termcolor

:mod:`pycomodo.io.write_netcdf`
-------------------------------

.. automodule:: pycomodo.io.write_netcdf
    :members:
    :undoc-members:

:mod:`pycomodo.io.customlogger`
-------------------------------

.. automodule:: pycomodo.io.customlogger
    :members:
    :undoc-members:

:mod:`pycomodo.io.termcolor`
----------------------------

.. automodule:: pycomodo.io.termcolor
    :members:
    :undoc-members:

