**pycomodo.core**
=================

.. automodule:: pycomodo.core

.. autosummary::

	pycomodo.core.archive
	pycomodo.core.dimension
	pycomodo.core.variable
	pycomodo.core.netcdf_variable
	pycomodo.core.checker
	pycomodo.core.exceptions

:mod:`pycomodo.core.archive`
----------------------------

.. automodule:: pycomodo.core.archive
    :members:
    :undoc-members:

:mod:`pycomodo.core.dimension`
------------------------------

.. automodule:: pycomodo.core.dimension
    :members:
    :undoc-members:

:mod:`pycomodo.core.variable`
-----------------------------

.. automodule:: pycomodo.core.variable
    :members:
    :undoc-members:

:mod:`pycomodo.core.netcdf_variable`
------------------------------------

.. automodule:: pycomodo.core.netcdf_variable
    :members:
    :undoc-members:

:mod:`pycomodo.core.checker`
----------------------------

.. automodule:: pycomodo.core.checker
    :members:
    :undoc-members:

:mod:`pycomodo.core.exceptions`
-------------------------------

.. automodule:: pycomodo.core.exceptions
    :members:
    :undoc-members:

