**pycomodo.stdnames**
=====================

.. automodule:: pycomodo.stdnames
    :members:

.. autosummary::

	pycomodo.stdnames.standard_name_parser
	pycomodo.stdnames.stdname_to_sphinx

:mod:`pycomodo.stdnames.standard_name_parser`
---------------------------------------------

.. automodule:: pycomodo.stdnames.standard_name_parser
    :members:
    :undoc-members:

:mod:`pycomodo.stdnames.stdname_to_sphinx`
------------------------------------------

.. automodule:: pycomodo.stdnames.stdname_to_sphinx
    :members:
    :undoc-members:

