cf-standard-name-table.xml
--------------------------

- **Contact :** `webmaster@pcmdi.llnl.gov`
- **Version :** 26
- **Last modified :** 2013-11-08T06:09:34Z

To see the complete documentation of this list, please refer to the reference `CF Standard Name table <http://cf-pcmdi.llnl.gov/documents/cf-standard-names/standard-name-table/current/cf-standard-name-table.html>`_

comodo-standard-name-table.xml
------------------------------

- **Contact :** `marc.honnorat@gmail.com`
- **Version :** 2
- **Last modified :** 2013-11-20T18:01:28Z

Standard Names
""""""""""""""

.. tabularcolumns:: |L|c|

+-------------------------------------------------------------------------------------------------------------------------------+--------+
|Name                                                                                                                           |Unit    |
+===============================================================================================================================+========+
|``x_grid_index``                                                                                                               |index   |
|                                                                                                                               |        |
|**Description :** Generic name for a coordinate variable corresponding to a *x*-axis. Should be suffixed by an indicator       |        |
|'``_at_[uvfw]_location``' to specify the staggering of the axis.                                                               |        |
+-------------------------------------------------------------------------------------------------------------------------------+--------+
|``y_grid_index``                                                                                                               |index   |
|                                                                                                                               |        |
|**Description :** Generic name for a coordinate variable corresponding to a *y*-axis. Should be suffixed by an indicator       |        |
|'``_at_[uvfw]_location``' to specify the staggering of the axis.                                                               |        |
+-------------------------------------------------------------------------------------------------------------------------------+--------+
|``z_grid_index``                                                                                                               |index   |
|                                                                                                                               |        |
|**Description :** Generic name for a coordinate variable corresponding to a *z*-axis. Should be suffixed by an indicator       |        |
|'``_at_[uvfw]_location``' to specify the staggering of the axis.                                                               |        |
+-------------------------------------------------------------------------------------------------------------------------------+--------+
|``plane_x_coordinate``                                                                                                         |m       |
|                                                                                                                               |        |
|**Description :** In contrast to ``projection_[xy]_coordinate``, Plane coordinates are distances in the x- and y-directions on |        |
|a cartesian plane which is not a projection of the surface of the Earth, such as a f-plane or ß-plane. "x" indicates a vector  |        |
|component along the grid x-axis, positive with increasing x.                                                                   |        |
+-------------------------------------------------------------------------------------------------------------------------------+--------+
|``plane_y_coordinate``                                                                                                         |m       |
|                                                                                                                               |        |
|**Description :** In contrast to ``projection_[xy]_coordinate``, Plane coordinates are distances in the x- and y-directions on |        |
|a cartesian plane which is not a projection of the surface of the Earth, such as a f-plane or ß-plane. "y" indicates a vector  |        |
|component along the grid y-axis, positive with increasing y.                                                                   |        |
+-------------------------------------------------------------------------------------------------------------------------------+--------+
|``cell_x_size``                                                                                                                |m       |
|                                                                                                                               |        |
|**Description :** Physical dimension (length) of the cells along the grid *x*-axis.                                            |        |
+-------------------------------------------------------------------------------------------------------------------------------+--------+
|``cell_y_size``                                                                                                                |m       |
|                                                                                                                               |        |
|**Description :** Physical dimension (length) of the cells along the grid *y*-axis.                                            |        |
+-------------------------------------------------------------------------------------------------------------------------------+--------+
|``ocean_s_coordinate_g1``                                                                                                      |1       |
|                                                                                                                               |        |
|**Description :** See `MyRoms Wiki <https://www.myroms.org/wiki/index.php/Vertical_S-coordinate>`_ and :ref:`COMODO            |        |
|documentation <vertical_s_coordinate_g1_index>`. (:ref:`vertical_axis-label`).                                                 |        |
+-------------------------------------------------------------------------------------------------------------------------------+--------+
|``ocean_s_coordinate_g2``                                                                                                      |1       |
|                                                                                                                               |        |
|**Description :** See `MyRoms Wiki <https://www.myroms.org/wiki/index.php/Vertical_S-coordinate>`_ and :ref:`COMODO            |        |
|documentation <vertical_s_coordinate_g2_index>`.                                                                               |        |
+-------------------------------------------------------------------------------------------------------------------------------+--------+
|``sea_binary_mask``                                                                                                            |1       |
|                                                                                                                               |        |
|**Description :** X_binary_mask has 1 where condition X is met, 0 elsewhere. 1 = sea, 0 = land.                                |        |
+-------------------------------------------------------------------------------------------------------------------------------+--------+
|``model_sea_floor_depth_below_geoid``                                                                                          |m       |
|                                                                                                                               |        |
|**Description :** The geoid is a surface of constant geopotential with which mean sea level would coincide if the ocean were at|        |
|rest. (The volume enclosed between the geoid and the sea floor equals the mean volume of water in the ocean.) In an ocean GCM  |        |
|the geoid is the surface of zero depth, or the rigid lid if the model uses that approximation.                                 |        |
+-------------------------------------------------------------------------------------------------------------------------------+--------+

Aliases
"""""""

+-------------------------------+--------+-------------------------------------+
|Alias Name                     |Unit    |Corresponding Standard Name          |
+===============================+========+=====================================+
|``sea_floor_depth_below_geoid``|m       |``model_sea_floor_depth_below_geoid``|
+-------------------------------+--------+-------------------------------------+
|``below_mean_level``           |m       |``model_sea_floor_depth_below_geoid``|
+-------------------------------+--------+-------------------------------------+
|``x_dimension_of_the_grid``    |index   |``x_grid_index``                     |
+-------------------------------+--------+-------------------------------------+
|``y_dimension_of_the_grid``    |index   |``y_grid_index``                     |
+-------------------------------+--------+-------------------------------------+

