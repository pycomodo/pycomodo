.. _budget-label:

Standard names for energy budget
=================================

Overview
---------

The purpose of the following tables is to document some values of the ``standard_name`` attribute
for variables corresponding to the different terms of a Kinetic Energy balance equation.
Following Capet *et. al*, 2008 [#capet08c]_, we consider:

.. math::

    \begin{array}{ccl}
        \frac12\partial_t\vec{\bf u}^2_h
        & = & -\vec{\bf u}_h\cdot\left(
            \left(\vec{\bf u}\cdot\nabla\right)\,\vec{\bf u}_h
            + f\,\vec{\bf k}\times\vec{\bf u}_h
            +\frac{1}{\rho_0}\nabla_{\!h}\,p - D_h - D_v \right) \\
        & = & -\vec{\bf u}_h\cdot\left(
            \left(\vec{\bf u}_h\cdot\nabla_{\!h}\right)\,\vec{\bf u}_h + w\,\partial_z\vec{\bf u}_h
            + f\,\vec{\bf k}\times\vec{\bf u}_h - D_h - D_v \right)
            -\frac{1}{\rho_0}\vec{\bf u}\cdot\nabla p\ +\ w\,b\\
    \end{array}

where:

- :math:`\vec{\bf u} = \left(\begin{array}{c} u \\ v \\ w\end{array}\right)\ ` ;
  :math:`\ \nabla = \left(\begin{array}{c}\partial_x \\ \partial_y \\ \partial_z\end{array}\right)\ ` ;
  :math:`\ \vec{\bf u}_h = \left(\begin{array}{c} u \\ v \end{array}\right)\ ` ;
  :math:`\ \nabla_{\!h} = \left(\begin{array}{c}\partial_x \\ \partial_y\end{array}\right)\ ` ;
  :math:`\ \vec{\bf k} = \left(\begin{array}{c} 0 \\ 0 \\ 1 \end{array}\right)`.
- :math:`D_h` stands for the horizontal mixing (sum of background diffusion and dissipation implied by upstream advection operator).
- :math:`D_v` is the vertical mixing term: :math:`\ \partial_z(\kappa_v\partial_z\vec{\bf u}_h)`
- :math:`b` is the buoyancy: :math:`-\frac{g}{\rho0}\rho`.
- :math:`f` is the Coriolis parameter.

To define the different standard names, we will follow as much as possible the official guidelines [#cf_guidelines]_.
In particular:

- ``x`` indicates a vector component along the grid x-axis, positive with increasing x.
- ``y`` indicates a vector component along the grid y-axis, positive with increasing y.
- ``upward`` indicates a vector component which is positive when directed upward (negative downward).

In the last column of the tables, a staggering value is specified for each ``standard_name``. It means that the
corresponding variable is expected to be staggered on the corresponding point of the C-grid (see :ref:`variables-comodo`).
For example, the ``standard_name`` for the tendency term of zonal velocity :math:`u` is ``tendency_of_sea_water_x_velocity``
and the expected staggering is 'u'. So a conforming NetCDF file should contain a variable with a ``standard_name``
attribute defined as follows: ::

	float dudt_x(time, level, nj, ni) ;
		dudt_x:standard_name = "tendency_of_sea_water_x_velocity_at_u_location" ;
		dudt_x:long_name = "du/dt : time trend at u location" ;
		dudt_x:units = "m s-2" ;

.. [#capet08c] \ X. Capet, J. C. McWilliams, M. J. Molemaker, A. F. Shchepetkin. (2008c): Mesoscale to
               Submesoscale Transition in the California Current System. Part III: Energy Balance and Flux.
               Journal of Physical Oceanography 38, 2256-2269.

.. [#cf_guidelines] Guidelines for Construction of CF Standard Names.
                    `(llnl.gov) <http://cf-pcmdi.llnl.gov/documents/cf-standard-names/guidelines>`_.


.. include:: table_budget.inc
