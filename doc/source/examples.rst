==========
 Examples
==========

Checker
========

A utility software named :download:`check_comodo.py <../../bin/check_comodo.py>` is provided at
install time. It is located at ``/install-path/bin/check_comodo.py``.

It checks the conformity of a file, or a set of files, to the Comodo file format
(see :ref:`[here] <norm-comodo>`). ::

	Usage:
		check_comodo.py [options] [-g GRID_FILE]... NETCDF_FILE [NETCDF_FILE]...

		  - GRID_FILE:   Name of a NetCDF grid file, ie. with an empty record dimension, if any.
		  - NETCDF_FILE: Name of a NetCDF data file with a record dimension.

		All NETCDF_FILE must have exactly the same structure: same dimensions, same variables.
		They are supposed to be samples of a unique output split along the record dimension.

		Check if a series of NetCDF files conforms to the COMODO norm. All files passed in argument
		are considered as a single archive.

		For more information, see: http://pycomodo.forge.imag.fr/norm.html

	Options:
	  --version                     show program's version number and exit
	  -h, --help                    show this help message and exit
	  -v, --verbose                 outputs more detailed messages
	  -d, --debug                   outputs debug messages
	  -g GRIDFILE, --grid=GRIDFILE  use GRIDFILE as a source of static grid data
	  -c, --concatenate             all data files are considered as a single one concatenated by the record dimension


Use examples
============

This section gathers some examples of scripts to load data from model NetCDF outputs and
produce diagnostics.

- load ROMS output file : :doc:`[ROMS] <examples/archive_roms>`
- baroclinic vortex : Note: :download:`[PDF] <examples/Test_Case_Baroclinic_Vortex.pdf>`,
  script: :download:`plot_vortex.py <examples/plot_vortex.py>`
- more to come...

.. toctree::
	:hidden:

	examples/archive_roms

