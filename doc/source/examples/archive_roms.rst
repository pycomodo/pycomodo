.. _archive_roms-label:

ROMS model output
-----------------

Load archive file: ::

	>>> import pycomodo
	>>> archive = pycomodo.Archive('examples/nc_files/roms_vortex.nc')

Print file content: ::

	>>> print archive.info(2)

	Archive[ ('examples/nc_files/roms_vortex.nc') ]:
	Dimensions :
		s_rho = 10
		s_w = 11
		eta_rho = 62
		eta_v = 61
		xi_rho = 62
		time = 3 [unlimited]
		xi_u = 61
	Variables :
		float32 time(time)
			time:standard_name = "time"
			time:axis = "T"
			time:units = "second"
		float32 s_rho(s_rho)
			s_rho:standard_name = "ocean_s_coordinate_g1"
			s_rho:axis = "Z"
			s_rho:positive = "up"
			s_rho:formula_terms = "s: sc_r C: Cs_r eta: zeta depth: h depth_c: hc"
		float32 s_w(s_w)
			s_w:standard_name = "ocean_s_coordinate_g1_at_w_location"
			s_w:axis = "Z"
			s_w:positive = "up"
			s_w:c_grid_axis_shift = -0.5
			s_w:formula_terms = "s: sc_w C: Cs_w eta: zeta depth: h depth_c: hc"
		float32 eta_rho(eta_rho)
			eta_rho:standard_name = "y_grid_index"
			eta_rho:axis = "Y"
			eta_rho:c_grid_dynamic_range = "2:61"
		float32 eta_v(eta_v)
			eta_v:standard_name = "x_grid_index_at_v_location"
			eta_v:axis = "Y"
			eta_v:c_grid_axis_shift = 0.5
			eta_v:c_grid_dynamic_range = "2:60"
		float32 xi_rho(xi_rho)
			xi_rho:standard_name = "x_grid_index"
			xi_rho:axis = "X"
			xi_rho:c_grid_dynamic_range = "2:61"
		float32 xi_u(xi_u)
			xi_u:standard_name = "x_grid_index_at_u_location"
			xi_u:axis = "X"
			xi_u:c_grid_axis_shift = 0.5
			xi_u:c_grid_dynamic_range = "2:60"
		float32 f(eta_rho, xi_rho)
			f:standard_name = "coriolis_parameter"
			f:units = "second-1"
			f:coordinates = "x_rho y_rho"
		float32 h(eta_rho, xi_rho)
			h:standard_name = "model_sea_floor_depth_below_geoid"
			h:units = "meter"
			h:coordinates = "x_rho y_rho"
		float32 pm(eta_rho, xi_rho)
			pm:standard_name = "inverse_of_cell_x_size"
			pm:units = "meter-1"
			pm:coordinates = "x_rho y_rho"
		float32 pn(eta_rho, xi_rho)
			pn:standard_name = "inverse_of_cell_y_size"
			pn:units = "meter-1"
			pn:coordinates = "x_rho y_rho"
		float32 rho(time, s_rho, eta_rho, xi_rho)
			rho:standard_name = "sea_water_sigma_t"
			rho:units = "kilogram meter-3"
		float32 temp(time, s_rho, eta_rho, xi_rho)
			temp:standard_name = "sea_water_potential_temperature"
			temp:units = "Celsius"
			temp:coordinates = "x_rho y_rho"
		float32 u(time, s_rho, eta_rho, xi_u)
			u:standard_name = "sea_water_x_velocity_at_u_location"
			u:units = "meter second-1"
		float32 ubar(time, eta_rho, xi_u)
			ubar:standard_name = "barotropic_sea_water_x_velocity_at_u_location"
			ubar:units = "meter second-1"
		float32 v(time, s_rho, eta_v, xi_rho)
			v:standard_name = "sea_water_y_velocity_at_v_location"
			v:units = "meter second-1"
		float32 vbar(time, eta_v, xi_rho)
			vbar:standard_name = "barotropic_sea_water_y_velocity_at_v_location"
			vbar:units = "meter second-1"
		float32 w(time, s_rho, eta_rho, xi_rho)
			w:standard_name = "upward_sea_water_velocity"
			w:units = "meter second-1"
			w:coordinates = "lat_rho lon_rho"
		float32 wstr(time, eta_rho, xi_rho)
			wstr:units = "N/m2"
			wstr:standard_name = "magnitude_of_surface_downward_stress"
			wstr:coordinates = "x_rho y_rho"
		float32 x_rho(eta_rho, xi_rho)
			x_rho:standard_name = "plane_x_coordinate"
			x_rho:units = "meter"
		float32 Cs_r(s_rho)
			Cs_r:long_name = "S-coordinate stretching curves at RHO-points"
		float32 Cs_w(s_w)
			Cs_w:long_name = "S-coordinate stretching curves at W-points"
		float32 hc()
			hc:long_name = "S-coordinate parameter, critical depth"
			hc:units = "meter"
		float32 sc_r(s_rho)
			sc_r:long_name = "ocean s roms coordinate at rho point"
			sc_r:Vtransform = "1"
		float32 sc_w(s_w)
			sc_w:long_name = "ocean s roms coordinate at w point"
			sc_w:Vtransform = "1"
		|S1 spherical()
			spherical:long_name = "grid type logical switch"
			spherical:option_T = "spherical"
			spherical:option_F = "cartesian"
		float32 Vtransform()
			Vtransform:long_name = "vertical terrain-following transformation equatio"
		float32 el()
			el:long_name = "domain length in the ETA-direction"
			el:units = "meter"
		float32 xl()
			xl:long_name = "domain length in the XI-direction"
			xl:units = "meter"
		float32 y_rho(eta_rho, xi_rho)
			y_rho:units = "meter"
			y_rho:standard_name = "plane_y_coordinate"
		float32 zeta(time, eta_rho, xi_rho)
			zeta:standard_name = "sea_surface_height"
			zeta:units = "meter"
			zeta:coordinates = "x_rho y_rho"
