#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os

import numpy as np
import matplotlib.pyplot as plt
from optparse import OptionParser
from logging import DEBUG

import pycomodo
import pycomodo.util.variables as puv
import pycomodo.util.plots as pup
from pycomodo import log, UnknownVariable

def plot_vortex(archive):

    try:
        log("=== Get time :")
        time = archive.get_variable(stdname='time')
        time.check(True)

        log("=== Call get_ssh")
        ssh = puv.get_ssh(archive)
        ssh.check(True)

    except UnknownVariable as err:
        log.error("variable {0} not found in '{1}'".format(err, archive))
        return

    latlon_slice = ssh.get_dynamic_slice("X", "Y")

    # If we have a time coordinate, let's plot just the last chunk
    if len(ssh.shape) > 2:
        slice_init  = np.index_exp[0]                + latlon_slice
        slice_final = np.index_exp[archive.ntimes-1] + latlon_slice
    else:
        slice_init  = latlon_slice
        slice_final = latlon_slice

    Zi = ssh[slice_init]
    Zf = ssh[slice_final]

    try:
        if ssh.units.startswith("m"):
            # Convert meters to centimeters.
            Zi *= 100.
            Zf *= 100.
    except AttributeError:
        log.error("unable to get units for variable '{0}'.".format(ssh))
        return

    Z_range = [ -10, 60 ]
    Z_ticks = pup.ticks_from_range(Z_range, digits=10, nticks=5)

    ib, ie = ssh.get_dynamic_range('X')
    jb, je = ssh.get_dynamic_range('Y')

    dx = 1800. / (ie-ib+1)
    dy = 1800. / (je-jb+1)

    Xrange = np.arange(-900.,901.,dx)
    Yrange = np.arange(-900.,901.,dy)
    X, Y = np.meshgrid(Xrange, Yrange)

    fig = plt.figure(1)
    ax = fig.add_axes([0.1,0.1,0.8,0.8], aspect='equal')
    ax.set_xlabel("X [km]")
    ax.set_ylabel("Y [km]")
    ax.set_xticks(range(-900,901,300))
    ax.set_yticks(range(-900,901,300))
    ax.autoscale_view(tight=True)

    Xrange_c = 0.5*(Xrange[1:]+Xrange[:-1])
    Yrange_c = 0.5*(Yrange[1:]+Yrange[:-1])

    csf = plt.pcolormesh(X, Y, Zf, vmin=Z_ticks.min(), vmax=Z_ticks.max())
    csi = plt.contour(Xrange_c, Yrange_c, Zi, vmin=0, vmax=Zi.max(), colors="0.75")

    cbar = fig.colorbar(csf, orientation='vertical', ticks=Z_ticks, cmap=plt.cm.copper)
    cbar.ax.set_xticklabels(Z_ticks)

    xtra = []
    ytra = []

    ssh_m = np.ma.masked_equal(ssh[latlon_slice], ssh.get_fill_value())

    X_m = 0.25*(X[:-1,:-1]+X[:-1,1:]+X[1:,:-1]+X[1:,1:])
    Y_m = 0.25*(Y[:-1,:-1]+Y[:-1,1:]+Y[1:,:-1]+Y[1:,1:])
    cutoff = 0.8

    for k in range(len(time[:])):
        min_j, min_i  = np.unravel_index(np.argmax(ssh_m[k,...]), ssh_m[k,...].shape)

        xk = X[min_j+1, min_i+1]
        yk = Y[min_j+1, min_i+1]
        maxk = ssh_m[k,...].max()
        jm, im = np.nonzero(ssh_m[k,...]>cutoff*maxk)
        w = 1 - np.abs(ssh_m[k,jm,im]-maxk)/((1-cutoff)*maxk)

        Xw = np.average(X_m[jm, im], weights=w)
        Yw = np.average(Y_m[jm, im], weights=w)

        xtra.append(Xw)
        ytra.append(Yw)

    plt.plot(xtra,ytra,'w-',lw=1.2)

    plt.plot([-900, 900], [0, 0], 'w-', lw=0.6)
    plt.plot([0, 0], [-900, 900], 'w-', lw=0.6)

    log("=== Write vortex.png")
    plt.title(u"Sea surface height — [cm]", fontsize='large', family='serif')
    plt.savefig('vortex', dpi=150, bbox_inches='tight')

if __name__ == "__main__":

    usage = "Usage: %prog NETCDF_FILE\n  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage)

    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")
    parser.add_option("-s", "--ncdir", dest="ncdir", default=".",
                      help=u"directory where NETCDF_FILE is stored (default: '.')")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    if options.debug:
        log.setLevel(DEBUG)

    ncfiles = []
    for ncfile in args:
        ncfiles.append(os.path.join(options.ncdir, ncfile))

    archive = pycomodo.Archive(ncfiles)
    plot_vortex(archive)
