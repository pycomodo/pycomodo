Installation
============

The source code of `pycomodo` is hosted here :
https://forge.imag.fr/projects/pycomodo/

Requirements:
	* python >= 2.6
	* numpy >= 1.3

Optional:
	* matplotlib >= 0.99  (http://matplotlib.org - only for visualization scripts)
	* python-netcdf4  (for reading NetCDF4 files - http://code.google.com/p/netcdf4-python/)
	* numexpr  (for efficient calls to pycomodo.operators.operation - http://code.google.com/p/numexpr/).

The recommended way to install the library is to clone the git repository:

  $ cd /path/to/somewhere
  $ export GIT_SSL_NO_VERIFY=true
  $ git clone https://forge.imag.fr/anonscm/git/pycomodo/pycomodo.git

You could write the following line in your ``~/.profile`` configuration file:

  $ export PYTHONPATH=/path/to/somewhere/pycomodo:$PYTHONPATH

Now you can use the library.

  $ cd pycomodo/examples
  $ ../bin/check_comodo.py nc_files/vortex.nc
  $ ./plot_ssh.py   nc_files/vortex.nc
  $ ./calc_pvort.py nc_files/vortex.nc

Then, to keep up-to-date, you may regularly pull new

  $ git pull

Test cases
==========

The directory testcases/ contains some scripts that produce diagnostics for the reference
test cases of the COMODO project. Please have a look at the corresponding README file.
