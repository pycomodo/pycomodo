Diagnostiques pour le cas-test "lock-exchange"
----------------------------------------------

Ce dossier contient des scripts Python permettant de produire des diagnostiques pour le
cas-test "lock-exchange".

Au préalable, il est nécessaire d'avoir une installation fonctionnelle de la librairie pycomodo.
Les instructions sont détaillées sur cette page :

http://pycomodo.forge.imag.fr/api/installation.html

Pour produire les figures :

$ ./plot_lock.py -m MON_MODEL ncfile.nc

sont créés deux fichiers :
- lock_den.png : histogramme de la distribution des densités à l'instant final (t=61200 s)
- lock_cut.png : coupe verticale de la densité à l'instant final
