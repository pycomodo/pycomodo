#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os

import logging
import numpy as np
import matplotlib.pyplot as plt
from optparse import OptionParser

import pycomodo
import pycomodo.util.variables as puv
import pycomodo.util.plots as pup
from pycomodo import log, UnknownVariable, UnknownCoordinate

DEBUG = False


def plot_lock(archive, model_name=None, grid_resolution=None):

    try:
        log("=== Get tracer...")
        var_temp = puv.get_variable_from_stdnames(archive, ("sea_water_potential_temperature",
                                                            "sea_water_temperature",
                                                            "sea_water_sigma_theta"))
        log(" - found {0} ({1})".format(var_temp, var_temp.standard_name))

    except UnknownVariable as err:
        log.error("variable {0} not found in '{1}'".format(err, archive))
        return

    try:
        tindex = archive.get_time_index(61200, "seconds")
    except:
        log.warning("Could not find time index for t=61200 s. I will take the last one.")
        tindex = archive.ntimes - 1

    xmin, xmax = var_temp.get_dynamic_range('X')
    ymin, ymax = var_temp.get_dynamic_range('Y')
    zmin, zmax = var_temp.get_dynamic_range('Z')
    yindex = ymin + (ymax-ymin) / 2

    x_slice  = np.index_exp[..., yindex, xmin:xmax+1]
    xt_slice = np.index_exp[tindex] + x_slice

    # Compute cell elevation
    log("=== Get vertical levels...")
    try:
        var_zw = archive.get_zlevels("w", axes="ZYX", name="zw")
    except UnknownCoordinate:
        log.warning("Unable to create a coordinate system for staggered 'w' variables. "
                    "Vertical positionment on pltos may be inacurate.")
        var_zw = archive.get_zlevels("t", axes="ZYX", name="zt")

    temp  = var_temp[xt_slice]
    zw   = var_zw[x_slice]

    # Make variables upside down if Z axis is reversed.
    if var_zw.axes["Z"].positive == "down":
        zw  = -zw[::-1, :]
        temp = temp[::-1, :]

    # Fix staggering for 'zw' if we couldn't build a proper w-staggered coordinate system
    if var_zw.position == "t":
        nz, nx = zw.shape
        zw_new = np.ones((nz+1, nx))
        zw_new[-1]  = 0
        zw_new[:-1] = zw[:] + zw[-1]
        zw = zw_new

    if DEBUG:
        print "z :", zw.shape, zmin, zmax
        print zw[:, 1]

    try:
        dx  = 1e-3*archive.get_metric("x", "t")[x_slice]
        xt  = np.cumsum(dx) - dx[0]
        xt -= ( xt[-1] + dx[0] ) / 2.
        grid_resolution = "{0} m".format(int(np.round(1000*dx[0])))
    except:
        xt = np.arange(xmax-xmin)
        if grid_resolution is None:
            grid_resolution = "unknown"
        else:
            grid_resolution = "{0} m".format(grid_resolution)

    if DEBUG:
        print "x :", xt.shape, xmin, xmax
        print xt

    if model_name is None:
        plot_title  = u"Lock Exchange (∆x = {0})".format(grid_resolution)
        fname_cut   = "lock_cut"
        fname_den   = "lock_den"
    else:
        plot_title  = u"{0} (∆x = {1})".format(model_name, grid_resolution)
        fmodel_name = model_name.replace(" ", "_").replace(".", "-")
        fname_cut = "lock_cut_{0}".format(fmodel_name)
        fname_den = "lock_den_{0}".format(fmodel_name)

    log("=== Plotting density distribution...")
    fig = plt.figure()
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])

    sigma_range = [ 19, 26 ]
    sigma  = 30 - 0.28*temp
    nbpu   = 15    # Number of bins per unit.
    nticks = int(sigma_range[1]-sigma_range[0])-1
    rbins  = np.linspace(sigma_range[0], sigma_range[1], (nticks+1)*nbpu+1)
    rbins[nticks*nbpu] += 1e-3  # Gros Hackkk pour inclure les éléments de plus grande densité
                                # dans la bonne barre.

    rhist, rbins = np.histogram(np.ravel(sigma), rbins, density=True)
    rwidth = (rbins[1:]-rbins[:-1])[2]

    ax.bar(rbins[:-1],           rhist,         width=rwidth, bottom=1e-5, log=True, color='blue', align='edge')
    ax.bar(rbins[:nbpu],         rhist[:nbpu],  width=rwidth, bottom=1e-5, log=True, color='red',  align='edge')
    ax.bar(rbins[-nbpu:]-rwidth, rhist[-nbpu:], width=rwidth, bottom=1e-5, log=True, color='red',  align='edge')
    ax.set_xlim(sigma_range)
    ax.set_ylim((0.001, 6))
    ax.set_xlabel("Density anomaly [$kg\ .\,m^{-3}$]", size='large')
    ax.autoscale_view(tight=True)
    plt.title(plot_title, fontsize='x-large', family='serif')
    plt.suptitle("Normalized density distribution", fontsize='x-large', family='serif')
    plt.grid(True)

    log(" -  Writing {0}.png...".format(fname_den))
    plt.savefig(fname_den, dpi=150, bbox_inches='tight')

    log("=== Plotting temperature vertical cut...")
    fig    = plt.figure(figsize=(9, 5))
    fig.subplots_adjust(top=0.85)
    ax     = plt.subplot()
    csf    = ax.pcolormesh(xt, zw, temp, vmin=15.857, vmax=37.714)
    cticks = pup.ticks_from_range([18, 36], nticks=7)
    cbar   = plt.colorbar(csf, orientation='vertical', ticks=cticks, cmap=plt.cm.copper, pad=0.01, aspect=15)
    ax.set_title(u"Temperature cut  —  "+plot_title, fontsize='x-large', family='serif', position=(0.5, 1.03))
    ax.vlines(0., -20., 0., colors='k', linestyles='dashed', lw=2)

    ax.set_xlim(-32., 32.)
    ax.set_ylim(-20.,  0.)
    ax.set_xlabel("X [km]")
    ax.set_ylabel("Z [m]", size='large')
    ax.set_xticks((-32, -24, -16, -8, 0, 8, 16, 24, 32))

    log(" -  Writing {0}.png...".format(fname_cut))
    plt.savefig(fname_cut, dpi=150, bbox_inches='tight')


if __name__ == "__main__":

    usage = "Usage: %prog NETCDF_FILE\n  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage)

    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")
    parser.add_option("-m", "--model", dest="model", default=None,
                      help="name of the model that produced NETCDF_FILE")
    parser.add_option("-r", "--resolution", dest="resolution", default=None,
                      help=u"grid resolution (∆x) for NETCDF_FILE")
    parser.add_option("-s", "--ncdir", dest="ncdir", default=".",
                      help=u"directory where NETCDF_FILE is stored (default: '.')")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    if options.debug:
        log.setLevel(logging.DEBUG)

    ncfiles = []
    for ncfile in args:
        ncfiles.append(os.path.join(options.ncdir, ncfile))

    archive = pycomodo.Archive(ncfiles)
    plot_lock(archive, options.model, options.resolution)
