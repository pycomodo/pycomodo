#!/bin/bash
set -u

NC_DIR="${HOME}/Documents/COMODO/cas_tests/lock"
PY_DIR="${HOME}/Documents/COMODO/pycomodo/testcases/lock"

LOGS_DIR="${NC_DIR}/logs"
PLOT_DIR="${NC_DIR}/plots"

RUN_ROMS=1
RUN_MARS=1
RUN_NEMO=0
RUN_S=0
RUN_HYCOM=1

cr="\033[0;31m" 
cg="\033[0;32m" 
ce="\033[0m"

function plot_lock()
{
	model=${1}
	cdf_dir=${2}
	cdf_files=${3}
	case_modif=${4:-""}
	
	modelstr=$(echo ${model}   |sed -e 's/[ \t ]//g')
	modelstr=$(echo ${modelstr}|sed -e 's/[\/]/-/g')

	png_cut_file="lock_cut_${modelstr}${case_modif}.png"
	png_den_file="lock_den_${modelstr}${case_modif}.png"
	log_file="${LOGS_DIR}/lock_${modelstr}${case_modif}.log"

	echo -n " . creating lock_${modelstr}${case_modif}... "
	cd ${PLOT_DIR}
	${PY_DIR}/plot_lock.py ${cdf_files} --ncdir "${cdf_dir}" --model "${model}" >& ${log_file}
	if (( $? )) ; then
		echo -e "${cr}[KO]${ce} - no file created, see log file ${log_file}"
		return
	fi
	mv lock_cut.png ${png_cut_file} >> ${log_file} 2>&1
	mv lock_den.png ${png_den_file} >> ${log_file} 2>&1
	if (( $? )) ; then
		echo -e "${cr}[KO]${ce} - no file created, see log file ${log_file}"
		return
	fi
	echo -e "${cg}[OK]${ce}"
}

echo "================================"
echo "=           Plot lock           "
echo "================================"
mkdir -p ${LOGS_DIR}
mkdir -p ${PLOT_DIR}

rm -f ${PLOT_DIR}/*.png

if (( ${RUN_ROMS} )) ; then
	plot_lock "ROMS - UP3 / v0"     "${NC_DIR}/ROMS" "gravadj_his_500m_UP3_visc0.nc"     "-500m"
	plot_lock "ROMS - UP3 / v200"   "${NC_DIR}/ROMS" "gravadj_his_500m_UP3_visc200.nc"   "-500m"
	plot_lock "ROMS - WENO5 / v200" "${NC_DIR}/ROMS" "gravadj_his_125m_WENO5_visc200.nc" "-125m"
	plot_lock "ROMS - WENO5 / v200" "${NC_DIR}/ROMS" "gravadj_his_500m_WENO5_visc200.nc" "-500m"
	plot_lock "ROMS - WENO5 / v200" "${NC_DIR}/ROMS" "gravadj_his_2km_WENO5_visc200.nc"  "-2km"
fi
if (( ${RUN_S} )) ; then
	plot_lock S "30 km - exp1" "${NC_DIR}/S" "grid_30km.nc vortex_S_30km.nc"
fi
if (( ${RUN_MARS} )) ; then
	plot_lock "MARS" "${NC_DIR}/MARS/" "lock_wall_mars.nc"
fi
if (( ${RUN_NEMO} )) ; then
	plot_lock NEMO "30 km" "${NC_DIR}/NEMO/" "vortex_30km_nemo.nc"
fi
if (( ${RUN_HYCOM} )) ; then
	plot_lock "HYCOM" "${NC_DIR}/HYCOM/" "hycom-lock.nc"
fi
