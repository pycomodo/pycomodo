#!/home6/caparmor/aleboyer/anaconda/bin/python
# -*- coding: utf-8 -*-

import sys
path ='../../../'
sys.path.append(path)

import logging
from optparse import OptionParser

import pycomodo
import pycomodo.util.variables as puv
import pycomodo.util.vertical  as puz
import pycomodo.util.plots     as pup
import pycomodo.operators      as op

from pycomodo import log

import numpy as np

import matplotlib.pyplot as plt

cst_rho0 = 1000

#fic_in     = '/home/shom_simurep/aleboyer/RUNS/EXP21/EXP215100/RUNS025/comodo_global.nc'

def proj_iso2z(sig,z,cible):
    if len(sig.shape)!=3:
       log.error("field to interpolate must be a T, Z, X field")
       return
    else:
       [T,Z,X]=sig.shape
       log(" - T = {0}".format(T))
       log(" - Z = {0}".format(Z))
       log(" - X = {0}".format(X))
    if (len(cible.shape)!=1):
       log.error("new z grid must be an array")
       return
    else:
       [Znew]=cible.shape
       log(" - new Z = {0}".format(Znew))
  
    new_sig=np.zeros([T,Znew,X])
    for t in np.arange(T):
       for x in np.arange(X):
        new_sig[t,:,x] = np.interp(cible,z[t,:,x],sig[t,:,x])
    return new_sig

def sortsig(sig,h,cible,dx,dy,vol_tot):
    T,Z,X=sig.shape
    sort_h=np.zeros([T,len(cible)])
    for t in range(T):
       for c in range(len(cible)):
          indx=np.where((sig[t,:,:]<=cible[c])&(sig[t,:,:]>0))
          tempo=h[t,indx[0],indx[1]].sum(axis=0)*dx*dy
          sort_h[t,c]=tempo/vol_tot[t]
    return sort_h

log("to use this programme you need to do ./density_profil_time_average -t t0 -T t1 -j j0 -i i0 -I i1 nc_file ")
log("t0,t1 : time boundaries ")
log("i0,i1 : x boundaries ")
log("j0    : y location (no y dependance) ")

def density_profil_time_average( fic_in, t0, t1, j0, i0, i1):
    
    archive = pycomodo.Archive(fic_in)
    log("Reading : {}".format(archive))
    log(" - t0 = {0}".format(t0))
    log(" - t1 = {0}".format(t1))
    log(" - j0 = {0}".format(j0))
    log(" - i0 = {0}".format(i0))
    log(" - i1 = {0}".format(i1))
        
    tm  = archive.ntimes
    jm, = archive.get_metric("x", "t").axes["Y"].shape
    im, = archive.get_metric("x", "t").axes["X"].shape
    
    if t0 > tm:
       log.error("t index t0 must be inferior to {0}.".format(tm))
       return
    if t1 > tm:
       log.error("t index t1 must be inferior to {0}.".format(tm))
       return
    if j0 >= jm:
       log.error("j index j0 must be inferior to {0}.".format(jm))
       return
    if i0 >= im:
       log.error("i index i0 must be inferior to {0}.".format(im))
       return
    if i1 >= im:
       log.error("i index i1 must be inferior to {0}.".format(im))
       return
   
    log("get archive variable zt  ")
    zt   = puv.get_variable_from_stdnames(archive,'depth_below_geoid')
    log("get archive variable ssh  ")
    eta  = puv.get_variable_from_stdnames(archive,'sea_surface_height_above_geoid')
    log("get archive variable bathy  ")
    depp = puv.get_variable_from_stdnames(archive,'sea_floor_depth_below_geoid')

    zt=zt[t0:t1,:,j0,i0:i1]
    eta=eta[t0:t1,j0,i0:i1]
    depp=depp[j0,i0:i1]
    log("get potential density var_rho  ")
    cst_alpha = 0.20
    cst_beta  = 0.77
    var_rho = puv.get_potential_density(archive, cst_alpha=cst_alpha, cst_beta=cst_beta)
    var_rho = var_rho[t0:t1,:,j0,i0:i1] 
    

    # changement de coordonees dimension z comprise entre H et 0 (prise en compte de la ssh)
    # probablement uniquement HYCOM
    log("change z coordinate (only HYCOM?) ")
    T,Z,X=zt.shape
    H=np.tile(depp,[T,Z,1])
    eta_3D  = np.rollaxis(np.tile(eta,[Z,1,1]),1,0)
    newz    = (H - zt) * H/(H- eta_3D) - H
   
    
 
  
    #projection
    log("var_rho projection on quasi coninuous grid: rhoz ")
    dept = 4000.
    nlev = 1000.
    cible    = np.linspace(dept/nlev/2,dept-dept/nlev/2,nlev)
    dz=cible[1]-cible[0]
    rhoz=proj_iso2z(var_rho,zt,cible)
    h_3D = np.zeros([T,len(cible),X])+dz
    
     
    log("creation rho_cible ")
    rho_min=rhoz[rhoz>0].min()
    rho_max=rhoz[rhoz>0].max()
  
    rho_cible=np.linspace(rho_min,rho_max,1000)
    drho=rho_cible[1]-rho_cible[0]
    dx=1000
    dy=1000
    vol_tot=(np.tile(depp,[T,1])+eta)*dx*dy
    vol_tot=vol_tot.sum(axis=1)

    log("compute profil ")
    density_profil=sortsig(rhoz,h_3D,rho_cible,dx,dy,vol_tot)
 
    log("plot profil")
    plt.plot(density_profil.mean(axis=0))
    plt.title('Normed volume per density bin - time average')
    plt.ylabel('Normed volume')
    plt.xlabel('$\\rho$ ($kg$ $m^-3$) $bin=$ $'+str(drho)+'$')
    plt.savefig(fic_in[0].split('/')[-1]+'_density_profil_time'+str(t0)+'_'+str(t1)+'_x'+str(i0)+str(i1)+'_average.png')
    plt.close()
    return rho_cible,density_profil,drho
print __name__
if __name__ == "__main__":

    parser = OptionParser(version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
						help="outputs debug messages")
    parser.add_option("-t", "--time", dest="t0", default=0,  type="int",
						help="time index to extract")
    parser.add_option("-T", "--TIME", dest="t1", default=0,  type="int",
						help="time index to extract")
    parser.add_option("-j", "--j0",   dest="j0", default=1, type="int",
						help="j index to extract")
    parser.add_option("-i", "--i0",   dest="i0", default=1, type="int",
						help="i index to extract")
    parser.add_option("-I", "--i1",   dest="i1", default=2, type="int",
						help="i index to extract")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)
    density_profil_time_average(args, options.t0, options.t1, options.j0, options.i0, options.i1)

