#!/home6/caparmor/aleboyer/anaconda/bin/python
# -*- coding: utf-8 -*-

import sys
path ='../../../'
sys.path.append(path)

import logging
from density_profil_time_average import density_profil_time_average
from pycomodo import log
import numpy as np
import matplotlib.pyplot as plt


fic_in     = '../ncfile/EXP325100_comodo_global.nc'
t0 = 1
t1 = 24
i0 = 420
i1 = 520
j0 = 0

A,B,drho=density_profil_time_average(fic_in,t0,t1,j0,i0,i1) 

t0 = 1417
t1 = 1441
C,D,drho=density_profil_time_average(fic_in,t0,t1,j0,i0,i1) 


log("plot profil")
plt.plot(A,B.mean(axis=0),label='1st cycle')
plt.plot(C,D.mean(axis=0),label='last cycle')
plt.legend(loc='best')
plt.title('Normed volume per density bin - time average')
plt.ylabel('Normed volume')
plt.xlabel('$\\rho$ ($kg$ $m^-3$) $bin=$ $'+str(drho)+'$')
plt.savefig('multi_density_profil_time_x'+str(i0)+str(i1)+'_average.png')
plt.close()
