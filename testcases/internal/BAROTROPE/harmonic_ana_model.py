#!/home6/caparmor/aleboyer/anaconda/bin/python
# -*- coding: utf-8 -*-
#
# principe: on reconstruit un signal analytique a partir de la solution
#           barotrope lineaire (fichiers de forcages) et on compare en rms 
#           temporelles avec les sorties modeles.


import sys
path ='../../../'
sys.path.append(path)

import logging
from optparse import OptionParser

import pycomodo
import pycomodo.util.variables as puv
import pycomodo.util.vertical  as puz
import pycomodo.util.plots     as pup
import pycomodo.operators      as op

from pycomodo import log

import numpy as np
import matplotlib.pyplot as plt


def main(fic_in):

    #fic_in comodo_global.nc
    archive = pycomodo.Archive('../ncfile/amp_ssh_S2.cdf')
    amp_ssh=archive.variables['amp_ssh_S2'][0,0,:]
    archive = pycomodo.Archive('../ncfile/pha_ssh_S2.cdf')
    pha_ssh=archive.variables['pha_ssh_S2'][0,0,:]
    archive = pycomodo.Archive('../ncfile/amp_u_S2.cdf')
    amp_u=archive.variables['amp_u_S2'][0,0,:]
    archive = pycomodo.Archive('../ncfile/pha_u_S2.cdf')
    pha_u=archive.variables['pha_u_S2'][0,0,:]
    archive = pycomodo.Archive('../ncfile/amp_v_S2.cdf')
    amp_v=archive.variables['amp_v_S2'][0,0,:]
    archive = pycomodo.Archive('../ncfile/pha_v_S2.cdf')
    pha_v=archive.variables['pha_v_S2'][0,0,:]

    archive = pycomodo.Archive(fic_in)

    Ut = puv.get_variable_at_location(archive, 'barotropic_sea_water_x_velocity', 't', interpolate=True)
    Vt = puv.get_variable_at_location(archive, 'barotropic_sea_water_y_velocity', 't', interpolate=True)
    Ut = Ut[:,0,:]
    Vt = Vt[:,0,:]
    
    # get dimension
    T,X=Ut.shape

    #get ssh
    SSH=puv.get_variable_from_stdnames(archive, 'sea_surface_height_above_geoid')
    SSH = SSH[:,0,:]


    # creation du signal analytique de l'harmonique S2
    # a partir des champs amplitude et phase de forcage  
    om=2*np.pi/12/3600
    t=np.linspace(0,86400*30+1,86400/60.+1)
    tt=np.tile(t,[X,1])
    tt=tt.T
    SSHana=amp_ssh*np.cos(om*tt-pha_ssh*np.pi/180.)
    Uana=amp_u*np.cos(om*tt-pha_u*np.pi/180.)
    Vana=amp_v*np.cos(om*tt-pha_v*np.pi/180.)

    #SSHcomp=ssh-np.tile(SSHana,[len(ssh),1,1])
    SSHcomp=SSH-SSHana
    Ucomp=Ut-Uana
    Vcomp=Vt-Vana
    
    rmsSSH=np.sqrt(np.mean(SSHcomp**2,axis=0))
    rmsU=np.sqrt(np.mean(Ucomp**2,axis=0))
    rmsV=np.sqrt(np.mean(Vcomp**2,axis=0))


    plot1=plt.axes([0.1,0.7,0.8,0.2]) 
    plot2=plt.axes([0.1,0.4,0.8,0.2]) 
    plot3=plt.axes([0.1,0.1,0.8,0.2]) 

    plot1.plot(rmsSSH[:-1]*100,linewidth=2)
    plot2.plot(rmsU[:-1]*100,linewidth=2)
    plot3.plot(rmsV[:-1]*100,linewidth=2)


    plot1.set_title('rms SSH')
    plot2.set_title('rms U')
    plot3.set_title('rms V')
  
    plot1.set_ylabel('cm')
    plot2.set_ylabel('cm/s')
    plot3.set_ylabel('cm/s')

    plot3.set_xlabel('x (km)')

    plt.savefig('rms_harmonique.png')
    plt.close()

if __name__ == "__main__":

    parser = OptionParser(version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
						help="outputs debug messages")
    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)
    main(args)
