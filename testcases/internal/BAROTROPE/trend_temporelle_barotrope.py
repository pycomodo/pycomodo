#!/home6/caparmor/aleboyer/anaconda/bin/python
# -*- coding: utf-8 -*-

import sys
path ='../../../'
sys.path.append(path)

import logging
from optparse import OptionParser

import pycomodo
import pycomodo.util.variables as puv
import pycomodo.util.vertical  as puz
import pycomodo.util.plots     as pup
import pycomodo.operators      as op

from pycomodo import log

import numpy as np
import matplotlib.pyplot as plt

def main( fic_in):
    
    archive = pycomodo.Archive(fic_in)
        
    tm  = archive.ntimes
    jm, = archive.get_metric("x", "t").axes["Y"].shape
    im, = archive.get_metric("x", "t").axes["X"].shape
    
    #get v barotrope
    Ut = puv.get_variable_at_location(archive, 'barotropic_sea_water_x_velocity', 't', interpolate=True)
    Vt = puv.get_variable_at_location(archive, 'barotropic_sea_water_y_velocity', 't', interpolate=True)
    Ut = Ut[:,0,:]
    Vt = Vt[:,0,:]

    #get ssh
    SSH=puv.get_variable_from_stdnames(archive, 'sea_surface_height_above_geoid')
    SSH = SSH[:,0,:]


    #moyenne sur un nombre entier de cycle
    SSHmean=np.zeros([tm/24,im])
    Umean=np.zeros([tm/24,im])
    Vmean=np.zeros([tm/24,im])
    for t in np.arange(tm/24):
        SSHmean[t,:]=SSH[t*24:(t+1)*24+1,:].mean(axis=0)
        Umean[t,:]=Ut[t*24:(t+1)*24+1,:].mean(axis=0)
        Vmean[t,:]=Vt[t*24:(t+1)*24+1,:].mean(axis=0)


    X=np.arange(im)
    X=np.tile(X,[tm/24,1])
    T=np.arange(tm/24)
    T=np.tile(T,[im,1]);T=T.T
 
    plt.pcolor(X,T,SSHmean)
    A=plt.colorbar()
    A.set_label('m')
    plt.title('Trend SSH') 
    plt.xlabel('x (km)') 
    plt.ylabel('time (tidal cycle)')
    plt.savefig('trend_ssh.png') 
    plt.close()

    plt.pcolor(X,T,Umean)
    A=plt.colorbar()
    A.set_label('m/s')
    plt.title('Trend ubavg') 
    plt.xlabel('x (km)') 
    plt.ylabel('time (tidal cycle)')
    plt.savefig('trend_ubavg.png') 
    plt.close()

    plt.pcolor(X,T,Vmean)
    A=plt.colorbar()
    A.set_label('m/s')
    plt.title('Trend vbavg') 
    plt.xlabel('x (km)') 
    plt.ylabel('time (tidal cycle)')
    plt.savefig('trend_vbavg.png') 
    plt.close()
 
if __name__ == "__main__":

    parser = OptionParser(version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
						help="outputs debug messages")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)
    main(args)
