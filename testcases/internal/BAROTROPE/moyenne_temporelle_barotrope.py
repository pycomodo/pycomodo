#!/home6/caparmor/aleboyer/anaconda/bin/python
# -*- coding: utf-8 -*-

import sys
path ='../../../'
sys.path.append(path)

import logging
from optparse import OptionParser

import pycomodo
import pycomodo.util.variables as puv
import pycomodo.util.vertical  as puz
import pycomodo.util.plots     as pup
import pycomodo.operators      as op

from pycomodo import log

import numpy as np
import matplotlib.pyplot as plt

def main( fic_in):
    
    archive = pycomodo.Archive(fic_in)
        
    tm  = archive.ntimes
    jm, = archive.get_metric("x", "t").axes["Y"].shape
    im, = archive.get_metric("x", "t").axes["X"].shape
    
    #get v barotrope
    Ut = puv.get_variable_at_location(archive, 'barotropic_sea_water_x_velocity', 't', interpolate=True)
    Vt = puv.get_variable_at_location(archive, 'barotropic_sea_water_y_velocity', 't', interpolate=True)
    Ut = Ut[:,0,:]
    Vt = Vt[:,0,:]

    #get ssh
    SSH=puv.get_variable_from_stdnames(archive, 'sea_surface_height_above_geoid')
    SSH = SSH[:,0,:]


    #moyenne sur un nombre entier de cycle
    SSHmean=SSH.mean(axis=0)
    Umean=Ut.mean(axis=0)
    Vmean=Vt.mean(axis=0)


    x=np.arange(SSH.shape[-1])
    plt.figure()
    plt.subplot(311)
    plt.plot(x,SSHmean*1000,label='SSH')
    plt.ylabel('SSH $mm$')
    plt.title('Moyenne')
    plt.subplot(312)
    plt.plot(x,Umean*1000,label='U')
    plt.ylabel('U $mm.s^{-1}$')
    plt.subplot(313)
    plt.plot(x,Vmean*1000,label='V')
    plt.ylabel('V $mm.s^{-1}$')
    plt.xlabel('x')
    plt.savefig('moyenne.png' )
    plt.close()
   
if __name__ == "__main__":

    parser = OptionParser(version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
						help="outputs debug messages")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)
    main(args)
