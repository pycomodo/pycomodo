#!/home6/caparmor/aleboyer/anaconda/bin/python
# -*- coding: utf-8 -*-

import sys
path ='../../../'
sys.path.append(path)

import logging
from optparse import OptionParser

import pycomodo
import pycomodo.util.variables as puv
from pycomodo import log
import numpy as np
import matplotlib.pyplot as plt


#fic_in     = '/home/shom_simurep/aleboyer/RUNS/EXP21/EXP215100/RUNS025/comodo_global.nc'
def conversion_rate( fic_in, t0, t1, j0, i0, i1 ):
    print fic_in[-1].split('/')[-1].split('_')[0]+'_conversion_t'+str(t0)+'_'+str(t1)+'.png' 
    archive = pycomodo.Archive(fic_in)
    log("Reading : {}".format(archive))
    log(" - t0 = {0}".format(t0))
    log(" - j0 = {0}".format(j0))
    log(" - i0 = {0}".format(i0))
    log(" - i1 = {0}".format(i1))
        
    tm  = archive.ntimes
    jm, = archive.get_metric("x", "t").axes["Y"].shape
    im, = archive.get_metric("x", "t").axes["X"].shape
   
 
    if t0 >= tm:
       log.error("t index t0 must be inferior to {0}.".format(tm))
       return
    if j0 >= jm:
       log.error("j index j0 must be inferior to {0}.".format(jm))
       return
    if i0 >= im-2:
       log.error("i index i0 must be inferior to {0}.".format(im))
       return
    if i1 >= im-1:
       log.error("i1 index i1 must be inferior to {0}.".format(im))
       return
    
    log("get archive variable ht  ")
    ht   = puv.get_variable_from_stdnames(archive,'cell_thickness')
    log("get archive variable ssh  ")
    eta  = puv.get_variable_from_stdnames(archive,'sea_surface_height_above_geoid')
    log("get archive variable sigma  ")
    sig  = puv.get_variable_from_stdnames(archive,'sea_water_sigma_theta')
    log("get archive variable bathy  ")
    depp = puv.get_variable_from_stdnames(archive,'sea_floor_depth_below_geoid')
    depu = puv.get_variable_from_stdnames(archive,'sea_floor_depth_below_geoid_at_u_location')
    log("get archive variable ut  ")
    ut = puv.get_variable_at_location(archive, 'sea_water_x_velocity', 't', interpolate=True)
     
    rho0 = 1000
    g    =9.81 # gravite
    log("check if zt have T axis ")
    if len(ht.shape)==4:
       log("compute conversion rate as Simmons_DSR2004: eq 12-13 ")
       log("rest state ")
       hini=ht[0,:,j0,i0:i1]
       sig_ini=sig[0,:,j0,i0:i1]
       eta0=eta[0,j0,i0:i1]
       log("reshape field ")
       #zt=zt[t0:t1,:,j0,i0:i1]
       ht=ht[t0:t1,:,j0,i0:i1]
       sig=sig[t0:t1,:,j0,i0:i1]
       eta=eta[t0:t1,j0,i0:i1]
       depp=depp[j0,i0:i1]
       depu=depu[j0,i0:i1+1]
       ut = ut[t0:t1,:,j0,i0:i1+1]
      
       T,Z,X=ht.shape 
       log("compute density")
       cst_alpha = 0.20
       cst_beta  = 0.77
       #var_rho = puv.get_potential_density(archive, cst_alpha=cst_alpha, cst_beta=cst_beta)
       #rho_0   = var_rho[0,:,j0,i0:i1]+rho0 
       #rho     = var_rho[t0:t1,:,j0,i0:i1]+rho0
       rho = sig+rho0 
       rho_0 = sig_ini+rho0 

       log("compute bottom baroclinic pressure perturbation pp")
       pbot   = -g*eta 
       p0_bot = -g*eta0 
       #pression au fond
       pbot    = pbot+np.sum(rho*g*ht,axis=1)
       pbotp   = pbot-pbot/ht.sum(axis=1) 
       p0_bot  = p0_bot+np.sum(rho_0*g*hini,axis=0) 
       p0_botp   = p0_bot-p0_bot/hini.sum(axis=0) 
       #anomalie de pression
       pp =(pbotp-np.tile(p0_botp,[T,1]))/rho0
       #gradiant de topo
       log("compute topographic gradiant")
       dx=1000
       topop=(depu[1:]-depu[:-1])/dx
       #Ubaro
       log("compute thickness weighted barotropic velocity U")
       u_p=np.ma.array(0.5*(ut[:,:,1:]+ut[:,:,:-1]))
       U=np.array(np.sum(u_p*ht,axis=1)/ht.sum(axis=1))
       log("compute converion rate pp*grad(topo)*U")
       CT=-np.tile(topop.data,[T,1])*U*pp
       # CS CT s equilibre avec les divergence du flux: (Hu'p')_x
       CTsm=CT.copy()
       #smoothing (cf Kelly JGR2010 fig3)
       log("smooth  converion see cf Kelly JGR2010 fig3")
       for x in range(5,X-5):
           CTsm[:,x]=CTsm[:,x-5:x+5].mean(axis=1)
           CTsm[:,x]=CTsm[:,x-5:x+5].mean(axis=1)
    
    x=range(5,X-5)
    xdomain=range(i0+5,i1-5)
    plot1=plt.axes([0.15, 0.3, 0.8, 0.5]) 
    plot2=plt.axes([0.15, 0.1, 0.8, 0.2]) 
    plot1.plot(xdomain,CTsm[:,x].mean(axis=0),label='Conversion rate')
    plot1.legend(loc='best')
    plot1.set_ylabel('$watt$ $m^{-2}$')
    plot1.set_title('Conversion rate t0:'+str(t0)+' t1:'+str(t1))
    plot1.set_xticklabels('')
    plot1.set_xlim([xdomain[0],xdomain[-1]])
    plot2.plot(xdomain,depp[x],'k',label='bathymetry')
    plot2.set_yticks([500,2000,4000])
    plot2.set_xlabel('$x$ $(km)$')
    plot2.set_ylabel('$depth$ $(m)$')
    plot2.legend(loc='best')
    plot2.set_xlim([xdomain[0],xdomain[-1]])
    plot2.invert_yaxis()
    plt.savefig(fic_in[-1].split('/')[-1].split('_')[0]+'_conversion_t'+str(t0)+'_'+str(t1)+'.png')
    plt.close()

    return xdomain,CTsm,depp

if __name__ == "__main__":

    parser = OptionParser(version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
						help="outputs debug messages")
    parser.add_option("-t", "--time", dest="t0", default=0,  type="int",
						help="time index to extract")
    parser.add_option("-T", "--t1", dest="t1", default=0,  type="int",
						help="time index to extract")
    parser.add_option("-j", "--j0",   dest="j0", default=1, type="int",
						help="j index to extract")
    parser.add_option("-i", "--i0",   dest="i0", default=1, type="int",
						help="i index to extract")
    parser.add_option("-I", "--i1",   dest="i1", default=2, type="int",
						help="i1 index to extract")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)
    conversion_rate(args, options.t0, options.t1, options.j0, options.i0, options.i1)

