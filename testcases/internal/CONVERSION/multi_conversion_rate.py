#!/home6/caparmor/aleboyer/anaconda/bin/python
# -*- coding: utf-8 -*-

import logging
from conversion_rate import conversion_rate
from pycomodo import log
import numpy as np
import matplotlib.pyplot as plt


fic_in1     = '../ncfile/EXP325100_comodo_global.nc'
t0 = 1441-240
t1 = 1441
i0 = 400
i1 = 520
j0 = 0

xdomain,A,depp=conversion_rate(fic_in1,t0,t1,j0,i0,i1) 

fic_in2     = '../ncfile/EXP315100_comodo_global.nc'
xdomain,B,depp=conversion_rate(fic_in2,t0,t1,j0,i0,i1) 


x=range(5,len(xdomain)-5)
log("plot conversion rate")
plot1=plt.axes([0.15, 0.3, 0.8, 0.5]) 
plot2=plt.axes([0.15, 0.1, 0.8, 0.2]) 
plot1.plot(xdomain[5:-5],A[:,x].mean(axis=0),label=fic_in1.split('/')[-1].split('_')[0])
plot1.plot(xdomain[5:-5],B[:,x].mean(axis=0),label=fic_in2.split('/')[-1].split('_')[0])
plot1.legend(loc='best')
plot1.set_ylabel('$watt$ $m^{-2}$')
plot1.set_title('Conversion rate t0:'+str(t0)+' t1:'+str(t1))
plot1.set_xticklabels('')
plot1.set_xlim([xdomain[5],xdomain[-5]])
plot2.plot(xdomain[5:-5],depp[x],'k',label='bathymetry')
plot2.set_yticks([500,2000,4000])
plot2.set_xlabel('$x$ $(km)$')
plot2.set_ylabel('$depth$ $(m)$')
plot2.legend(loc='best')
plot2.set_xlim([xdomain[5],xdomain[-5]])
plot2.invert_yaxis()
plt.savefig(fic_in1.split('/')[-1].split('_')[0]+fic_in2.split('/')[-1].split('_')[0]+'_conversion_t'+str(t0)+'_'+str(t1)+'.png')
plt.close()
