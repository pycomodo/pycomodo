#!/home6/caparmor/aleboyer/anaconda/bin/python

import matplotlib
#matplotlib.use('qt4agg')
import numpy as np
from mode_baro import mode 
from scipy.io import netcdf

dept = 4000.
nlev = 2000.
cible    = np.linspace(dept/nlev/2,dept-dept/nlev/2,nlev)
vaisala=0.002
g=9.806
sigma=26.20
romoy=(1000+sigma)/(1-vaisala**2*dept/(2*g))
sig=cible*0
h=cible*0+2*cible[0]

# EXP 2

#sig[1:]=26.2+romoy*(0.5*(cible[:-1]+cible[1:])-50.)*vaisala**2/g
#sig[0]=sig[1]-(sig[2]-sig[1])

#EXP3
sig[np.where(cible<=50)]=25.
ind5060=np.where((cible>50) & (cible<=60))
sig[ind5060]=26.2+romoy*(cible[ind5060]-50.)*vaisala**2/g+1.2*(cible[ind5060]-60.)/10
sig[np.where(cible>60)]=26.2+romoy*(cible[np.where(cible>60)]-50.)*vaisala**2/g

#EXP1
#sig[np.where(cible<=50)]=25.8
#sig[np.where(cible>=50)]=27.


[hN,N2,MOD,VEC,rayon,celerite]=mode(sig,h)

#important pour pouvoir lire les netcdf avec ncview et ncdump

f = netcdf.netcdf_file('EXP3_mode_barocline_'+str(int(nlev))+'.nc', 'w')
#f = netcdf.netcdf_file('EXP2_mode_barocline_'+str(int(nlev))+'.nc', 'w')
f.history = 'created by mode_baro_netcdf.py'
f.createDimension('Depth',len(cible))
f.createDimension('n_mode',len(cible))

ncVEC = f.createVariable('VEC','f',('Depth','n_mode'))
ncVEC[:] = VEC
ncVEC.units = 'no units'

ncMOD = f.createVariable('MOD','f',('n_mode',))
ncMOD[:] = MOD
ncMOD.units = 'no units'

ncN2 = f.createVariable('N2','f',('Depth',))
ncN2[:] = N2
ncN2.units = 's-1'

ncrayon = f.createVariable('rayon','f',('n_mode',))
ncrayon[:] = rayon
ncrayon.units = 'km'

nccelerite = f.createVariable('celerite','f',('n_mode',))
nccelerite[:] = celerite
nccelerite.units = 'm s-1'

ncdepth = f.createVariable('Depth','f',('Depth',))
ncdepth[:] = cible
ncdepth.units = 'm'
f.close()

#depth = f.createAxis(cible,id='depth')
#depth.long_name = 'Depth'
#depth.units = 'm'
#depth.designateLevel() # depth.axis = 'Z'
#nmode = cdms2.createAxis(np.arange(nlev),id='nmode')
#nmode.long_name = 'Mode number'
#nmode.units = 'n'
#ncVEC = cdms2.createVariable(VEC,typecode='f',id='V',
#    fill_value=1.e20,axes=[depth,nmode],copyaxes=0,
#    attributes=dict(long_name='Vecteur Propre'))
#
#ncMOD = cdms2.createVariable(MOD,typecode='f',id='MOD',
#    fill_value=1.e20,axes=[nmode],copyaxes=0,
#    attributes=dict(long_name='Valeur prore'))
#
#ncN2 = cdms2.createVariable(N2,typecode='f',id='N2',
#    fill_value=1.e20,axes=[depth],copyaxes=0,
#    attributes=dict(long_name='N2'))
#
#ncrayon = cdms2.createVariable(rayon,typecode='f',id='rayon',
#    fill_value=1.e20,axes=[nmode],copyaxes=0,
#    attributes=dict(long_name='rayon deformation 1/(f0*sqrt(MOD))'))
#
#nccelerite = cdms2.createVariable(celerite,typecode='f',id='celerite',
#    fill_value=1.e20,axes=[nmode],copyaxes=0,
#    attributes=dict(long_name='celerite 1/sqrt(MOD)'))

#f.write(ncVEC)
#f.write(ncMOD)
#f.write(ncN2)
#f.write(ncrayon)
#f.write(nccelerite)
#f.close() # fermeture

