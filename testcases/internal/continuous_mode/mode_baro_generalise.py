##################################################
######## Calcul des modes barocline  #############
##################################################
# comodo project
# written by A. Le Boyer
# python users 
##################################################
#            resolution de l'equation 
#   d/dz [ 1/N^2 dpsi_n/dz ] + 1/C_n^2 psi_n = 0
#
#   f(z)  = 1/N^2 dpsi_n/dz
#   df/dz = -1/C_n^2 psi_n (probleme aux valeurs propres)
#   df/dz = A psi_{n-1} + B psi_n + C psi_{n+1}
#
#
#                conditions limites
#       
#       dpsi_n/dz = 0 pour z = -H
#       dpsi_n/dz + N^2/g psi_n = 0 pour z = 0
#
#       N^2 = -g/rho_0 drho/dz
#####################################################
###########        resolution  ######################
#####################################################
#                                                ####
#''''''''''''''      '''''''''                   ####
#'b c 0 0 0 0 '      'psi_1  '                   ####
#'a b c 0 0 0 '      'psi_2  '                   ####
#'0 a b c 0 0 ' time ' .     '  = 1/C psi_n      ####
#'0 0 a b c 0 '      ' .     '                   ####
#'0 0 0 a b c '      ' .     '                   ####
#'0 0 0 0 a b '      ' psi_n '                   ####
#''''''''''''''      '''''''''                   ####
#####################################################
#####################################################
#
#      arguments d'entree:  ---N2_{i-1}------------
#                                         |h_{i-1}
#  density profil(sig)      ---N2_i --------------- 
#  au milieu des couches                  |
#  pour avoir N2 aux interfaces           |
#                                         |
#  layer thickness(h)                     |h_i
#                           ---N2_{i+1}------------
#
#
###################################################
import numpy as np
import scipy as sc   
import scipy.linalg   
from numpy.matlib import repmat

def mode(sig,h):

   # parametres physique
   g    = 9.81   # constante de gravitation  
   rau0 = 1000   # masse volumique moyenne de l'eau 
  
   # initialisation
   Z   = len(h)
   dz = 0.5*( h[1:]+h[:-1] ) # epaisseur de couche entre les sigma
   hN=np.zeros(Z)            # hN epaisseur de couche des points N  
   hN=h.copy()

   # calcul de N2
   # N2(z) positionne a l'interface, N2 lineaire entre chaques points
   # N2_{surface} n'est pas definit car inutile pour la definition des coef de la matrice MAT 

   N2=np.zeros(Z)
   N2[:-1] = (sig[1:]-sig[:-1])/dz
   N2      = N2*g/rau0
   N2[-1]=N2[-2] #on force N2 au fond = N2 au dessus du fond
   
   # cas de la couche de melange - uniquement en coordonees z 
   bool=N2==0
   if bool.sum()!=0:
      hmel=hN[bool].sum()
      N2[bool]=1e-10 #pour permettre la diagonalisation
    
   #normalisation
   H0=hN.mean()
   N0=N2*hN
   N0=N0.sum()/hN.sum()
   NORM=1 # apres test avec N cst pas besoin de normalistation
   xlat=46.0
   f0=4*np.arccos(-1.)/86400.*np.sin(xlat*np.arccos(-1.)/180.)
   
   # conditions limites
   # en surface 
   bs = NORM/(g*hN[0]) - NORM/(N2[0]*hN[0]*hN[0])
   #bs = - NORM/(N2[0]*hN[0]*hN[0])  #avec toit rigide
   cs = NORM/(N2[0]*hN[0]*hN[0])

   # au fond 
   af = NORM/(N2[-1]*hN[-1]*hN[-2]) 
   bf = -af


   # matrice coefficients de psi
   A = np.zeros(Z-1)
   B = np.zeros(Z)
   C = np.zeros(Z-1)

   A       = NORM/(N2[:-1]*hN[1:]*hN[:-1])  # attention dimension de N2 
   B[1:-1] = -NORM/(N2[1:-1]*hN[1:-1]*hN[1:-1]) - NORM/(N2[:-2]*hN[1:-1]*hN[:-2])
   C       = NORM/(N2[:-1]*hN[:-1]*hN[:-1])

   

   C[0]  = cs
   B[0]  = bs
   B[-1] = bf
   A[-1] = af

   MAT        = np.diag(B)+np.diag(A,-1)+np.diag(C,1)
   MATB       = np.ones(Z)
   MATB[0]    = 0
   MATB[-1]   = 0
   MATB=np.diag(MATB)
   #[MOD,VEC]  = np.linalg.eig(MAT,MATB) 
   [MOD,VEC]  = sc.linalg.eig(MAT,MATB) 
   ind        = np.argsort(MOD,axis=0)
   MOD        = MOD[ind]
   VEC        = VEC[:,ind]/np.sqrt(np.sum(np.tile(h,[Z,1]).T*VEC[:,ind]**2,axis=0)/h.sum())
   #VEC        = VEC[:,ind]/np.sqrt(VEC[;,ind]/Z)
   rayon      = 1/f0 / np.sqrt(np.abs(MOD)) 
   celerite   = 1/np.sqrt(np.abs(MOD))
  
   return hN,N2,MOD,VEC,rayon,celerite

