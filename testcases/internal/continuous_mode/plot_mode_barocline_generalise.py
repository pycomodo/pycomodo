#!/home6/caparmor/aleboyer/anaconda/bin/python
# -*- coding: utf-8 -*-

import sys
path ='../../../'
sys.path.append(path)

import logging
from optparse import OptionParser

import pycomodo
import pycomodo.util.variables as puv
import pycomodo.util.vertical  as puz
import pycomodo.util.plots     as pup
import pycomodo.operators      as op
#from mode_baro_generalise import mode
from mode_baro import mode

from pycomodo import log

import numpy as np

import matplotlib.pyplot as plt

cst_rho0 = 1000

def main( fic_in, t0, j0, i0 ):
    
    archive = pycomodo.Archive(fic_in)
    log("Reading : {}".format(archive))
    log(" - t0 = {0}".format(t0))
    log(" - j0 = {0}".format(j0))
    log(" - i0 = {0}".format(i0))
        
    tm  = archive.ntimes
    jm, = archive.get_metric("x", "t").axes["Y"].shape
    im, = archive.get_metric("x", "t").axes["X"].shape
    
    if t0 >= tm:
       log.error("t index t0 must be inferior to {0}.".format(tm))
       return
    if j0 >= jm:
       log.error("j index j0 must be inferior to {0}.".format(jm))
       return
    if i0 >= im:
       log.error("i index i0 must be inferior to {0}.".format(im))
       return
    
    var_dzt = puv.get_variable_from_stdnames(archive, 'cell_thickness')
    var_dzt = var_dzt[t0,:,j0,i0]   
    
    cst_alpha = 0.20
    cst_beta  = 0.77
    var_rho = puv.get_potential_density(archive, cst_alpha=cst_alpha, cst_beta=cst_beta)
    var_rho = var_rho[t0,:,j0,i0] 
    [hN,N2,MOD,VEC,rayon,celerite]=mode(var_rho,var_dzt)
   
    z=0*hN
    z=hN.cumsum()-0.5*hN
   
    plt.plot(VEC[:,-4:],z)
    plt.xlim([-2,2])
    plt.savefig('test.png')
    plt.close()

if __name__ == "__main__":

    parser = OptionParser(version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
						help="outputs debug messages")
    parser.add_option("-t", "--time", dest="t0", default=0,  type="int",
						help="time index to extract")
    parser.add_option("-j", "--j0",   dest="j0", default=1, type="int",
						help="j index to extract")
    parser.add_option("-i", "--i0",   dest="i0", default=1, type="int",
						help="i index to extract")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)
    main(args, options.t0, options.j0, options.i0)

