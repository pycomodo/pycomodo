#!/home6/caparmor/aleboyer/anaconda/bin/python
# -*- coding: utf-8 -*-

import sys
path ='../../'
sys.path.append(path)
path ='../continuous_mode/'
sys.path.append(path)

import logging
from optparse import OptionParser

import pycomodo
import pycomodo.util.variables as puv
import pycomodo.util.vertical  as puz
import pycomodo.util.plots     as pup
import pycomodo.operators      as op
from mode_baro import mode

from pycomodo import log

import numpy as np

import matplotlib.pyplot as plt

cst_rho0 = 1000

#fic_in     = '/home/shom_simurep/aleboyer/RUNS/EXP21/EXP215100/RUNS025/comodo_global.nc'

def proj_iso2z(sig,z,cible):
    if len(sig.shape)!=3:
       log.error("field to interpolate must be a T, Z, X field")
       return
    else:
       [T,Z,X]=sig.shape
       log(" - T = {0}".format(T))
       log(" - Z = {0}".format(Z))
       log(" - X = {0}".format(X))
    if (len(cible.shape)!=1):
       log.error("new z grid must be an array")
       return
    else:
       [Znew]=cible.shape
       log(" - new Z = {0}".format(Znew))
    new_sig=np.zeros([T,Znew,X])
    for t in np.arange(T):
       for x in np.arange(X):
        new_sig[t,:,x] = np.interp(cible,z[t,:,x],sig[t,:,x])
    return new_sig
 

log("to use this programme you need to do ./KE_per_mode_time_average -t t0 -T t1 -j j0 -i i0 -I i1 -n name_EXP nc_file ")
log("t0,t1 : time boundaries ")
log("i0,i1 : x boundaries ")
log("j0    : y location (no y dependance) ")
log("name EXP : name of the config to get the associated continuous baroclinic mode  ")

def KE_per_mode_time_average( fic_in, t0, t1, j0, i0, i1, name_EXP ):
    
    archive = pycomodo.Archive(fic_in)
    log("Reading : {}".format(archive))
    log(" - t0 = {0}".format(t0))
    log(" - t1 = {0}".format(t1))
    log(" - j0 = {0}".format(j0))
    log(" - i0 = {0}".format(i0))
    log(" - i1 = {0}".format(i1))
        
    tm  = archive.ntimes
    jm, = archive.get_metric("x", "t").axes["Y"].shape
    im, = archive.get_metric("x", "t").axes["X"].shape
    
    if t0 > tm:
       log.error("t index t0 must be inferior to {0}.".format(tm))
       return
    if t1 > tm:
       log.error("t index t1 must be inferior to {0}.".format(tm))
       return
    if j0 >= jm:
       log.error("j index j0 must be inferior to {0}.".format(jm))
       return
    if i0 >= im:
       log.error("i index i0 must be inferior to {0}.".format(im))
       return
    if i1 >= im:
       log.error("i index i1 must be inferior to {0}.".format(im))
       return
    log("get archive variable zt  ")
    zt   = puv.get_variable_from_stdnames(archive,'depth_below_geoid')
    log("get archive variable ssh  ")
    eta  = puv.get_variable_from_stdnames(archive,'sea_surface_height_above_geoid')
    log("get archive variable bathy  ")
    depp = puv.get_variable_from_stdnames(archive,'sea_floor_depth_below_geoid')
    
    log("get archive variable ut  ")
    ut = puv.get_variable_at_location(archive, 'sea_water_x_velocity', 't', interpolate=True)
    #log("get archive variable vt  ")
    #vt = puv.get_variable_at_location(archive, 'sea_water_y_velocity', 't', interpolate=True)
   
    zt=zt[t0:t1,:,j0,i0:i1]
    eta=eta[t0:t1,j0,i0:i1]
    depp=depp[j0,i0:i1]
    ut = ut[t0:t1,:,j0,i0:i1]
    #vt = vt[t0:t1,:,j0,i0:i1]
    
    T,Z,X=zt.shape

    # definition d une grille haute resolution pour projeter
    # les sorties modele dessus, on s affranchi de l aspect
    # lagrangien verticale de HYCOM et permet de projeter les 
    # vitesses sur des mode baroclines quasi continus defini sur la stratif initiale
    
    # lecture des mode baroclines "quasi continu"
    log("get quasi coninuous baroclinic mode ")
    ncmode = pycomodo.Archive('../continuous_mode/'+name_EXP+'_mode_barocline_2000.nc')
    VEC    = ncmode.get_variable('VEC')[:]
    # lecture de la grille cible des mode 
    log("get quasi coninuous grid ")
    cible = ncmode.get_variable('Depth')[:]
    # epaisseur de couche de la nouvelle grille 
    dz    = cible[1]-cible[0]  
  
    #projection
    log("ut projection on quasi coninuous grid: uz ")
    uz=proj_iso2z(ut,-zt,cible)
    #log("vt projection on quasi coninuous grid: vz ")
    #vz=proj_iso2z(vt,-z,cible)
    
    # projection des vistesses sur les modes baroclines
    log("uz,vz projection on quasi coninuous baroclinic modes: u_mod, v_mod ")
    udz     = uz*dz
    #vdz     = vz*dz
    Vecdz   = VEC*dz  
    NormVec = np.mat(VEC.T)*np.mat(Vecdz)
    
    nbmode = len(cible)
    u_mod   = np.zeros([nbmode,T,X]) #u_mode (mode,time,x)
    #v_mod   = np.zeros([nbmode,T,X]) #u_mode (mode,time,x)
   
    for x in range(X):
        u_mod[:,:,x] = np.mat(VEC).T*np.mat(udz[:,:,x]).T
        u_mod[:,:,x] = np.squeeze(np.array([u_mod[i,:,x]/NormVec[i,i] for i in range(nbmode)]))
        #v_mod[:,:,x] = np.mat(VEC.T)*np.mat(vdz[:,:,x]).T
        #v_mod[:,:,x] = np.squeeze(np.array([v_mod[i,:,x]/NormVec[i,i] for i in range(nbmode)]))
       
    
    log("compute KE as std (umode)  ")
    log("KE time and x rms")
    KE=np.zeros(len(cible))
    for n in range(len(cible)):
        KE[n]=u_mod[n,:,:].std()**2
    KE=KE[-11:-1]
  
    
    log("plot KE per mode")
    plt.plot(KE[::-1])
    plt.title('KE per mode time and x average')
    plt.xlabel('mode')
    plt.ylabel('$\\frac{1}{2}(u_{mode}^2+v_{mode}^2)$ $    m^2$ $s^{-2}$')
    plt.savefig(fic_in.split('/')[-1]+'KE_time_x_average.png')
    plt.close()

    return KE[::-1]
if __name__ == "__main__":

    parser = OptionParser(version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
						help="outputs debug messages")
    parser.add_option("-t", "--time", dest="t0", default=0,  type="int",
						help="time index to extract")
    parser.add_option("-T", "--TIME", dest="t1", default=0,  type="int",
						help="time index to extract")
    parser.add_option("-j", "--j0",   dest="j0", default=1, type="int",
						help="j index to extract")
    parser.add_option("-i", "--i0",   dest="i0", default=1, type="int",
						help="i index to extract")
    parser.add_option("-I", "--i1",   dest="i1", default=2, type="int",
						help="i index to extract")
    parser.add_option("-n", "--name_EXP",   dest="name_EXP", default='EXP2', type="str",
						help="EXP to extract")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)
    KE_per_mode_time_average(args, options.t0, options.t1, options.j0, options.i0, options.i1, options.name_EXP)

