#!/home6/caparmor/aleboyer/anaconda/bin/python
# -*- coding: utf-8 -*-


import sys
path ='../../../'
sys.path.append(path)
#path ='../continuous_mode/'
#sys.path.append(path)

import logging
from KE_per_mode_time_average import KE_per_mode_time_average
from pycomodo import log
import numpy as np
import matplotlib.pyplot as plt


fic_in     = '../ncfile/EXP315100_comodo_global.nc'
t0 = 1441-24*10
t1 = 1441
i0 = 120
i1 = 420
j0 = 0
name_EXP='EXP3'
A=KE_per_mode_time_average(fic_in,t0,t1,j0,i0,i1,name_EXP) 

fic_in     = '../ncfile/EXP325100_comodo_global.nc'
B=KE_per_mode_time_average(fic_in,t0,t1,j0,i0,i1,name_EXP) 


log("plot KE")
plt.plot(A,label='iso')
plt.plot(B,label='z')
plt.legend(loc='best')
plt.title('KE per mode time and x average')
plt.xlabel('mode')
#plt.ylabel('$\\frac{1}{2}(u_{mode}^2+v_{mode}^2)$ $    m^2$ $s^{-2}$')
plt.ylabel('$std(u_{mode})^2$ $    m^2$ $s^{-2}$')
plt.savefig('multi_KE_per_mode_time_average_x'+str(i0)+str(i1)+'_average.png')
plt.close()
