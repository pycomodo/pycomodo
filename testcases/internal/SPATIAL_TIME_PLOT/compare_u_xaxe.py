#!/home6/caparmor/aleboyer/anaconda/bin/python
# -*- coding: utf-8 -*-

import sys
path ='../../../'
sys.path.append(path)

import logging
from optparse import OptionParser

import pycomodo
import pycomodo.util.variables as puv
import pycomodo.util.vertical  as puz
import pycomodo.util.plots     as pup
import pycomodo.operators      as op

from pycomodo import log
import matplotlib.pyplot as plt

import numpy as np
fic_in= '../ncfile/EXP315100_comodo_global.nc'
t0=0
j0=0
z0=0
def main( fics_in, t0, z0, j0):
    plot1=plt.axes([0.1,0.2,0.7,0.4])
    plot2=plt.axes([0.1,0.7,0.7,0.2])
    plot3=plt.axes([0.1,0.1,0.7,0.1])
    select_time=np.arange(1,6)*24*10
    c='br'
    compt=0
    name_save=''
    name_label=[]
    leg=[]
    umin=9999;
    umax=-9999;
    for fic_in in fics_in: 
        archive = pycomodo.Archive(fic_in)
        log("Reading : {}".format(archive))
        log(" - t0 = {0}".format(t0))
        log(" - j0 = {0}".format(j0))
        log(" - z0 = {0}".format(z0))
            
        tm  = archive.ntimes
        jm, = archive.get_metric("x", "t").axes["Y"].shape
        zm, = archive.get_metric("z", "t").axes["Z"].shape
        
        if t0 >= tm:
           log.error("t index t0 must be inferior to {0}.".format(tm))
           return
        if j0 >= jm:
           log.error("j index j0 must be inferior to {0}.".format(jm))
           return
        if z0 >= zm :
           log.error("z index z0 must be inferior to {0}.".format(zm))
           return
    

        ut = puv.get_variable_at_location(archive, 'sea_water_x_velocity', 't', interpolate=True)
        ut = ut[t0:,0,j0,:]
        Ut = puv.get_variable_at_location(archive, 'barotropic_sea_water_x_velocity_at_u_location', 't', interpolate=True)
        Ut = Ut[t0:,j0,:]
        u =ut-Ut
        depp = puv.get_variable_at_location(archive, 'sea_floor_depth_below_geoid','t')
        depp = depp[j0,:]
        
        name_save=name_save+'_'+fic_in.split('/')[-1].split('_')[0]
        name_label.append(fic_in.split('/')[-1].split('_')[0])
    
        T,X=ut.shape
        xaxe=np.arange(X)
        taxe=np.arange(T)/48. # axee en jours 48. pour sortie 30 min a changer pour ne pas etre en dur
        j=1.0
        xpoint=100
        l, = plot2.plot(taxe,100*u[:,xpoint],color=c[compt])
        for t in select_time: 
            plot1.plot(xaxe,j-u[t,:],color=c[compt],linewidth=2)
            plot2.plot([t/48.,t/48.],[100*u[:,xpoint].min(),100*u[:,xpoint].max()],'k',linewidth=2)
            if compt == len(fics_in)-1:
               plot1.text(xaxe[10],j+0.02,('day %1.0f' %(t/48.)),backgroundcolor='w')
               plot2.text(t/48.-1.7,100*u[:,xpoint].max()+2,('day %1.f' %(t/48.)),backgroundcolor='w')
            j=j-0.5
        compt += 1
        leg.append(l)
        umin=np.min(umin,u[:,xpoint].min())
        umax=np.max(umax,u[:,xpoint].max())
    plot3.plot(xaxe,depp/1000,'k',linewidth=2)
    plot3.plot([0,0],[0,4],'k--')
    A=plot2.legend(leg,name_label,bbox_to_anchor=(1.28,1.),fontsize=10)
    A.set_title('Experiment')
    plot2.set_ylabel('$u$ $cm$ $s^{-1}$')
    plot2.set_xlabel('$days$')
    plot2.set_title('$u_s$')
    plot1.set_ylabel('$u$ $cm$ $s^{-1}$')
    plot1.set_xticklabels('')
    plot1.set_xlim([0,X])
    plot3.set_xlim([0,X])
    plot2.set_ylim([100*u[:,xpoint].min(),100*u[:,xpoint].max()+5])
    plot3.set_ylabel('$Depth$ $(km)$')
    plot3.set_xlabel('$x$ $(km)$')
    plot3.set_yticks([2,4])
    plot1.set_yticks([-1.,-0.5,0,0.5,1])
    plot3.invert_yaxis()
    
    plt.savefig('compare'+name_save+'_xandtime.png')
    #plt.show()
    plt.close()

if __name__ == "__main__":

    parser = OptionParser(version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
						help="outputs debug messages")
    parser.add_option("-t", "--time", dest="t0", default=0,  type="int",
						help="time index to extract")
    parser.add_option("-j", "--j0",   dest="j0", default=1, type="int",
						help="j index to extract")
    parser.add_option("-z", "--z0",   dest="z0", default=1, type="int",
						help="z index to extract")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)
    main(args, options.t0, options.j0, options.z0)

