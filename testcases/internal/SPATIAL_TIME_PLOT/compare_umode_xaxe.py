#!/home6/caparmor/aleboyer/anaconda/bin/python
# -*- coding: utf-8 -*-

import sys
path ='../../../'
sys.path.append(path)
path ='../continuous_mode/'
sys.path.append(path)

import logging
from optparse import OptionParser

import pycomodo
import pycomodo.util.variables as puv
import pycomodo.util.vertical  as puz
import pycomodo.util.plots     as pup
import pycomodo.operators      as op
from mode_baro import mode

from pycomodo import log
import matplotlib.pyplot as plt

import numpy as np

def proj_iso2z(sig,z,cible):
    if len(sig.shape)!=3:
       log.error("field to interpolate must be a T, Z, X field")
       return
    else:
       [T,Z,X]=sig.shape
       log(" - T = {0}".format(T))
       log(" - Z = {0}".format(Z))
       log(" - X = {0}".format(X))
    if (len(cible.shape)!=1):
       log.error("new z grid must be an array")
       return
    else:
       [Znew]=cible.shape
       log(" - new Z = {0}".format(Znew))
    new_sig=np.zeros([T,Znew,X])
    for t in np.arange(T):
       for x in np.arange(X):
        new_sig[t,:,x] = np.interp(cible,z[t,:,x],sig[t,:,x])
    return new_sig




def main( fics_in, t0, j0, nmode):
    # get the netcdf files
    plot1=plt.axes([0.1,0.1,0.6,0.5])
    plot2=plt.axes([0.1,0.7,0.7,0.2])
    plot3=plt.axes([0.8,0.1,0.15,0.5])
    
    select_time=np.arange(0,1441-t0,48)
    selec_time=select_time[:5]
    c='br'
    compt=0
    name_save=''
    name_label=[]
    leg=[]
    xpoint=100
    for fic_in in fics_in: 
        archive = pycomodo.Archive(fic_in)
        log("Reading : {}".format(archive))
        log(" - t0 = {0}".format(t0))
        log(" - j0 = {0}".format(j0))
        log(" - nmode = {0}".format(nmode))
            
        tm  = archive.ntimes
        jm, = archive.get_metric("x", "t").axes["Y"].shape
        zm, = archive.get_metric("z", "t").axes["Z"].shape
        
        if t0 >= tm:
           log.error("t index t0 must be inferior to {0}.".format(tm))
           return
        if j0 >= jm:
           log.error("j index j0 must be inferior to {0}.".format(jm))
           return
        if nmode >= zm/2 :
           log.error("nmode index z0 must be inferior to {0}.".format(zm/2))
           return
    
        # lecture des mode baroclines "quasi continu"
        log("get quasi coninuous baroclinic mode ")
        ncmode = pycomodo.Archive('../continuous_mode/'+fic_in.split('/')[-1][:4]+'_mode_barocline_2000.nc')
        VEC_c    = ncmode.get_variable('VEC')[:,-nmode-1]
        # lecture de la grille cible des mode 
        log("get quasi coninuous grid ")
        cible = ncmode.get_variable('Depth')[:]
        # epaisseur de couche de la nouvelle grille 
        dz    = cible[1]-cible[0]

        plot3.plot(VEC_c*np.sign(VEC_c[-1]),cible/1000,'k',linewidth=2)
        ut = puv.get_variable_at_location(archive, 'sea_water_x_velocity', 't', interpolate=True)
        ut = ut[t0:,:,j0,120:420]
        h   = puv.get_variable_at_location(archive, 'cell_thickness', 't')
        h   = h[0,:,j0,xpoint]
        sig = puv.get_variable_at_location(archive, 'sea_water_sigma_theta', 't')
        sig = sig[0,:,j0,xpoint]
        
        [hN,N2,MOD,VEC,rayon,celerite]=mode(sig,h)
        z=h*0
        z[0]=h[0]/2.
        hsum=h.cumsum()
        for i in range(1,len(h)):
            z[i]=hsum[i-1]+h[i]/2.


        log("get archive variable zt  ")
        zt   = puv.get_variable_from_stdnames(archive,'depth_below_geoid')
        log("get archive variable ssh  ")
        eta  = puv.get_variable_from_stdnames(archive,'sea_surface_height_above_geoid')
        log("get archive variable bathy  ")
        depp = puv.get_variable_from_stdnames(archive,'sea_floor_depth_below_geoid')
        zt=zt[t0:,:,j0,120:420]
        eta=eta[t0:,j0,120:420]
        depp=depp[j0,120:420]

        # changement de coordonees dimension z comprise entre H et 0 (prise en compte de la ssh)
        # probablement uniquement HYCOM
        log("change z coordinate (only HYCOM?) ")
        T,Z,X=zt.shape
        #projection
        log("ut projection on quasi coninuous grid: uz ")
        uz=proj_iso2z(ut,-zt,cible)
    
        # projection des vistesses sur les modes baroclines
        log("uz projection on quasi coninuous baroclinic modes: u_mod ")
        udz     = uz*dz
        Vecdz   = VEC_c*dz
        NormVec = np.mat(VEC_c)*np.mat(Vecdz).T
    
        nbmode = len(cible)
        u_mod   = np.zeros([T,X]) #u_mode (mode,time,x)
        for x in range(X):
            u_mod[:,x] = np.mat(VEC_c)*np.mat(udz[:,:,x]).T
            u_mod[:,x] = u_mod[:,x]/NormVec

        xaxe=np.arange(X)+120
        taxe=np.arange(T)/48.
        j=0.5
        l, = plot2.plot(taxe,100*u_mod[:,xpoint],color=c[compt])
        for t in select_time: 
            plot1.plot(xaxe,j-u_mod[t,:],color=c[compt],linewidth=2)
            plot2.plot([t/48.,t/48.],[100*u_mod[:,xpoint].min(),100*u_mod[:,xpoint].max()],'k',linewidth=2)
            if compt==1:
               plot1.text(xaxe[10],j+0.02,('day %1.0f' %(t/48.)))
               plot2.text(t/48.-1.7,100*u_mod[:,xpoint].max(),('day %1.f' %(t/48.)))
            j=j-0.1
        compt += 1
        leg.append(l)
        name_save=name_save+fic_in.split('/')[-1].split('_')[0]
        name_label.append(fic_in.split('/')[-1].split('_')[0])
        plot3.plot(VEC[:,-nmode-1]*np.sign(VEC[-1,-nmode-1]),z/1000,'k',marker='*')

    #plot3.plot(VEC[:,-nmode]*np.sign(VEC[-1,-nmode]),z/1000,'k',marker='*')
    plot3.legend(['continuous','discret'],bbox_to_anchor=(0.9,1.15),fontsize=8)
    plot3.plot([0,0],[0,4],'k--')
    A=plot2.legend(leg,name_label,bbox_to_anchor=(1.28,1.),fontsize=10)
    A.set_title('Experiment')
    plot2.set_ylabel('$u$ $cm$ $s^{-1}$')
    plot2.set_xlabel('$days$')
    plot2.set_title('$mode%i$' %(nmode))
    plot1.set_ylabel('$u$ $cm$ $s^{-1}$')
    plot1.set_xlabel('$x$ $(km)$')
    plot1.set_ylim([0,0.6])
    plot2.set_ylim([100*u_mod[:,xpoint].min(),100*u_mod[:,xpoint].max()+1])
    plot3.set_xticks([-1,0,1])
    plot3.set_ylabel('$Depth$ $(km)$')
    plot3.set_xlabel('Mode profil')
    plot3.invert_yaxis()
    
    plt.savefig(name_save+'xandtime_compare_mode%i.png' %(nmode))
    #plt.show()
    plt.close()

if __name__ == "__main__":

    parser = OptionParser(version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
						help="outputs debug messages")
    parser.add_option("-t", "--time", dest="t0", default=0,  type="int",
						help="time index to extract")
    parser.add_option("-j", "--j0",   dest="j0", default=1, type="int",
						help="j index to extract")
    parser.add_option("-n", "--nmode",   dest="nmode", default=1, type="int",
						help="mode number to extract")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)
    main(args, options.t0, options.j0, options.nmode)

