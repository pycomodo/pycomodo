Diagnostiques pour le cas-test "Onde interne"
------------------------------------

Ce dossier contient des scripts Python permettant de produire des diagnostiques pour le
cas-test "Onde interne".

Au préalable, il est nécessaire d'avoir une installation fonctionnelle de la librairie pycomodo.
Les instructions sont détaillées sur cette page :

http://pycomodo.forge.imag.fr/api/installation.html

Les scripts '...1D.....py' peuvent être utiliser par toutles modeles (enfin j'espere), ils proposent des diagnostics
pour un t,x,y fixes. 

Du fait que les diagnostics pour ce cas test sont construit en utilisant les sorties d' hycom, les diagnostiques 
avec des moyennes temporelles (ou autres moyennes) pose probleme. En effet le moyennage de variable lagrangienne sur z
(comme celle d'hycom) est different du moyennage de variable Eulerienne. 
Les scripts '...time_average....' prennent le parti de projeter tout les champs sur une grille tres haute resolution
(2000 couches) pour faire les diagnostiques. Cela permet de s'affranchir de ce probleme de moyennes et surtout de pouvoir 
projeter les sorties des modeles sur des modes barocline quasi continu calcules a partir de la stratification initiale. 
Par consequent les calculs sont un peu long si le domaine est trop grand. 

!!!! info !!!!!!!!!
Une autre manip est possible pour le moyennage: l'utilisation de l equation 9 de MacIntosh/MacDougall JPO1996 

example d' utilisation  des routines
dans SPATIAL_TIME_PLOT 
$./compare_umode_xaxe.py -t 1200 -j 0 -n 3 ../ncfile/EXP315100_comodo_global.nc ../ncfile/EXP325100_comodo_global.nc
dans KE
$./KE_per_mode_1D.py -t 0 -j 0 -i 100 ncfile/EXP215100_comodo_global.nc 
dans DENSITY_PROFIL
$./density_profil.py -t 0 -j 0 -i 100 ../ncfile/EXP215100_comodo_global.nc 

les options
-t : position en temps
-j : position en y
-i : position en x

attention!!! ligne 48 KE_per_mode_1D, density_profil.py  modele dependant
var_dzt=archive.metrics['dz_t_TZYX'][t0,:,j0,i0] (HYCOM)

$./KE_per_mode_time_average -t 1 -T 24 -j 0 -i 120 -I 420 -n EXP2 ../ncfile/EXP215100_comodo_global.nc
to use this programme you need to do ./KE_per_mode_time_average -t t0 -T t1 -j j0 -i i0 -I i1 -n name_EXP nc_file 
t0,t1    : time boundaries 
i0,i1    : x boundaries 
j0       : y location (no y dependance) 
name EXP : name of the config to get the associated continuous baroclinic mode  

$./density_profil_time_average -t 0 -T 24 -j 0 -i 120 -I 420 EXP2 ../ncfile/EXP215100_comodo_global.nc
to use this programme you need to do ./density_profil_time_average -t t0 -T t1 -j j0 -i i0 -I i1 nc_file
t0,t1 : time boundaries
i0,i1 : x boundaries
j0    : y location (no y dependance)

$./conversion_rate -t 1201 -T 1441 -j 0 -i 400 -I 520 ../ncfile/EXP315100_comodo_global.nc
to use this programme you need to do ./density_profil_time_average -t t0 -T t1 -j j0 -i i0 -I i1 nc_file
t0,t1 : time boundaries
i0,i1 : x boundaries
j0    : y location (no y dependance)


Pour comparer different density_profil_time_average et KE_per_mode_time_average
./multi_KE_per_mode_time_average         
./multi_density_profil_time_average 
./multi_conversion_rate 


a venir tres rapidement: diag ordre de grandeur, diag mode baro generalise
et d autre


