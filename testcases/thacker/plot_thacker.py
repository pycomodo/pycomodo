#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as col
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib.ticker import MultipleLocator

import pycomodo
import pycomodo.util.variables as puv
import pycomodo.operators as op
from pycomodo import log, UnknownVariable

pp   = lambda tab: "[ "+", ".join("{0:+12.8f}".format(x) for x in tab)+" ]"

def plot_thacker(archive, model_name=None, grid_resolution=None, tmax=72., plot_u=False):

    try:
        log("=== Get time :")
        var_time = archive.get_variable(stdname='time')
        var_time.check(True)

        log("=== Get ssh :")
        var_ssh = puv.get_ssh(archive)
        var_ssh.check(True)

        log("=== Get sea_floor_depth :")
        var_bathy = puv.get_sea_floor_depth(archive, "T")
        var_bathy.check(True)

        log("=== Get 3D velocities :")
        var_uz = archive.get_variable(stdname="sea_water_x_velocity_at_u_location")
        var_vz = archive.get_variable(stdname="sea_water_y_velocity_at_v_location")
        var_uz.check(True)
        var_vz.check(True)

        log("=== Get barotropic velocities :")
        var_ub = archive.get_variable(stdname="barotropic_sea_water_x_velocity_at_u_location")
        var_vb = archive.get_variable(stdname="barotropic_sea_water_y_velocity_at_v_location")
        var_ub.check(True)
        var_vb.check(True)

    except UnknownVariable as err:
        log.error("variable {0} not found in '{1}'".format(err, archive))
        return

    # Compute analytical solution
    cst_eta  = 0.1      # Adimensional amplitude
    cst_dx   = 1000.    # Grid resolution
    cst_f    = 1.e-4    # Coriolis parameter
    cst_g    = 9.806    # Gravity
    cst_L    = 80000.   # Basin width at rest
    cst_D0   = 10.      # Maximal depth
    cst_omega = 0.5*cst_f + np.sqrt(0.25*cst_f**2 +2.*cst_g*cst_D0/(cst_L**2))

    xmin, xmax = var_ssh.get_global_range('X')
    ymin, ymax = var_ssh.get_global_range('Y')

    bathy  = var_bathy[:]
    ssh    = var_ssh[:]

    # Find central point (maximum depth)
    jzero, izero = np.unravel_index(np.argmax(bathy), bathy.shape)

    # Bathy for central point should be equal to cst_D0.
    bathy_shift = bathy[jzero, izero] - cst_D0
    bathy -= bathy_shift
    ssh   += bathy_shift

    # Create slices
    times  = archive.get_time(Ellipsis, "hours")
    times -= archive.get_time(0,        "hours")
    ntimes = len(times)
    try:
        tindex = archive.get_time_index(tmax, "hours")
    except Exception as e:
        log.error(e)
        log.error("You should try to use the '--time' ('-t') option.")
        return

    t_slice   = np.index_exp[tindex]
    yx_slice  = np.index_exp[..., jzero, xmin:xmax]
    tyx_slice = t_slice + yx_slice

    # Get data arrays from variables
    ub_mod = var_ub[yx_slice]
    vb_mod = var_vb[yx_slice]

    dx = archive.get_metric("x", "t")[:]
    dy = archive.get_metric("y", "t")[:]

    nx_t = var_ssh.axes["X"].shape[0]
    nx_u = var_uz .axes["X"].shape[0]
    shift_u = var_uz.axes["X"].c_grid_axis_shift
    shift_v = var_uz.axes["Y"].c_grid_axis_shift

    x1d_t = np.arange(nx_t) * dx[0,0] - izero*dx[0,0]
    x1d_u = np.arange(nx_u) * dx[0,0] - izero*dx[0,0] + shift_u * dx[0,0]
    x1d_t *= 1e-3
    x1d_u *= 1e-3
    x2d_t = np.cumsum(dx, axis=1) - (izero+1)*dx[0,0]
    y2d_t = np.cumsum(dy, axis=0) - (jzero+1)*dy[0,0]
    x2d_t = x2d_t[jzero]
    y2d_t = y2d_t[jzero]

    # Actually compute analytical solution, on a central X axis (j = jzero).
    b_t = np.repeat(bathy[[jzero]], times.shape, axis=0) # Add dummy dimensions for proper broadcasting
    w_t = 3600.*cst_omega*times[:, None]

    u_ana = -cst_eta*cst_omega*cst_L*np.sin(w_t) * np.ones(ub_mod.shape)
    v_ana = -cst_eta*cst_omega*cst_L*np.cos(w_t) * np.ones(vb_mod.shape)
    e_ana = 2.*(cst_eta*cst_D0) * ( x2d_t*np.cos(w_t)/cst_L - y2d_t*np.sin(w_t)/cst_L - 0.5*cst_eta )

    ssh_ana = np.ma.where( e_ana > -b_t, e_ana, -b_t  )

    #=============================================================
    # --- plot SSH & velocity error at final time step
    #=============================================================
    if True:

        fig = plt.figure(figsize=(7,6))
        fig.subplots_adjust(top=0.90, bottom=0.08, left=0.05, right=0.95)
        filename = u"thacker_ssh" if model_name is None                         \
              else u"thacker_ssh_{0}".format(model_name)
        title_ax = u"SSH at t = {0} h".format(tmax) if model_name is None   \
              else u"{0} — SSH at t = {1} h".format(model_name, tmax)

        ax = plt.subplot()
        ax.set_title(title_ax, fontsize=20, family='serif', position=(0.5, 1.05))

        if plot_u:
            # if requested ('-u' option), try to plot the error on the baroclinic x-velocity.
            # This should work only with ROMS outputs.
            try:
                err_u   = var_uz[tyx_slice] - u_ana[tindex]
                var_z2w = archive.get_zlevels("w", "ZX", name="z2w", tpoint=tindex, hpoint=(jzero, izero))
                zaxis   = var_z2w.axes["Z"]
                x2dz_t  = x1d_t[None, :] * np.ones((zaxis.shape[0], 1))
                if zaxis.positive == "down":
                    err_u = err_u[::-1]
                    z2w   = -var_z2w[::-1]
                else:
                    z2w   = var_z2w[:]
                cmap_bwr = col.LinearSegmentedColormap("bwr_20", plt.get_cmap('bwr')._segmentdata, N=31)
#                 m = ax.pcolormesh(x2dz_t, z2w, err_u, vmin=-1., vmax=1., cmap=cmap_bwr, edgecolors='k', lw=0.01)
                m    = ax.pcolormesh(x2dz_t, z2w, err_u, vmin=-1., vmax=1., cmap=cmap_bwr)
                ax2  = inset_axes(ax, width="50%",  height="4%", loc=8)
                cbar = plt.colorbar(m, cax=ax2, orientation='horizontal',
                                    ticks=MultipleLocator(.5), label='Error on $u$ (m/s)', )
                cbar.ax.axis["bottom"].toggle(all=False)
                cbar.ax.axis["top"].toggle(all=True)
                for t in cbar.ax.get_xticklabels():
                    t.set_fontsize(9)
            except:
                log.warning("Cannot plot 3d velocity error")

        ax.plot(x1d_t, ssh_ana[-1],    'r', lw=2, label='analytical')
        ax.plot(x1d_t, ssh[-1, jzero], 'g', lw=2, label='model')
        ax.plot(x1d_t, -b_t[-1],       'k', lw=4)
        ax.set_xlim(-100, 100)
        ax.set_ylim(-2.5, 2.5)
        ax.legend(fontsize=12, loc=1)
        log("=== Write {0}.png...".format(filename))
        plt.savefig(filename, dpi=200)

    #=============================================================
    # --- plot time series at center point
    #=============================================================
    if True:

        plot_err = True

        fig = plt.figure(figsize=(7,6))
        fig.subplots_adjust(top=0.90, bottom=0.08, left=0.10, right=0.95)
        ax = plt.subplot()
        filename = u"thacker_ts_u" if model_name is None    \
              else u"thacker_ts_u_{0}".format(model_name)
        title_ax = u"$u$ at center" if model_name is None   \
              else u"{0} — $u$ at center".format(model_name)

        ax.set_title(title_ax, fontsize=20, family='serif', position=(0.5, 1.05))
        if plot_err:
            ax.plot(times, ub_mod[:,izero]-u_ana [:,izero], 'r', label="error")
            ax.set_ylim(-0.4, 0.4)
        else:
            ax.plot(times, u_ana [:,izero], 'g', label="analytical", lw=2)
            ax.plot(times, ub_mod[:,izero], 'r', label="model")
        ax.plot(times, np.zeros_like(times), 'k', ls='dashed')
        ax.set_xlabel("Times (hours)", fontsize=12)
        ax.set_ylabel("$u$ (m/s)", fontsize=12)
        ax.set_xlim(0, tmax)
        ax.xaxis.grid(color='gray', linestyle=':', which='both')
        ax.yaxis.grid(color='gray', linestyle=':', which='both')
        ax.legend(fontsize=12, loc=2)
        log("=== Write {0}.png...".format(filename))
        plt.savefig(filename, dpi=200)

        fig = plt.figure(figsize=(7,6))
        fig.subplots_adjust(top=0.90, bottom=0.08, left=0.10, right=0.95)
        ax = plt.subplot()
        filename = u"thacker_ts_v" if model_name is None    \
              else u"thacker_ts_v_{0}".format(model_name)
        title_ax = u"$v$ at center" if model_name is None   \
              else u"{0} — $v$ at center".format(model_name)

        ax.set_title(title_ax, fontsize=20, family='serif', position=(0.5, 1.05))
        if plot_err:
            ax.plot(times, vb_mod[:,izero]-v_ana [:,izero], 'r', label="error")
            ax.set_ylim(-0.4, 0.4)
        else:
            ax.plot(times, v_ana [:,izero], 'g', label="analytical", lw=2)
            ax.plot(times, vb_mod[:,izero], 'r', label="model")
        ax.plot(times, np.zeros_like(times), 'k', ls='dashed')
        ax.set_xlabel("Times (hours)", fontsize=12)
        ax.set_ylabel("$v$ (m/s)", fontsize=12)
        ax.set_xlim(0, tmax)
        ax.xaxis.grid(color='gray', linestyle=':', which='both')
        ax.yaxis.grid(color='gray', linestyle=':', which='both')
        ax.legend(fontsize=12, loc=2)
        log("=== Write {0}.png...".format(filename))
        plt.savefig(filename, dpi=200)

    #=============================================================
    # --- plot phase error at center point
    #=============================================================
    if True:

        ff_u_ana = np.fft.rfft(u_ana [:,izero])
        ff_v_ana = np.fft.rfft(v_ana [:,izero])
        ff_u_mod = np.fft.rfft(ub_mod [:,izero])
        ff_v_mod = np.fft.rfft(vb_mod [:,izero])
        x_freq = np.fft.rfftfreq(ntimes, d=1.)
        angle_u_ana = np.angle(ff_u_ana)
        angle_u_mod = np.angle(ff_u_mod)
        angle_v_ana = np.angle(ff_v_ana)
        angle_v_mod = np.angle(ff_v_mod)

        def crop_angle(angle):
            angle[angle <  0]     += np.pi
            angle[angle >= np.pi] -= np.pi

        crop_angle(angle_u_mod)
        crop_angle(angle_v_mod)
        crop_angle(angle_u_ana)
        crop_angle(angle_v_ana)

        with np.errstate(invalid='ignore'):
            err_angle_u = angle_u_mod / angle_u_ana
            err_angle_v = angle_v_mod / angle_v_ana

        plt.figure()
        ax = plt.subplot()
        filename = u"thacker_phase" if model_name is None    \
              else u"thacker_phase_{0}".format(model_name)
        title_ax = u"Phase error at center" if model_name is None   \
              else u"{0} — Phase error at center".format(model_name)

        ax.set_title(title_ax, fontsize=18, family='serif', position=(0.5, 1.05))
        ax.plot(x_freq, err_angle_u,           'r', lw=2, label='$u$')
        ax.plot(x_freq, err_angle_v,           'g', lw=2, label='$v$')
        ax.plot(x_freq, np.ones(x_freq.shape), 'k', lw=3, ls='dashed')
        ax.set_xlabel("Times (hours)", fontsize=12)
        ax.set_ylim(0.8, 1.2)
        ax.legend(fontsize=14)
        log("=== Write {0}.png...".format(filename))
        plt.savefig(filename, dpi=200)

    #============================================================
    # --- plot sea level extremes at three different times
    #============================================================
    def plot_sea_level(index, t1, t2, t3):

        time_1, txt_1 = t1
        time_2, txt_2 = t2
        time_3, txt_3 = t3

        try:
            tidx1 = archive.get_time_index(time_1, "hours")
            tidx2 = archive.get_time_index(time_2, "hours")
            tidx3 = archive.get_time_index(time_3, "hours")
        except:
            log.warning("plot_sea_level: could not find one of these time indices: {0}"
                        .format((time_1, time_2, time_3)))
            return

        ssh_a_1 = ssh_ana[tidx1]
        ssh_a_2 = ssh_ana[tidx2]
        ssh_a_3 = ssh_ana[tidx3]

        ssh_m_1 = ssh[tidx1, jzero]
        ssh_m_2 = ssh[tidx2, jzero]
        ssh_m_3 = ssh[tidx3, jzero]

        fig = plt.figure(figsize=(7,6))
        fig.subplots_adjust(top=0.90, bottom=0.08, left=0.05, right=0.95)
        ax = plt.subplot()
        filename = u"thacker_ssh_zoom_{0}".format(index) if model_name is None    \
              else u"thacker_ssh_zoom_{0}_{1}".format(index, model_name)
        title_ax = u"Sea level evolution" if model_name is None   \
              else u"{0} — Sea level evolution".format(model_name)

        ax.set_title(title_ax, fontsize=20, family='serif', position=(0.5, 1.05))
        ax.plot(x1d_t, ssh_a_1,  'k--', lw=2, label='analytical')
        ax.plot(x1d_t, ssh_m_1,  'k',   lw=1, label='model')
        ax.plot(x1d_t, ssh_a_2,  'r--', lw=2)
        ax.plot(x1d_t, ssh_m_2,  'r',   lw=1)
        ax.plot(x1d_t, ssh_a_3,  'g--', lw=2)
        ax.plot(x1d_t, ssh_m_3,  'g',   lw=1)
        ax.plot(x1d_t, -b_t[-1], 'k',   lw=3)
        ax.text(65, ssh_a_1[165]+0.20, txt_1, color='k', fontsize=14, ha='center');
        ax.text(60, ssh_a_2[160]+0.20, txt_2, color='r',   fontsize=14, ha='center');
        ax.text(55, ssh_a_3[155]+0.20, txt_3, color='g', fontsize=14, ha='center');
        ax.set_xlim(20, 100)
        ax.set_ylim(-3, 3)
        ax.xaxis.grid(color='gray', linestyle=':', which='both')
        ax.yaxis.grid(color='gray', linestyle=':', which='both')
        ax.legend(loc=2, fontsize=14)
        log("=== Write {0}.png...".format(filename))
        plt.savefig(filename, dpi=200)

    if True:

        plot_sea_level( "1", (8.0, "08:00"), (9.5, "09:30"), (11., "11:00") )
        plot_sea_level( "2", (34., "34:00"), (36., "36:00"), (38., "38:00") )
        plot_sea_level( "3", (60., "60:00"), (62., "62:00"), (64., "64:00") )


def main():

    import sys, os
    from optparse import OptionParser
    import logging

    usage = "Usage: %prog NETCDF_FILE\n  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage)

    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")
    parser.add_option("-m", "--model", dest="model", default=None,
                      help="name of the model that produced NETCDF_FILE")
    parser.add_option("-r", "--resolution", dest="resolution", default=None,
                      help=u"grid resolution (∆x) for NETCDF_FILE")
    parser.add_option("-t", "--time", dest="time", type="float", default=72,
                      help=u"Time (in hours) of the last valid index.")
    parser.add_option("-u", "--plot-u", action="store_true", dest="plot_u",
                      help=u"Try to plot baroclinic velocity error")
    parser.add_option("-s", "--ncdir", dest="ncdir", default=".",
                      help=u"directory where NETCDF_FILE is stored (default: '.')")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    if options.debug:
        log.setLevel(logging.DEBUG)

    ncfiles = []
    for ncfile in args:
        ncfiles.append(os.path.join(options.ncdir, ncfile))

    archive = pycomodo.Archive(ncfiles)
    plot_thacker(archive, options.model, options.resolution, options.time, options.plot_u)

if __name__ == "__main__":

    main()
