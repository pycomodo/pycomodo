#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from optparse import OptionParser
from logging import DEBUG

import pycomodo
import pycomodo.util.variables as puv
import pycomodo.operators as op
from pycomodo import log, UnknownVariable

def plot_upwelling(archive, model_name=None, flip=False):

    if model_name is not None:
        out_modelname = model_name.replace(' ', '').replace('.', '-')
        plot_filename = "plot_upwelling_{0}".format(out_modelname)
        diff_filename = "diff_upwelling_{0}".format(out_modelname)
    else:
        plot_filename = "plot_upwelling"
        diff_filename = "diff_upwelling"

    try:
        log("=== Get time :")
        time = archive.get_variable(stdname='time')
        time.check(True)

        log("=== Get SSH :")
        ssh = puv.get_ssh(archive)
        ssh.check(True)

        log("=== Get sea_floor_depth (T)")
        depth_t = puv.get_sea_floor_depth(archive, "t")
        depth_t.check(True)

        log("=== Get sea_floor_depth (U)")
        depth_u = puv.get_sea_floor_depth(archive, "u")
        depth_u.check(True)

        log("=== Get velocities :")
        var_u1 = puv.get_variable_from_stdnames(archive, 'sea_water_x_velocity', 'u')
        var_u1.check(True)

        var_v1 = puv.get_variable_from_stdnames(archive, 'sea_water_y_velocity', 'v')
        var_v1.check(True)

        # Compute cell elevation
        log("=== Create 'zt'")
        var_zt = archive.get_zlevels(location="t", axes="ZYX", name="zt", tpoint=-1)
        var_zt.check(True)

        log("=== Create 'zwu'")
        var_zwu = archive.get_zlevels(location="uw", axes="ZYX", name="zwu", tpoint=-1)
        var_zwu.check(True)

        # Get the cell thickness at U points
        log("=== Create 'dzu'")
        var_dzu = var_zwu.diff_z(dummy=True)
        var_dzu.check(True)

    except UnknownVariable as err:
        log.error("variable {0} not found in '{1}'".format(err, archive))
        return

#     pycomodo.write_netcdf("dzu.nc", (ssh, depth_t, depth_u, var_u1, var_v1, var_zt, var_zwu, var_dzu))

    hoDmax = 2.5    # index of offshore limit h/D, where h is bottom depth and D is Ekman depth
    g      = 9.81   # gravity acceleration (m^2/s)
    rho0   = 1000   # reference density (kg/m^3)
    tauy   = 0.07   # alongshore wind-stress (Pa)
    taux   = 0      # onshore wind-stress (Pa)
    ug     = 0.02   # onshore geostrophic flow (m/s)
    f      = (4*np.pi/86400)*np.sin((-21)*np.pi/180)    # Coriolis parameter (21°S)
    gamma  = f/abs(f)
    kv     = 0.01
    D      = np.pi*np.sqrt(2*kv/np.abs(f))              # Ekman depth
    c      = (1+1j*gamma)*np.pi/D
    cu0    = (1-1j*gamma)*np.pi*np.complex(taux,tauy)/(rho0*np.abs(f)*D);

    if flip:
        xmin, xmax = ssh.get_dynamic_range('Y')
        ymin, ymax = ssh.get_dynamic_range('X')
    else:
        xmin, xmax = ssh.get_dynamic_range('X')
        ymin, ymax = ssh.get_dynamic_range('Y')

    yindex = ymin + (ymax-ymin) / 2

    if flip:
        hD = np.ma.where(depth_t[..., yindex]/D <= hoDmax*1.05)[0]
        x_slice = np.index_exp[..., hD[0]:min(hD[-1], xmax), yindex]
    else:
        hD = np.ma.where(depth_t[..., yindex, :]/D <= hoDmax*1.05)[0]
        x_slice = np.index_exp[..., yindex, hD[0]:min(hD[-1], xmax)]

    xt_slice = np.index_exp[archive.ntimes-1] + x_slice

    # Extract data arrays from variables
    ht = depth_t[x_slice]
    hu = depth_u[x_slice]
    if flip:
        v1 = -var_u1[xt_slice]
        u1 =  var_v1[xt_slice]
    else:
        u1 = var_u1[xt_slice]
        v1 = var_v1[xt_slice]

    zt  = var_zt[x_slice]
    zwu = var_zwu[x_slice]
    dzu = var_dzu[x_slice]

    # Make variables upside down if Z axis is reversed.
    if var_zt.axes["Z"].positive == "down":
        zt  =  -zt[::-1,:]
        zwu = -zwu[::-1,:]
        u1  =   u1[::-1,:]
        v1  =   v1[::-1,:]
        dzu =  dzu[::-1,:]

    mask_t = zt  < -ht
    mask_u = zwu < -hu
    zt[mask_t]  = np.ma.masked
    u1[mask_u]  = np.ma.masked
    v1[mask_u]  = np.ma.masked
    dzu[mask_u] = np.ma.masked
    zwu[mask_u] = np.ma.masked

    # ... compute geostrophic velocities ...
    plot_vgu_m = """
        qsiu=pi*hu/D;
        alphau=(cosh(qsiu).^2).*(cos(qsiu).^2)+(sinh(qsiu).^2).*(sin(qsiu).^2);
        s1u=cosh(qsiu).*cos(qsiu)./alphau;
        t1u=cosh(qsiu).*sinh(qsiu)./alphau;
        s2u=sinh(qsiu).*sin(gamma*qsiu)./alphau;
        t2u=cos(qsiu).*sin(gamma*qsiu)./alphau;
        f1u=sinh(qsiu).*cos(qsiu);
        f2u=cosh(qsiu).*sin(gamma*qsiu);
        vgu=2*pi/(rho0*f*D)*((1-s1u)*tauy+s2u*taux)-ug.*(t1u+gamma*t2u-2*qsiu);
        vgu=vgu./(gamma*t1u-t2u);
    """
    qsiu = np.pi*hu/D
    cqu  = np.cos(qsiu)
    squ  = np.sin(qsiu)
    sgqu = np.sin(gamma*qsiu)
    chqu = np.cosh(qsiu)
    shqu = np.sinh(qsiu)

    alphau = chqu*chqu*cqu*cqu + shqu*shqu*squ*squ
    s1u  = chqu*cqu  / alphau
    s2u  = shqu*sgqu / alphau
    t1u  = chqu*shqu / alphau
    t2u  = cqu*sgqu  / alphau
    f1u  = shqu*cqu
    f2u  = chqu*sgqu
    vgu  = (2*np.pi/(rho0*f*D))*((1-s1u)*tauy+s2u*taux)-ug*(t1u+gamma*t2u-2*qsiu)
    with np.errstate(invalid='ignore'):
        vgu /= gamma*t1u-t2u

    ugu = ug*np.ones_like(vgu)
    cugu = ugu + 1j*vgu

    if flip:
        vg1 = (g/f)*ssh.diff_y()[xt_slice]
    else:
        vg1 = (g/f)*ssh.diff_x()[xt_slice]

    plot_vgr_m = """
        qsir=pi*hr/D;
        alphar=(cosh(qsir).^2).*(cos(qsir).^2)+(sinh(qsir).^2).*(sin(qsir).^2);
        s1r=cosh(qsir).*cos(qsir)./alphar;
        s2r=sinh(qsir).*sin(gamma*qsir)./alphar;
        t1r=cosh(qsir).*sinh(qsir)./alphar;
        t2r=cos(qsir).*sin(gamma*qsir)./alphar;
        vgr=2*pi/(rho0*f*D)*((1-s1r)*tauy+s2r*taux)-ug.*(t1r+gamma*t2r-2*qsir);
        vgr=vgr./(gamma*t1r-t2r);

        wer=zeros(N,L);wer=complex(wer,wer);
        for j=1:L
          wer(:,j)=cu0*sinh(c*(zr(:,j)+hr(j)))/cosh(c*hr(j))- ...
                   cugr(j)*cosh(c*zr(:,j))/cosh(c*hr(j));
        end

        tmp=repmat(vgr,[N 1]);
        v2=imag(wer)+tmp;
    """
    qsit = np.pi*ht/D
    cqt  = np.cos(qsit)
    sqt  = np.sin(qsit)
    sgqt = np.sin(gamma*qsit)
    chqt = np.cosh(qsit)
    shqt = np.sinh(qsit)
    alphat = chqt*chqt*cqt*cqt + shqt*shqt*sqt*sqt
    s1t  = chqt*cqt  / alphat
    s2t  = shqt*sgqt / alphat
    t1t  = chqt*shqt / alphat
    t2t  = cqt*sgqt  / alphat
    vgt  = (2*np.pi/(rho0*f*D))*((1-s1t)*tauy+s2t*taux)-ug*(t1t+gamma*t2t-2*qsit)
    with np.errstate(invalid='ignore'):
        vgt /= gamma*t1t-t2t

    ugt = ug*np.ones_like(vgt)
    cugt = ugt + 1j*vgt

    wet = cu0*np.sinh(c*(zt+ht))/np.cosh(c*ht) - cugt*np.cosh(c*zt)/np.cosh(c*ht)
    v2  = np.tile(vgt, [zt.shape[0], 1]) + wet.imag
    v2[mask_u] = np.ma.masked

    # ... compute model stream function ...
    plot_psi1_m = """
        dzwu=zwu(2:end,:)-zwu(1:end-1,:);            % ---> zru
        psi1=zeros(N+1,L-1);
        for k=1:N
          psi1(k+1,:)=psi1(k,:)-u1(k,:).*dzwu(k,:);
        end
    """
    # Remove masked values for dzu
    dzu[dzu.mask] = 0.

    psi1 = 0*zwu

    if zwu.shape[0] == dzu.shape[0]:                        # For Hycom, NEMO
        for k in reversed(range(psi1.shape[0]-1)):
            psi1[k,:] = psi1[k+1,:] + u1[k+1,:]*dzu[k,:]
    else:                                                   # For MARS, ROMS
        for k in reversed(range(psi1.shape[0]-1)):
            psi1[k,:] = psi1[k+1,:] + u1[k,:]*dzu[k,:]

    # ... compute analytical stream function ...
    plot_psi2_m = """
        psi2=zeros(N+1,L-1);
        psi2=complex(psi2,psi2);
        for j=1:L-1
          psi2(:,j)=cu0*(1-cosh(c*(zwu(:,j)+hu(j)))/cosh(c*hu(j))) ...
                          +cugu(j)*sinh(c*zwu(:,j))/cosh(c*hu(j));
          psi2(:,j)=psi2(:,j)/c-cugu(j)*zwu(:,j);
        end
        psi2=real(psi2);
    """
    psi2 = cu0*(1-np.cosh(c*(zwu+hu))/np.cosh(c*hu)) + cugu*np.sinh(c*zwu)/np.cosh(c*hu)
    psi2 = psi2/c - cugu*zwu
    psi2 = psi2.real

    tmpxu = np.tile(-hu/D,[zwu.shape[0],1])
    Uek   = np.abs(tauy/rho0/f)
    psirange1 = range(-150,0,10)
    psirange2 = range(10,151,10)

    tmpxt = np.tile(-ht/D,[zt.shape[0],1])
    vrange1 = range(-100,0,5)
    vrange2 = range(0,101,5)

    sa_top = 0.98 if model_name is None else 0.935

    fig = plt.figure(figsize=(6,9))
    fig.subplots_adjust(top=sa_top, bottom=0.08, left=0.15, right=0.96, hspace=0.07)

    # ... plot geostrophic velocity ...
    ax1 = plt.subplot(311)
    if model_name is not None:
        ax1.set_title(u"Upwelling — {0}".format(model_name), fontsize=18, family='serif', position=(0.5, 1.05))
    ax1.set_xticks(np.arange(-hoDmax,0.2,0.4), minor=True)
    ax1.set_yticks(np.arange(-0.2,0.2,0.1))
    ax1.set_xticklabels([])
    ax1.set_xlim(-hoDmax, -0.1)
    ax1.set_ylim(-0.18, 0.18)
    ax1.xaxis.set_minor_locator(MultipleLocator(0.25))
    ax1.yaxis.set_minor_locator(MultipleLocator(0.05))
    ax1.xaxis.grid(color='gray', linestyle=':', which='both')
    ax1.yaxis.grid(color='gray', linestyle=':', which='both')
    ax1.plot(-hu/D, vgu, c=[0.7, 0.7, 0.7], lw=4, label="analytical")
    ax1.plot(-hu/D, vg1, 'k', ls='--', lw=2.0, dashes=(4,2), label="numerical")
    ax1.text(-2.2, 0.1, r"$v_G [m/s]$", horizontalalignment='left', fontsize=20, backgroundcolor='w', bbox=dict(lw=1, ec='k'))
    ax1.legend(loc=4, labelspacing=0.5, fontsize='medium')
    handles, labels = ax1.get_legend_handles_labels()

    ax2 = plt.subplot(312)
    ax2.set_xticks(np.arange(-hoDmax,0.2,0.4), minor=True)
    ax2.set_yticks(np.arange(-hoDmax,0.5,0.5), minor=True)
    ax2.set_xticklabels([])
    ax2.set_xlim(-hoDmax, -0.1)
    ax2.set_ylim(-hoDmax, 0.)
    ax2.xaxis.set_minor_locator(MultipleLocator(0.25))
    ax2.yaxis.set_minor_locator(MultipleLocator(0.25))
    ax2.xaxis.grid(color='gray', linestyle=':', which='both')
    ax2.yaxis.grid(color='gray', linestyle=':', which='both')
    ax2.set_ylabel("$z/D$", fontsize=20)
    ca1 = ax2.contour(tmpxu, zwu/D, -100*psi2/Uek, psirange1, colors=([0.7, 0.7, 0.7],), linewidths=4, labels="analytical")
    ca2 = ax2.contour(tmpxu, zwu/D, -100*psi2/Uek, psirange2, colors=([0.7, 0.7, 0.7],), linewidths=4)
    cs1 = ax2.contour(tmpxu, zwu/D, -100*psi1/Uek, psirange1, colors=('k',), linestyles=("--",), linewidths=1.5, labels="numerical")
    cs2 = ax2.contour(tmpxu, zwu/D, -100*psi1/Uek, psirange2, colors=('k',), linestyles=("-",),  linewidths=1.5)
    ax2.plot(-hu/D, -hu/D, c='k')
    ax2.text(-1.0, -1.6, r"$\psi / U_{ek} [\%]$", horizontalalignment='left', fontsize=20, backgroundcolor='w', bbox=dict(lw=1, ec='k'))
    ax2.legend(handles, labels, loc=4, labelspacing=0.5, fontsize='medium')
    ax2.clabel(cs1, fontsize=8, fmt="%.0f")
    ax2.clabel(cs2, fontsize=8, fmt="%.0f")
    for c in cs1.collections:
        c.set_dashes([(0, (6.0, 3.0))])

    ax3 = plt.subplot(313)
    ax3.set_xticks(np.arange(-hoDmax,0.2,0.4), minor=True)
    ax3.set_yticks(np.arange(-hoDmax,0.5,0.5), minor=True)
    ax3.xaxis.set_minor_locator(MultipleLocator(0.25))
    ax3.yaxis.set_minor_locator(MultipleLocator(0.25))
    ax3.set_xlim(-hoDmax, -0.1)
    ax3.set_ylim(-hoDmax, 0.)
    ax3.xaxis.grid(color='gray', linestyle=':', which='both')
    ax3.yaxis.grid(color='gray', linestyle=':', which='both')
    ax3.set_xlabel("$h/D$", fontsize=20)
    ax3.set_ylabel("$z/D$", fontsize=20)
    ca3 = ax3.contour(tmpxt, zt/D, 100*v2, vrange1, colors=([0.7, 0.7, 0.7],), linewidths=4, labels="analytical")
    ca4 = ax3.contour(tmpxt, zt/D, 100*v2, vrange2, colors=([0.7, 0.7, 0.7],), linewidths=4)
    cs3 = ax3.contour(tmpxt, zt/D, 100*v1, vrange1, colors=('k',), linestyles=("--",), linewidths=1.5, labels="numerical")
    cs4 = ax3.contour(tmpxt, zt/D, 100*v1, vrange2, colors=('k',), linestyles=("-",),  linewidths=1.5)
    ax3.plot(-ht/D, -ht/D, c='k')
    ax3.text(-1.0, -1.6, r"$v [cm/s]$", horizontalalignment='left', fontsize=20, backgroundcolor='w', bbox=dict(lw=1, ec='k'))
    ax3.legend(handles, labels, loc=4, labelspacing=0.5, fontsize='medium')
    ax3.clabel(cs3, fontsize=8, fmt="%.0f")
    ax3.clabel(cs4, fontsize=8, fmt="%.0f")
    for c in cs3.collections:
        c.set_dashes([(0, (6.0, 3.0))])

    for axx in (ax1, ax2, ax3):
        for axis in (axx.xaxis, axx.yaxis):
            axis.get_offset_text().set_fontsize(10)
            for xtl in axis.get_ticklabels():
                xtl.set_fontsize(10)

    log("=== Write {0}.png...".format(plot_filename))
    plt.savefig(plot_filename, dpi=200)

    fig = plt.figure(figsize=(6,9))
    fig.subplots_adjust(top=sa_top, bottom=0.08, left=0.15, right=0.96, hspace=0.07)

    # ... plot diff of geostrophic velocity ...
    vgdiff = vg1-vgu
    vgmax = max(abs(vgdiff.min()), abs(vgdiff.max()))
    ax1 = plt.subplot(311)
    if model_name is not None:
        ax1.set_title(u"Upwelling — {0} (error)".format(model_name), fontsize=18, family='serif', position=(0.5, 1.05))
    ax1.set_xticks(np.arange(-hoDmax,0.2,0.4), minor=True)
    ax1.set_yticks(np.arange(-vgmax,0.2,vgmax), minor=True)
    ax1.set_xticklabels([])
    ax1.set_xlim(-hoDmax, -0.1)
    ax1.set_ylim(-vgmax, vgmax)
    ax1.xaxis.set_minor_locator(MultipleLocator(0.25))
    ax1.yaxis.set_minor_locator(MultipleLocator(0.05))
    ax1.yaxis.get_major_formatter().set_powerlimits((0,0))
    ax1.xaxis.grid(color='gray', linestyle=':', which='both')
    ax1.yaxis.grid(color='gray', linestyle=':', which='both')
    ax1.plot(-hu/D, vgdiff, 'k', ls='-', lw=1.5, label="difference")
    ax1.text(-2.2, 0.6*vgmax, r"$v_G [m/s]$", horizontalalignment='left', fontsize=20, backgroundcolor='w', bbox=dict(lw=1, ec='k'))
    ax1.legend(loc=4, labelspacing=0.5, fontsize='medium')
    handles, labels = ax1.get_legend_handles_labels()

    ax2 = plt.subplot(312)
    ax2.set_xticks(np.arange(-hoDmax,0.2,0.4), minor=True)
    ax2.set_yticks(np.arange(-hoDmax,0.5,0.5), minor=True)
    ax2.set_xticklabels([])
    ax2.set_xlim(-hoDmax, -0.1)
    ax2.set_ylim(-hoDmax, 0.)
    ax2.xaxis.set_minor_locator(MultipleLocator(0.25))
    ax2.yaxis.set_minor_locator(MultipleLocator(0.25))
    ax2.xaxis.grid(color='gray', linestyle=':', which='both')
    ax2.yaxis.grid(color='gray', linestyle=':', which='both')
    ax2.set_ylabel("$z/D$", fontsize=20)
    psidiff = 100*(psi1-psi2)/Uek
    diffmin = min(np.ceil(abs(psidiff.min())), 10)
    diffmax = min(np.ceil(abs(psidiff.max())), 10)
    diffmax = max(diffmin, diffmax)
    cs2 = ax2.pcolor(tmpxu, zwu/D, psidiff, cmap="RdBu", vmin=-diffmax, vmax=diffmax)
    ax2ins = inset_axes(ax2, width="40%",  height="8%", loc=4, borderpad=0.8)
    cbar = plt.colorbar(cs2, cax=ax2ins, ticks=MultipleLocator(diffmax), orientation='horizontal')
    cbar.ax.axis["top"].toggle(all=True)
    cbar.ax.axis["bottom"].toggle(all=False)
    ax2.plot(-hu/D, -hu/D, c='k')
    ax2.text(-1.0, -1.6, r"$\psi / U_{ek} [\%]$", horizontalalignment='left', fontsize=20, backgroundcolor='w', bbox=dict(lw=1, ec='k'))

    ax3 = plt.subplot(313)
    ax3.set_xticks(np.arange(-hoDmax,0.2,0.4), minor=True)
    ax3.set_yticks(np.arange(-hoDmax,0.5,0.5), minor=True)
    ax3.xaxis.set_minor_locator(MultipleLocator(0.25))
    ax3.yaxis.set_minor_locator(MultipleLocator(0.25))
    ax3.set_xlim(-hoDmax, -0.1)
    ax3.set_ylim(-hoDmax, 0.)
    ax3.xaxis.grid(color='gray', linestyle=':', which='both')
    ax3.yaxis.grid(color='gray', linestyle=':', which='both')
    ax3.set_xlabel("$h/D$", fontsize=20)
    ax3.set_ylabel("$z/D$", fontsize=20)
    vdiff = 100*(v1-v2)
    vmax = np.ceil(max(abs(vdiff.min()), abs(vdiff.max())))
    cs3 = ax3.pcolor(tmpxt, zt/D, vdiff, cmap="RdBu", vmin=-vmax, vmax=vmax)
    ax3ins = inset_axes(ax3, width="40%",  height="8%", loc=4, borderpad=0.8)
    cbar = plt.colorbar(cs3, cax=ax3ins, ticks=MultipleLocator(vmax), orientation='horizontal')
    cbar.ax.axis["top"].toggle(all=True)
    cbar.ax.axis["bottom"].toggle(all=False)
    ax3.plot(-ht/D, -ht/D, c='k')
    ax3.text(-1.0, -1.6, r"$v [cm/s]$", horizontalalignment='left', fontsize=20, backgroundcolor='w', bbox=dict(lw=1, ec='k'))

    # Change fontsize for all axis labels
    for axx in (ax1, ax2, ax3):
        for axis in (axx.xaxis, axx.yaxis):
            axis.get_offset_text().set_fontsize(10)
            for xtl in axis.get_ticklabels():
                xtl.set_fontsize(10)

    log("=== Write {0}.png...".format(diff_filename))
    plt.savefig(diff_filename, dpi=200)

    err_vg  = np.sqrt(np.mean(vgdiff**2))
    err_v   = np.sqrt(np.mean(vdiff**2))
    err_psi = np.sqrt(np.mean(psidiff**2))

    log("RMSE(vg)  = {0:.2e} (cm/s)".format(100*err_vg))
    log("RMSE(v)   = {0:.2e} (cm/s)".format(err_v))
    log("RMSE(psi) = {0:.2e} (%)".format(err_psi))

if __name__ == "__main__":

    usage = "Usage: %prog NETCDF_FILE\n  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage)

    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")
    parser.add_option("-m", "--model", dest="model", default=None,
                      help="name of the model that produced NETCDF_FILE")
    parser.add_option("-s", "--ncdir", dest="ncdir", default=".",
                      help=u"directory where NETCDF_FILE is stored (default: '.')")
    parser.add_option("-f", "--flip", action="store_true", dest="flip", default=False,
                      help=u"Flip dimensions X and Y.")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    if options.debug:
        log.setLevel(DEBUG)

    ncfiles = []
    for ncfile in args:
        ncfiles.append(os.path.join(options.ncdir, ncfile))

    archive = pycomodo.Archive(ncfiles)
    plot_upwelling(archive, options.model, options.flip)
