Diagnostiques pour le cas-test "upwelling"
------------------------------------------

Ce dossier contient des scripts Python permettant de produire des diagnostiques pour le
cas-test "upwelling".

Au préalable, il est nécessaire d'avoir une installation fonctionnelle de la librairie pycomodo.
Les instructions sont détaillées sur cette page :

http://pycomodo.forge.imag.fr/api/installation.html

Pour produire la figure de référence :

$ ./plot_upwelling.py ncfile.nc -m MODELNAME
