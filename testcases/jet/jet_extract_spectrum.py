#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import logging

import numpy as np
import cPickle as pk

import numpy.fft as fft
from optparse import OptionParser

import pycomodo
from pycomodo import log
from pycomodo import write_netcdf

from utilities import tukeywin, compute_dk
from utilities import u2t_xper, v2t_xper
from utilities import horizontal_advection_C4
from utilities import integ_fft2d_new as integ_fft2d

# Global variables
#------------------
CENTERED_ADVECTION = False
DEBUG = False
MODEL = "NONE"   # Set this variable to "ROMS", "MARS" or "NEMO"
                 # to handle specific, non-standard behaviors.

cst_g    = 9.81
cst_rho0 = 1024

STDNAMES_ROMS = {
    'dudt':    'time_rate_of_change_term_u_component',
    'dvdt':    'time_rate_of_change_term_v_component',
    'hadv_u':  'UNDEFINED',
    'hadv_v':  'UNDEFINED',
    'hadv_ux': 'horizontal_xi_advection_term_u_component',
    'hadv_vx': 'horizontal_xi_advection_term_v_component',
    'hadv_uy': 'horizontal_eta_advection_term_u_component',
    'hadv_vy': 'horizontal_eta_advection_term_v_component',
    'zadv_u':  'vertical_advection_term_u_component',
    'zadv_v':  'vertical_advection_term_v_component',
    'ediff_u': 'horizontal_mixing_term_u_component',
    'ediff_v': 'horizontal_mixing_term_v_component',
    'vmix_u':  'vertical_mixing_term_u_component',
    'vmix_v':  'vertical_mixing_term_v_component',
    'dxp':     'pressure_gradient_term_u_component',
    'dyp':     'pressure_gradient_term_v_component',
    'edxp':    'UNDEFINED',
    'edyp':    'UNDEFINED',
    'idxp':    'UNDEFINED',
    'idyp':    'UNDEFINED',
    'corio_u': 'coriolis_term_in_momentum_u_component',
    'corio_v': 'coriolis_term_in_momentum_v_component'
}
STDNAMES_NEMO = {
    'dudt':    'UNDEFINED',
    'dvdt':    'UNDEFINED',
    'hadv_u':  'sea_water_x_trend_had',
    'hadv_v':  'sea_water_y_trend_had',
    'hadv_ux': 'UNDEFINED',
    'hadv_vx': 'UNDEFINED',
    'hadv_uy': 'UNDEFINED',
    'hadv_vy': 'UNDEFINED',
    'zadv_u':  'sea_water_x_trend_zad',
    'zadv_v':  'sea_water_y_trend_zad',
    'ediff_u': 'sea_water_x_trend_ldf',
    'ediff_v': 'sea_water_y_trend_ldf',
    'vmix_u':  'sea_water_x_trend_zdf',
    'vmix_v':  'sea_water_y_trend_zdf',
    'dxp':     'UNDEFINED',
    'dyp':     'UNDEFINED',
    'edxp':    'sea_water_x_trend_spg',
    'edyp':    'sea_water_y_trend_spg',
    'idxp':    'sea_water_x_trend_hpg',
    'idyp':    'sea_water_y_trend_hpg',
    'corio_u': 'UNDEFINED',
    'corio_v': 'UNDEFINED'
}
STDNAMES_COMODO = {
    'dudt':    'tendency_of_sea_water_x_velocity_at_u_location',
    'dvdt':    'tendency_of_sea_water_y_velocity_at_v_location',
    'hadv_u':  'non_linear_advection_of_sea_water_x_velocity_at_u_location',
    'hadv_v':  'non_linear_advection_of_sea_water_y_velocity_at_v_location',
    'hadv_ux': 'x_non_linear_advection_of_sea_water_x_velocity_at_u_location',
    'hadv_vx': 'x_non_linear_advection_of_sea_water_y_velocity_at_v_location',
    'hadv_uy': 'y_non_linear_advection_of_sea_water_x_velocity_at_u_location',
    'hadv_vy': 'y_non_linear_advection_of_sea_water_y_velocity_at_v_location',
    'zadv_u':  'upward_non_linear_advection_of_sea_water_x_velocity_at_u_location',
    'zadv_v':  'upward_non_linear_advection_of_sea_water_y_velocity_at_v_location',
    'ediff_u': 'horizontal_diffusion_of_sea_water_x_velocity_at_u_location',
    'ediff_v': 'horizontal_diffusion_of_sea_water_y_velocity_at_v_location',
    'vmix_u':  'upward_diffusion_of_sea_water_x_velocity_at_u_location',
    'vmix_v':  'upward_diffusion_of_sea_water_y_velocity_at_v_location',
    'dxp':     'x_pressure_gradient_at_u_location',
    'dyp':     'y_pressure_gradient_at_v_location',
    'edxp':    'x_external_pressure_gradient_at_u_location',
    'edyp':    'y_external_pressure_gradient_at_v_location',
    'idxp':    'x_internal_pressure_gradient_at_u_location',
    'idyp':    'y_internal_pressure_gradient_at_v_location',
    'corio_u': 'x_coriolis_acceleration_at_u_location',
    'corio_v': 'y_coriolis_acceleration_at_v_location'
}

STDNAMES = STDNAMES_COMODO
SIGNED_TERMS = False

if MODEL == "ROMS":
    STDNAMES = STDNAMES_ROMS

if MODEL == "MARS":
    SIGNED_TERMS = True

if MODEL == "NEMO":
    STDNAMES = STDNAMES_NEMO


def get_basename(resolution, model, depth, tlength):
    """
    Returns a file name for a given set of resolution, model name, and integration depth (z) and length (t).
    """
    if tlength == 0:
        return "{0}K_{1}_z-{2}m_days-all".format(resolution, model, int(depth))
    else:
        return "{0}K_{1}_z-{2}m_days-{3:03}".format(resolution, model, int(depth), tlength)

def compute_budget( archive, model, resolution, depth=200., tlength=0, mmean=False, window=False ):
    """
    Setting main parameters of for the calculation:
     * if mean == True, we remove the mean of each variable and use the fluctuation instead u'=u-ubar
     * if window == True, we use windowing (useful for non-periodic solution)
    """

    data_filename = "data_{0}.pck".format(get_basename(resolution, model, depth, tlength))
    advection_filename = "advection_{0}.pck".format(get_basename(resolution, model, depth, tlength))
    dx = float(resolution)*1000
    dy = float(resolution)*1000

    ntimes = archive.ntimes

    if tlength == 0:
        tlength = ntimes

    log("== Look for state variables...")
    var_u   = archive.get_variable(stdname='sea_water_x_velocity_at_u_location')
    var_v   = archive.get_variable(stdname='sea_water_y_velocity_at_v_location')
    var_wt  = archive.get_variable(stdname='upward_sea_water_velocity', nasty=False)
    var_ww  = archive.get_variable(stdname='upward_sea_water_velocity_at_w_location', nasty=False)
    var_b   = archive.get_variable(stdname='sea_water_buoyancy', nasty=False)
    var_rho = archive.get_variable(stdname='sea_water_potential_density', nasty=False)

    if var_rho is None:
        var_rho  = archive.get_variable(stdname='sea_water_sigma_theta', nasty=False)
    var_temp = archive.get_variable(stdname='sea_water_potential_temperature', nasty=False)

    if var_temp is None:        # Bug in ROMS standard_name attribute.
        var_temp = archive.get_variable(stdname='sea_water_temperature', nasty=False)
    var_salt = archive.get_variable(stdname='sea_water_salinity', nasty=False)

    log("== Look for budget variables...")
    var_dudt    = archive.get_variable(stdname=STDNAMES['dudt'], nasty=False)        # tendency : du/dt
    var_dvdt    = archive.get_variable(stdname=STDNAMES['dvdt'], nasty=False)        # tendency : dv/dt
    var_hadv_ux = archive.get_variable(stdname=STDNAMES['hadv_ux'], nasty=False)     # horiz. advection : u.du/dx
    var_hadv_vx = archive.get_variable(stdname=STDNAMES['hadv_vx'], nasty=False)     # horiz. advection : u.dv/dx
    var_hadv_uy = archive.get_variable(stdname=STDNAMES['hadv_uy'], nasty=False)     # horiz. advection : v.du/dy
    var_hadv_vy = archive.get_variable(stdname=STDNAMES['hadv_vy'], nasty=False)     # horiz. advection : v.dv/dy
    var_hadv_u  = archive.get_variable(stdname=STDNAMES['hadv_u'], nasty=False)      # horiz. advection : u.du/dx+v.du/dy
    var_hadv_v  = archive.get_variable(stdname=STDNAMES['hadv_v'], nasty=False)      # horiz. advection : u.dv/dx+v.dv/dy
    var_zadv_u  = archive.get_variable(stdname=STDNAMES['zadv_u'], nasty=False)      # vert. advection : w.du/dz
    var_zadv_v  = archive.get_variable(stdname=STDNAMES['zadv_v'], nasty=False)      # vert. advection : w.dv/dz
    var_ediff_u = archive.get_variable(stdname=STDNAMES['ediff_u'], nasty=False)     # Expl. horiz. diffusion : ∇_h·(Kh ∇_h·u)
    var_ediff_v = archive.get_variable(stdname=STDNAMES['ediff_v'], nasty=False)     # Expl. horiz. diffusion : ∇_h·(Kh ∇_h·v)
    var_corio_u = archive.get_variable(stdname=STDNAMES['corio_u'], nasty=False)     # Coriolis : -fv
    var_corio_v = archive.get_variable(stdname=STDNAMES['corio_v'], nasty=False)     # Coriolis :  fu
    var_vmix_u  = archive.get_variable(stdname=STDNAMES['vmix_u'], nasty=False)      # vert. diffusion : (d/dz)(Kv.du/dz)
    var_vmix_v  = archive.get_variable(stdname=STDNAMES['vmix_v'], nasty=False)      # vert. diffusion : (d/dz)(Kv.dv/dz)
    var_dxp     = archive.get_variable(stdname=STDNAMES['dxp'], nasty=False)         # Pres. gradient : (1/rho0)*dP/dx
    var_dyp     = archive.get_variable(stdname=STDNAMES['dyp'], nasty=False)         # Pres. gradient : (1/rho0)*dP/dy
    var_edxp    = archive.get_variable(stdname=STDNAMES['edxp'], nasty=False)        # External pres. gradient
    var_edyp    = archive.get_variable(stdname=STDNAMES['edyp'], nasty=False)        # External pres. gradient
    var_idxp    = archive.get_variable(stdname=STDNAMES['idxp'], nasty=False)        # Internal pres. gradient
    var_idyp    = archive.get_variable(stdname=STDNAMES['idyp'], nasty=False)        # Internal pres. gradient

    if (var_b, var_rho, var_temp) == (None, None, None):
        log.error("Unable to compute buoyancy. Please provide at least a potential temperature !")
        sys.exit(1)

    compute_b   = True
    compute_rho = False

    if var_b is not None:
        compute_b = False
        var_t     = var_b
    elif var_rho is not None:
        log.debug("We take variable '{0}' ('{1}') for potential density."
                    .format(var_rho, var_rho.standard_name))
        var_t = var_rho
    else:
        log.warning("We take temperature variable '{0}' ('{1}') instead of potential density."
                        .format(var_temp, var_temp.standard_name))
        compute_rho = True
        var_t       = var_temp

    # Check the terms that have been found or not...
    process_tend      = None not in (var_dudt, var_dvdt)
    process_hadv_xy   = None not in (var_hadv_ux, var_hadv_uy, var_hadv_vx, var_hadv_vy)
    process_hadv      = None not in (var_hadv_u,  var_hadv_v)
    process_zadv      = None not in (var_zadv_u,  var_zadv_v)
    process_pgrd      = None not in (var_dxp, var_dyp)
    process_epgrd     = None not in (var_edxp, var_edyp)
    process_ipgrd     = None not in (var_idxp, var_idyp)
    process_ediff     = None not in (var_ediff_u, var_ediff_v)
    process_coriolis  = None not in (var_corio_u, var_corio_v)
    process_vmix      = None not in (var_vmix_u, var_vmix_v)
    interpolate_w     = var_wt is None and var_ww is not None
    process_wb        = var_wt is not None or interpolate_w
    compute_pgrd      = (not process_pgrd) and (process_epgrd and process_ipgrd)
    process_residuals = process_tend and (process_hadv or process_hadv_xy) and (process_pgrd or compute_pgrd)  \
                        and process_coriolis and process_vmix and process_zadv and process_wb

    # ...and warn about the result.
    if not process_tend:
        log.warning("Impossible to process velocity tendency terms 'du/dt'... They will be null.")

    if not ( process_hadv_xy or process_hadv ):
        log.warning("Impossible to process horizontal advection terms 'u.du/dx'... They will be null.")

    if not ( process_pgrd or compute_pgrd ):
        log.warning("Impossible to process pressure gradient terms 'dp/dx'... They will be null.")

    if not process_coriolis:
        log.warning("Impossible to process Coriolis terms... They will be null.")

    if not process_vmix:
        log.warning("Impossible to process vertical mixing terms 'd[Kv.(du/dz)]/dz'... They will be null.")

    if not process_zadv:
        log.warning("Impossible to process vertical advection terms 'w.du/dz'... They will be null.")

    if not process_wb:
        log.warning("Impossible to process upward velocity term 'w'... It will be null.")

    if not process_ediff:
        log.warning("Impossible to process explicit dissipation terms... They will be null.")

    if not process_residuals:
        log.warning("Impossible to process residual terms... They will be null.")

    log("== Compute time bounds...")
    it1 = ntimes-tlength
    it2 = ntimes
    log(" - ntimes = {0} ({1}...{2})".format(tlength, it1, it2))

    log("== Compute horizontal bounds...")
    sh_t = var_t.get_dynamic_slice("Y", "X")
    sh_u = var_u.get_dynamic_slice("Y", "X")
    sh_v = var_v.get_dynamic_slice("Y", "X")

    log.debug("dynamic_slice t: {0}".format(sh_t))
    log.debug("dynamic_slice u: {0}".format(sh_u))
    log.debug("dynamic_slice v: {0}".format(sh_v))

    ist, iet = var_t.get_dynamic_range("X")
    jst, jet = var_t.get_dynamic_range("Y")
    isu, ieu = var_u.get_dynamic_range("X")
    jsu, jeu = var_u.get_dynamic_range("Y")
    isv, iev = var_v.get_dynamic_range("X")
    jsv, jev = var_v.get_dynamic_range("Y")

    Ni = iet - ist + 1
    Nj = jet - jst + 1
    Ni_u = ieu - isu + 1
    Nj_u = jeu - jsu + 1
    Ni_v = iev - isv + 1
    Nj_v = jev - jsv + 1
    log(" - Nj_t, Ni_t = {0}".format((Nj, Ni)))
    log(" - Nj_u, Ni_u = {0}".format((Nj_u, Ni_u)))
    log(" - Nj_v, Ni_v = {0}".format((Nj_v, Ni_v)))

    log("== Get vertical levels...")
    var_zw = archive.get_zlevels("w", "Z", name="zw", hpoint=(Nj/2,Ni/2))   # for 'w' points
    var_zt = archive.get_zlevels("t", "Z", name="zt", hpoint=(Nj/2,Ni/2))   # for 't' points
    log.debug(var_zw.info())
    log.debug(var_zt.info())

    if DEBUG:
        log("== Write 'zt.nc'...")
        write_netcdf("zt.nc", (var_zw, var_zt))

    log("== Compute indices...")
    positive = var_zw.axes["Z"].positive

    if positive == "down":
        kmin   = 0
        kmax_t = np.argmin(np.abs(-var_zt[:]+depth))
    elif positive == "up":
        kmin   = np.argmin(np.abs(var_zt[:]+depth))
        kmax_t = var_zt.shape[0]-1
    else:
        log.error("Malformed z axis.")
        sys.exit(1)

    kmax_w = kmax_t + 1
    Nk     = kmax_w - kmin
    dz     = var_zw[kmin+1:kmax_w+1] - var_zw[kmin:kmax_w]
    norm_factor = 1./( (Nj*Ni)**2 * dz.sum() * tlength )

    log(" - kmin   = {0:3} -> zt[kmin] = {1:7.2f}".format(kmin, var_zt[kmin]))
    log(  "                -> zw[kmin] = {1:7.2f}".format(kmin, var_zw[kmin]))
    log(" - kmax_t = {0:3} -> zt[kmax] = {1:7.2f}".format(kmax_t, var_zt[kmax_t]))
    log(" - kmax_w = {0:3} -> zw[kmax] = {1:7.2f}".format(kmax_w, var_zw[kmax_w]))
    log(" - nlevels = {0}".format(Nk))
    log(" - sum(dz) = {0}".format(dz.sum()))
    log(" - norm_factor = {0}".format(norm_factor))

    log.debug("zw: {0:.2f} {1:.2f} ... {2:.2f} {3:.2f}".format(var_zw[kmin], var_zw[kmin+1], var_zw[kmax_w-1], var_zw[kmax_w]))
    log.debug("zt: {0:.2f} {1:.2f} ... {2:.2f} {3:.2f}".format(var_zt[kmin], var_zt[kmin+1], var_zt[kmax_t-1], var_zt[kmax_t]))

    ################################################################
    #
    if window:
        cff_tukey = 0.25
        wdw_j = tukeywin(Nj, -1.0)
        wdw_i = tukeywin(Ni, cff_tukey)
        wdw_ii, wdw_jj = np.meshgrid(wdw_j, wdw_i, sparse=True)
        wdw = wdw_ii * wdw_jj

    if DEBUG and CENTERED_ADVECTION:
        C4_hadvection_u  = np.zeros((tlength, Nk, Nj, Ni))
        C4_hadvection_v  = np.zeros((tlength, Nk, Nj, Ni))
        UP3_hadvection_u = np.zeros((tlength, Nk, Nj, Ni))
        UP3_hadvection_v = np.zeros((tlength, Nk, Nj, Ni))

    u        = np.zeros((Nk, Nj, Ni))
    v        = np.zeros((Nk, Nj, Ni))
    dudt     = np.zeros((Nk, Nj, Ni))
    dvdt     = np.zeros((Nk, Nj, Ni))
    hadv_u   = np.zeros((Nk, Nj, Ni))
    hadv_v   = np.zeros((Nk, Nj, Ni))
    hadv_u_c = np.zeros((Nk, Nj, Ni))
    hadv_v_c = np.zeros((Nk, Nj, Ni))
    zadv_u   = np.zeros((Nk, Nj, Ni))
    zadv_v   = np.zeros((Nk, Nj, Ni))
    dxp      = np.zeros((Nk, Nj, Ni))
    dyp      = np.zeros((Nk, Nj, Ni))
    corio_u  = np.zeros((Nk, Nj, Ni))
    corio_v  = np.zeros((Nk, Nj, Ni))
    vmix_u   = np.zeros((Nk, Nj, Ni))
    vmix_v   = np.zeros((Nk, Nj, Ni))
    resu     = np.zeros((Nk, Nj, Ni))
    resv     = np.zeros((Nk, Nj, Ni))
    ediff_u  = np.zeros((Nk, Nj, Ni))
    ediff_v  = np.zeros((Nk, Nj, Ni))

    # Initialize 2D spectra
    spec2d = {}
    for term in ( "ke",         # kinetic energy
                  "tend",       # time rate of change
                  "hadv_c",     # C4  horiz advection
                  "hadv",       # UP3 horiz advection
                  "zadv",       # vertical advection
                  "idiff",      # implicit horiz. diffusion
                  "ediff",      # explicit horiz. diffusion
                  "pgrd",       # pressure gradient
                  "vmix",       # vertical dissipation
                  "resi",       # residual
                  "wb",         # baroclinic conversion wb
                  "cori",       # coriolis
                ):
        spec2d[term] = np.zeros((Nj, Ni))

    #
    #  TEMPORAL LOOP ================================
    #
    log("== TEMPORAL LOOP...")
    for it in np.arange(it1, it2):

        it0 = it - it1
        time_days = archive.get_time(it, 'days')
        log(" - process rec #{0} (day {1})".format(it, time_days))

        slice_t = np.index_exp[it, kmin:kmax_t+1] + sh_t
        slice_u = np.index_exp[it, kmin:kmax_t+1] + sh_u
        slice_v = np.index_exp[it, kmin:kmax_t+1] + sh_v
        slice_w = np.index_exp[it, kmin:kmax_w+1] + sh_t

        ## Velocity
        ##
        _u = var_u[slice_u]
        _v = var_v[slice_v]

        u2t_xper(_u, u)
        v2t_xper(_v, v)

        #### UPSTREAM BIASED NL TERMS ####
        #### computed as if in the rhs (- sign already included).

        if   process_hadv:
            _hadv_u = var_hadv_u[slice_u]
            _hadv_v = var_hadv_v[slice_v]
            u2t_xper(_hadv_u, hadv_u)
            v2t_xper(_hadv_v, hadv_v)

        elif process_hadv_xy:
            _hadv_u = var_hadv_ux[slice_u] + var_hadv_uy[slice_u]
            _hadv_v = var_hadv_vx[slice_v] + var_hadv_vy[slice_v]
            u2t_xper(_hadv_u, hadv_u)
            v2t_xper(_hadv_v, hadv_v)

        if CENTERED_ADVECTION:

            _hadv_u_c, _hadv_v_c = horizontal_advection_C4(_u, _v, dx, dy, dz)

            u2t_xper(_hadv_u_c, hadv_u_c)
            v2t_xper(_hadv_v_c, hadv_v_c)

            if DEBUG:
                UP3_hadvection_u[it0] = hadv_u
                UP3_hadvection_v[it0] = hadv_v
                C4_hadvection_u[it0] = hadv_u_c
                C4_hadvection_v[it0] = hadv_v_c
        else:
            hadv_u_c = hadv_u
            hadv_v_c = hadv_v

        ## implicit dissipation
        ## computed as in the rhs
        ##
        idiss_u = hadv_u_c - hadv_u
        idiss_v = hadv_v_c - hadv_v

        ## explicit dissipation
        ## computed as if in the rhs (- sign already included).
        ##
        if process_ediff:
            _ediff_u = var_ediff_u[slice_u]
            _ediff_v = var_ediff_v[slice_v]
            u2t_xper(_ediff_u, ediff_u)
            v2t_xper(_ediff_v, ediff_v)

        ## tendency term
        ##
        if process_tend:
            _dudt = var_dudt[slice_u]
            _dvdt = var_dvdt[slice_v]

            u2t_xper(_dudt, dudt)
            v2t_xper(_dvdt, dvdt)

        ## wb term
        ##
        if process_wb:

            if interpolate_w:
                _w = var_ww[slice_w]
                w  = 0.5*(_w[:-1]+_w[1:])
            else:
                w  = var_wt[slice_t]

            if compute_b:
                if compute_rho:
                    rho = var_temp[slice_t]     # FIXME: you should compute rho from var_temp and var_salt
                else:
                    rho = var_rho[slice_t]
                b = -(cst_g/cst_rho0)*rho       # conversion from density to buoyancy
            else:
                b = var_b[slice_t]

        # Pressure term
        #
        if process_pgrd:
            dxp_u = var_dxp[slice_u]
            dyp_v = var_dyp[slice_v]

            u2t_xper(dxp_u, dxp)
            v2t_xper(dyp_v, dyp)

        elif compute_pgrd:
            dxp_u = var_edxp[slice_u] + var_idxp[slice_u]
            dyp_v = var_edyp[slice_v] + var_idyp[slice_v]

            u2t_xper(dxp_u, dxp)
            v2t_xper(dyp_v, dyp)

        # Coriolis
        #
        if process_coriolis:
            _corio_u = var_corio_u[slice_u]
            _corio_v = var_corio_v[slice_v]

            u2t_xper(_corio_u, corio_u)
            v2t_xper(_corio_v, corio_v)

        # Vertical dissipation
        #
        if process_vmix:
            _vmix_u = var_vmix_u[slice_u]
            _vmix_v = var_vmix_v[slice_v]

            u2t_xper(_vmix_u, vmix_u)
            v2t_xper(_vmix_v, vmix_v)

        # Vertical advection
        #
        if process_zadv:
            _zadv_u = var_zadv_u[slice_u]
            _zadv_v = var_zadv_v[slice_v]

            u2t_xper(_zadv_u, zadv_u)
            v2t_xper(_zadv_v, zadv_v)

        # Residuals
        #
        if process_residuals:

            _resu = var_dudt[slice_u]    -   \
                    var_vmix_u[slice_u]  -   \
                    var_ediff_u[slice_u]

            _resv = var_dvdt[slice_v]    -   \
                    var_vmix_v[slice_v]  -   \
                    var_ediff_v[slice_v]

            if process_pgrd:
                _resu -= var_dxp[slice_u]
                _resv -= var_dyp[slice_v]
            elif compute_pgrd:
                _resu -= var_edxp[slice_u] + var_idxp[slice_u]
                _resv -= var_edyp[slice_v] + var_idyp[slice_v]

            _signed_terms_u = var_corio_u[slice_u] + var_zadv_u[slice_u]
            _signed_terms_v = var_corio_v[slice_v] + var_zadv_v[slice_v]

            if   process_hadv:
                _signed_terms_u += var_hadv_u[slice_u]
                _signed_terms_v += var_hadv_v[slice_v]
            elif process_hadv_xy:
                _signed_terms_u += var_hadv_ux[slice_u] + var_hadv_uy[slice_u]
                _signed_terms_v += var_hadv_vx[slice_v] + var_hadv_vy[slice_v]

            if SIGNED_TERMS:
                _resu += _signed_terms_u
                _resv += _signed_terms_v
            else:
                _resu -= _signed_terms_u
                _resv -= _signed_terms_v

            u2t_xper(_resu, resu)
            v2t_xper(_resv, resv)

        #
        #  VERTICAL LOOP ================================
        #
        for k in range(Nk):

            w[k] -= w[k].mean()
            b[k] -= b[k].mean()

            if mmean:
                # b and w already done
                u[k]        -= u[k].mean()
                v[k]        -= v[k].mean()
                dudt[k]     -= dudt[k].mean()
                dvdt[k]     -= dvdt[k].mean()
                hadv_u_c[k] -= hadv_u_c[k].mean()
                hadv_v_c[k] -= hadv_v_c[k].mean()
                hadv_u[k]   -= hadv_u[k].mean()
                hadv_v[k]   -= hadv_v[k].mean()
                zadv_u[k]   -= zadv_u[k].mean()
                zadv_v[k]   -= zadv_v[k].mean()
                vmix_u[k]   -= vmix_u[k].mean()
                vmix_v[k]   -= vmix_v[k].mean()
                ediff_u[k]  -= ediff_u[k].mean()
                ediff_v[k]  -= ediff_v[k].mean()
                corio_u[k]  -= corio_u[k].mean()
                corio_v[k]  -= corio_v[k].mean()
                dxp[k]      -= dxp[k].mean()
                dyp[k]      -= dyp[k].mean()
                resu[k]     -= resu[k].mean()
                resv[k]     -= resv[k].mean()

            if window:
                u[k]        *= wdw
                v[k]        *= wdw
                w[k]        *= wdw
                b[k]        *= wdw
                dudt[k]     *= wdw
                dvdt[k]     *= wdw
                hadv_u_c[k] *= wdw
                hadv_v_c[k] *= wdw
                hadv_u[k]   *= wdw
                hadv_v[k]   *= wdw
                zadv_u[k]   *= wdw
                zadv_v[k]   *= wdw
                ediff_u[k]  *= wdw
                ediff_v[k]  *= wdw
                dxp[k]      *= wdw
                dyp[k]      *= wdw
                vmix_u[k]   *= wdw
                vmix_v[k]   *= wdw
                corio_u[k]  *= wdw
                corio_v[k]  *= wdw
                resu[k]     *= wdw
                resv[k]     *= wdw

        #
        #  SPECTRAL PROJECTION
        #
            cff  = dz[k] * norm_factor
            uconj = fft.fft2(u[k]).conj()
            vconj = fft.fft2(v[k]).conj()

            def _compute_coamp(_f, _g):
                return cff * np.real(fft.fft2(_f).conj()*fft.fft2(_g))

            def _compute_uvamp(_u, _v):
                return cff * np.real(uconj*fft.fft2(_u) + vconj*fft.fft2(_v))

            spec2d["ke"]     += _compute_uvamp(u[k], v[k])                        # kinetic energy
            spec2d["hadv_c"] += _compute_uvamp(hadv_u_c[k], hadv_v_c[k])          # centered horizontal advection
            spec2d["hadv"]   += _compute_uvamp(hadv_u[k], hadv_v[k])              # upwind   horizontal advection
            spec2d["ediff"]  += _compute_uvamp(ediff_u[k], ediff_v[k])            # explicit horizontal diffusion
            spec2d["idiff"]  += _compute_uvamp(idiss_u[k], idiss_v[k])            # implicit horizontal diffusion
            spec2d["tend"]   += _compute_uvamp(dudt[k],  dvdt[k])                 # time rate of change
            spec2d["pgrd"]   += _compute_uvamp(dxp[k],   dyp[k])                  # pressure gradient
            spec2d["vmix"]   += _compute_uvamp(vmix_u[k], vmix_v[k])              # vertical dissipation
            spec2d["zadv"]   += _compute_uvamp(zadv_u[k], zadv_v[k])              # vertical advection
            spec2d["resi"]   += _compute_uvamp(resu[k],  resv[k])                 # residual
            spec2d["wb"]     += _compute_coamp(w[k], b[k])                        # baroclinic conversion wb
            spec2d["cori"]   += _compute_uvamp(corio_u[k], corio_v[k])            # coriolis

        # END OF vertical loop
    # END OF temporal loop
    log("-- End of TEMPORAL LOOP.")

    ## get 1D spectra
    spec1d = {}
    kx, kxky = compute_dk((Nj,Ni), dx)

    for term in spec2d:
        spec1d[term] = integ_fft2d(fft.fftshift(spec2d[term]), kx, kxky)
        log(" - Sum of 1D {0} spectrum: {1:.7e}".format(term, spec1d[term].sum()))

    log(" - Sum of 2D KE spectrum: {0:.7e}".format(spec2d["ke"].sum()))
    log(" - Sum of 1D KE spectrum: {0:.7e}".format(spec1d["ke"].sum()))

    log("== Write '{0}' ...".format(data_filename))

    data_out = {
        "resolution": resolution,
        "model": model,
        "depth": depth,
        "tlength": tlength,
        "kx": kx,
        "spec": spec1d
    }

    with open(data_filename, 'w') as fout:
        pk.dump(data_out, fout)

    if DEBUG and CENTERED_ADVECTION:
        with open(advection_filename, 'w') as fout:
            pk.dump((C4_hadvection_u, C4_hadvection_v, UP3_hadvection_u, UP3_hadvection_v), fout)


if __name__ == "__main__":

    usage = """
    %prog -r RESOLUTION [-g GRID_FILE] NETCDF_FILE [NETCDF_FILE...] [-c]

      - RESOLUTION:  Simulation resolution in km (e.g. '20').
      - GRID_FILE:   Name of a NetCDF grid file, ie. with an empty record dimension, if any.
      - NETCDF_FILE: Name of a NetCDF file that complies the Comodo norm. """

    parser = OptionParser(usage)

    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")
    parser.add_option("-m", "--model", dest="model", default="UNKNOWN",
                      help="name of the model that produced NETCDF_FILE")
    parser.add_option("-r", "--resolution", dest="resolution", default=None,
                      help=u"grid resolution (∆x) for NETCDF_FILE")
    parser.add_option("-s", "--ncdir", dest="ncdir", default=".",
                      help=u"directory where NetCDF files are stored (default: '.')")
    parser.add_option("-z", "--depth", dest="depth", type=float, default=4000.,
                      help=u"")
    parser.add_option("-t", "--tlength", dest="tlength", type=int, default=0,
                      help=u"")
    parser.add_option("-g", "--grid", action="append", dest="gridfile", default=None,
                      help="grid file")
    parser.add_option("-c", "--multifile", action="store_true", dest="multifile", default=False,
                      help="the data files should be concatenated along the record dimension")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    DEBUG = options.debug

    if DEBUG:
        log.setLevel(logging.DEBUG)

    ncfiles = []
    for ncfile in args:
        ncfiles.append(os.path.join(options.ncdir, ncfile))

    if options.resolution is None:
        log.error("Please provide a resolution in km (e.g. '-r 20')")
        parser.print_help()
        sys.exit(-1)

    archive = pycomodo.Archive(ncfiles, grids=options.gridfile, multifile=options.multifile)
    compute_budget(archive, options.model, options.resolution, options.depth, tlength=options.tlength)
