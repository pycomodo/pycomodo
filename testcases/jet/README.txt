Diagnostiques pour le cas-test "jet"
------------------------------------

Ce dossier contient des scripts Python permettant de produire des diagnostiques pour le
cas-test "jet".

Au préalable, il est nécessaire d'avoir une installation fonctionnelle de la librairie pycomodo.
Les instructions sont détaillées sur cette page :

http://pycomodo.forge.imag.fr/api/installation.html

Le script 'jet_extract_spectrum.py' calcule les spectre 1D de l'énergie cinétique et des différents
termes du bilan d'énergie. Il est pour l'instant spécifique à chaque code. En attendant de le rendre
plus générique, il convient donc de modifier la ligne 26 pour l'adapter à votre code.
Il s'utilise de la manière suivante :

$ ./jet_extract_spectrum.py -r RESOLUTION -m MODEL [-g GRID_FILE] NETCDF_FILE [NETCDF_FILE...] [-c]
		[-t NTIMES] [-z DEPTH]

où RESOLUTION est la résolution du cas-test en km (20, 10, 5, 2).
   MODEL est le nom de votre modèle (influe uniquement sur les légendes des graphes).
   GRID_FILE est un éventuel fichier de grille contentant uniquement des données statiques
   NETCDF_FILE est un ou plusieurs fichiers de données

l'option -c est à utiliser si les fichiers de données sont séparés selon la dimension 'unlimited'
(un fichier par pas de temps, comme en produit S.).
Elle nécessite l'installation du paquet netcdf4-python (https://code.google.com/p/netcdf4-python/).

l'option -t NTIMES restreint l'analyse aux NTIMES derniers pas de temps.
l'option -z DEPTH  restreint l'intégration verticale aux DEPTH premiers mètres sous la surface libre.

Exemple pour ROMS :
$ ./jet_extract_spectrum.py -r 20 -m ROMS 20Kjet_his.nc 20Kjet_diaM.nc

Exemple pour NEMO (utiliser au préalable le script nemo2comodo.bash) :
$ ./jet_extract_spectrum.py -r 20 -m NEMO BJET200-*-COMODO_grid_*.nc

Exemple pour S :
$ ./jet_extract_spectrum.py -r 20 -m S.26 -g grid.nc -c 20*.nc
# /!\ attention : si l'option -c est omise, toutes les données sont recopiées en mémoire,
#                 ça passera difficilement avec le volume de données du cas-test.

Ce script produit un fichier .pck de quelques Ko, dont le nom dépend de la résolution, du modèle,
de la profondeur d'intégration.

-----
Les figures sont crées en utilisant les deux autres scripts avec ce fichier .pck :

$ ./jet_plot_ke.py data_20K_*.pck
$ ./jet_plot_budget.py data_20K_*.pck

