#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import logging
import math

import numpy as np
import pylab as plt
import cPickle as pk

from optparse import OptionParser
from matplotlib.ticker import AutoMinorLocator, MaxNLocator

import pycomodo
from pycomodo import log

# Global variables
#------------------
DEBUG = False

def get_basename(resolution, model, depth, tlength):
    """
    Returns a file name for a given set of resolution, model name, and integration depth (z) and length (t).
    """
    if tlength == 0:
        return "{0}K_{1}_z-{2}m_days-all".format(resolution, model, int(depth))
    else:
        return "{0}K_{1}_z-{2}m_days-{3:03}".format(resolution, model, int(depth), tlength)


def plot_budget( data ):

    try:
        resolution = data["resolution"]
        model      = data["model"]
        tlength    = data["tlength"]
        depth      = data["depth"]
        kx         = data["kx"]
        spec       = data["spec"]
    except:
        log.error("Input file was not produced by an up-to-date version of 'jet_extract_spectrum.py'.")
        sys.exit(-1)

    basename = get_basename(resolution, model, depth, tlength)
    pict1_filename = "{0}_budget"  .format(basename).replace('.', '-')
    pict2_filename = "{0}_residual".format(basename).replace('.', '-')

    signed_terms = model.upper() == "MARS"

    spec["resi2"] = spec["tend"] - ( spec["pgrd"]+spec["vmix"]+spec["ediff"] )

    if signed_terms:
        spec["resi2"] += ( spec["hadv"]+spec["zadv"]+spec["cori"] )
    else:
        spec["resi2"] -= ( spec["hadv"]+spec["zadv"]+spec["cori"] )

    #############################################################################
    #
    # PLOT
    #
    istr = 0

    K = kx[istr:]

#     print "dk_min = ", np.pi/(1000.*float(resolution))
#     print "K   :", K

    log("== Plot budget:   '{0}.png'".format(pict1_filename))
    fig1, a1 = plt.subplots()
    a1.plot(K, spec["tend"],            'r',   label=r'$\partial_t{u}$', lw=2.0)
    a1.plot(K, spec["pgrd"]-spec["wb"], 'c',   label=r'P3D')
    a1.plot(K, spec["hadv"],            'g',   label=r'h. advection')
    a1.plot(K, spec["zadv"],            'g--', label=r'v. advection')
    a1.plot(K, spec["vmix"],            'b--', label=r'v. diffusion',)
    a1.plot(K, spec["wb"],              'y--', label=r'buoyancy flux')
    a1.plot(K, spec["cori"],            'r--', label=r'Coriolis')
    a1.set_xscale('log')
    a1.set_xlabel('Wavenumber k [rad/m]')
    y_amax = np.max(np.abs(a1.get_ybound()))    # Symmetrize 'y' axis
    a1.set_ybound(-y_amax, y_amax)
    a1.yaxis.get_major_formatter().set_powerlimits((0,0))
    a1.yaxis.set_minor_locator(AutoMinorLocator())
    a1.legend(fontsize=10)
    a1.set_title('{0} - {1}K\nspectral budget'.format(model, resolution))
    fig1.savefig(pict1_filename)

    log("== Plot residual: '{0}.png'".format(pict2_filename))
    fig2, a2 = plt.subplots()
    a2.plot(K, spec["resi2"], 'k-s', label='Residual (spectral)', ms=3.0)
    a2.plot(K, spec["resi"],  'c--', label='Residual')
    a2.set_xscale('log')
    a2.set_xlabel('Wavenumber k [rad/m]')
    a2.set_ylabel('KE Tendencies [$m^2/s^3$]')
    a2.yaxis.set_minor_locator(AutoMinorLocator())
    a2.legend(fontsize=10)
    y_amax = np.max(np.abs(a2.get_ybound()))    # Symmetrize 'y' axis
    a2.set_ybound(-y_amax, y_amax)
    a2.set_title('{0} - {1}K\nresidual'.format(model, resolution))
    fig2.savefig(pict2_filename)


if __name__ == "__main__":

    usage = """
    %prog DATA_FILE

      - DATA_FILE:  .pck file produced by jet_extract_spectrum.py"""

    parser = OptionParser(usage)

    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                        help="outputs debug messages")

    (options, args) = parser.parse_args()

    if len(args) != 1:
        parser.print_help()
        sys.exit(-1)

    DEBUG = options.debug

    if DEBUG:
        log.setLevel(logging.DEBUG)

    try:
        with open(args[0], 'r') as fin:
            data = pk.load(fin)
    except pk.UnpicklingError:
        log.error("Input file '{0}' was not produced by 'jet_extract_spectrum.py'.".format(args[0]))
        parser.print_help()
        sys.exit(-1)

    plot_budget(data)
