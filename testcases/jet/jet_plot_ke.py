#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import logging

import numpy as np
import pylab as plt
import cPickle as pk
from matplotlib.ticker import ScalarFormatter

from optparse import OptionParser
from fractions import Fraction

import pycomodo.util.plots as pup
from pycomodo import log

# Global variables
#------------------
DEBUG = False
COLOR_BY_INDEX = [ 'go', 'ro', 'bo', 'ko', 'yo', 'mo', 'co' ]
COLOR_BY_RESOL = { '20':'ko', '10': 'bo', '5': 'ro', '2': 'go' }

STYLE_BY_MODEL = { 'ROMS': 'solid', 'NEMO': 'dashed', 'MARS': 'dotted' }

LEGEND_LOC = { 'tr': 1, 'tl': 2, 'bl': 3, 'br': 4, 'cl': 6, 'cr': 7, 'bc': 8, 'tc': 9, 'cc': 10 }

def get_basename(resolution, model, depth, tlength):
    """
    Returns a file name for a given set of resolution, model name, and integration depth (z) and length (t).
    """
    basename = "plot"
    if model is not None:
        basename += "_{0}".format(model)
    if resolution is not None:
        basename += "_{0:02}K".format(int(resolution))
    if depth is not None:
        basename += "_z{0}m".format(int(depth))
    if tlength is not None:
        if tlength == 0:
            basename += "_days-all"
        else:
            basename += "_days-{0:03}".format(tlength)

    return basename

def get_label_auto(dataset, resolution, model, depth, tlength):

    label_items = []
    if model is None:
        label_items.append(u"{0}".format(dataset["model"]))
    if resolution is None:
        label_items.append(u"{0:02}K".format(int(dataset["resolution"])))
    if depth is None:
        label_items.append(u"z{0}m".format(int(dataset["depth"])))
    if tlength is None:
        if dataset["tlength"] == 0:
            label_items.append(u" (all days)")
        else:
            label_items.append(u" ({0} days)".format(dataset["tlength"]))

    return u" ".join(label_items)

def get_style(dataset, model):

    return "solid" if dataset["model"] == model else "dashed"

def get_title(varname, resolution, model, depth, tlength):

    title_items = []
    if model is not None:
        title_items.append(u"{0}".format(model))
    if resolution is not None:
        title_items.append(u"{0:02}K".format(int(resolution)))
    if depth is not None:
        title_items.append(u"z={0}m".format(int(depth)))
    if tlength is not None:
        if tlength == 0:
            title_items.append(u"(all days)")
        else:
            title_items.append(u"({0} days)".format(tlength))

    if title_items:
        return u" – ".join(title_items) + u"\n{0} spectrum".format(varname.upper())
    else:
        return u"{0} spectrum".format(varname.upper())

def uniq_list( data_list, key ):
    """
    Order-preserving d
    >>> data_list = [ {"model": 1, "resolution": 20},
                      {"model": 2, "resolution": 20},
                      {"model": 1, "resolution": 10},
                      {"model": 2, "resolution": 10} ]
    >>> print uniq_list( data_list, "model" )
    [1, 2]
    >>> print uniq_list( data_list, "resolution" )
    [20, 10]
    """
    seen = set()
    seq  = (d[key] for d in data_list)
    return [ x for x in seq if x not in seen and not seen.add(x) ]

def compute_ke_transfert( data_list ):

    for dataset in data_list:

        try:
            model      = dataset["model"]
            kx         = dataset["kx"]
            hadv       = dataset["spec"]["hadv"]
            zadv       = dataset["spec"]["zadv"]
            # next lines actually depend if diffusion has been computed online or offline
            if model.lower() == 'nemo':
                idiff  = dataset["spec"]["ediff"]
            else:
                idiff  = dataset["spec"]["idiff"]
        except:
            log.error("Input file was not produced by an up-to-date version of 'jet_extract_spectrum.py'.")
            sys.exit(-1)

        A  = hadv[:]+zadv[:]-idiff[:]  # idiff computed as A_up3-A_c4
        PI  = np.zeros(kx.shape[0])

        for k in range(kx.shape[0]):
            PI[k] = np.trapz(A[k:], kx[k:])

        dataset["spec"]["ketr"] = PI

def plot_variables( data_list, variables, powerfactor=None, ylog=True, title=None, outname=None,
                   format="png", corner="tr" ):

    # First determine the plot filename from input data
    try:
        models      = uniq_list(data_list, "model")
        resolutions = uniq_list(data_list, "resolution")
        depths      = uniq_list(data_list, "depth")
        tlengths    = uniq_list(data_list, "tlength")
    except:
        log.error("Input file was not produced by an up-to-date version of 'jet_extract_spectrum.py'.")
        sys.exit(-1)

    resolution = resolutions[0] if len(resolutions) == 1 else None
    model      = models[0]      if len(models) == 1      else None
    depth      = depths[0]      if len(depths) == 1      else None
    tlength    = tlengths[0]    if len(tlengths) == 1    else None

    varname = "Unkown"

    if outname is None:
        basename = get_basename(resolution, model, depth, tlength)
        outname  = "{0}_{1}".format(basename, varname).replace('.', '-')
        outname += ".{0}".format(format)
    else:
        outname = os.path.splitext(outname)[0] + ".{0}".format(format)

    if title is None:
        title = get_title(varname, resolution, model, depth, tlength)
    else:
        title = title.decode('utf8')
        title = title.replace(u"\\n", u'\n').replace(u"\\t", u'\t')
        title = title.strip()

    if "ketr" in variables:
        compute_ke_transfert(data_list)

    # Carry out plot
    log("== Plot {0}: '{1}'".format(varname, outname))
    fig = plt.figure(figsize=(8, 6))
    ax  = plt.subplot()

    istr   = 1
    kx_max = []
    y_gmin = 1e10
    y_gmax = 0
    plotted_labels = set()

    for varname in variables:

        for index, dataset in enumerate(data_list):

            resol = dataset["resolution"]
            kx    = dataset["kx"][istr:]
            try:
                var = dataset["spec"][varname][istr:]
            except KeyError:
                log.error("Unkown variable name '{0}'.".format(varname))
                log.error("Available variables are :\n {0}".format(", ".join(dataset["spec"].keys())))
                sys.exit(-1)

            if powerfactor is not None:
                f_powerfactor = eval("1.*"+powerfactor)     # convert powerfactor to float
                var *= kx**f_powerfactor

            if DEBUG:
                print "-"*10
                print "resol  = ", resol
                print "dk_min = ", np.pi/(1000.*float(resol))
                print "kx     = ", kx

            if ylog:
                y_min = 10 ** np.floor(np.log10(var.min()))
                y_max = 10 ** np.ceil( np.log10(var.max()))
                if y_min < y_gmin:
                    y_gmin = y_min
                    k_gmax = kx[0]
                if y_max > y_gmax:
                    y_gmax = y_max

            if len(kx) > len(kx_max):
                kx_max = kx

            # Plot line
            label  = get_label_auto(dataset, resolution, model, depth, tlength)
            lcolor = COLOR_BY_INDEX[index]
            lstyle = "-"

    #         label  = dataset["model"]
    #         lcolor = COLOR_BY_RESOL[dataset["resolution"]]
    #         lstyle = STYLE_BY_MODEL[dataset["model"]]

            l = ax.plot(kx, var, lcolor, lw=2, ls=lstyle)

            if label not in plotted_labels:
                plotted_labels.add(label)
                l[0].set_label(label)

    if ylog:
        # Set y axis to log scale
        ax.set_yscale('log')
        ax.set_ybound(y_gmin, y_gmax)
        if powerfactor is None:
            # Plot lines k^p for p=-5/3, -2, -3
            ax.plot(kx_max,    0.1*y_gmax*(0.5*kx_max/k_gmax)**(-5./3), 'k--', lw=0.5)
            ax.plot(kx_max,    0.1*y_gmax*(1.0*kx_max/k_gmax)**(-2)   , 'k',   lw=0.5)
            ax.plot(kx_max,    0.1*y_gmax*(1.5*kx_max/k_gmax)**(-3)   , 'k:',  lw=0.5)
            ax.text(0.82*kx_max[0], 0.1*y_gmax*(0.5*kx_max[0]/k_gmax)**(-5./3), '$k^{-5/3}$')
            ax.text(0.82*kx_max[0], 0.1*y_gmax*(1.0*kx_max[0]/k_gmax)**(-2),    '$k^{-2}$')
            ax.text(0.82*kx_max[0], 0.1*y_gmax*(1.5*kx_max[0]/k_gmax)**(-3),    '$k^{-3}$')
    else:
        ax.yaxis.get_major_formatter().set_powerlimits((0,0))

    if powerfactor is None:
        ax.set_ylabel('{0} tendency [$m^3/s^2$]'.format(varname.upper()))
    else:
        if powerfactor == "3":
            varunits = "s^{-2}"
        elif powerfactor == "2":
            varunits = "m/s^2"
        else:
            varunits = "m^{" + str(Fraction(3)-Fraction(powerfactor)) + "}/s^2"
        k_powerfactor = "^{" + powerfactor + "}" if powerfactor != "1" else ""
        ax.set_ylabel('$k{0}$ {1} [${2}$]'.format(k_powerfactor, varname.upper(), varunits))

    # Set bottom x-scale bounds (rad/m)
    ax.set_xscale('log')
    pw_min = int(np.ceil( -np.log10(kx_max[ 0])))
    pw_max = int(np.floor(-np.log10(kx_max[-1])))
    xbound_minmin = pup.floor_to_digit(kx_max[ 0], pw_min)
    xbound_maxmin = pup.floor_to_digit(kx_max[ 0], pw_min+1)
    xbound_maxmax = pup.ceil_to_digit( kx_max[-1], pw_max)
    xbound_minmax = pup.ceil_to_digit( kx_max[-1], pw_max+1)
    xbound_min = (xbound_minmin + xbound_maxmin) / 2
    xbound_max = xbound_minmax
    ax.set_xlabel('Wavenumber k [rad/m]')
    ax.set_xbound(xbound_min, xbound_max)

    # Set top x-scale bounds (km)
    xconv = lambda x: 2*0.001*np.pi/x
    ax_up = ax.twiny()
    ax_up.invert_xaxis()
    ax_up.set_xscale('log')
    ax_up.xaxis.set_major_formatter(ScalarFormatter())
    ax_up.set_xbound(xconv(xbound_min), xconv(xbound_max))
    ax_up.set_xlabel('Wavelength [km]', fontsize=12, color="#606060")
    for xtl in ax_up.xaxis.get_ticklabels():
        xtl.set_color("#606060")

    # Resize labels
    for axx in (ax, ax_up):
        for axis in (axx.xaxis, axx.yaxis):
            axis.get_offset_text().set_fontsize(10)
            for xtl in axis.get_ticklabels():
                xtl.set_fontsize(11)

    # Set grid
    ax.xaxis.grid(color='gray', linestyle=':', which='both')
    ax.yaxis.grid(color='gray', linestyle=':', which='major')

    # Set figure title & legend
    ax.legend(fontsize=10, loc=LEGEND_LOC[corner])
    if title:
        fig.text(0.5, 0.975, title, horizontalalignment='center', verticalalignment='top', size=18)
        sp_top = 0.83 - 0.05*title.count('\n')
    else:
        sp_top = 0.90

    # Save figure to file
    fig.subplots_adjust(top=sp_top, bottom=0.10, left=0.10, right=0.90, hspace=0.07)
    fig.savefig(outname, format=format)

def parse_variables(variables_argument):

    if variables_argument is None:
        return ["ke"]

    # Black magic here :
    # - 'variables_argument' can be ['arg1,arg2', 'arg3']
    # - finally returns ['arg1', 'arg2', 'arg3']
    return [ varname for argument in variables_argument for varname in argument.split(",") ]

def main():

    usage = """
    %prog DATA_FILE

      - DATA_FILE:  .pck file produced by jet_extract_spectrum.py"""

    parser = OptionParser(usage)

    parser.add_option("-c", "--corner", dest="corner", default='tr',
                        choices=['tl', 'tr', 'tc', 'bl', 'br', 'bc', 'cl', 'cr', 'cc'],
                        help="corner where to put the legend. write it 'yx' where "
                             "'y' is 't' for top, 'b' for bottom or 'c' for center."
                             "'x' is 'l' for left, 'r' for right or 'c' for center."
                             "(default : 'tr')")
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                        help="outputs debug messages")
    parser.add_option("-f", "--format", dest="format",  default="png",
                        help="output format (png, pdf, svg)")
    parser.add_option("-l", "--ylog",  action="store_true", dest="ylog",  default=False,
                        help="log scale for y-axis")
    parser.add_option("-o", "--output", dest="outname",  default=None,
                        help="name of output file")
    parser.add_option("-p", "--powerfactor",  dest="powerfactor",  default=None,
                        help="power factor for compensated spectrum")
    parser.add_option("-t", "--title",  dest="title",  default=None,
                        help="figure title")
    parser.add_option("-v", "--variable",  dest="variables", action="append", default=None,
                        help="variables to plot (default: 'ke')")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    DEBUG = options.debug

    if DEBUG:
        log.setLevel(logging.DEBUG)

    dataset_list = []

    for arg in args:
        try:
            log("Reading {0}...".format(arg))
            with open(arg, 'r') as fin:
                dataset_list.append( pk.load(fin) )
        except pk.UnpicklingError:
            log.error("Input file '{0}' was not produced by 'jet_extract_spectrum.py'.".format(args[0]))
            parser.print_help()
            sys.exit(-1)

    plot_variables( dataset_list,
        variables   = parse_variables(options.variables),
        powerfactor = options.powerfactor,
        ylog        = options.ylog,
        title       = options.title,
        outname     = options.outname,
        format      = options.format,
        corner      = options.corner )

if __name__ == "__main__":

    main()
