#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 20 16:53:58 2013

@author: soufflet
"""

import numpy as np
import numpy.fft as fft
import pylab as plt             # debug

def plot_matrix(array, title, cmap=None):
    _fig, _ax = plt.subplots()
    _cax  = _ax.matshow(array, cmap=cmap)
    _cbar = _fig.colorbar(_cax)
    _ax.set_title(title)

def plot_compare_matrix(array1, title1, array2, title2, cmap=None):
    _fig, (_ax1, _ax2) = plt.subplots(1, 2, sharey=True)
    _cax1  = _ax1.matshow(array1, cmap=cmap)
    _cax2  = _ax2.matshow(array2, cmap=cmap)
    _cbar = _fig.colorbar(_cax1)
    _ax1.set_title(title1)
    _ax2.set_title(title2)

def tukeywin(window_length, alpha=0.5):
    """The Tukey window, also known as the tapered cosine window, can be regarded as a cosine lobe of width \alpha * N / 2
    that is convolved with a rectangle window of width (1 - \alpha / 2). At \alpha = 1 it becomes rectangular, and
    at \alpha = 0 it becomes a Hann window.

    We use the same reference as MATLAB to provide the same results in case users compare a MATLAB output to this function
    output.

    .. [Ref.1] http://www.mathworks.com/help/signal/ref/tukeywin.html
    .. [Ref.2] Bloomfield P. Fourier Analysis of Time Series: An Introduction, New York: Wiley-Interscience, 2000, p.69.
    """
    # copied from http://leohart.wordpress.com/2006/01/29/hello-world/
    # Special cases
    if alpha <= 0:
        return np.ones(window_length) #rectangular window
    elif alpha >= 1:
        return np.hanning(window_length)

    # Normal case
    x = np.linspace(0, 1, window_length)
    w = np.ones(x.shape)

    # first condition 0 <= x < alpha/2
    first_condition = x<alpha/2
    w[first_condition] = 0.5 * (1 + np.cos(2*np.pi/alpha * (x[first_condition] - alpha/2) ))

    # second condition already taken care of

    # third condition 1 - alpha / 2 <= x <= 1
    third_condition = x>=(1 - alpha/2)
    w[third_condition] = 0.5 * (1 + np.cos(2*np.pi/alpha * (x[third_condition] - 1 + alpha/2)))

    return w

def integ_fft2d_old(tmpamp, dx, debug=False):

    cff = 2*np.pi/dx
    (L,M) = tmpamp.shape
    a = np.zeros((L,1))
    b = np.zeros((1,M))

    a[:,0] = np.arange(L)
    b[0,:] = np.arange(M)

    Lh = L/2.
    Mh = M/2.
    kk = np.tile(a-Lh, (1,M))
    ll = np.tile(b-Mh, (L,1))

    Kh = (kk/L)**2 + (ll/M)**2
    Kh = cff*np.sqrt(Kh)
    #
    # Ring integration method with non constant dk
    # accounts for different lengths in xi and eta
    #
    if M > L:
        dcase = 1
        Lmin  = L
        Lmax  = M  #xi dim is bigger
    else:
        dcase = 0
        Lmin  = M
        Lmax  = L

    leng  = int(Lmin/2)#+1 #+int(Lmax/Lmin)-1
    dk    = np.zeros(leng)
    amp   = np.zeros(leng)
    ktmp  = np.zeros(leng)
    count = np.zeros(leng)
    Kcount = np.zeros_like(Kh)      # debug

    ### first sample the long wavelengths permitted in longest
    ### direction with wavenumber 0 in shortest direction
    indaux = 0
    for ind in range(int(Lmax/Lmin)-1):
        amp[ind]   = tmpamp[Lh+(1-dcase)*(ind+1), Mh+dcase*(ind+1)]
        count[ind] = 1
        ktmp[ind]  = cff/Lmax*(ind+1)
        dk[ind]    = cff/Lmax
        indaux += 1
        Kcount[Lh+(1-dcase)*(ind+1), Mh+dcase*(ind+1)] += 1      # debug


    ### second do the regular summation over rings with
    ### step that is function of k step for shortest dimension
    ### ie we use the largest step.
    for ind in range(int(Lmax/Lmin)-1, leng):
        ktmp[ind] = cff/Lmin*(ind+1-indaux)
        II = tmpamp[abs(Kh-ktmp[ind])<cff/(2*Lmin)]
        amp[ind]   = II.sum()
        count[ind] = II.shape[0]
        dk[ind]    = cff/Lmin
        Kcount[abs(Kh-ktmp[ind])<cff/(2*Lmin)] += 1      # debug

    if debug:
        plot_matrix(Kcount, "Kcount orig")  # debug
        plt.show()                          # debug

    ### third correct for spectral defects due to summation over
    ### finite number of points that imperfectly sample the wavenumber
    ### space.
    amp *= 2*dk[:]*ktmp[:]/(ktmp[-1]**2)*sum(count)/count[:]

    return amp, count, ktmp, dk

def compute_dk( shape, dx, debug=False ):

    Nj, Ni = shape
    k_max  = np.pi / dx

    kx = fft.fftshift(fft.fftfreq(Ni, d=1./(2.*k_max)))
    ky = fft.fftshift(fft.fftfreq(Nj, d=1./(2.*k_max)))
    kkx, kky = np.meshgrid( kx, ky )
    Kh = np.sqrt(kkx**2 + kky**2)

    if debug:
        print "k_max =", k_max
        def fk(_in_k):
            return " ".join(["{0:.4e}".format(_k) for _k in _in_k ])
        print 'kx:', fk((kx[0],)), "...", fk(kx[Ni/2-2:Ni/2+2]), "...", fk((kx[-1],))
        print 'ky:', fk((ky[0],)), "...", fk(ky[Nj/2-2:Nj/2+2]), "...", fk((ky[-1],))
        print "Kh: {0}\n".format(Kh.shape), Kh[47:54,11:14]

    #
    # Ring integration of 2d spectrum
    #
    Nmin  = min(Ni,Nj)
    leng  = Nmin/2+1
    kstep = np.zeros(leng)

    ### first sample the long wavelengths permitted in longest
    ### direction with wavenumber 0 in shortest direction
    kstep[0] =  k_max / Nmin

    if debug:
        Kcount = np.zeros_like(Kh)
        count  = np.zeros(leng)     # useless
        krange = Kh <= kstep[0]     # useless
        count[0] = krange.sum()     # useless

    ### second do the regular summation over rings with
    ### step that is function of k step for shortest dimension
    ### ie we use the largest step.
    for ind in range(1, leng):
        kstep[ind] = kstep[ind-1] + 2*k_max/Nmin

    if debug:
        for ind in range(leng):
            if ind == 0:
                krange = Kh <= kstep[ind]
            elif ind < leng-1:
                krange = (kstep[ind-1] < Kh) & (Kh <= kstep[ind])   # useless, for debug only
            else:
                krange = (kstep[ind-1] < Kh)   # useless, for debug only
            count[ind] = krange.sum()
            Kcount[krange] += 1 #ind
            if ind == 0:
                print "dk[{0:02d}] = {1}".format(ind, kstep[0])
            else:
                print "dk[{0:02d}] = {1}".format(ind, kstep[ind]-kstep[ind-1])
        print "kstep :", len(kstep), fk((kstep[0],)), fk((kstep[1],)), "...", fk((kstep[-2],)), fk((kstep[-1],))
        print "count :", count
        plot_matrix(Kcount, "Kcount", cmap=plt.cm.Set1)
        plt.show()

    return kstep, Kh

def integ_fft2d_new(spec_2d, kx, Kh, normalize=True):
    #
    # Ring integration of 2d spectrum
    #
    leng    = len(kx)
    spec_1d = np.zeros(leng)

    ### first sample the long wavelengths permitted in longest
    ### direction with wavenumber 0 in shortest direction
    krange     = Kh <= kx[0]
    spec_1d[0] = spec_2d[krange].sum()

    ### second do the regular summation over rings with
    ### step that is function of k step for shortest dimension
    ### ie we use the largest step.
    for ind in range(1, leng):
        krange = (kx[ind-1] < Kh) & (Kh <= kx[ind])
        spec_1d[ind] = spec_2d[krange].sum()

    if normalize:
        spec_1d[0] /= kx[0]
        for ind in range(1, leng):
            spec_1d[ind] /= kx[ind]-kx[ind-1]

    return spec_1d

def integ_fft2d_new2(spec_2d, kx, Kh, normalize=True):
    #
    # Ring integration of 2d spectrum
    #
    leng    = len(kx)
    spec_1d = np.zeros(leng)

    ### first sample the long wavelengths permitted in longest
    ### direction with wavenumber 0 in shortest direction
    krange     = Kh <= kx[0]
    spec_1d[0] = spec_2d[krange].sum()

    ### second do the regular summation over rings with
    ### step that is function of k step for shortest dimension
    ### ie we use the largest step.
    for ind in range(1, leng-1):
        krange = (kx[ind-1] < Kh) & (Kh <= kx[ind])
        spec_1d[ind] = spec_2d[krange].sum()

    # For the smallest wavenumber, integrate all that remains.
    krange      = Kh > kx[-2]
    spec_1d[-1] = spec_2d[krange].sum()

    if normalize:
        spec_1d[0] /= kx[0]
        for ind in range(1, leng):
            spec_1d[ind] /= kx[ind]-kx[ind-1]

    return spec_1d

_ = """
            +-------+-------+-------+ ... +-------+
            |       |       |       |     |       |
     Ny-1...|   T   U   T   U   T   U     U   T   U
            |       |       |       |     |       |
       Ny-1 +---V---F---V---F---V---F ... F---V---F
            |       |       |       |     |       |
     Ny-2...|   T   U   T   U   T   U     U   T   U
            |       |       |       |     |       |
       Ny-2 +---V---F---V---F---V---F ... F---V---F
            .                       .     .       .
            .                       .     .       .
            .                       .     .       .
        1   +---V---F---V---F---V---F ... F---V---F
            |       |       |       |     |       |
      1 ... |   T   U   T   U   T   U     U   T   U
            |       |       |       |     |       |
        0   +---V---F---V---F---V---F ... F---V---F
            |       |       |       |     |       |
      0 ... |   T   U   T   U   T   U     U   T   U
            |       |       |       |     |       |
            +-------+-------+-------+ ... +-------+
                .   0   .   1   .   2         .  Nx-1
                0       1       2            Nx-1

    var_at_T.shape = ( Ny, Nx )
    var_at_U.shape = ( Ny, Nx )
    var_at_V.shape = ( Ny-1, Nx )
    var_at_F.shape = ( Ny-1, Nx )

                /.........(j,i).......\
                :                    :
         F-----------V----------F    :
      (j,i-1)   :  (j,i)      (j,i)  :
         |      :               |    :
         |      :               |    :
         U      :    T          U    :
      (j,i-1)   :  (j,i)      (j,i)  :
         |      \...............|..../
         |                      |
         F-----------V----------F
     (j-1,i-1)    (j-1,i)    (j-1,i)


"""

def u2t_xper(in_u, out_t=None):
    """
    u2t_xper(u) = ( u[j,i] + u[j,i+1] ) / 2   with periodic conditions in i.
    Transforms  U -> T
    """
    if out_t is None:
        out_t = np.empty(in_u.shape)

    out_t[...,1:] = 0.5 * ( in_u[...,:-1] + in_u[...,1:] )
    out_t[..., 0] = 0.5 * ( in_u[..., -1] + in_u[..., 0] )

    return out_t

def u2f_xper(in_u, out_f=None):
    """
    u2f_xper(u) = ( u[j,i] + u[j+1,i] ) / 2   with periodic conditions in i.
    Transforms  U -> F
    """
    if out_f is None:
        out_shape = list(in_u.shape)
        out_shape[-2] -= 1
        out_f = np.empty(out_shape)

    out_f[...,:,:] = 0.5 * ( in_u[...,:-1,:] + in_u[...,1:,:] )

    return out_f

def v2t_xper(in_v, out_t=None):
    """
    v2t_xper(v) = ( v[j,i] + v[j+1,i] ) / 2   with periodic conditions in i.
    Transforms  V -> T
    """
    if out_t is None:
        out_shape = list(in_v.shape)
        out_shape[-2] += 1
        out_t = np.empty(out_shape)

    out_t[...,1:-1,:] = 0.5 * ( in_v[...,:-1,:] + in_v[...,1:,:] )
    out_t[...,   0,:] = 2*in_v[..., 0,:] - out_t[..., 1,:]
    out_t[...,  -1,:] = 2*in_v[...,-1,:] - out_t[...,-2,:]

    return out_t

def v2f_xper(in_v, out_f=None):
    """
    v2f_xper(v) = ( v[j,i] + v[j,i+1] ) / 2   with periodic conditions in i.
    Transforms  V -> F
    """
    if out_f is None:
        out_f = np.empty(in_v.shape)

    out_f[...,:-1] = 0.5 * ( in_v[...,:-1] + in_v[...,1:] )
    out_f[..., -1] = 0.5 * ( in_v[...,  0] + in_v[...,-1] )
    return out_f

def diff_i_t2u_xper(in_t, out_u=None):
    """
    diff_i(t) = ( t[j,i+1] - t[j,i] ) with periodic conditions in i.
    """
    if out_u is None:
        out_u = np.empty(in_t.shape)

    out_u[...,:-1] = in_t[...,1:] - in_t[...,:-1]
    out_u[..., -1] = in_t[..., 0] - in_t[..., -1]
    return out_u

def diff_i_f2v_xper(in_f, out_v=None):
    """
    diff_i(t) = ( f[j,i+1] - f[j,i] ) with periodic conditions in i.
    """
    if out_v is None:
        out_v = np.empty(in_f.shape)

    out_v[...,1:] = in_f[...,1:] - in_f[...,:-1]
    out_v[..., 0] = in_f[..., 0] - in_f[..., -1]
    return out_v

def diff_j_f2u_xper(in_f, out_u=None):
    """
    diff_j(t) = ( f[j+1,i] - f[j,i] ) with periodic conditions in i.
    """
    if out_u is None:
        out_shape = list(in_f.shape)
        out_shape[-2] += 1
        out_u = np.empty(out_shape)

    out_u[...,1:-1,:] = in_f[...,1:,:] - in_f[...,:-1,:]
    out_u[...,   0,:] = out_u[..., 1,:]
    out_u[...,  -1,:] = out_u[...,-2,:]
    return out_u

def diff_j_t2v_xper(in_t, out_v=None):
    """
    diff_j(t) = ( t[j+1,i] - t[j,i] ) with periodic conditions in i.
    """
    if out_v is None:
        out_shape = list(in_t.shape)
        out_shape[-2] -= 1
        out_v = np.empty(out_shape)

    out_v[...,:,:] = in_t[...,1:,:] - in_t[...,:-1,:]
    return out_v

def lap_i_xper(in_a, out_a=None):
    """
    ∆_i(a) = ( a[j,i-1] - 2*a[j,i] + a[j,i+1] )
    """
    if out_a is None:
        out_a = np.empty(in_a.shape)

    out_a[...,1:-1] = in_a[...,0:-2] - 2*in_a[...,1:-1] + in_a[...,2:]
    out_a[...,   0] = in_a[...,  -1] - 2*in_a[...,   0] + in_a[..., 1]
    out_a[...,  -1] = in_a[...,  -2] - 2*in_a[...,  -1] + in_a[..., 0]
    return out_a

def lap_j_xper(in_a, out_a=None):
    """
    ∆_j(a) = ( a[j-1,i] - 2*a[j,i] + a[j+1,i] )
    """
    if out_a is None:
        out_a = np.empty(in_a.shape)

    out_a[...,1:-1,:] = in_a[...,0:-2,:] - 2*in_a[...,1:-1,:] + in_a[...,2:,:]
    out_a[...,   0,:] = in_a[...,   1,:]
    out_a[...,  -1,:] = in_a[...,  -2,:]
    return out_a

def horizontal_advection_C4 ( u, v, dx, dy, dz, debug=True ):
    """
    Compute diagonal [UFx,VFe] and off-diagonal [UFe,VFx] components
    of tensor of momentum flux due to horizontal advection.

                 i,j+1/2    i+1/2,j+1/2
            F-------V-------F
            |               |
            |      i,j      |
   i-1/2,j  U       T       U  i+1/2,j
            |               |
            |               |
            F-------V-------F
                 i,j-1/2    i+1/2,j-1/2

    notations : Let 'a' be a 2d-array :
    - (a)^i = ( a[i,j] + a[i+1,j] ) / 2     transforms  U <-> T  and  V <-> F
    - (a)^j = ( a[i,j] + a[i,j+1] ) / 2     transforms  U <-> F  and  V <-> T
    - ∆_i(a) = ( a[i-1,j] - 2*a[i,j] + a[i+1,j] )
    - ∆_j(a) = ( a[i,j-1] - 2*a[i,j] + a[i,j+1] )

    """
    Nk = u.shape[0]
    Nj = u.shape[1]
    Ni = v.shape[2]

    dz = dz[:, None, None]
    Hu = u  * dz * dy
    Hv = v  * dz * dx
    dv = dx * dy * dz

    u_at_T  = u2t_xper(u)   # (u)^i  ~= u  defined at T {i,j}
    v_at_T  = v2t_xper(v)   # (v)^j  ~= v  defined at T {i,j}
    u_at_F  = u2f_xper(u)   # (u)^j  ~= u  defined at F {i+1/2,j+1/2}
    v_at_F  = v2f_xper(v)   # (v)^i  ~= v  defined at F {i+1/2,j+1/2}
    Hu_at_T = u2t_xper(Hu)  # (Hu)^i ~= Hu defined at T {i,j}
    Hv_at_T = v2t_xper(Hv)  # (Hv)^j ~= Hv defined at T {i,j}
    Hu_at_F = u2f_xper(Hu)  # (Hu)^j ~= Hu defined at F {i+1/2,j+1/2}
    Hv_at_F = v2f_xper(Hv)  # (Hv)^i ~= Hv defined at F {i+1/2,j+1/2}

    if debug:
        print "u     :", u.shape
        print "v     :", v.shape
        print "u_at_T:", u_at_T.shape
        print "v_at_T:", v_at_T.shape
        print "u_at_F:", u_at_F.shape
        print "v_at_F:", v_at_F.shape

    d2i_u  = lap_i_xper(u)  # ∆_i(u)   defined at U {i+1/2,j}
    d2j_u  = lap_j_xper(u)  # ∆_j(u)   defined at U {i+1/2,j}
    d2i_v  = lap_i_xper(v)  # ∆_i(v)   defined at V {i,j+1/2}
    d2j_v  = lap_j_xper(v)  # ∆_j(v)   defined at V {i,j+1/2}
    d2i_Hu = lap_i_xper(u)  # ∆_i(Hu)  defined at U {i+1/2,j}
    d2j_Hu = lap_j_xper(u)  # ∆_j(Hu)  defined at U {i+1/2,j}
    d2i_Hv = lap_i_xper(v)  # ∆_i(Hv)  defined at V {i,j+1/2}
    d2j_Hv = lap_j_xper(v)  # ∆_j(Hv)  defined at V {i,j+1/2}

    d2i_u_at_T  = u2t_xper(d2i_u)   # (∆_i(u))^i  ~= dx².(∂²/∂x)(u)  defined at T
    d2j_v_at_T  = v2t_xper(d2j_v)   # (∆_j(v))^j  ~= dy².(∂²/∂y)(v)  defined at T
    d2j_u_at_F  = u2f_xper(d2j_u)   # (∆_j(u))^j  ~= dy².(∂²/∂y)(u)  defined at F
    d2i_v_at_F  = v2f_xper(d2i_v)   # (∆_i(v))^i  ~= dx².(∂²/∂x)(v)  defined at F
    d2i_Hu_at_T = u2t_xper(d2i_Hu)  # (∆_i(Hu))^i ~= dx².(∂²/∂x)(Hu) defined at T
    d2j_Hv_at_T = v2t_xper(d2j_Hv)  # (∆_j(Hv))^j ~= dy².(∂²/∂y)(Hv) defined at T
    d2j_Hu_at_F = u2f_xper(d2j_Hu)  # (∆_j(Hu))^j ~= dy².(∂²/∂y)(Hu) defined at F
    d2i_Hv_at_F = v2f_xper(d2i_Hv)  # (∆_i(Hv))^i ~= dx².(∂²/∂x)(Hv) defined at F

    UFx = (u_at_T - 0.125*d2i_u_at_T)*(Hu_at_T - 0.125*d2i_Hu_at_T)
    VFe = (v_at_T - 0.125*d2j_v_at_T)*(Hv_at_T - 0.125*d2j_Hv_at_T)

    UFe = (u_at_F - 0.125*d2j_u_at_F)*(Hv_at_F - 0.125*d2i_Hv_at_F)
    VFx = (v_at_F - 0.125*d2i_v_at_F)*(Hu_at_F - 0.125*d2j_Hu_at_F)

#     hadv_c4_u = ( UFx[...,1:]-UFx[...,:-1] + UFe[...,1:,:]-UFe[...,:-1,:] ) / dv
#     hadv_c4_v = ( VFx[...,1:]-VFx[...,:-1] + VFe[...,1:,:]-VFe[...,:-1,:] ) / dv

    hadv_c4_u = - ( diff_i_t2u_xper(UFx) + diff_j_f2u_xper(UFe) )
    hadv_c4_v = - ( diff_j_t2v_xper(VFe) + diff_i_f2v_xper(VFx) )

    print "u ", hadv_c4_u.shape
    print "v ", hadv_c4_v.shape
    print "dv", dv.shape

    hadv_c4_u[:] /= dv
    hadv_c4_v[:] /= dv

    return hadv_c4_u, hadv_c4_v

def horizontal_advection_C4_yves(u,v,Hz,Ni,Nj,dx,dy):
  '''
  =======================================================================
  ROMS 4th order centered  horizontal advection of momentum
  =======================================================================
  Compute diagonal [UFx,VFe] and off-diagonal [UFe,VFx] components
  of tensor of momentum flux due to horizontal advection; after that
  compute horizontal advection terms in units of [m/s2]

  Note: The difficulty is in dealing with 0 array indices not allowed by
  matlab. This requires attention in the computation of off-diagonal
  components which mixes staggered U and V type of variables.
  ========================================================================
  '''
  Hu = Hz * dy * u
  Hv = Hz * dx * v
  dv = dx * dy * Hz

  wrk1 = np.zeros((Nj, Ni))
  wrk2 = np.zeros((Nj, Ni))
  cffX = np.zeros((Nj, Ni))
  cffE = np.zeros((Nj, Ni))
  curvX = np.zeros((Nj, Ni))
  curvE = np.zeros((Nj, Ni))
  UFx = np.zeros((Nj, Ni))
  UFe = np.zeros((Nj, Ni))
  VFx = np.zeros((Nj, Ni))
  VFe = np.zeros((Nj, Ni))
  MU_Xadv = np.zeros_like(u)
  MU_Yadv = np.zeros_like(u)
  MV_Xadv = np.zeros_like(v)
  MV_Yadv = np.zeros_like(v)

  ##=========================
  ## Diagonal component UFx
  ##

  wrk1[:,1:-2] =  u[:,:-2] - 2* u[:,1:-1] +  u[:,2:]
  wrk2[:,1:-2] = Hu[:,:-2] - 2*Hu[:,1:-1] + Hu[:,2:]

  wrk1[:,0] = wrk1[:,1]
  wrk2[:,0] = wrk2[:,1]
  wrk1[:,-2] = wrk1[:,-3]
  wrk2[:,-2] = wrk2[:,-3]

  cffX[:,:-2]  = u[:,:-1] + u[:,1:]
  curvX[:,:-2] = 0.5*(wrk1[:,:-2]+wrk1[:,1:-1])

  UFx[1:-1,:-2]=0.25*(cffX[1:-1,:-2]-0.25*curvX[1:-1,:-2])*(Hu[1:-1,:-1]+Hu[1:-1,1:]-0.25*0.5*(wrk2[1:-1,:-2]+wrk2[1:-1,1:-1]))

  ##=========================
  ## Diagonal component VFe
  ##

  wrk1[:]=0 ; wrk2[:]=0
  wrk1[1:-2,:]=v[:-2,:]-2*v[1:-1,:]+v[2:,:]
  wrk2[1:-2,:]=Hv[:-2,:]-2*Hv[1:-1,:]+Hv[2:,:]

  wrk1[0,:]=wrk1[1,:]
  wrk1[-2,:]=wrk1[-3,:]
  wrk2[0,:]=wrk2[1,:]
  wrk2[-2,:]=wrk2[-3,:]

  cffE[:-2,:]=v[:-1,:]+v[1:,:]
  curvE[:-2,:]=0.5*(wrk1[:-2,:]+wrk1[1:-1,:])

  VFe[:-2,1:-1]=0.25*(cffE[:-2,1:-1]-0.25*curvE[:-2,1:-1])*(Hv[:-1,1:-1]+Hv[1:,1:-1]-0.25*0.5*(wrk2[:-2,1:-1]+wrk2[1:-1,1:-1]))

  ##=========================
  ## off-diagonal component UFe
  ##
  ## notations :  (a)^i = ( a[i,j] + a[i+1,j] ) / 2     transforme  U <-> T, V <-> F
  ##           :  (a)^j = ( a[i,j] + a[i,j+1] ) / 2     transforme  U <-> F, V <-> T
  ##           :  ∆_i(a) = ( a[i-1,j] - 2*a[i,j] + a[i+1,j] )
  ##           :  ∆_j(a) = ( a[i,j-1] - 2*a[i,j] + a[i,j+1] )

  wrk1[:]=0 ; wrk2[:]=0
  wrk1[1:-1,:-1]=u[:-2,:]-2*u[1:-1,:]+u[2:,:]       # ~ ∆_j(u)  at {i+1/2,j} (U)
  wrk2[1:,:-2]=Hv[:,:-2]-2*Hv[:,1:-1]+Hv[:,2:]      # ~ ∆_i(Hv) at {i,j+1/2} (V)
  wrk1[0,:]=wrk1[1,:]
  wrk1[-1,:]=wrk1[-2,:]

  cffX[:]=0
  cffE[:]=0
  curvX[:]=0
  cffX[1:,:-1]=0.5*(u[1:,:]+u[:-1,:])               # ~ (u)^i  at {i+1/2,j+1/2} (F)
  cffE[1:,1:-2]=0.5*(Hv[:,2:-1]+Hv[:,1:-2])         # ~ (Hv)^j at {i+1/2,j+1/2} (F)
  curvX[1:,:]=0.5*(wrk1[:-1,:]+wrk1[1:,:])          # ~ (∆j(u))_{j+1/2}
  curvE[1:,1:-2]=0.5*(wrk2[:,2:-1]+wrk2[:,1:-2])    # ~ (∆i(Hv))_{i+1/2}

  UFe[1:,1:-2] = (cffX[1:,1:-2]-0.125*curvX[1:,1:-2])*(cffE[1:,1:-2]-0.125*curvE[1:,1:-2])
# UFe   = ( (u)_j - (1/8)*(∆j(u))_j ) * ( (Hv)_i - (1/8)*(∆i(Hv))_i )

  ##=========================
  ## off-diagonal component VFx
  ##
  wrk1[:]=0 ; wrk2[:]=0
  wrk1[:-1,1:-1]=v[:,:-2]-2*v[:,1:-1]+v[:,2:]
  wrk2[:-2,1:]=Hu[:-2,:]-2*Hu[1:-1,:]+Hu[2:,:]
  wrk1[:,0]=wrk1[:,1]
  wrk1[:,-1]=wrk1[:,-2]

  cffX[:]=0
  cffE[:]=0
  curvE[:]=0

  cffE[:-1,1:]=v[:,1:]+v[:,:-1]
  cffX[1:-2,1:]=Hu[2:-1,:]+Hu[1:-2,:]
  curvE[:,1:]=0.5*(wrk1[:,:-1]+wrk1[:,1:])

  VFx[1:-2,1:]=0.25*(cffE[1:-2,1:]-0.25*curvE[1:-2,1:])*(cffX[1:-2,1:]-0.125*(wrk2[1:-2,1:]+wrk2[:-3,1:]))

  ##===================================
  ## Compute advection terms.
  ## Then Divide advection terms by the cell volume Hz/(pm*pn).
  ## There after the unit of diag terms are :
  ## (unit of velocity) / s  =  [m/s2]
  ##
  MU_Xadv[1:-1,1:-1] = -UFx[1:-1,1:-2]+UFx[1:-1,:-3]
  MU_Yadv[1:-1,1:-1] = -UFe[2:,1:-2]+UFe[1:-1,1:-2]
  MV_Xadv[1:-1,1:-1] = -VFx[1:-2,2:]+VFx[1:-2,1:-1]
  MV_Yadv[1:-1,1:-1] = -VFe[1:-2,1:-1]+VFe[:-3,1:-1]

  advu = (MU_Xadv+MU_Yadv) / dv
  advv = (MV_Xadv+MV_Yadv) / dv


  return advu,advv
