#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import re

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

from optparse import OptionParser
from collections import defaultdict
import pycomodo.util.plots as pup
from operator import itemgetter
from logging  import DEBUG
from pycomodo import log

class TraceRecord:

    def __init__(self, filename):
        self.model, self.resolution, self.modif = self._interpret_filename(filename)
        self.t = []
        self.x = []
        self.y = []
        self.z = []

    def _interpret_filename(self, filename):
        """ rms_MARS_05km.txt -> |MARS|, |5 km|"""

        bname = os.path.basename(filename)
        r = re.match("trace_([a-zA-Z0-9_-]*)_([a-zA-Z0-9]*)km(.*).txt", bname)
        model, resolution, modif = r.groups()
        return model, int(resolution), modif

    def add_data(self, string_record):
        _float_record = [float(item) for item in string_record.strip().split(" ") if item != '']
        self.t.append(_float_record[0])
        self.x.append(_float_record[1])
        self.y.append(_float_record[2])
        self.z.append(100.*_float_record[3])

    def get_label(self):
        if self.modif != '':
            label = "{0} - {1} km ({2})".format(self.model, self.resolution, self.modif)
        else:
            label = "{0} - {1} km".format(self.model, self.resolution)
        return label


def plot_trace(trace_files, plot_type="BOTH", title=None, plotx=True):

    if plot_type not in ["MODELS", "RES", "BOTH"]:
        print "## Error : unknown plot_type: {0}".format(plot_type)
        sys.exit(1)

    line_style = defaultdict(lambda: '-')
    line_color = defaultdict(lambda: 'k')

    if plot_type != 'MODELS':
        line_style[30] = '-'
        line_style[20] = '--'
        line_style[10] = '-.'
        line_style[5]  = ':'
    elif plot_type != 'RES':
        line_color["NEMO"]  = 'm'
        line_color["MARS"]  = 'r'
        line_color["ROMS"]  = 'g'
        line_color["S"]     = 'b'
        line_color["HYCOM"] = 'k'

    resolutions = set()
    models = set()

    fig = plt.figure(1)
    ax  = fig.add_axes([0.1,0.1,0.8,0.8])

    for trace_file in trace_files:

        tracerec = TraceRecord(trace_file)
        with open(trace_file, "r") as f:
            for rec in f:
                tracerec.add_data(rec)

        if tracerec.modif != '':
            model_name = "{0} ({1})".format(tracerec.model, tracerec.modif)
        else:
            model_name = tracerec.model

        resolution_name = "{0} km".format(tracerec.resolution)

        resolutions.add(resolution_name)
        models.add(model_name)

        if   plot_type == 'MODELS':
            label = model_name
        elif plot_type == 'RES':
            label = resolution_name
        else:
            label = ""

        plt.plot(tracerec.t, tracerec.z, lw=2., ls=line_style[tracerec.resolution], \
                 c=line_color[tracerec.model], label=label)

    handles, labels = ax.get_legend_handles_labels()
    ncol = 1

    if plot_type == 'MODELS':
        hl = sorted(zip(handles, labels), key=itemgetter(1))
        handles, labels = zip(*hl)
    elif plot_type == 'BOTH':
        ncol = 2
        labels  = []
        handles = []
        for m in models:
            model = m.split(" ")[0]
            labels.append(m)
            handles.append(Line2D([], [], lw=2., ls="-", c=line_color[model]))
        for r in sorted(resolutions, key=lambda res: int(res.split(" ")[0])):
            res = int(r.split(" ")[0])
            labels.append(r)
            handles.append(Line2D([], [], lw=2., ls=line_style[res], c="k"))

    ax.set_xlabel("Time [days]")
    ax.set_ylabel("Elevation of vortex center [cm]")
    ax.set_xlim(0,  100)
    ax.set_ylim(20, 100)
    ax.legend(handles, labels, fontsize=12, ncol=ncol)

    if title is not None:
        plt.title(title, fontsize='large', family='serif')

    if plotx:
        plt.show()
    else:
        log("=== Writing trace.png ...")
        plt.savefig('trace', dpi=100, bbox_inches='tight')


if __name__ == "__main__":

    usage = "Usage: %prog TRACE_FILE [TRACE_FILE]\n  TRACE_FILE: Name of a text file "+\
        "with 4 columns: |time x y z|"

    parser = OptionParser(usage)

    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")
    parser.add_option("-p", "--plot_type", dest="plot_type", default="BOTH",
                      help="type of plot [MODELS|RES|BOTH]")
    parser.add_option("-t", "--title", dest="title", default=None, help="plot title (default: None)")
    parser.add_option("-x", "--x-window", action="store_true", dest="plotx", default=False,
                      help="plot in a window instead of PNG file.")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    if options.debug:
        log.setLevel(DEBUG)

    plot_trace(args, options.plot_type, options.title, options.plotx)
