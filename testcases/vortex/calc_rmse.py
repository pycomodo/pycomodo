#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import numpy as np
from optparse import OptionParser
from logging import DEBUG
import matplotlib.pyplot as plt

import pycomodo
import pycomodo.util.variables as puv
from pycomodo import log, UnknownVariable

def calc_rmse(archive, output, resolution):

    try:
        log("=== Get time :")
        time = archive.get_variable(stdname='time')
        time.check(True)

        log("=== Call get_ssh")
        ssh = puv.get_ssh(archive)
        ssh.check(True)

    except UnknownVariable as err:
        log.error("variable {0} not found in '{1}'".format(err, archive))
        return

    latlon_slice = ssh.get_dynamic_slice("X", "Y")

    # If we have a time coordinate, let's plot just the last chunk
    if len(ssh.shape) > 2:
        slice_init  = np.index_exp[0]                + latlon_slice
        slice_final = np.index_exp[archive.ntimes-1] + latlon_slice
    else:
        slice_init  = latlon_slice
        slice_final = latlon_slice

    try:
        Zi = np.ma.masked_values(ssh[slice_init],  ssh.get_fill_value())
    except:
        Zi = ssh[slice_init]

    npj = int(360/float(resolution))
    npi = int(360/float(resolution))

    def build_slice(j,i):
        return np.index_exp[j-npj:j+npj+1, i-npi:i+npi+1]

    j0, i0 = np.unravel_index(np.argmax(Zi), Zi.shape)
    Zi_max = Zi.max()
    slice_i = build_slice(j0,i0)

    Ziw  = Zi[slice_i]
    cmin = Ziw.min()
    cmax = Ziw.max()
    rms_array = []

    for k in range(0,len(time[:])):

        slice_k = np.index_exp[k] + latlon_slice
        Zk = np.ma.masked_equal(ssh[slice_k], ssh.get_fill_value())

        jk, ik = np.unravel_index(np.argmax(Zk), Zk.shape)
        slice_w = build_slice(jk,ik)

        Zk_max = Zk.max()
        Zk *= Zi_max/Zk_max

        bias = 100.*(1-(Zi_max-Zk_max)/Zi_max)
        err = Zk[slice_w]-Ziw
        rms  = np.sqrt(np.sum(err*err)/err.size);

        rms_array.append("{0:5.1f}  {1:.4e}  {2:.4e}\n".format(time[k]/(3600*24), rms, bias))

#     fig0 = plt.figure(0)
#     csi0 = plt.pcolor(Ziw, cmap=plt.cm.bwr, vmin=cmin, vmax=cmax)
#     cbar0= fig0.colorbar(csi0, orientation='vertical')
#     fig1 = plt.figure(1)
#     csi1 = plt.pcolor(Zk[slice_w], cmap=plt.cm.bwr, vmin=cmin, vmax=cmax)
#     cbar1= fig1.colorbar(csi1, orientation='vertical')
#     absmax = np.abs(err).max()
#     fig2 = plt.figure(2)
#     csi2 = plt.pcolor(err, cmap=plt.cm.bwr, vmin=-absmax, vmax=absmax)
#     cbar2= fig2.colorbar(csi2, orientation='vertical')
#     plt.show()

    with open(output, "w") as f:
        log("=== Writing '{0}' ...".format(output))
        f.write("".join(rms_array))

if __name__ == "__main__":

    usage = u"""Usage: %prog -r RESOLUTION NETCDF_FILE

  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage)

    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")
    parser.add_option("-o", "--output", dest="output", default=None,
                      help="name of the model that produced NETCDF_FILE")
    parser.add_option("-s", "--ncdir", dest="ncdir", default=".",
                      help=u"directory where NETCDF_FILE is stored (default: '.')")
    parser.add_option("-r", "--resolution", dest="resolution", default=None,
                      help=u"grid resolution (∆x) for NETCDF_FILE")
    parser.add_option("-m", "--model", dest="model", default="MODEL",
                      help=u"model name")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    if options.debug:
        log.setLevel(DEBUG)

    if options.resolution is None:
        log.error("You must provide a grid resolution.")
        parser.print_help()
        sys.exit(-1)

    if options.output is None:
        output = "rmse_{0}_{1}km.txt".format(options.model, options.resolution)
    else:
        output = options.output

    ncfiles = []
    for ncfile in args:
        ncfiles.append(os.path.join(options.ncdir, ncfile))

    archive = pycomodo.Archive(ncfiles)
    calc_rmse(archive, output, options.resolution)
