#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os, re

import numpy as np
import matplotlib.pyplot as plt
from optparse import OptionParser
from logging  import DEBUG
from pycomodo import log
from operator import itemgetter

class RMSRecord:

    def __init__(self, filename):
        self.model, self.resolution, self.modif = self._interpret_filename(filename)
        self.time = []
        self.rmse = []
        self.bias = []

    def _interpret_filename(self, filename):
        """ rmse_MARS_05km.txt -> |MARS|, |5 km|"""

        bname = os.path.basename(filename)
        r = re.match("rmse_([a-zA-Z0-9_-]*)_([a-zA-Z0-9]*)km(.*).txt", bname)
        model, resolution, modif = r.groups()
        return model, float(resolution), modif

    def add_data(self, string_record):
        _float_record = [float(item) for item in string_record.strip().split(" ") if item != '']
        self.time.append(_float_record[0])
        self.rmse.append(100.*_float_record[1])
        self.bias.append(_float_record[2])

    def get_label(self):
        if self.modif != '':
            label = "{0} - {1} km ({2})".format(self.model, self.resolution, self.modif)
        else:
            label = "{0} - {1} km".format(self.model, self.resolution)
        return label

class RMSModel:

    def __init__(self, model):
        self.model = model
        self.resolution = []
        self.rmse = []
        self.bias = []

    def add_data(self, record):
        self.resolution.append(record.resolution)
        self.rmse.append(record.rmse)
        self.bias.append(record.bias)


def plot_rmse(rms_files, plotx=True):

    rms_data = {}

    for rms_file in rms_files:

        rmsrec = RMSRecord(rms_file)
        with open(rms_file, "r") as f:
            rmsrec.add_data(list(f)[-1])

        if rmsrec.modif != '':
            modelkey = "{0} {1}".format(rmsrec.model, rmsrec.modif)
        else:
            modelkey = rmsrec.model

        if modelkey not in rms_data:
            rms_data[modelkey] = RMSModel(modelkey)

        rms_data[modelkey].add_data(rmsrec)

    fig1 = plt.figure(1)
    fig2 = plt.figure(2)
    ax1 = fig1.add_axes([0.1,0.1,0.8,0.8])
    ax2 = fig2.add_axes([0.1,0.1,0.8,0.8])

    for modelkey in rms_data:
        rmsmodel = rms_data[modelkey]
        ax1.plot(rmsmodel.resolution, rmsmodel.rmse, 'o-', label=modelkey, ms=8, lw=2.5)
        ax2.plot(rmsmodel.resolution, rmsmodel.bias, 'o-', label=modelkey, ms=8, lw=2.5)

    handles, labels = ax2.get_legend_handles_labels()
    hl = sorted(zip(handles, labels), key=itemgetter(1))
    handles, labels = zip(*hl)
    ax1.set_xlabel("Resolution [km]", size='x-large')
    ax2.set_xlabel("Resolution [km]", size='x-large')
    ax1.set_ylabel("RMSE [cm]", size='x-large')
    ax2.set_ylabel("Preservation of initial vortex [%]", size='x-large')

    ax1.legend(handles, labels, loc=1, fontsize=12)
    ax2.legend(handles, labels, loc=4, fontsize=12)
    ax1.set_xlim(35.,0.)
    ax2.set_xlim(35.,0.)
    ax1.set_ylim(0.,12)
    ax2.set_ylim(30.,70.)
    ax1.set_xticks(range(0,36,5))
    ax2.set_xticks(range(0,36,5))
    ax1.autoscale_view(tight=True)
    ax2.autoscale_view(tight=True)

    labels = ax1.get_xticklabels() + ax2.get_xticklabels() + \
             ax1.get_yticklabels() + ax2.get_yticklabels()

    for label in labels:
        label.set_fontsize('large')

    if plotx:
        plt.show()
    else:
        ax1.set_title("RMSE at t=99 days", fontsize='xx-large', family='serif')
        log("=== Writing rmse.png ...")
        fig1.savefig('rmse', dpi=140, bbox_inches='tight')
        ax2.set_title("Preservation of initial vortex at t=99 days", fontsize='xx-large', family='serif')
        log("=== Writing bias.png ...")
        fig2.savefig('bias', dpi=140, bbox_inches='tight')


if __name__ == "__main__":

    usage = "Usage: %prog RMS_FILE\n  RMS_FILE: Name of a text file with 3 columns: |time rms biais|"

    parser = OptionParser(usage)

    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")
    parser.add_option("-x", "--x-window", action="store_true", dest="plotx", default=False,
                      help="plot in a window instead of PNG file.")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    if options.debug:
        log.setLevel(DEBUG)

    plot_rmse(args, options.plotx)
