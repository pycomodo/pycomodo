Diagnostiques pour le cas-test "vortex barocline"
-------------------------------------------------

Ce dossier contient des scripts Python permettant de produire des diagnostiques pour le
cas-test "vortex barocline".

Au préalable, il est nécessaire d'avoir une installation fonctionnelle de la librairie pycomodo.
Les instructions sont détaillées sur cette page :

http://pycomodo.forge.imag.fr/api/installation.html

Dans la suite, nous supposons que les fichiers de sortie du modèle s'appellent 'ncfile_30km.nc',
'ncfile_20km.nc', etc. Si les variables sont réparties dans plusieurs fichiers (ex: un fichier par
pas de temps, un fichier séparé pour la définition de la grille, etc.), il suffit d'appeler les
scripts avec la liste de tous les fichiers concernés ('grille.nc ncfile_t1.nc ncfile_t2.nc etc.').

Les scripts dont le nom commence par 'plot_' créent un ou plusieurs fichiers PNG.
Ceux commençant par 'calc_' produisent des fichiers au format texte.

Pour produire la figure de la SSH à l'instant final avec la trajectoire :

$ ./plot_vortex.py -m MON_MODEL -r '30 km' ncfile_30km.nc

Pour faire les plots de RMSE et BIAS en fonction de la résolution :

$ ./calc_rmse.py -m MON_MODEL -r 30 ncfile_30km.nc
$ ./calc_rmse.py -m MON_MODEL -r 20 ncfile_20km.nc
$ ./calc_rmse.py -m MON_MODEL -r 10 ncfile_10km.nc
$ ./plot_rmse.py rmse_MON_MODEL_*.txt

Pour faire le plot de l'intensité du vortex en fonction du temps :

$ ./calc_trace.py -m MON_MODEL -r 30 ncfile_30km.nc
$ ./calc_trace.py -m MON_MODEL -r 20 ncfile_20km.nc
$ ./calc_trace.py -m MON_MODEL -r 10 ncfile_10km.nc
$ ./plot_trace.py -t "MON MODEL" -p RES trace_MON_MODEL_*.txt

Pour faire les plots des coordonnées X,Y du centre du vortex en fonction du temps :

$ ./plot_xy -t "MON MODEL" trace_MON_MODEL_*.txt
