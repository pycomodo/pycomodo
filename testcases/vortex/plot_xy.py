#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import re

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

from optparse import OptionParser
import pycomodo.util.plots as pup
from operator import itemgetter
from collections import defaultdict
from logging  import DEBUG
from pycomodo import log

class TraceRecord:

    def __init__(self, filename):
        self.model, self.resolution, self.modif = self._interpret_filename(filename)
        self.t = []
        self.x = []
        self.y = []
        self.z = []

    def _interpret_filename(self, filename):
        """ rms_MARS_05km.txt -> |MARS|, |5 km|"""

        bname = os.path.basename(filename)
        r = re.match("trace_([a-zA-Z0-9_-]*)_([a-zA-Z0-9]*)km(.*).txt", bname)
        model, resolution, modif = r.groups()
        return model, int(resolution), modif

    def add_data(self, string_record):
        _float_record = [float(item) for item in string_record.strip().split(" ") if item != '']
        self.t.append(_float_record[0])
        self.x.append(_float_record[1])
        self.y.append(_float_record[2])
        self.z.append(100.*_float_record[3])

    def get_label(self):
        if self.modif != '':
            label = "{0} - {1} km ({2})".format(self.model, self.resolution, self.modif)
        else:
            label = "{0} - {1} km".format(self.model, self.resolution)
        return label


def plot_trace(trace_files, plot_type="RES", title=None, plotx=True):

    if plot_type not in ["MODELS", "RES"]:
        print "## Error : unknown plot_type: {0}".format(plot_type)
        sys.exit(1)

    line_style_x = defaultdict(lambda: '-')
    line_style_y = defaultdict(lambda: '--')
    line_color_x = defaultdict(lambda: 'k')
    line_color_y = defaultdict(lambda: 'r')

    if   plot_type == 'MODELS':
        line_color_x["NEMO"]  = line_color_y["NEMO"]  = 'm'
        line_color_x["MARS"]  = line_color_y["MARS"]  = 'r'
        line_color_x["ROMS"]  = line_color_y["ROMS"]  = 'g'
        line_color_x["S"]     = line_color_y["S"]     = 'b'
        line_color_x["HYCOM"] = line_color_y["HYCOM"] = 'k'
    elif plot_type == 'RES':
        line_style_x[30]  = line_style_y[30]  = '-'
        line_style_x[20]  = line_style_y[20]  = '--'
        line_style_x[10]  = line_style_y[10]  = '-.'
        line_style_x[5]   = line_style_y[5]   = ':'

    resolutions = set()
    models = set()

    fig = plt.figure(1)
    ax  = fig.add_axes([0.1,0.1,0.8,0.8])

    for trace_file in trace_files:

        tracerec = TraceRecord(trace_file)
        with open(trace_file, "r") as f:
            for rec in f:
                tracerec.add_data(rec)

        if tracerec.modif != '':
            model_name = "{0} ({1})".format(tracerec.model, tracerec.modif)
        else:
            model_name = tracerec.model

        resolution_name = "{0} km".format(tracerec.resolution)

        resolutions.add(resolution_name)
        models.add(model_name)

        if   plot_type == 'MODELS':
            label = model_name
        elif plot_type == 'RES':
            label = resolution_name

        t = np.array(tracerec.t)
        x = np.array(tracerec.x)
        y = np.array(tracerec.y)

        ax.plot(t, x, lw=2., ls=line_style_x[tracerec.resolution], \
                 c=line_color_x[tracerec.model], label=label)
        ax.plot(t, y, lw=2., ls=line_style_y[tracerec.resolution], \
                 c=line_color_y[tracerec.model])
#         ax.plot(t, -np.sqrt(x*x+y*y), lw=2., ls=line_style_y[tracerec.resolution], \
#                  c=line_color_y[tracerec.model])

#     if   plot_type == 'MODELS':
#         plt.plot(t, -3*t, lw=.5, ls='-', c='k')
#         plt.plot(t, -5*t, lw=.5, ls='--', c='k')
#     elif plot_type == 'RES':
#         plt.plot(t, -3*t, lw=.5, ls='-', c='k')
#         plt.plot(t, -5*t, lw=.5, ls='-', c='r')

    ax.set_xlabel("Time [days]")
    ax.set_ylabel("Coordinate of vortex center [km]")
    ax.set_xlim(0,  100)
    ax.set_ylim(-500, 0)

    handles, labels = ax.get_legend_handles_labels()

    if plot_type == 'MODELS':
        hl = sorted(zip(handles, labels), key=itemgetter(1))
        handles, labels = zip(*hl)

    leg1 = ax.legend(handles, labels, fontsize=12, loc=1)

    if   plot_type == 'MODELS':
        h1 = Line2D([], [], lw=2., ls="-", c='k')
        h2 = Line2D([], [], lw=2., ls="--", c='k')
    elif plot_type == 'RES':
        h1 = Line2D([], [], lw=2., ls="-", c='k')
        h2 = Line2D([], [], lw=2., ls="-", c='r')
    leg2 = ax.legend([h1, h2], ["x", "y"], loc=3, handlelength=3)

    plt.gca().add_artist(leg1)
    plt.gca().add_artist(leg2)

    if title is not None:
        plt.title(title, fontsize='large', family='serif')

    if plotx:
        plt.show()
    else:
        log("=== Writing xy.png ...")
        plt.savefig('xy', dpi=100, bbox_inches='tight')


if __name__ == "__main__":

    usage = "Usage: %prog TRACE_FILE [TRACE_FILE]\n  TRACE_FILE: Name of a text file "+\
        "with 4 columns: |time x y z|"

    parser = OptionParser(usage)

    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")
    parser.add_option("-p", "--plot_type", dest="plot_type", default="RES",
                      help="type of plot [MODELS|RES]")
    parser.add_option("-t", "--title", dest="title", default=None, help="plot title (default: None)")
    parser.add_option("-x", "--x-window", action="store_true", dest="plotx", default=False,
                      help="plot in a window instead of PNG file.")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    if options.debug:
        log.setLevel(DEBUG)

    plot_trace(args, options.plot_type, options.title, options.plotx)
