#!/bin/bash
set -u

COMODO_DIR="${HOME}/Documents/COMODO"
NC_DIR="${COMODO_DIR}/cas_tests/vortex"
PY_DIR="${COMODO_DIR}/pycomodo/testcases/vortex"

LOGS_DIR="${NC_DIR}/logs"
PLOT_DIR="${NC_DIR}/plots"
TRAJ_DIR="${PLOT_DIR}/trajectories"
RMSE_DIR="${PLOT_DIR}/rmse"
TRACE_DIR="${PLOT_DIR}/traces"

RUN_TRAJ=1
RUN_RMSE=1
RUN_TRACE=1

RUN_ROMS=1
RUN_MARS=1
RUN_NEMO=1
RUN_S=1
RUN_HYCOM=1

RECOMPUTE=1

cr="\033[0;31m" 
cg="\033[0;32m" 
ce="\033[0m"

function plot_vortex()
{
	model=${1}
	resdx=${2}
	cdf_dir=${3}
	cdf_files=${4}
	case_modif=${5:-""}
	
	resdx2=$(echo ${resdx}|sed -e 's/[ \t ]//g')
	png_file="vortex_${model}_${resdx2}${case_modif}.png"
	log_file="${LOGS_DIR}/$(basename ${png_file} .png).log"

	echo -n " . creating ${png_file}... "
	cd ${TRAJ_DIR}
	${PY_DIR}/plot_vortex.py ${cdf_files} --ncdir "${cdf_dir}" --model "${model}" --resolution "${resdx}" >& ${log_file}
	if (( $? )) ; then
		echo -e "${cr}[KO]${ce} - no file created, see log file ${log_file}"
		return
	fi
	mv vortex.png ${png_file} >> ${log_file} 2>&1
	if (( $? )) ; then
		echo -e "${cr}[KO]${ce} - no file created, see log file ${log_file}"
		return
	fi
	echo -e "${cg}[OK]${ce}"
}

function calc_rmse()
{
	model=${1}
	resdx=${2}
	cdf_dir=${3}
	cdf_files=${4}
	case_modif=${5:-""}
	
	txt_file="rmse_${model}_${resdx}km${case_modif}.txt"
	log_file="${LOGS_DIR}/$(basename ${txt_file} .txt).log"

	echo -n " . creating ${txt_file}... "
	cd ${RMSE_DIR}
	
	${PY_DIR}/calc_rmse.py --ncdir "${cdf_dir}" --resolution "${resdx}" --output "${txt_file}" \
		${cdf_files} >& ${log_file}
	if (( $? )) ; then
		echo -e "${cr}[KO]${ce} - no RMSE computed, see log file ${log_file}"
		return
	fi
	echo -e "${cg}[OK]${ce}"
}

function plot_rmse()
{
	model=${1}
	case_modif=${2:-""}

	cd ${RMSE_DIR}
	
	if [[ ${model} == "ALL" ]] ; then
		rmse_files=$(find . -name "rmse_*km*.txt")
	else
		rmse_files=$(find . -name "rmse_${model}_*km${case_modif}.txt")
	fi
	png_file_rmse="rmse_${model}${case_modif}.png"
	png_file_bias="bias_${model}${case_modif}.png"
	log_file="${LOGS_DIR}/plot_rmse_${model}${case_modif}.log"
	
	echo -n " . creating ${png_file_rmse} and ${png_file_bias}..."
	
	${PY_DIR}/plot_rmse.py ${rmse_files} >& ${log_file}
	if (( $? )) ; then
		echo -e "${cr}[KO]${ce} - no RMSE plotted, see log file ${log_file}"
		return
	fi
	mv rmse.png ${png_file_rmse} >> ${log_file} 2>&1
	if (( $? )) ; then
		echo -e "${cr}[KO]${ce} - no RMSE plot created, see log file ${log_file}"
		return
	fi
	mv bias.png ${png_file_bias} >> ${log_file} 2>&1
	if (( $? )) ; then
		echo -e "${cr}[KO]${ce} - no BIAS plot created, see log file ${log_file}"
		return
	fi
	echo -e "${cg}[OK]${ce}"
}

function calc_trace()
{
	model=${1}
	resdx=${2}
	cdf_dir=${3}
	cdf_files=${4}
	case_modif=${5:-""}
	
	txt_file="trace_${model}_${resdx}km${case_modif}.txt"
	log_file="${LOGS_DIR}/$(basename ${txt_file} .txt).log"

	echo -n " . creating ${txt_file}... "
	cd ${TRACE_DIR}
	
	${PY_DIR}/calc_trace.py --ncdir "${cdf_dir}" --resolution "${resdx}" --output "${txt_file}" \
		${cdf_files} >& ${log_file}
	if (( $? )) ; then
		echo -e "${cr}[KO]${ce} - no trace computed, see log file ${log_file}"
		return
	fi
	echo -e "${cg}[OK]${ce}"
}

function plot_trace()
{
	model=${1}
	resdx=${2}
	case_modif=${3:-""}
	
	cd ${TRACE_DIR}

	if [[ ${model} == "ALL" ]] ; then
		trace_files=$(find . -name "trace_*_${resdx}km*.txt")
		plot_type="MODELS"
		title="Elevation at vortex center (dx=${resdx} km)"
		png_file="trace_${resdx}.png"
	else
		trace_files=$(find . -name "trace_${model}_*km${case_modif}*.txt")
		plot_type="RES"
		title="${model}${case_modif}: elevation at vortex center"
		png_file="trace_${model}${case_modif}.png"
	fi
	log_file="${LOGS_DIR}/$(basename ${png_file} .png).log"

	echo -n " . creating ${png_file}..."

	${PY_DIR}/plot_trace.py -p ${plot_type} -t "${title}" ${trace_files} >& ${log_file}
	if (( $? )) ; then
		echo -e "${cr}[KO]${ce} - no plot created, see log file ${log_file}"
		return
	fi
	mv trace.png ${png_file} >> ${log_file} 2>&1
	if (( $? )) ; then
		echo -e "${cr}[KO]${ce} - no plot created, see log file ${log_file}"
		return
	fi
	echo -e "${cg}[OK]${ce}"
}

function plot_xy()
{
	model=${1}
	resdx=${2}
	case_modif=${3:-""}
	
	cd ${TRACE_DIR}

	if [[ ${model} == "ALL" ]] ; then
		trace_files=$(find . -name "trace_*_${resdx}km*.txt")
		plot_type="MODELS"
		title="Coordinates of vortex center (dx=${resdx} km)"
		png_file="xy_${resdx}.png"
	else
		trace_files=$(find . -name "trace_${model}_*km${case_modif}*.txt")
		plot_type="RES"
		title="${model}${case_modif}: coordinates of vortex center"
		png_file="xy_${model}${case_modif}.png"
	fi
	log_file="${LOGS_DIR}/$(basename ${png_file} .png)_xy.log"

	echo -n " . creating ${png_file}..."

	${PY_DIR}/plot_xy.py -p ${plot_type} -t "${title}" ${trace_files} >& ${log_file}
	if (( $? )) ; then
		echo -e "${cr}[KO]${ce} - no plot created, see log file ${log_file}"
		return
	fi
	mv xy.png ${png_file} >> ${log_file} 2>&1
	if (( $? )) ; then
		echo -e "${cr}[KO]${ce} - no plot created, see log file ${log_file}"
		return
	fi
	echo -e "${cg}[OK]${ce}"
}


mkdir -p ${LOGS_DIR}

if (( ${RUN_TRAJ} )) ; then
	echo "================================"
	echo "=  Plot vortex trajectories     "
	echo "================================"
	mkdir -p ${TRAJ_DIR}

	rm -f ${TRAJ_DIR}/*.png

	if (( ${RUN_ROMS} )) ; then
		plot_vortex ROMS "30 km - c4 smag" "${NC_DIR}/ROMS/" "vortex_his30_C4smag.nc"
		plot_vortex ROMS "30 km - c4 vis0" "${NC_DIR}/ROMS/" "vortex_his30_C4visc0.nc"
		plot_vortex ROMS "05 km - c4 vis0" "${NC_DIR}/ROMS/" "vortex_his05_C4visc0.nc"
		plot_vortex ROMS "30 km" "${NC_DIR}/ROMS/" "vortex_his30.nc"
		plot_vortex ROMS "20 km" "${NC_DIR}/ROMS/" "vortex_his20.nc"
		plot_vortex ROMS "10 km" "${NC_DIR}/ROMS/" "vortex_his10.nc"
		plot_vortex ROMS "05 km" "${NC_DIR}/ROMS/" "vortex_his05.nc"
	fi
	if (( ${RUN_S} )) ; then
		plot_vortex S "30 km - exp1" "${NC_DIR}/S" "grid_30km.nc vortex_S_30km.nc"
		plot_vortex S "10 km - exp1" "${NC_DIR}/S" "grid_10km.nc vortex_S_10km.nc"
		plot_vortex S "05 km - exp1" "${NC_DIR}/S" "grid_05km.nc vortex_S_05km.nc"
		plot_vortex S "30 km - exp2" "${NC_DIR}/S" "grid_30km.nc vortex_S_30km_up3.nc"
		plot_vortex S "10 km - exp2" "${NC_DIR}/S" "grid_10km.nc vortex_S_10km_up3.nc"
		plot_vortex S "05 km - exp2" "${NC_DIR}/S" "grid_05km.nc vortex_S_05km_up3.nc"
	fi
	if (( ${RUN_MARS} )) ; then
		plot_vortex MARS "30 km" "${NC_DIR}/MARS/" "vortex_30km_mars_nc3.nc"
		plot_vortex MARS "20 km" "${NC_DIR}/MARS/" "vortex_20km_mars_nc3.nc"
		plot_vortex MARS "10 km" "${NC_DIR}/MARS/" "vortex_10km_mars_nc3.nc"
		plot_vortex MARS "05 km" "${NC_DIR}/MARS/" "vortex_05km_mars_nc3.nc"
	fi
	if (( ${RUN_NEMO} )) ; then
		plot_vortex NEMO "30 km" "${NC_DIR}/NEMO/" "vortex_30km_nemo.nc"
		plot_vortex NEMO "20 km" "${NC_DIR}/NEMO/" "vortex_20km_nemo.nc"
		plot_vortex NEMO "10 km" "${NC_DIR}/NEMO/" "vortex_10km_nemo.nc"
		plot_vortex NEMO "05 km" "${NC_DIR}/NEMO/" "vortex_05km_nemo.nc"
	fi
	if (( ${RUN_HYCOM} )) ; then
		plot_vortex "HYCOM-z"   "30 km" "${NC_DIR}/HYCOM/" "hycom-vortex-30km-z-b0.05.nc"
		plot_vortex "HYCOM-z"   "20 km" "${NC_DIR}/HYCOM/" "hycom-vortex-20km-z-b0.05.nc"
		plot_vortex "HYCOM-z"   "10 km" "${NC_DIR}/HYCOM/" "hycom-vortex-10km-z-b0.05.nc"
		plot_vortex "HYCOM-z"   "05 km" "${NC_DIR}/HYCOM/" "hycom-vortex-05km-z-b0.05.nc"
		plot_vortex "HYCOM-iso" "30 km" "${NC_DIR}/HYCOM/" "hycom-vortex-30km-iso-b0.05.nc"
		plot_vortex "HYCOM-iso" "20 km" "${NC_DIR}/HYCOM/" "hycom-vortex-20km-iso-b0.05.nc"
		plot_vortex "HYCOM-iso" "10 km" "${NC_DIR}/HYCOM/" "hycom-vortex-10km-iso-b0.05.nc"
		plot_vortex "HYCOM-iso" "05 km" "${NC_DIR}/HYCOM/" "hycom-vortex-05km-iso-b0.05.nc"
	fi
fi

if (( ${RUN_RMSE} )) ; then
	echo "================================"
	echo "=  Compute & plot RMSE          "
	echo "================================"
	mkdir -p ${RMSE_DIR}

	if (( ${RECOMPUTE} )) ; then
		rm -f ${RMSE_DIR}/*.txt
		rm -f ${RMSE_DIR}/*.png
		if (( ${RUN_ROMS} )) ; then
			calc_rmse ROMS 30 "${NC_DIR}/ROMS/" "vortex_his30_C4smag.nc" "-c4-smag"
			calc_rmse ROMS 30 "${NC_DIR}/ROMS/" "vortex_his30_C4visc0.nc" "-c4-vis0"
			calc_rmse ROMS 05 "${NC_DIR}/ROMS/" "vortex_his05_C4visc0.nc" "-c4-vis0"
			calc_rmse ROMS 30 "${NC_DIR}/ROMS/" "vortex_his30.nc"
			calc_rmse ROMS 20 "${NC_DIR}/ROMS/" "vortex_his20.nc"
			calc_rmse ROMS 10 "${NC_DIR}/ROMS/" "vortex_his10.nc"
			calc_rmse ROMS 05 "${NC_DIR}/ROMS/" "vortex_his05.nc"
		fi
		if (( ${RUN_S} )) ; then
			calc_rmse S 30 "${NC_DIR}/S" "grid_30km.nc vortex_S_30km.nc" "-exp1"
			calc_rmse S 10 "${NC_DIR}/S" "grid_10km.nc vortex_S_10km.nc" "-exp1"
			calc_rmse S 05 "${NC_DIR}/S" "grid_05km.nc vortex_S_05km.nc" "-exp1"
			calc_rmse S 30 "${NC_DIR}/S" "grid_30km.nc vortex_S_30km_up3.nc" "-exp2"
			calc_rmse S 10 "${NC_DIR}/S" "grid_10km.nc vortex_S_10km_up3.nc" "-exp2"
			calc_rmse S 05 "${NC_DIR}/S" "grid_05km.nc vortex_S_05km_up3.nc" "-exp2"
		fi
		if (( ${RUN_MARS} )) ; then
			calc_rmse MARS 30 "${NC_DIR}/MARS/" "vortex_30km_mars_nc3.nc"
			calc_rmse MARS 20 "${NC_DIR}/MARS/" "vortex_20km_mars_nc3.nc"
			calc_rmse MARS 10 "${NC_DIR}/MARS/" "vortex_10km_mars_nc3.nc"
			calc_rmse MARS 05 "${NC_DIR}/MARS/" "vortex_05km_mars_nc3.nc"
		fi
		if (( ${RUN_HYCOM} )) ; then
			calc_rmse "HYCOM-z"   30 "${NC_DIR}/HYCOM/" "hycom-vortex-30km-z-b0.05.nc"
			calc_rmse "HYCOM-z"   20 "${NC_DIR}/HYCOM/" "hycom-vortex-20km-z-b0.05.nc"
			calc_rmse "HYCOM-z"   10 "${NC_DIR}/HYCOM/" "hycom-vortex-10km-z-b0.05.nc"
			calc_rmse "HYCOM-z"   05 "${NC_DIR}/HYCOM/" "hycom-vortex-05km-z-b0.05.nc"
# 			calc_rmse "HYCOM-iso" 30 "${NC_DIR}/HYCOM/" "hycom-vortex-30km-iso-b0.05.nc"
# 			calc_rmse "HYCOM-iso" 20 "${NC_DIR}/HYCOM/" "hycom-vortex-20km-iso-b0.05.nc"
# 			calc_rmse "HYCOM-iso" 10 "${NC_DIR}/HYCOM/" "hycom-vortex-10km-iso-b0.05.nc"
# 			calc_rmse "HYCOM-iso" 05 "${NC_DIR}/HYCOM/" "hycom-vortex-05km-iso-b0.05.nc"
		fi
		if (( ${RUN_NEMO} )) ; then
			calc_rmse NEMO 30 "${NC_DIR}/NEMO/" "vortex_30km_nemo.nc"
			calc_rmse NEMO 20 "${NC_DIR}/NEMO/" "vortex_20km_nemo.nc"
			calc_rmse NEMO 10 "${NC_DIR}/NEMO/" "vortex_10km_nemo.nc"
			calc_rmse NEMO 05 "${NC_DIR}/NEMO/" "vortex_05km_nemo.nc"
		fi
	fi
	plot_rmse ALL
fi
if (( ${RUN_TRACE} )) ; then
	echo "================================"
	echo "=  Compute & plot traces        "
	echo "================================"
	mkdir -p ${TRACE_DIR}

	rm -f ${TRACE_DIR}/trace_*.txt
	rm -f ${TRACE_DIR}/trace_*.png
	rm -f ${TRACE_DIR}/xy_*.png

	if (( ${RUN_ROMS} )) ; then
 		calc_trace ROMS 30 "${NC_DIR}/ROMS/" "vortex_his30.nc"
		calc_trace ROMS 20 "${NC_DIR}/ROMS/" "vortex_his20.nc"
		calc_trace ROMS 10 "${NC_DIR}/ROMS/" "vortex_his10.nc"
		calc_trace ROMS 05 "${NC_DIR}/ROMS/" "vortex_his05.nc"
		plot_trace ROMS ALL
		plot_xy    ROMS ALL
	fi
	if (( ${RUN_S} )) ; then
		calc_trace S 30 "${NC_DIR}/S/" "grid_30km.nc vortex_S_30km.nc" ""
		calc_trace S 10 "${NC_DIR}/S/" "grid_10km.nc vortex_S_10km.nc" ""
		calc_trace S 05 "${NC_DIR}/S/" "grid_05km.nc vortex_S_05km.nc" ""
# 		calc_trace S 30 "${NC_DIR}/S/" "grid_30km.nc vortex_S_30km_up3.nc" "-exp2"
# 		calc_trace S 10 "${NC_DIR}/S/" "grid_10km.nc vortex_S_10km_up3.nc" "-exp2"
# 		calc_trace S 05 "${NC_DIR}/S/" "grid_05km.nc vortex_S_05km_up3.nc" "-exp2"
		plot_trace S ALL
		plot_xy    S ALL
	fi
	if (( ${RUN_MARS} )) ; then
		calc_trace MARS 30 "${NC_DIR}/MARS/" "vortex_30km_mars_nc3.nc"
		calc_trace MARS 20 "${NC_DIR}/MARS/" "vortex_20km_mars_nc3.nc"
		calc_trace MARS 10 "${NC_DIR}/MARS/" "vortex_10km_mars_nc3.nc"
		calc_trace MARS 05 "${NC_DIR}/MARS/" "vortex_05km_mars_nc3.nc"
		plot_trace MARS ALL
		plot_xy    MARS ALL
	fi
	if (( ${RUN_HYCOM} )) ; then
		calc_trace "HYCOM-z"   30 "${NC_DIR}/HYCOM/" "hycom-vortex-30km-z-b0.05.nc"
		calc_trace "HYCOM-z"   20 "${NC_DIR}/HYCOM/" "hycom-vortex-20km-z-b0.05.nc"
		calc_trace "HYCOM-z"   10 "${NC_DIR}/HYCOM/" "hycom-vortex-10km-z-b0.05.nc"
		calc_trace "HYCOM-z"   05 "${NC_DIR}/HYCOM/" "hycom-vortex-05km-z-b0.05.nc"
		plot_trace "HYCOM-z"   ALL
		plot_xy    "HYCOM-z"   ALL
# 		calc_trace "HYCOM-iso" 30 "${NC_DIR}/HYCOM/" "hycom-vortex-30km-iso-b0.05.nc"
# 		calc_trace "HYCOM-iso" 20 "${NC_DIR}/HYCOM/" "hycom-vortex-20km-iso-b0.05.nc"
# 		calc_trace "HYCOM-iso" 10 "${NC_DIR}/HYCOM/" "hycom-vortex-10km-iso-b0.05.nc"
# 		calc_trace "HYCOM-iso" 05 "${NC_DIR}/HYCOM/" "hycom-vortex-05km-iso-b0.05.nc"
# 		plot_trace "HYCOM-iso" ALL
# 		plot_xy    "HYCOM-iso" ALL
	fi
	if (( ${RUN_NEMO} )) ; then
		calc_trace NEMO 30 "${NC_DIR}/NEMO/" "vortex_30km_nemo.nc"
		calc_trace NEMO 20 "${NC_DIR}/NEMO/" "vortex_20km_nemo.nc"
		calc_trace NEMO 10 "${NC_DIR}/NEMO/" "vortex_10km_nemo.nc"
		calc_trace NEMO 05 "${NC_DIR}/NEMO/" "vortex_05km_nemo.nc"
		plot_trace NEMO ALL
		plot_xy    NEMO ALL
	fi

	plot_trace ALL 30
	plot_trace ALL 20
	plot_trace ALL 10
	plot_trace ALL 05
	plot_xy ALL 30
	plot_xy ALL 20
	plot_xy ALL 10
	plot_xy ALL 05
fi