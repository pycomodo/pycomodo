#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import numpy as np
from optparse import OptionParser
from logging import DEBUG
import matplotlib.pyplot as plt

import pycomodo
import pycomodo.util.variables as puv
from pycomodo import log, UnknownVariable

def calc_trace(archive, output, resolution):

    try:
        log("=== Get time :")
        time = archive.get_variable(stdname='time')
        time.check(True)

        log("=== Call get_ssh")
        ssh = puv.get_ssh(archive)
        ssh.check(True)

    except UnknownVariable as err:
        log.error("variable {0} not found in '{1}'".format(err, archive))
        return

    latlon_slice = ssh.get_dynamic_slice("X", "Y")
    ssh_m = np.ma.masked_equal(ssh[latlon_slice], ssh.get_fill_value())

    dims_map = {}
    for dim_name in ssh.dimensions:
        coord_var = archive.get_variable(dim_name)
        dims_map[coord_var.axis] = coord_var

    ib, ie = archive.get_dimension_dynamic_range(str(dims_map['X']))
    jb, je = archive.get_dimension_dynamic_range(str(dims_map['Y']))

    dx = 1800. / (ie-ib+1)
    dy = 1800. / (je-jb+1)

    Xrange = np.arange(-900.,901.,dx)
    Yrange = np.arange(-900.,901.,dy)
    X, Y = np.meshgrid(Xrange, Yrange)

    X_m = 0.25*(X[:-1,:-1]+X[:-1,1:]+X[1:,:-1]+X[1:,1:])
    Y_m = 0.25*(Y[:-1,:-1]+Y[:-1,1:]+Y[1:,:-1]+Y[1:,1:])
    cutoff = 0.8
    trace_array = []

    for k in range(len(time[:])):

        Tk = archive.get_time(k, 'days')
        Zk = ssh_m[k,...].max()
        jm, im = np.nonzero(ssh_m[k,...]>cutoff*Zk)
        w = 1 - np.abs(ssh_m[k,jm,im]-Zk)/((1-cutoff)*Zk)

        Xw = np.average(X_m[jm, im], weights=w)
        Yw = np.average(Y_m[jm, im], weights=w)

        trace_array.append("{0:5.1f} {1:.4e} {2:.4e} {3:.4e}\n".format(Tk, Xw, Yw, Zk))

#     fig0 = plt.figure(0)
#     csi0 = plt.pcolor(Ziw, cmap=plt.cm.bwr, vmin=cmin, vmax=cmax)
#     cbar0= fig0.colorbar(csi0, orientation='vertical')
#     fig1 = plt.figure(1)
#     csi1 = plt.pcolor(Zk[slice_w], cmap=plt.cm.bwr, vmin=cmin, vmax=cmax)
#     cbar1= fig1.colorbar(csi1, orientation='vertical')
#     absmax = np.abs(err).max()
#     fig2 = plt.figure(2)
#     csi2 = plt.pcolor(err, cmap=plt.cm.bwr, vmin=-absmax, vmax=absmax)
#     cbar2= fig2.colorbar(csi2, orientation='vertical')
#     plt.show()

    with open(output, "w") as f:
        log("=== Writing '{0}' ...".format(output))
        f.write("".join(trace_array))

if __name__ == "__main__":

    usage = """Usage: %prog -r RESOLUTION NETCDF_FILE

  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage)

    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")
    parser.add_option("-o", "--output", dest="output", default=None,
                      help="name of the model that produced NETCDF_FILE")
    parser.add_option("-s", "--ncdir", dest="ncdir", default=".",
                      help=u"directory where NETCDF_FILE is stored (default: '.')")
    parser.add_option("-r", "--resolution", dest="resolution", default=None,
                      help=u"grid resolution (∆x) for NETCDF_FILE")
    parser.add_option("-m", "--model", dest="model", default="MODEL",
                      help=u"model name")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    if options.debug:
        log.setLevel(DEBUG)

    if options.resolution is None:
        log.error("You must provide a grid resolution.")
        parser.print_help()
        sys.exit(-1)

    if options.output is None:
        output = "trace_{0}_{1}km.txt".format(options.model, options.resolution)
    else:
        output = options.output

    ncfiles = []
    for ncfile in args:
        ncfiles.append(os.path.join(options.ncdir, ncfile))

    archive = pycomodo.Archive(ncfiles)
    calc_trace(archive, output, options.resolution)
