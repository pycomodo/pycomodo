#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import logging
import pycomodo
import optparse

if __name__ == "__main__":

    usage = """
    %prog [options] [-g GRID_FILE]... NETCDF_FILE [NETCDF_FILE]...

      - GRID_FILE:   Name of a NetCDF grid file, ie. with an empty record dimension, if any.
      - NETCDF_FILE: Name of a NetCDF data file with a record dimension.

    All NETCDF_FILE must have exactly the same structure: same dimensions, same variables.
    They are supposed to be samples of a unique output split along the record dimension.

    Check if a series of NetCDF files conforms to the COMODO norm. All files passed in argument
    are considered as a single archive.

    For more information, see: http://pycomodo.forge.imag.fr/norm.html """

    formatter = optparse.IndentedHelpFormatter(max_help_position=60, width=120)
    parser = optparse.OptionParser(usage, version="%prog {0}".format(pycomodo.__version__),
                                          formatter=formatter)
    parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False,
                      help="outputs more detailed messages")
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")
    parser.add_option("-g", "--grid", action="append", dest="gridfile", default=None,
                      help="use GRIDFILE as a source of static grid data")
    parser.add_option("-c", "--multifile", action="store_true", dest="multifile", default=False,
                      help="all data files are considered as a single one concatenated "
                           "by the record dimension")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    if options.debug:
        pycomodo.log.setLevel(logging.DEBUG)

    arch = pycomodo.Archive(args, grids=options.gridfile, multifile=options.multifile)
    arch.check(options.verbose)
