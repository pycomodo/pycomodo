#!/bin/bash
set -u
#set -v

# convert NEMO file to COMODO format
#
# use    : ./nemo2comodo.bash file_gridX.nc 
# output :  file-COMODO_gridX.nc

time_variable="time_counter"

##########################################################################################################
# FUNCTION DEFINITIONS
##########################################################################################################

#---------------------------------------------------------------------------------------------------------
# get_mesh_size
#---------------------------------------------------------------------------------------------------------
function get_mesh_size
{
	local fname=$1
	
	local re_t="[[:space:]]([a-zA-Z0-9_]+) = UNLIMITED ;"
	local re_x="[[:space:]](x[_tuvwf]*) = ([0-9]+) ;"
	local re_y="[[:space:]](y[_tuvwf]*) = ([0-9]+) ;"
	local re_z="[[:space:]](z[_tuvwf]*|depth[tuvwf]) = ([0-9]+) ;"
	local ncdump=$(ncdump -h ${fname})
	
	xsize="" ; ysize="" ; zsize=""
	xname="" ; yname="" ; zname="" ; tname=""
	
	if [[ ${ncdump} =~ ${re_t} ]] ; then
		tname="${BASH_REMATCH[1]}"
	fi
	if [[ ${ncdump} =~ ${re_x} ]] ; then
		xname="${BASH_REMATCH[1]}"
		xsize="${BASH_REMATCH[2]}"
	fi
	if [[ ${ncdump} =~ ${re_y} ]] ; then
		yname="${BASH_REMATCH[1]}"
		ysize="${BASH_REMATCH[2]}"
	fi
	if [[ ${ncdump} =~ ${re_z} ]] ; then
		zname="${BASH_REMATCH[1]}"
		zsize="${BASH_REMATCH[2]}"
	fi
}

#---------------------------------------------------------------------------------------------------------
# set_std_names
#---------------------------------------------------------------------------------------------------------
function set_std_names
{
	local infile=${1}
	
	local ncdump=$(ncdump -h ${infile})
	local re_toce=" (toce|thetao|votemper|ivotemper)\("
	local re_soce=" (soce|so|vosaline|ivosaline)\("
	local re_uoce=" (uoce|uo|vozocrtx)\("
	local re_voce=" (voce|vo|vomecrty)\("
	local re_woce=" (woce|wo|vovecrtz)\("
	local  re_sst=" (tos|sst|sosstsst|isosstsst)\("
	local  re_ssh=" (zos|ssh|sossheig|isossheig)\("
	local  re_sss=" (sos|sss|sosaline|isosaline)\("
	local  re_avt=" (avt|difvho|votkeavt)\("
	local  re_avs=" (avs|difvso|voddmavs)\("
	local  re_avm=" (avm|difvmo|votkeavm)\("
	local  re_lat=" (nav_lat_[tuvwf])\("
	local  re_lon=" (nav_lon_[tuvwf])\("
	local re_dept=" (depth[tuvwf])\("

	# For budget terms
	local re_u_had=" (u_had)\("
	local re_v_had=" (v_had)\("
	local re_u_hpg=" (u_hpg)\("
	local re_v_hpg=" (v_hpg)\("
	local re_u_spg=" (u_spg)\("
	local re_v_spg=" (v_spg)\("
	local re_u_ldf=" (u_ldf)\("
	local re_v_ldf=" (v_ldf)\("
	local re_u_zad=" (u_zad)\("
	local re_v_zad=" (v_zad)\("
	local re_u_zdf=" (u_zdf)\("
	local re_v_zdf=" (v_zdf)\("
	
	local stdcmd=""
	[[ ${ncdump} =~ ${re_lat}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,latitude""${atloc}"
	[[ ${ncdump} =~ ${re_lon}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,longitude""${atloc}"
	[[ ${ncdump} =~ ${re_toce} ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,sea_water_potential_temperature""${atloc}"
	[[ ${ncdump} =~ ${re_soce} ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,sea_water_salinity""${atloc}"
	[[ ${ncdump} =~ ${re_uoce} ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,sea_water_x_velocity""${atloc}"
	[[ ${ncdump} =~ ${re_voce} ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,sea_water_y_velocity""${atloc}"
	[[ ${ncdump} =~ ${re_woce} ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,upward_sea_water_velocity""${atloc}"
	[[ ${ncdump} =~ ${re_sst}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,sea_surface_temperature""${atloc}"
	[[ ${ncdump} =~ ${re_sss}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,sea_surface_salinity""${atloc}"
	[[ ${ncdump} =~ ${re_ssh}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,sea_surface_height_above_geoid""${atloc}"
	[[ ${ncdump} =~ ${re_avt}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,ocean_vertical_heat_diffusivity""${atloc}"
	[[ ${ncdump} =~ ${re_avs}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,ocean_vertical_salt_diffusivity""${atloc}"
	[[ ${ncdump} =~ ${re_avm}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,ocean_vertical_momentum_diffusivity""${atloc}"
	[[ ${ncdump} =~ ${re_dept} ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,model_level_number""${atloc}"

	[[ ${ncdump} =~ ${re_u_had}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,non_linear_advection_of_sea_water_x_velocity""${atloc}"
	[[ ${ncdump} =~ ${re_v_had}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,non_linear_advection_of_sea_water_y_velocity""${atloc}"
	[[ ${ncdump} =~ ${re_u_hpg}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,x_internal_pressure_gradient""${atloc}"
	[[ ${ncdump} =~ ${re_v_hpg}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,y_internal_pressure_gradient""${atloc}"
	[[ ${ncdump} =~ ${re_u_spg}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,x_external_pressure_gradient""${atloc}"
	[[ ${ncdump} =~ ${re_v_spg}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,y_external_pressure_gradient""${atloc}"
	[[ ${ncdump} =~ ${re_u_ldf}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,horizontal_diffusion_of_sea_water_x_velocity""${atloc}"
	[[ ${ncdump} =~ ${re_v_ldf}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,horizontal_diffusion_of_sea_water_y_velocity""${atloc}"
	[[ ${ncdump} =~ ${re_u_zad}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,upward_non_linear_advection_of_sea_water_x_velocity""${atloc}"
	[[ ${ncdump} =~ ${re_v_zad}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,upward_non_linear_advection_of_sea_water_y_velocity""${atloc}"
	[[ ${ncdump} =~ ${re_u_zdf}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,upward_diffusion_of_sea_water_x_velocity""${atloc}"
	[[ ${ncdump} =~ ${re_v_zdf}  ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},o,c,upward_diffusion_of_sea_water_y_velocity""${atloc}"

	# Also fix 'positive' attribute on depth
	[[ ${ncdump} =~ ${re_dept} ]] && stdcmd="${stdcmd} -a positive,${BASH_REMATCH[1]},m,c,down"

	[[ -n ${stdcmd} ]] && ncatted ${stdcmd} ${infile}
}

#---------------------------------------------------------------------------------------------------------
# set_std_names_mesh
#---------------------------------------------------------------------------------------------------------
function set_std_names_mesh
{
	local infile=${1}
	local vpos=${2}
	
	local ncdump=$(ncdump -h ${infile})

	local re_hdep=" (hdep[tuvwf])\("
	local re_glam=" (glam[tuvwf])\("
	local re_gphi=" (gphi[tuvwf])\("
	local   re_e1=" (e1[tuvwf])\("
	local   re_e2=" (e2[tuvwf])\("
	local   re_e3=" (e3[tuvwf])\("
	local    re_f=" (f[tuvwf])\("
	local re_bath=" (mbathy)\("
	local re_mask=" ([tuvwf]mask)\("

	[[ ${vpos} == "t" ]] && atloc="" || atloc="_at_${vpos}_location"

	local stdcmd=""
	[[ ${ncdump} =~ ${re_hdep} ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},c,c,sea_floor_depth""${atloc}"
# 	[[ ${ncdump} =~ ${re_glam} ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},c,c,longitude""${atloc}"
# 	[[ ${ncdump} =~ ${re_gphi} ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},c,c,latitude""${atloc}"
	[[ ${ncdump} =~ ${re_e1}   ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},c,c,cell_x_size""${atloc}"
	[[ ${ncdump} =~ ${re_e2}   ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},c,c,cell_y_size""${atloc}"
	[[ ${ncdump} =~ ${re_e3}   ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},c,c,cell_thickness""${atloc}"
	[[ ${ncdump} =~ ${re_f}    ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},c,c,coriolis_parameter""${atloc}"
	[[ ${ncdump} =~ ${re_bath} ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},c,c,model_level_number_at_sea_floor""${atloc}"
	[[ ${ncdump} =~ ${re_mask} ]] && stdcmd="${stdcmd} -a standard_name,${BASH_REMATCH[1]},c,c,sea_binary_mask""${atloc}"

	[[ -n ${stdcmd} ]] && ncatted ${stdcmd} ${infile}
}

#---------------------------------------------------------------------------------------------------------
# rename_dimensions
#---------------------------------------------------------------------------------------------------------
function rename_dimensions
{
	local fout=${1}
	local vpos=${2}
	
	get_mesh_size ${fout}

	local ncdump=$(ncdump -h ${fout})
	local xnewname="x_${vpos}"
	local ynewname="y_${vpos}"
	local znewname="depth${vpos}"
	local re_oldx=" (${xname})\("
	local re_oldy=" (${yname})\("
	local re_oldz=" (${zname})\("
	local nccmd=""
	
	# Rename dimensions
	[[ -n ${xname} && ${xname} != ${xnewname} ]] && nccmd="${nccmd} -d ${xname},${xnewname}"
	[[ -n ${yname} && ${yname} != ${ynewname} ]] && nccmd="${nccmd} -d ${yname},${ynewname}"
	[[ -n ${zname} && ${zname} != ${znewname} ]] && nccmd="${nccmd} -d ${zname},${znewname}"

	# Also rename dimension-variables
	[[ -n ${xname} && ${xname} != ${xnewname} && ${ncdump} =~ ${re_oldx} ]] && nccmd="${nccmd} -v ${xname},${xnewname}"
	[[ -n ${yname} && ${yname} != ${ynewname} && ${ncdump} =~ ${re_oldy} ]] && nccmd="${nccmd} -v ${yname},${ynewname}"
	[[ -n ${zname} && ${zname} != ${znewname} && ${ncdump} =~ ${re_oldz} ]] && nccmd="${nccmd} -v ${zname},${znewname}"

	# Rename time dimension if it is different from time variable
	[[ -n ${tname} && ${tname} != ${time_variable} ]] && nccmd="${nccmd} -d ${tname},${time_variable}"

	[[ -n ${nccmd} ]] && ncrename -O ${nccmd} ${fout}
}

#---------------------------------------------------------------------------------------------------------
# create_dimension_variables
#---------------------------------------------------------------------------------------------------------
function create_dimension_variables
{
	local foutput=${1}
	local vpos=${2}

	get_mesh_size ${foutput}

	local ncdump=$(ncdump -h ${foutput})
	local xnewname="x_${vpos}"
	local ynewname="y_${vpos}"
	local znewname="depth${vpos}"

	local nccmd=""
	local re_varx=" (${xnewname})\("
	local re_vary=" (${ynewname})\("
	local re_varz=" (${znewname})\("

	# Create dimension variables for x, y if necessary
	# (ie. if the dimension does exist but the variable doesn't)
	[[ -n ${xname} && ! ${ncdump} =~ ${re_varx} ]] && nccmd="${nccmd} ${xnewname}[\$${xnewname}] = 1.f ;"
	[[ -n ${yname} && ! ${ncdump} =~ ${re_vary} ]] && nccmd="${nccmd} ${ynewname}[\$${ynewname}] = 1.f ;"
	[[ -n ${nccmd} ]] && ncap2 -s "${nccmd}" ${foutput} 2> /dev/null

	# Add required attributes to the dimension variables
	[[ ${vpos} == "t" ]] && local atloc="" || local atloc="_at_${vpos}_location"
	local xdeb=2
	local ydeb=2
	local xend=$((xsize-1))
	local yend=$((ysize-1))
	local x_axis_shift=0.
	local y_axis_shift=0.
	local z_axis_shift=0.

	case ${vpos} in
		t)	;;
		u)	x_axis_shift=0.5
			(( ! ${PERIODIC_X} )) && xend=$((xsize-2))
			;;
		v)	y_axis_shift=0.5
			(( ! ${PERIODIC_Y} )) && yend=$((ysize-2))
			;;
		w)	z_axis_shift=-0.5
			;;
		f)	xend=$((xsize-2))
			yend=$((ysize-2))
			x_axis_shift=0.5
			y_axis_shift=0.5
			;;
	esac
	
	local x_axis_range="${xdeb}:${xend}"
	local y_axis_range="${ydeb}:${yend}"
	
	nc_cmd=""
	if [[ -n ${tname} ]] ; then
		nc_cmd="${nc_cmd}	-a axis,${time_variable},o,c,T							    \
							-a standard_name,${time_variable},o,c,time"
	fi
	if [[ -n ${xname} ]] ; then
		nc_cmd="${nc_cmd}	-a axis,${xnewname},o,c,X									\
							-a standard_name,${xnewname},o,c,x_grid_index${atloc}		\
			 				-a c_grid_axis_shift,${xnewname},o,f,${x_axis_shift}		\
			 				-a c_grid_dynamic_range,${xnewname},o,c,${x_axis_range}"
	fi
	if [[ -n ${yname} ]] ; then
		nc_cmd="${nc_cmd}	-a axis,${ynewname},o,c,Y									\
							-a standard_name,${ynewname},o,c,y_grid_index${atloc}		\
			 				-a c_grid_axis_shift,${ynewname},o,f,${y_axis_shift}		\
			 				-a c_grid_dynamic_range,${ynewname},o,c,${y_axis_range}"
	fi
	if [[ -n ${zname} && ${ncdump} =~ ${re_varz} ]] ; then
		nc_cmd="${nc_cmd}	-a axis,${znewname},o,c,Z									\
							-a standard_name,${znewname},o,c,model_level_number${atloc}	\
							-a c_grid_axis_shift,${znewname},o,f,${z_axis_shift}"
	fi
	
	[[ -n ${nc_cmd} ]] && ncatted ${nc_cmd} ${foutput}
}

#---------------------------------------------------------------------------------------------------------
# handle_mesh_file
#---------------------------------------------------------------------------------------------------------
function handle_mesh_file
{
	local dname=${1}
	local fname=${2}

	local foutput="${dname}/${fname}-COMODO.nc"
	local fout_temp="${dname}/${fname}-temp.nc"
	local foutput_t="${dname}/${fname}-temp_gridT.nc"
	local foutput_u="${dname}/${fname}-temp_gridU.nc"
	local foutput_v="${dname}/${fname}-temp_gridV.nc"
	local foutput_w="${dname}/${fname}-temp_gridW.nc"
	local foutput_f="${dname}/${fname}-temp_gridF.nc"

	echo " - copy input file..."
	if [[ -n ${tname} ]] ; then							# There is a record variable ...
		ncwa -O -a ${tname} ${FINPUT} ${fout_temp}		# ... so get rid of it.
	else
		ln -sf ${FINPUT} ${fout_temp}
	fi

	echo " - split variables into separate files..."

	local vars_t="(e1t|e2t|e3t|glamt|gphit|tmask|hdept|mbathy)"
	local vars_u="(e1u|e2u|e3u|glamu|gphiu|umask)"
	local vars_v="(e1v|e2v|e3v|glamv|gphiv|vmask)"
	local vars_f="(e1f|e2f|e3f|glamf|gphif|fmask|ff)"
	local vars_w="(e1w|e2w|e3w|glamw|gphiw|hdepw)"

	ncks -O -v ${vars_t} ${fout_temp} ${foutput_t} > /dev/null;
	ncks -O -v ${vars_u} ${fout_temp} ${foutput_u} > /dev/null;
	ncks -O -v ${vars_v} ${fout_temp} ${foutput_v} > /dev/null;
	ncks -O -v ${vars_w} ${fout_temp} ${foutput_w} > /dev/null;
	ncks -O -v ${vars_f} ${fout_temp} ${foutput_f} > /dev/null;
	rm -f ${fout_temp}

	local gridT_ok=$(( ! $(ncdump -h ${foutput_t}|grep variables: >&/dev/null ; echo $?) ))
	local gridU_ok=$(( ! $(ncdump -h ${foutput_u}|grep variables: >&/dev/null ; echo $?) )) 
	local gridV_ok=$(( ! $(ncdump -h ${foutput_v}|grep variables: >&/dev/null ; echo $?) ))
	local gridW_ok=$(( ! $(ncdump -h ${foutput_w}|grep variables: >&/dev/null ; echo $?) ))
	local gridF_ok=$(( ! $(ncdump -h ${foutput_f}|grep variables: >&/dev/null ; echo $?) ))

	(( ${gridT_ok} )) || rm -f ${foutput_t}
	(( ${gridU_ok} )) || rm -f ${foutput_u}
	(( ${gridV_ok} )) || rm -f ${foutput_v}
	(( ${gridW_ok} )) || rm -f ${foutput_w}
	(( ${gridF_ok} )) || rm -f ${foutput_f}

	echo " - rename dimensions..."
	(( ${gridT_ok} )) && rename_dimensions ${foutput_t} "t"
	(( ${gridU_ok} )) && rename_dimensions ${foutput_u} "u"
	(( ${gridV_ok} )) && rename_dimensions ${foutput_v} "v"
	(( ${gridW_ok} )) && rename_dimensions ${foutput_w} "w"
	(( ${gridF_ok} )) && rename_dimensions ${foutput_f} "f"

	echo " - create dimension variables..."
	(( ${gridT_ok} )) && create_dimension_variables ${foutput_t} "t"
	(( ${gridU_ok} )) && create_dimension_variables ${foutput_u} "u"
	(( ${gridV_ok} )) && create_dimension_variables ${foutput_v} "v"
	(( ${gridW_ok} )) && create_dimension_variables ${foutput_w} "w"
	(( ${gridF_ok} )) && create_dimension_variables ${foutput_f} "f"

	echo " - set standard_names..."
	(( ${gridT_ok} )) && set_std_names_mesh ${foutput_t} "t"
	(( ${gridU_ok} )) && set_std_names_mesh ${foutput_u} "u"
	(( ${gridV_ok} )) && set_std_names_mesh ${foutput_v} "v"
	(( ${gridW_ok} )) && set_std_names_mesh ${foutput_w} "w"
	(( ${gridF_ok} )) && set_std_names_mesh ${foutput_f} "f"

	echo " - create output file '${foutput}'"
	rm -f ${foutput}
	(( ${gridT_ok} )) && ncks -A ${foutput_t} ${foutput} && rm -f ${foutput_t}
	(( ${gridU_ok} )) && ncks -A ${foutput_u} ${foutput} && rm -f ${foutput_u}
	(( ${gridV_ok} )) && ncks -A ${foutput_v} ${foutput} && rm -f ${foutput_v}
	(( ${gridW_ok} )) && ncks -A ${foutput_w} ${foutput} && rm -f ${foutput_w}
	(( ${gridF_ok} )) && ncks -A ${foutput_f} ${foutput} && rm -f ${foutput_f}
}
#---------------------------------------------------------------------------------------------------------
# handle_data_file
#---------------------------------------------------------------------------------------------------------
function handle_data_file
{
	local foutput=${1}
	local grid=${2/_/}
	local vpos=$(echo ${grid#grid}|tr [:upper:] [:lower:])
	
	[[ ${vpos} == "t" ]] && atloc="" || atloc="_at_${vpos}_location"

	echo " - create output file ${foutput}"  
# 	cp -r ${FINPUT} ${foutput}
	ncks -O -3 ${FINPUT} ${foutput}

	echo " - rename dimensions..."
	rename_dimensions ${foutput} ${vpos}

	echo " - rename lon/lat variables..."
	ncrename -O -v nav_lat,"nav_lat_${vpos}" -v nav_lon,"nav_lon_${vpos}" ${FOUTPUT}
	ncatted  -O -a coordinates,,m,c,"${time_variable} nav_lat_${vpos} nav_lon_${vpos}" ${FOUTPUT}

	echo " - create dimension variables..."
	create_dimension_variables ${foutput} ${vpos}

	echo " - set standard_names..."
	set_std_names ${foutput} ${vpos}
}

##########################################################################################################

FINPUT=${1:?"Error: please specify input file"}
PERIODIC_X=${2:-0}
PERIODIC_Y=${3:-0}

if [[ ! -f ${FINPUT} ]] ; then
	echo "Error: file ${FINPUT} cannot be found."
	exit 1
fi

FNAME=$(basename ${FINPUT} .nc)
DNAME=$(dirname ${FINPUT})

echo "Input  file  : ${FINPUT}"
get_mesh_size ${FINPUT}
echo "----------------------"
echo "Grid size :"
[[ -n ${xname} ]] && echo " ${xname} = ${xsize}"
[[ -n ${yname} ]] && echo " ${yname} = ${ysize}"
[[ -n ${zname} ]] && echo " ${zname} = ${zsize}"
echo    "----------------------"
echo -n "Periodic in X : " ;  (( ${PERIODIC_X} )) && echo "yes" || echo "no"
echo -n "Periodic in Y : " ;  (( ${PERIODIC_Y} )) && echo "yes" || echo "no"
echo    "----------------------"

PREFIX=${FNAME%%grid*}
GRID=${FNAME#${PREFIX}}

if [[ -n ${GRID} ]] ; then
	echo "I guess this is a *DATA* file"
	if [[ ! ${GRID} =~ grid_?[tuvwfTUVWF] ]] ; then
		echo "## ERROR: unknown grid '${GRID}'." 
		exit 1
	fi
	FOUTPUT=${DNAME}/"${PREFIX%%_}-COMODO_${GRID}.nc"
	handle_data_file ${FOUTPUT} ${GRID}
else
	echo "I guess this is a *GRID MESH* file"
	handle_mesh_file ${DNAME} ${FNAME}
fi

exit 0
