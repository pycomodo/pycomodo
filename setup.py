from distutils.core import setup

setup(
    name='PyComodo',
    version='1.0.0',
    author='Marc Honnorat',
    author_email='marc.honnorat@gmail.com',
    packages=['pycomodo', 'pycomodo.util', 'pycomodo.operators'],
    package_data={'pycomodo': ['standard_name_tables/*.xml',
                               'standard_name_tables/*.xsd']},
    scripts=['bin/check_comodo.py'],
    url='http://http://pycomodo.forge.imag.fr/',
    license='LICENSE.txt',
    description='Python library to manipulate NetCDF files.',
    long_description=open('README.txt').read(),
)