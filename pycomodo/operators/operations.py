#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Defines some functions to carry out arithmetic operations with :class:`.Variable` instances.
"""
import re
import numbers
import pycomodo

from pycomodo import log
from pycomodo.compat import evaluate, USE_NUMEXPR

OP_NAME = {
    "+": "plus",
    "-": "minus",
    "*": "by",
    "/": "over",
}


def _are_compatible(var1, var2):
    """
    Checks whether two variables are compatible for a simple arithmetic binary operation.
    Both must have the same shape and be defined on the same grid.
    """
    return ( (var1.shape == var2.shape) and
             (var1.position == var2.position) )


def add(var1, var2, name=None, std_name=None):
    """
    Computes the addition of two instances of :class:`.Variable` and returns the result as a new instance.
    """
    return _simple_op("+", var1, var2, name, std_name)


def sub(var1, var2, name=None, std_name=None):
    """
    Computes the subtraction of *var2* to *var1*, two instances of :class:`.Variable` and returns the
    result as a new instance.
    """
    return _simple_op("-", var1, var2, name, std_name)


def mul(var1, var2, name=None, std_name=None):
    """
    Computes the multiplication of two instances of :class:`.Variable` and returns the result as a new instance.
    """
    return _simple_op("*", var1, var2, name, std_name)


def div(var1, var2, name=None, std_name=None):
    """
    Computes the division of *var1* by *var2*, two instances of :class:`.Variable` and returns the result
    as a new instance.
    """
    return _simple_op("/", var1, var2, name, std_name)


def _simple_op(code, var1, var2_in, name=None, std_name=None ):
    """
    Carries out a simple arithmetic operation on the data of `var1` and `var2`.
    """
    log.debug("# _simple_op: {0}{1}{2}".format(var1, code, var2_in))
    # If var2 is a scalar, create a temporary array with the same shape as var1.
    if isinstance(var2_in, numbers.Number):
        var2 = var1._data.copy()
        var2[:] = var2_in

    # Else, check shapes compatibility
    else:
        var2 = var2_in
        if not _are_compatible(var1, var2):
            error_str  = "_simple_op({0}) error: ".format(code) + \
                         "both variables are not defined on the same grid.\n"
            error_str += " - Names  : '{0}' and '{1}'\n".format(var1, var2)
            error_str += " - Shapes : {0} and {1}\n".format(var1.shape, var2.shape)
            error_str += " - Grids  : '{0}' and '{1}'".format(var1.position, var2.position)
            raise Exception(error_str)

    # Create new variable
    if name is None:
        name = "{0}_{1}_{2}".format(str(var1), OP_NAME[code], str(var2_in))
    if std_name is None:
        try:
            std_name = "{0}_{1}_{2}".format(var1.standard_name, OP_NAME[code], var2.standard_name)
        except AttributeError:
            std_name = name

    # Input variable position on the C grid
    archive = var1.archive
    new_var = archive.new_variable(name, model=var1)

    if code == "+":
        new_var[:] = var1[:] + var2[:]
    elif code == "-":
        new_var[:] = var1[:] - var2[:]
    elif code == "*":
        new_var[:] = var1[:] * var2[:]
    elif code == "/":
        new_var[:] = var1[:] / var2[:]
    else:
        raise Exception("unknown operation: {0}".format(code))

    new_var.standard_name = std_name

    return new_var


def recode(code, **kwargs):
    """
    Returns a transformed version of the arithmetic operation described by *code*.
    Normally, you should not have to use this function. Really.
    """
    # No need to rewrite if we use USE_NUMEXPR.
    if USE_NUMEXPR:
        return code

    # For each name found in the 'code':
    #   - if it's a variable, we append '[:]'
    #   - if it is followed by a '(', we prefix it by 'np.':
    # ex: 'sqrt(a)' => 'np.sqrt(a[:])'
    newcode = bytearray(code)
    for res in reversed(tuple(re.finditer(r"([a-zA-Z][a-zA-Z0-9_]*)", code))):
        vname = res.group(1)
        vstart, vend  = res.start(), res.end()
        nextchar = code[vend:vend+1]
        if nextchar == '(':                     # this should be a function
            newcode[vstart:vend] = "np.{0}".format(vname)
        elif vname in kwargs.keys():            # we've got a variable name
            if hasattr(kwargs[vname], '__getitem__'):
                newcode[vend:vend] = "[:]"      # and it's a pycomodo Variable, a (masked-)numpy ndarray, etc.

    return str(newcode)


def operation(code, name=None, stdname=None, dtype=None, **kwargs):
    """
    Returns a new variable whose content is the result of the operation described by *code*.

    :param str code: code of the operation.
    :param name: name for the new variable.
    :param stdname: value for the ``standard_name`` attribute.
    :param dtype: Numpy dtype for the new variable.
    :param kwargs: dictionary mapping names to :class:`.Variable` instances.

    To compute the square of the sea surface elevation:

    >>> sshsq = op.operation("ssh**2", name="sshsq", stdname="square_of_sea_surface_height", ssh=ssh)
    >>> print sshsq.info()
        float32 sshsq(time, eta_rho, xi_rho)
            sshsq:standard_name = "square_of_sea_surface_height"
    >>> print (sshsq[:] - ssh[:]**2 < 1e-9).all()
    True

    To compute the volume of the grid cells, using :meth:`.Archive.get_metric`:

    >>> dx_t = archive.get_metric("x", "t")
    >>> dy_t = archive.get_metric("y", "t")
    >>> dz_t = archive.get_metric("z", "t", axes="ZYX")
    >>> volume = op.operation("1/(dx*dy*dz)", name="volume", dx=dx_t, dy=dy_t, dz=dz_t)
    >>> print volume.axes_str, volume.dimensions
    ZYX ['s_rho', 'eta_rho', 'xi_rho']
    """
    log.debug("# operation : '{0}'".format(code))

    # We may have to rewrite the code in order to use python's 'eval()' statement
    # instead of numexpr.evaluate()
    code = recode(code, **kwargs)

    argvars = {}
    archive = None
    varref  = None

    for vname in kwargs:
        vvar = kwargs[vname]
        if isinstance(vvar, pycomodo.Variable):
            archive = vvar.archive
            if varref is None or (vvar.size > varref.size):
                varref  = vvar
        if hasattr(vvar, '__getitem__'):    # vvar is a pycomodo Variable, a (masked-)numpy ndarray, etc.
            argvars[vname] = vvar[:]
        else:                               # or maybe it's a simple float...
            argvars[vname] = vvar

    if archive is None:
        raise Exception("operation : was not able to find any pycomodo Variable in expression : {0}".format(code))

    # Create new variable
    new_var = archive.new_variable(name, model=varref, dtype=dtype)
    new_var[:] = evaluate(code, globals(), argvars)

    if USE_NUMEXPR:
        for var in argvars.itervalues():
            try:
                mask_1 = new_var._data.mask
                mask_2 = var.mask
                new_var.mask[:] = evaluate("mask_1 | mask_2")
            except AttributeError:
                pass

    if stdname is not None:
        new_var.standard_name = stdname

    return new_var
