#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Defines a function to carry out interpolation of :class:`.Variable` instances.
"""

import numpy as np
import pycomodo.util.stdnames as stdnames

from pycomodo import log
from pycomodo.compat import evaluate, USE_NUMEXPR
from pycomodo.operators.operations import recode
from pycomodo.core.exceptions import InterpolationError

from . import indices


def interpolate(var_in, loc_dest, name=None, std_name=None, method='arithmetic'):
    """
    Interpolate the data of *var_in* to the grid type specified by *loc_dest*.
    Returns a new instance of :class:`.Variable`.

    :param var_in: source variable, instance of :class:`.Variable`.
    :param loc_dest: position of the destination on :ref:`Arakawa grid <arakawa-label>`.
    :param name: name for the new variable.
    :param std_name: value for the ``standard_name`` attribute.
    :param method: interpolation method.

    The interpolation method my be chosen among the following :

    - ``'arithmetic'`` : :math:`d_{i+\\frac12} = \\frac{1}{2}\\left(s_i+s_{i+1}\\right)`
    - ``'geometric'`` : :math:`d_{i+\\frac12} = \\sqrt{s_i\ s_{i+1}}`
    - ``'harmonic'`` : :math:`d_{i+\\frac12} = 2\\frac{s_i\ s_{i+1}}{s_i+s_{i+1}}`
    - ``'flux'`` :  :math:`d_{i+\\frac12} = \\max(s_{i+1}, 0) - \\min(s_i, 0)`

    where :math:`s_i` and :math:`s_{i+1}` are the source data from *var_in* and
    :math:`d_{i+\\frac12}` the interpolation result stored in the returned variable.

    >>> print op.interpolate(xvel, 't').info()
        float32 u_at_t_location(time, s_rho, eta_rho, xi_rho)
            u_at_t_location:standard_name = "sea_water_x_velocity" = op.interpolate(xvel, 't')
            u_at_t_location:long_name = "u-momentum component"
            u_at_t_location:units = "meter second-1"

    >>> print op.interpolate(xvel, 'fw', name='xvel_at_fw', method='geometric').info()
        float32 xvel_at_fw(time, s_w, eta_v, xi_u)
            xvel_at_fw:standard_name = "sea_water_x_velocity_at_fw_location"
            xvel_at_fw:long_name = "u-momentum component"
            xvel_at_fw:units = "meter second-1"
    """
    # Input variable position on the C grid
    loc_from = var_in.position
    loc_dest = loc_dest.lower()

    if loc_dest == loc_from:
        # No interpolation needed.
        return var_in

    log.debug("# interpolate variable '{0}' from '{1}' to '{2}'"
              " (method='{3}')".format(var_in, loc_from, loc_dest, method))

    try:
        direction = indices.direction_from_to(loc_from, loc_dest)
        var_out = _interpolate_1d( var_in, loc_from, direction, loc_dest, name, std_name, method )
    except KeyError:
        if len(loc_dest) == 2 and loc_from[0] != loc_dest[1]:
            direction = 'z'
        else:
            direction = 'x'
        loc_dest_tmp = indices.var_position_on_c_grid(var_in, direction)
        log.debug("- no direction found for '{0}' -> '{1}'. Let's first try with {2}: '{3}' -> '{4}'"
                  .format(loc_from, loc_dest, direction, loc_from, loc_dest_tmp))
        var_tmp = _interpolate_1d( var_in, loc_from, 'x', loc_dest_tmp, None, None, method )
        var_out = interpolate( var_tmp, loc_dest, name, std_name, method )

    return var_out


def _interpolate_1d(var_in, loc_from, direction, loc_dest, name, std_name, method):
    """
    Carries out the actual interpolation in one *direction*.
    """
    # Create new variable
    if name is None:
        new_name = "{0}_at_{1}_location".format(var_in, loc_dest)
    else:
        new_name = name

    log.debug("  - _interpolate_1d: '{0}' (at {1}) -> '{2}' (at {3})"
              .format(var_in, loc_from, new_name, loc_dest))
    archive = var_in.archive
    new_dims = archive.get_coordinate_system(loc_dest, mock=var_in)
    new_var  = archive.new_variable(new_name, new_dims, model=var_in, copy_attrs=True)

    # Try to define the new standard_name
    if std_name is None:
        try:
            root_stdname = stdnames.get_root(var_in.standard_name)
            new_var.standard_name = stdnames.get_positioned(root_stdname, loc_dest)
        except AttributeError:
            pass
    else:
        new_var.standard_name = std_name

    loc_check = new_var.position
    if loc_check != loc_dest:
        msg_err = """_interpolate_1d:
We expected for variable '{0}' a grid type '{1}', but we got '{2}' instead.
Check the definition of your coordinate variables.
Do they have the correct 'c_grid_axis_shift' attribute ?\
""".format(new_name, loc_dest, loc_check)
        raise InterpolationError(msg_err)

    # Try to set the fill value
    fill_value = var_in.get_fill_value()
    if fill_value is not None:
        new_var._FillValue = fill_value

    s_to, s_in1, s_in2 = indices.get_interp_indices_1d(var_in, loc_from, loc_dest, direction)

    v1 = var_in[s_in1]      # v1 and v2 are 'numpy.ma.core.MaskedArray'
    v2 = var_in[s_in2]      # new_var is a 'pycomodo.variable.Variable'

    if method == 'arithmetic':
        new_var[s_to] = evaluate("( v1+v2 ) * 0.5")
    elif method == 'geometric':
        new_var[s_to] = evaluate(recode("sqrt(v1*v2)"))
    elif method == 'harmonic':
        new_var[s_to] = evaluate("2.*(v1*v2)/(v1+v2)")
    elif method == 'flux':
        # new_var = max(var_in[i+1/2],0) - min(var_in[i-1/2],0)
        new_var[s_to] = evaluate(recode("where(v2<0,0,v2)-where(v1>0,0,v1)"))

    if USE_NUMEXPR and np.any(v1.mask):
        new_var[s_to][v1.mask] = np.ma.masked
        new_var[s_to][v2.mask] = np.ma.masked

    log.debug("  -> result of interpolate : '{0}' shape {1}".format(new_var, new_var.shape))
    return new_var
