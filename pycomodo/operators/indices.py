#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Defines some functions used to manipulates indices of a :class:`.Variable` instance.

In the following, we will talk about 'positions' for variables. They correspond to the staggering
of the representative grid and are defined as a letter which can be either ``'t'``, ``'u'``, ``'v'``,
``'w'``, ``'f'``, ``'uw'``, ``'vw'`` or ``'fw'`` (see also :ref:`Comodo norm<arakawa-label>`). ::

        Top view :                Vertical view :

           'f'----'v'----'f'         'fw'---'w'---'fw'
            |             |           |             |
            |             |           |             |
           'u'    't'    'u'         'u'    't'    'u'
            |             |           |             |
        Y   |             |       Z   |             |
        ^  'f'----'v'----'f'      ^  'fw'---'w'---'fw'
        |                         |
        +—> X                     +—> X

Most code samples will be based on the archive built with a ROMS model output file :

>>> import pycomodo
>>> import pycomodo.operators as op
>>> archive = pycomodo.Archive('examples/nc_files/roms_vortex.nc')
>>> ssh  = archive.get_variable(stdname='sea_surface_height')
>>> xvel = archive.get_variable(stdname='sea_water_x_velocity_at_u_location')

A detailed output is given :doc:`[here]<../examples/archive_roms>`.
"""

import numpy as np

from pycomodo import log
import pycomodo.util.stdnames as stdnames

DIFF_GRID = {
    "t":  {"x": "u",  "y": "v",  "z": "w",  "t": "t",  None: "t" },
    "u":  {"x": "t",  "y": "f",  "z": "uw", "t": "u",  None: "u" },
    "v":  {"x": "f",  "y": "t",  "z": "vw", "t": "v",  None: "v" },
    "f":  {"x": "v",  "y": "u",  "z": "fw", "t": "f",  None: "f" },
    "w":  {"x": "uw", "y": "vw", "z": "t",  "t": "w",  None: "w" },
    "uw": {"x": "w",  "y": "fw", "z": "u",  "t": "uw", None: "uw"},
    "vw": {"x": "fw", "y": "w",  "z": "v",  "t": "vw", None: "vw"},
    "fw": {"x": "vw", "y": "uw", "z": "f",  "t": "fw", None: "fw"}
}

GRID_TO_GRID = {
    "t":  {"u":  "x", "v":  "y", "w":  "z", "t":  None },
    "u":  {"t":  "x", "f":  "y", "uw": "z", "u":  None },
    "v":  {"f":  "x", "t":  "y", "vw": "z", "v":  None },
    "f":  {"v":  "x", "u":  "y", "fw": "z", "f":  None },
    "w":  {"uw": "x", "vw": "y", "t":  "z", "w":  None },
    "uw": {"w":  "x", "fw": "y", "u":  "z", "uw": None },
    "vw": {"fw": "x", "w":  "y", "v":  "z", "vw": None },
    "fw": {"vw": "x", "uw": "y", "f":  "z", "fw": None }
}

SHIFT_TO_GRID = {
    (False, False, False): "t",
    (True,  False, False): "u",
    (False, True,  False): "v",
    (True,  True,  False): "f",
    (False, False, True ): "w",
    (True,  False, True ): "uw",
    (False, True,  True ): "vw",
    (True,  True,  True ): "fw",
}


class _idx_range(object):
    """
    Represents a slice of indices.
    """
    def __init__(self, imin=None, imax=None, jmin=None, jmax=None, kmin=None, kmax=None):
        self.imin = imin
        self.imax = imax
        self.jmin = jmin
        self.jmax = jmax
        self.kmin = kmin
        self.kmax = kmax

    def __repr__(self):
        return "(i={0}:{1}, j={2}:{3}, k={4}:{5})".format(self.imin, self.imax,
                                                          self.jmin, self.jmax,
                                                          self.kmin, self.kmax)

    def set_i(self, in_range):
        """
        Updates slice information for *i* axis.
        """
        self.imin = in_range[0] if in_range[0] is not None else 0
        self.imax = in_range[1] if in_range[1] is not None else 0

    def set_j(self, in_range):
        """
        Updates slice information for *j* axis.
        """
        self.jmin = in_range[0] if in_range[0] is not None else 0
        self.jmax = in_range[1] if in_range[1] is not None else 0

    def set_k(self, in_range):
        """
        Updates slice information for *k* axis.
        """
        self.kmin = in_range[0] if in_range[0] is not None else 0
        self.kmax = in_range[1] if in_range[1] is not None else 0

    def expand(self, dxi=0, dyj=0, dzk=0):
        """
        Increases upper limits of the slice.
        """
        self.imax += dxi
        self.jmax += dyj
        self.kmax += dzk
        return self

    def shrink(self, dxi=0, dyj=0, dzk=0):
        """
        Decreases upper limits of the slice.
        """
        self.expand(-dxi, -dyj, -dzk)
        return self

    def shift(self, dxi=0, dyj=0, dzk=0):
        """
        Shifts the slice.
        """
        self.imin += dxi
        self.imax += dxi
        self.jmin += dyj
        self.jmax += dyj
        self.kmin += dzk
        self.kmax += dzk
        return self

    def range_i(self):
        """
        Returns a slice object for axis *i*.
        """
        return np.s_[self.imin:self.imax]

    def range_j(self):
        """
        Returns a slice object for axis *j*.
        """
        return np.s_[self.jmin:self.jmax]

    def range_k(self):
        """
        Returns a slice object for axis *k*.
        """
        return np.s_[self.kmin:self.kmax]

    def copy(self):
        """
        Returns a copy of itself.
        """
        return self.__class__(self.imin, self.imax, self.jmin, self.jmax, self.kmin, self.kmax)


def var_position_on_c_grid(var, direction=None):
    """
    Returns the position of *var* on the Arakawa grid as a letter ('t', 'u', 'v', 'f', 'w', 'uw',
    'vw' or 'fw').
    If *direction* is given, returns the position shifted to that direction.

    :param var: instance of :class:`.Variable`.
    :param direction: direction of staggering (either 'x', 'y' or 'z').

    >>> op.var_position_on_c_grid(ssh)
    't'
    >>> op.var_position_on_c_grid(xvel)
    'u'
    >>> op.var_position_on_c_grid(xvel, direction='y')
    'f'
    >>> op.var_position_on_c_grid(xvel, direction='z')
    'uw'
    """
    # Position given by the attribute 'c_grid_axis_shift' of the axes.
    axes_position = axes_position_on_c_grid(var.axes, direction)

    # Position given by the standard_name suffix 'at_xx_location'.
    try:
        stdname_position = stdnames.get_at_location(var.standard_name)
        stdname_position = DIFF_GRID[stdname_position][direction]
    except AttributeError:
        stdname_position = axes_position

    # We now have to choose between the two. 'stdname_position' may be more specific.
    # Example 1: an horizontal metric field (2D) defined on a 'fw' location will have 'axes_position'
    # improperly set to 'f' and 'stdname_position' correctly set to 'fw'.
    # Example 2: a dimension-variable 'nj_f' staggered at a 'f' location will have 'axes_position' inappropriately
    # set to 'v' and 'stdname_position' set to 'f'.
    # One the other hand, if the standard_name is not fully documented, we shall choose the value
    # of 'axes_position'.
    # We will use some simple heuristics.
    position = axes_position
    if ('w' in stdname_position):
        position = stdname_position
    if ( (axes_position == 't'  and stdname_position in 'uvf') or
         (axes_position in 'uv' and stdname_position == 'f') ):
        position = stdname_position

    return position


def axes_position_on_c_grid(axes, direction=None):
    """
    Returns the position on the Arakawa grid of a variable that would have *axes* as coordinates.

    The position is returned as a letter ('t', 'u', 'v', 'f', 'w', 'uw', 'vw' or 'fw').

    :param axes: dictionary such as :obj:`.Variable.axes`.
    :param direction: direction of staggering (either 'x', 'y' or 'z').

    >>> op.axes_position_on_c_grid(ssh.axes)
    't'
    >>> op.axes_position_on_c_grid(ssh.axes, direction='y')
    'v'
    """
    shift_x = bool(axes["X"] and abs(axes["X"].c_grid_axis_shift) > 0.00001)
    shift_y = bool(axes["Y"] and abs(axes["Y"].c_grid_axis_shift) > 0.00001)
    shift_z = bool(axes["Z"] and abs(axes["Z"].c_grid_axis_shift) > 0.00001)

    # Get position from 'c_grid_axis_shift' attributes.
    position = SHIFT_TO_GRID[(shift_x, shift_y, shift_z)]

    return shift_location(position, direction)


def direction_from_to(loc_from, loc_dest):
    """
    Returns the direction ('x', 'y' or 'z') you have to take to go from
    location *loc_from* to location *loc_dest*.

    >>> op.direction_from_to('t', 'u')
    'x'
    >>> op.direction_from_to('w', 't')
    'z'
    """
    return GRID_TO_GRID[loc_from][loc_dest]


def shift_location(loc_from, direction):
    """
    Returns the position on the Arakawa grid of *loc_from* (could be 't', 'u', 'v', 'f', 'w',
    'uw', 'vw' or 'fw'), when shifted to *direction* (either 'x', 'y' or 'z').

    >>> op.shift_location('t', 'x')
    'u'
    >>> op.shift_location('u', 'x')
    't'
    >>> op.shift_location('u', 'y')
    'f'
    """
    if ( direction is not None ):
        direction = direction.lower()

    return DIFF_GRID[loc_from.lower()][direction]


def expand_location(location):
    """
    Redefine *location* so that it follows the form *'xy'* where:

    - *x* is the horizontal location ('t', 'u', 'v' or 'f')
    - *y* is the vertical location ('t' or 'w')

    >>> op.expand_location('u')
    'ut'
    >>> op.expand_location('w')
    'tw'
    """
    new_loc = location.lower()
    if new_loc == "w":
        new_loc = "tw"
    elif len(new_loc) == 1:
        new_loc += "t"
    return new_loc


def get_interp_indices_1d(var_from, loc_from, loc_dest, direction):
    """
    Returns a triplet of slices: *(s_dest, s_from_1, s_from_2)*:

    - *s_dest*  : slice of the destination array (*loc_dest*)
    - *s_from_1* : 1st slice of the source array (*loc_from*)
    - *s_from_2* : 2nd slice of the source array (*loc_from*)

    :param var_from: source variable (instance of :class:`.Variable`).
    :param loc_from: source position
    :param loc_dest: destination position

    .. code::

        # Get the indices to go from 't' to 'u'
        s_dest, s_from_1, s_from_2 = op.get_interp_indices_1d(ssh, 't', 'u', 'x')

        # For an interpolation:
        var_to[s_dest] = alpha*ssh[s_from_1] + (1-alpha)*ssh[s_from_2]

        # For a derivative:
        var_to[s_dest] = ( ssh[s_from_2] - ssh[s_from_1] ) / dx
    """
    log.debug("# get_interp_indices_1d_{0} for '{1}': from '{2}' to '{3}':"
              .format(direction, var_from, loc_from, loc_dest))

    archive = var_from.archive

    dims_from = archive.get_coordinate_system(loc_from, mock=var_from)
    dims_dest = archive.get_coordinate_system(loc_dest, mock=var_from)

    # Make a correspondance between a dimension name and its coordinate variable.
    coord_vars_from = {"X": None, "Y": None, "Z": None}
    coord_vars_dest = {"X": None, "Y": None, "Z": None}
    for dim_name in dims_from:
        coordvar = archive.get_variable(dim_name)
        coord_vars_from[coordvar.axis] = coordvar
    for dim_name in dims_dest:
        coordvar = archive.get_variable(dim_name)
        coord_vars_dest[coordvar.axis] = coordvar

    # For each dimension, get stored dynamic range
    dim_i_from = coord_vars_from["X"]
    dim_j_from = coord_vars_from["Y"]
    dim_k_from = coord_vars_from["Z"]
    dim_i_dest = coord_vars_dest["X"]
    dim_j_dest = coord_vars_dest["Y"]
    dim_k_dest = coord_vars_dest["Z"]

    range_i_from = archive.get_dimension_global_range(dim_i_from)
    range_j_from = archive.get_dimension_global_range(dim_j_from)
    range_k_from = archive.get_dimension_global_range(dim_k_from)
    range_i_dest = archive.get_dimension_global_range(dim_i_dest)
    range_j_dest = archive.get_dimension_global_range(dim_j_dest)
    range_k_dest = archive.get_dimension_global_range(dim_k_dest)

    idx = {}
    idx["from"] = _idx_range()
    if range_i_from:
        idx["from"].set_i(range_i_from)
    if range_j_from:
        idx["from"].set_j(range_j_from)
    if range_k_from:
        idx["from"].set_k(range_k_from)

    # Initialize destination ranges with dynamic_range of 'var_dest'.
    idx["dest"] = _idx_range()
    if range_i_dest:
        idx["dest"].set_i(range_i_dest)
    if range_j_dest:
        idx["dest"].set_j(range_j_dest)
    if range_k_dest:
        idx["dest"].set_k(range_k_dest)

    # Shift indices for 'from' with respect to 'dest'.
    isf = dim_i_from.c_grid_axis_shift if dim_i_from else 0
    isd = dim_i_dest.c_grid_axis_shift if dim_i_dest else 0
    jsf = dim_j_from.c_grid_axis_shift if dim_j_from else 0
    jsd = dim_j_dest.c_grid_axis_shift if dim_j_dest else 0
    ksf = dim_k_from.c_grid_axis_shift if dim_k_from else 0
    ksd = dim_k_dest.c_grid_axis_shift if dim_k_dest else 0

    ddx = (direction == 'x')
    ddy = (direction == 'y')
    ddz = (direction == 'z')
    dxi = 1 if ddx else 0
    dyj = 1 if ddy else 0
    dzk = 1 if ddz else 0

    # i_shift is the index shift of a 'dest' point that is just
    #     to the right of a 'from' point. Only when direction == 'x'
    # j_shift is the index shift of a 'dest' point that is just
    #     to the top of a 'from' point. Only when direction == 'y'
    # i index of 'dest' and 'from' points is always the same for d/dx.
    # j index of 'dest' and 'from' points is always the same for d/dx.
    i_shift = 1 if ( ddx and (isf > isd) ) else 0
    j_shift = 1 if ( ddy and (jsf > jsd) ) else 0
    k_shift = 1 if ( ddz and (ksf > ksd) ) else 0

    # We cannot interpolate the first line, nor the last one
    idx["dest"].imin = max( idx["dest"].imin, idx["from"].imin + i_shift )
    idx["dest"].jmin = max( idx["dest"].jmin, idx["from"].jmin + j_shift )
    idx["dest"].kmin = max( idx["dest"].kmin, idx["from"].kmin + k_shift )

    i_shift_max = (1-i_shift) if ddx else 0
    j_shift_max = (1-j_shift) if ddy else 0
    k_shift_max = (1-k_shift) if ddz else 0

    idx["dest"].imax = min( idx["dest"].imax, idx["from"].imax - i_shift_max )
    idx["dest"].jmax = min( idx["dest"].jmax, idx["from"].jmax - j_shift_max )
    idx["dest"].kmax = min( idx["dest"].kmax, idx["from"].kmax - k_shift_max )

    # Let's now computes min/max indices for t.
    idx["from"] = idx["dest"].copy().shift(-i_shift, -j_shift, -k_shift)
    idx["from"].expand(dxi, dyj, dzk)

    # Convert max indices to slice upper boundary
    for slc in idx.itervalues():
        slc.expand(1, 1, 1)

    # Location of 'f1' and 'f2' points relatively to 'u':
    #    j     f2
    #    ^  t  u  t
    #    |     f1
    #    o-> i
    # Location of 't1' and 't2' point relatively to 'u':
    #    j      f
    #    ^  t1  u  t2
    #    |      f
    #    o-> i

    idx["from_1"] = idx["from"].copy().shrink(dxi, dyj, dzk)
    idx["from_2"] = idx["from_1"].copy().shift(dxi, dyj, dzk)

    # Let each idx item's dimensions be ordered as those of var_from
    s_dest   = np.index_exp[...]
    s_from_1 = np.index_exp[...]
    s_from_2 = np.index_exp[...]

    for dim in dims_from:
        if dim == str(coord_vars_from["X"]):
            s_dest   += np.index_exp[idx["dest"].range_i()]
            s_from_1 += np.index_exp[idx["from_1"].range_i()]
            s_from_2 += np.index_exp[idx["from_2"].range_i()]
        if dim == str(coord_vars_from["Y"]):
            s_dest   += np.index_exp[idx["dest"].range_j()]
            s_from_1 += np.index_exp[idx["from_1"].range_j()]
            s_from_2 += np.index_exp[idx["from_2"].range_j()]
        if dim == str(coord_vars_from["Z"]):
            s_dest   += np.index_exp[idx["dest"].range_k()]
            s_from_1 += np.index_exp[idx["from_1"].range_k()]
            s_from_2 += np.index_exp[idx["from_2"].range_k()]

    return s_dest, s_from_1, s_from_2
