#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This package contains some methodes to carry out computations on pycomodo :class:`variables <.Variable>`.
"""
from .indices import    \
    var_position_on_c_grid, axes_position_on_c_grid, direction_from_to,    \
    get_interp_indices_1d, shift_location, expand_location

from .differentiate import diff_x, diff_y, diff_z
from .operations import add, sub, mul, div, operation
from .interpolation import interpolate
