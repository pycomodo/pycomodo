#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Defines some functions to compute spatial derivatives of :class:`.Variable` instances.
"""
import numpy as np

from pycomodo import log
from . import indices


def diff_x( var_in, name=None, stdname=None, dummy=False ):
    """
    Returns the spatial derivative in 'x' direction of :class:`.Variable` instance *var_in*.

    :param var_in: source :class:`.Variable` instance.
    :param name: name for the new variable.
    :param stdname: value of the ``standard_attribute`` for the new variable.
    :param dummy: if True, does not compute derivative, but only differences (spatial metric is 1).

    >>> dvx = op.diff_x(xvel)
    >>> print dvx.info()
        float32 d_u_dx(time, s_rho, eta_rho, xi_rho)
            d_u_dx:standard_name = "derivative_of_sea_water_x_velocity_at_u_location_wrt_x"
    >>> xvel.position, dvx.position
    ('u', 't')
    """
    return _diff_generic(var_in, 'x', name, stdname, dummy)


def diff_y( var_in, name=None, stdname=None, dummy=False ):
    """
    Returns the spatial derivative in 'y' direction of :class:`.Variable` instance *var_in*.

    :param var_in: source :class:`.Variable` instance.
    :param name: name for the new variable.
    :param stdname: value of the ``standard_attribute`` for the new variable.
    :param dummy: if True, does not compute derivative, but only differences (spatial metric is 1).

    >>> dvy = op.diff_y(xvel)
    >>> print dvy.info()
        float32 d_u_dy(time, s_rho, eta_v, xi_u)
            d_u_dy:standard_name = "derivative_of_sea_water_x_velocity_at_u_location_wrt_y"
    >>> xvel.position, dvy.position
    ('u', 'f')
    """
    return _diff_generic(var_in, 'y', name, stdname, dummy)


def diff_z( var_in, name=None, stdname=None, dummy=False ):
    """
    Returns the spatial derivative in 'z' direction of :class:`.Variable` instance *var_in*.

    :param var_in: source :class:`.Variable` instance.
    :param name: name for the new variable.
    :param stdname: value of the ``standard_attribute`` for the new variable.
    :param dummy: if True, does not compute derivative, but only differences (spatial metric is 1).

    >>> dvz = op.diff_z(xvel)
    >>> print dvz.info()
        float32 d_u_dz(time, s_w, eta_rho, xi_u)
            d_u_dz:standard_name = "derivative_of_sea_water_x_velocity_at_u_location_wrt_z"
    >>> xvel.position, dvz.position
    ('u', 'uw')
    """
    return _diff_generic(var_in, 'z', name, stdname, dummy)


def _diff_generic( var_in, direction, name, stdname, dummy ):
    """
    Actually computes the spatial derivative of *var_in* in *direction*.
    """
    # Input variable position on the C grid
    loc_from = indices.var_position_on_c_grid(var_in)

    # Output variable position on the C grid
    loc_dest = indices.var_position_on_c_grid(var_in, direction)

    log.debug("# diff_{0} for '{1}' shape {2}".format(direction, str(var_in), var_in.shape) +
              "\n\t   - direction '{0}', from '{1}' to '{2}' (dummy={3})"
              .format(direction, loc_from, loc_dest, dummy))

    # Check type of input variable: if it is an array of integers, its derivative should be real anyway.
    old_dtype_name = var_in.dtype.name
    if "int8" in old_dtype_name:
        new_dtype = np.dtype(np.float16)
    elif "int16" in old_dtype_name:
        new_dtype = np.dtype(np.float32)
    elif "int32" in old_dtype_name:
        new_dtype = np.dtype(np.float64)
    elif "int64" in old_dtype_name:
        new_dtype = np.dtype(np.float64)
    else:
        new_dtype = None

    # Create new variable
    if name is None:
        diff_name = "d_{0}_d{1}".format(var_in, direction)
    else:
        diff_name = name

    archive = var_in.archive

    new_dims = archive.get_coordinate_system(loc_dest, mock=var_in)
    new_var = archive.new_variable(diff_name, new_dims, model=var_in, dtype=new_dtype)

    loc_check = new_var.position
    if loc_check != loc_dest:
        msg_err = """_diff_generic:
We expected for variable '{0}' a grid type '{1}', but we got '{2}' instead.
Check the definition of your coordinate variables.
Do they have the correct 'c_grid_axis_shift' attribute ?\
""".format(diff_name, loc_dest, loc_check)
        raise Exception(msg_err)

    # Try to define the new standard_name
    if stdname is None:
        try:
            new_var.standard_name = "derivative_of_{0}_wrt_{1}".format(var_in.standard_name, direction)
        except AttributeError:
            pass
    else:
        new_var.standard_name = stdname

    # Get interpolation indices
    if direction in ('x', 'y', 'z'):
        s_to, s_in1, s_in2 = indices.get_interp_indices_1d(var_in, loc_from, loc_dest, direction)
    else:
        raise Exception("Unknown direction")

    if not dummy:
        # Get the metric
        dx = archive.get_metric(direction, loc_dest.lower(), direct_call=False)
        s_dx = [ s for (axis, s) in zip(new_var.axes, s_to) if axis in dx.axes ]
        d_dx = np.ma.masked_values(dx[s_dx], 0.)

    # Compute diff
    if "T" in new_var.axes:
        for k in range(archive.ntimes):
            s_to_k    = np.index_exp[k] + s_to
            data_in_k = var_in[k]
            new_var[s_to_k] = data_in_k[s_in2] - data_in_k[s_in1]
    else:
        new_var[s_to] = var_in[s_in2] - var_in[s_in1]

    if not dummy:
        new_var[s_to] /= d_dx

    log.debug("  -> result of diff_{0} : '{1}' shape {2}".format(direction, str(new_var), new_var.shape))

    return new_var
