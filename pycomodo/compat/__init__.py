#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Compatibility layer for uninstalled modules. Provides some fallbacks and warnings.
"""


from pycomodo.io.customlogger import log

try:
    import netCDF4 as cdf
    USE_NETCDF4 = True
except ImportError:
    USE_NETCDF4 = False
    log.warning("Unable to import netCDF4. We'll try to fall back to our own implementation "
                "of NetCDF. It will work only for NetCDF 3 files.")
    from pycomodo.compat import netcdf as cdf

try:
    from collections import OrderedDict
except ImportError:
    from pycomodo.compat.ordereddict import OrderedDict

class DefaultOrderedDict(OrderedDict):
    """
    OrderedDict that do not throw a KeyError exception for missing values, but
    returns None instead.
    """
    def __missing__(self, k):
        return None

try:
    from numexpr import evaluate
    USE_NUMEXPR = True
except ImportError:
    USE_NUMEXPR = False
    evaluate = eval
    log.warning("We were unable to import 'numexpr' package. You may expect poor performance "
                "on large datasets." )
