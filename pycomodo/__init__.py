#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
From the main package :mod:`pycomodo`, the following objects can be usually imported directly
into a post-processing script :

    :class:`.Archive`
        Designed to gather all data and metadata from a set of NetCDF files.

    :class:`.Variable`
        Represents a NetCDF variable with dimensions, data and attributes.

    :obj:`log <pycomodo.io.customlogger.log>`
        Default logging facility for the whole library.

    :func:`.write_netcdf`
        Function to write a new NetCDF file with any list of :class:`.Variable` instances.

For an example use of these classes and function, see the :ref:`Overview<overview>`.
"""

from pycomodo.io.customlogger import log

from pycomodo.compat import cdf, USE_NETCDF4
from pycomodo.io.write_netcdf import write_netcdf
from pycomodo.core.variable import Variable
from pycomodo.core.archive import Archive
from pycomodo.core.checker import ArchiveChecker
from pycomodo.core.exceptions import *

__version__ = "1.6"

import resource
try:
    resource.setrlimit(resource.RLIMIT_NOFILE, (10240, -1))
except ValueError:
    pass
