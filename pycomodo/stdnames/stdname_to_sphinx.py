#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
TODO: Documentation of `stdname_to_sphinx` module.
"""

import sys
import os
import codecs
import pycomodo.stdnames.standard_name_parser as snp


sys.stdout = codecs.getwriter('utf8')(sys.stdout)


def chunk_str_in_words(string, width):
    """
    Split the string ``string`` into a list of equal length items.

    >>> print chunk_str_in_words("1 22 333 4444", 4)
    ['1 22', '333 ', '4444']
    >>> for line in chunk_str_in_words("1 22 333 4444 55555 666666", 10):
    ...    print "|{0}|".format(line)
    |1 22 333  |
    |4444 55555|
    |666666    |
    """
    words = string.split()
    lst = [u""]
    i = 0
    while words:
        word = words.pop(0)
        if lst[i]:
            line = lst[i]+" "+word
        else:
            line = word
        if len(line) <= width:
            lst[i] = line
        else:
            lst[i] += " "*(width-len(lst[i]))
            lst.append(word)
            i += 1
    lst[i] += " "*(width-len(lst[i]))
    return lst


class StandardNameConverter:
    """
    Converts an XML file containing standard_name definitions into a reST documentation::

        comodo_converter = StandardNameConverter(StandardNameParser.COMODO_XML)
        comodo_converter.write_header()
        comodo_converter.write_rst()

    """
    def __init__(self, xmlfile, out=sys.stdout):
        self.xmldef_file = xmlfile
        self.parser = snp.StandardNameParser(self.xmldef_file)
        self.out = out

    def write(self, string):
        self.out.write(string)

    def write_title(self, title, underline_char):
        """
        Write ranked header title.
        """
        self.out.write("{0}\n".format( title ))
        self.out.write(underline_char*len(title) + "\n\n" )

    def write_h1(self, title):
        """
        Write first rank header title.
        """
        self.write_title(title, "=")

    def write_h2(self, title):
        """
        Write second rank header title.
        """
        self.write_title(title, "-")

    def write_h3(self, title):
        """
        Write third rank header title.
        """
        self.write_title(title, "\"")

    def write_header(self):
        """
        Write header title.
        """
        self.write_h2(os.path.basename(self.xmldef_file))
        self.out.write("- **Contact :** `{0}`\n".format(self.parser.contact))
        self.out.write("- **Version :** {0}\n".format(self.parser.version))
        self.out.write("- **Last modified :** {0}\n\n".format(self.parser.last_modified))

    def write_rst(self):
        """
        Write the content of `self.xmldef_file` in ReST format.
        """
        self.write_stdnames()
        self.write_aliases()

    def write_stdnames(self):

        entries = self.parser.entries

        max_name_width = max(max(len(name) for name in entries), 120)
        max_unit_width = max(max(len(unit) for unit in self.parser.get_iterunits()), 8)

        col_width = { "c1w": max_name_width+7, "c2w": max_unit_width }

        self.write_h3("Standard Names")

        # Array header
        self.out.write(".. tabularcolumns:: |L|c|\n\n")
        self.out.write("+{0:{c1w}}+{1:{c2w}}+\n".format("-"*col_width["c1w"],
                                                        "-"*col_width["c2w"], **col_width))
        self.out.write("|{0:{c1w}}|{1:{c2w}}|\n".format("Name", "Unit", **col_width))
        self.out.write("+{0:{c1w}}+{1:{c2w}}+\n".format("="*col_width["c1w"],
                                                        "="*col_width["c2w"], **col_width))
        first = True

        for entry_name, entry in entries.iteritems():

            # Line delimitor
            if first:
                first = False
            else:
                self.out.write("+{0:{c1w}}+{1:{c2w}}+\n"
                               .format("-"*col_width["c1w"], "-"*col_width["c2w"], **col_width))

            # Actual line
            self.out.write("|{0:{c1w}}|{1:{c2w}}|\n"
                           .format("``"+entry_name[:max_name_width]+"``", entry.units, **col_width))
            if entry.description:
                self.out.write("|{0:{c1w}}|{1:{c2w}}|\n".format(" ", " ", **col_width))
                description_text = u"**Description :** " + entry.description
                description = chunk_str_in_words(description_text, col_width["c1w"])
                self.out.write(u"|{0}|{1:{c2w}}|\n".format(description[0], "", **col_width))
                for desc in description[1:]:
                    self.out.write(u"|{0}|{1:{c2w}}|\n".format(desc, "", **col_width))

        # Bottom line
        self.out.write("+{0:{c1w}}+{1:{c2w}}+\n\n"
                       .format("-"*col_width["c1w"], "-"*col_width["c2w"], **col_width))

    def write_aliases(self):

        aliases = self.parser.aliases
        entries = self.parser.entries

        if not aliases:
            return

        self.write_h3("Aliases")

        max_alias_width = max(len(name) for name in aliases) + 4
        max_name_width  = max(len(name) for name in entries) + 4
        max_unit_width  = max(max(len(unit) for unit in self.parser.get_iterunits()), 8)
        col_width = { "c1w": max_alias_width, "c2w": max_unit_width, "c3w": max_name_width }

        # Array header
        self.out.write("+{0:{c1w}}+{1:{c2w}}+{2:{c3w}}+\n"
                       .format("-"*col_width["c1w"], "-"*col_width["c2w"],
                               "-"*col_width["c3w"], **col_width))
        self.out.write("|{0:{c1w}}|{1:{c2w}}|{2:{c3w}}|\n"
                       .format("Alias Name", "Unit", "Corresponding Standard Name", **col_width))
        self.out.write("+{0:{c1w}}+{1:{c2w}}+{2:{c3w}}+\n"
                       .format("="*col_width["c1w"], "="*col_width["c2w"],
                               "="*col_width["c3w"], **col_width))
        first = True
        for alias_name, entry in aliases.iteritems():
            # Line delimitor
            if first:
                first = False
            else:
                self.out.write("+{0:{c1w}}+{1:{c2w}}+{2:{c3w}}+\n"
                               .format("-"*col_width["c1w"], "-"*col_width["c2w"],
                                       "-"*col_width["c3w"], **col_width))
            # Actual line
            self.out.write("|{0:{c1w}}|{1:{c2w}}|{2:{c3w}}|\n"
                           .format("``"+alias_name+"``", entry.units,
                                   "``"+entry.name+"``", **col_width))
        # Bottom line
        self.out.write("+{0:{c1w}}+{1:{c2w}}+{2:{c3w}}+\n\n"
                       .format("-"*col_width["c1w"], "-"*col_width["c2w"],
                               "-"*col_width["c3w"], **col_width))


class BudgetTermsConverter(StandardNameConverter):
    """
    Converts an XML file containing standard_name definitions into a reST documentation::

        comodo_converter = BudgetTermsConverter(StandardNameParser.COMODO_XML)
        comodo_converter.write_header()
        comodo_converter.write_rst()

    """
    def __init__(self, xmlfile, out=sys.stdout):
        self.xmldef_file = xmlfile
        self.parser = snp.BudgetTermsParser(self.xmldef_file)
        self.out = out

    def write_stdnames(self):

        entries    = self.parser.entries
        categories = self.parser.categories

        max_name_width = max(max(len(name) for name in entries), 100)
        max_unit_width = max(max(len(unit) for unit in self.parser.get_iterunits()), 10)
        col_width = { "c1w": max_name_width+7, "c2w": max_unit_width, "c3w": max_unit_width }

        for category_name, entry_list in categories.iteritems():

            self.write_h3("{0} terms".format(category_name.capitalize()))
            self.write_category(entry_list, col_width)

    def write_category(self, entries, col_width):

        # Array header
        self.out.write(".. tabularcolumns:: |L|c|c| \n\n")
        self.out.write("+{0:{c1w}}+{1:{c2w}}+{2:{c3w}}+\n"
                       .format("-"*col_width["c1w"], "-"*col_width["c2w"], "-"*col_width["c3w"], **col_width))
        self.out.write("|{0:{c1w}}|{1:{c2w}}|{2:{c3w}}|\n"
                       .format("Standard Name", "Unit", "Staggering", **col_width))
        self.out.write("+{0:{c1w}}+{1:{c2w}}+{2:{c3w}}+\n"
                       .format("="*col_width["c1w"], "="*col_width["c2w"], "="*col_width["c3w"], **col_width))
        first = True

        for entry in entries:

            # Line delimitor
            if first:
                first = False
            else:
                self.out.write("+{0:{c1w}}+{1:{c2w}}+{2:{c3w}}+\n"
                               .format("-"*col_width["c1w"], "-"*col_width["c2w"], "-"*col_width["c3w"], **col_width))

            # Actual line
            self.out.write("|{0:{c1w}}|{1:{c2w}}|{2:{c3w}}|\n"
                           .format("``"+entry.name+"``", entry.units, entry.staggering, **col_width))
            if entry.description:
                self.out.write("|{0:{c1w}}|{1:{c2w}}|{2:{c3w}}|\n".format(" ", " ", " ", **col_width))
                description_text = u"**Description :** " + entry.description
                description = chunk_str_in_words(description_text, col_width["c1w"])
                self.out.write(u"|{0}|{1:{c2w}}|{2:{c3w}}|\n".format(description[0], " ", " ", **col_width))
                for desc in description[1:]:
                    self.out.write(u"|{0}|{1:{c2w}}|{2:{c3w}}|\n".format(desc, " ", " ", **col_width))

        # Bottom line
        self.out.write("+{0:{c1w}}+{1:{c2w}}+{2:{c3w}}+\n\n"
                       .format("-"*col_width["c1w"], "-"*col_width["c2w"], "-"*col_width["c3w"], **col_width))


def build_table_cf(cf_xml, write_rst=False):
    # Set up dictionary of standard_names and their assoc. units
    cf_converter = StandardNameConverter(cf_xml)
    cf_converter.write_header()

    if write_rst:
        cf_converter.write_rst()
    else:
        cf_converter.write(
            "To see the complete documentation of this list, please refer to the reference " +
            "`CF Standard Name table <http://cf-pcmdi.llnl.gov/documents/cf-standard-names/" +
            "standard-name-table/current/cf-standard-name-table.html>`_\n\n")


def build_table_comodo(comodo_xml):
    comodo_converter = StandardNameConverter(comodo_xml)
    comodo_converter.write_header()
    comodo_converter.write_rst()


def build_table_budget(budget_xml):
    budget_converter = BudgetTermsConverter(budget_xml)
    budget_converter.write_header()
    budget_converter.write_rst()


def build_tables(names):

    thisdir    = os.path.dirname(__file__)
    dir_tables = os.path.join(thisdir, "standard_name_tables")

    cf_xml     = os.path.join(dir_tables, "cf-standard-name-table.xml")
    comodo_xml = os.path.join(dir_tables, "comodo-standard-name-table.xml")
    budget_xml = os.path.join(dir_tables, "comodo-budget-terms.xml")

    for name in names:
        if name == 'stdnames':
            build_table_cf(cf_xml)
        elif name == 'fullstdnames':
            build_table_cf(cf_xml, True)
        elif name == 'comodo':
            build_table_comodo(comodo_xml)
        elif name == 'budget':
            build_table_budget(budget_xml)


if __name__ == "__main__":

    from pycomodo import log

    if len(sys.argv) < 2:
        log.error("stdname_to_sphinx.py called without arguments. Use 'stdnames', 'comodo' or 'budget'.")
    else:
        build_tables(sys.argv[1:])
