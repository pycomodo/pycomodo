<?xml version="1.0" encoding="UTF-8"?>
<standard_name_table xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="cf-standard-name-table-1.1.xsd">

	<version_number>1</version_number>
	<last_modified>2013-11-21T06:00:00Z</last_modified>
	<institution>COMODO</institution>
	<contact>marc.honnorat@gmail.com</contact>
	
	<category id='tendency'>
		<entry id="tendency_of_sea_water_x_velocity">
			<canonical_units>m s-2</canonical_units><staggering>u</staggering>
			<description>
			Derivative of :math:`u` with respect to time: :math:`\frac{\partial u}{\partial t}`.
			</description>
		</entry>
		<entry id="tendency_of_sea_water_y_velocity">
			<canonical_units>m s-2</canonical_units><staggering>v</staggering>
			<description>
			Derivative of :math:`v` with respect to time: :math:`\frac{\partial v}{\partial t}`.
			</description>
		</entry>
	</category>
	
	<category id='advection'>
		<entry id="non_linear_advection_of_sea_water_x_velocity">
			<canonical_units>m s-2</canonical_units><staggering>u</staggering>
			<description>
			Non linear advection terms for :math:`u` in momentum equation:
			:math:`\ (\vec{\bf u}\cdot\nabla)\,u\ =\ u\,\partial_x u + v\,\partial_y u + w\,\partial_z u`      
			</description>
		</entry>
		<entry id="x_non_linear_advection_of_sea_water_x_velocity">
			<canonical_units>m s-2</canonical_units><staggering>u</staggering>
			<description>
			Non linear advection term for :math:`u` along *x* axis in momentum equation:
			:math:`\ u\,\partial_x u`      
			</description>
		</entry>
		<entry id="y_non_linear_advection_of_sea_water_x_velocity">
			<canonical_units>m s-2</canonical_units><staggering>u</staggering>
			<description>
			Non linear advection term for :math:`u` along *y* axis in momentum equation:
			:math:`\ v\,\partial_y u`      
			</description>
		</entry>
		<entry id="upward_non_linear_advection_of_sea_water_x_velocity">
			<canonical_units>m s-2</canonical_units><staggering>u</staggering>
			<description>
			Non linear upward advection term for :math:`u` in momentum equation:
			:math:`\ w\,\partial_z u`      
			</description>
		</entry>
		<entry id="non_linear_advection_of_sea_water_y_velocity">
			<canonical_units>m s-2</canonical_units><staggering>v</staggering>
			<description>
			Non linear advection terms for :math:`v` in momentum equation:
			:math:`\ (\vec{\bf u}\cdot\nabla)\,v\ =\ u\,\partial_x v + v\,\partial_y v + w\,\partial_z v`      
			</description>
		</entry>
		<entry id="x_non_linear_advection_of_sea_water_y_velocity">
			<canonical_units>m s-2</canonical_units><staggering>v</staggering>
			<description>
			Non linear advection term for :math:`v` along *x* axis in momentum equation:
			:math:`\ u\,\partial_x v`      
			</description>
		</entry>
		<entry id="y_non_linear_advection_of_sea_water_y_velocity">
			<canonical_units>m s-2</canonical_units><staggering>v</staggering>
			<description>
			Non linear advection term for :math:`v` along *y* axis in momentum equation:
			:math:`\ v\,\partial_y v`      
			</description>
		</entry>
		<entry id="upward_non_linear_advection_of_sea_water_y_velocity">
			<canonical_units>m s-2</canonical_units><staggering>v</staggering>
			<description>
			Non linear upward advection term for :math:`v` in momentum equation:
			:math:`\ w\,\partial_z v`      
			</description>
		</entry>
	</category>

	<category id='pressure gradient'>
		<entry id="x_pressure_gradient">
			<canonical_units>m s-2</canonical_units><staggering>u</staggering>
			<description>
			grid x-axis component of total pressure gradient:
			:math:`\ \frac{1}{\rho_0}\partial_xp`
			</description>
		</entry>
		<entry id="x_external_pressure_gradient">
			<canonical_units>m s-2</canonical_units><staggering>u</staggering>
			<description>
			grid x-axis component of surface or external pressure gradient.
			</description>
		</entry>
		<entry id="x_internal_pressure_gradient">
			<canonical_units>m s-2</canonical_units><staggering>u</staggering>
			<description>
			grid x-axis component of surface or internal pressure gradient.
			</description>
		</entry>
		<entry id="y_pressure_gradient">
			<canonical_units>m s-2</canonical_units><staggering>v</staggering>
			<description>
			grid v-axis component of total pressure gradient:
			:math:`\ \frac{1}{\rho_0}\partial_vp`
			</description>
		</entry>
		<entry id="y_external_pressure_gradient">
			<canonical_units>m s-2</canonical_units><staggering>v</staggering>
			<description>
			grid v-axis component of surface or external pressure gradient.
			</description>
		</entry>
		<entry id="y_internal_pressure_gradient">
			<canonical_units>m s-2</canonical_units><staggering>v</staggering>
			<description>
			grid v-axis component of surface or internal pressure gradient.
			</description>
		</entry>
	</category>
	
	<category id='mixing'>
		<entry id="horizontal_diffusion_of_sea_water_x_velocity">
			<canonical_units>m s-2</canonical_units><staggering>u</staggering>
			<description>
			Horizontal diffusion term for :math:`u` in momentum equation:
			:math:`\ \nabla_{\!h}\cdot\left(\kappa_h\nabla_{\!h}u\right)\ =\ `
			:math:`\frac{\partial}{\partial x}\left(\kappa_h\frac{\partial u}{\partial x}\right)+`
			:math:`\frac{\partial}{\partial y}\left(\kappa_h\frac{\partial u}{\partial y}\right)`
			</description>
		</entry>
		<entry id="x_diffusion_of_sea_water_x_velocity">
			<canonical_units>m s-2</canonical_units><staggering>u</staggering>
			<description>
			Diffusion term for :math:`u` along *x* axis in momentum equation:
			:math:`\ \frac{\partial}{\partial x}\left(\kappa_h\frac{\partial u}{\partial x}\right)`
			</description>
		</entry>
		<entry id="y_diffusion_of_sea_water_x_velocity">
			<canonical_units>m s-2</canonical_units><staggering>u</staggering>
			<description>
			Diffusion term for :math:`u` along *y* axis in momentum equation:
			:math:`\ \frac{\partial}{\partial y}\left(\kappa_h\frac{\partial u}{\partial y}\right)`
			</description>
		</entry>
		<entry id="upward_diffusion_of_sea_water_x_velocity">
			<canonical_units>m s-2</canonical_units><staggering>u</staggering>
			<description>
			Vertical mixing term for :math:`u` in momentum equation:
			:math:`\ \frac{\partial}{\partial z}\left(\kappa_v\frac{\partial u}{\partial z}\right)`
			</description>
		</entry>
		<entry id="horizontal_diffusion_of_sea_water_y_velocity">
			<canonical_units>m s-2</canonical_units><staggering>v</staggering>
			<description>
			Horizontal diffusion term for :math:`v` in momentum equation:
			:math:`\ \nabla_{\!h}\cdot\left(\kappa_h\nabla_{\!h}v\right)\ =\ `
			:math:`\frac{\partial}{\partial x}\left(\kappa_h\frac{\partial v}{\partial x}\right)+`
			:math:`\frac{\partial}{\partial y}\left(\kappa_h\frac{\partial v}{\partial y}\right)`
			</description>
		</entry>
		<entry id="x_diffusion_of_sea_water_y_velocity">
			<canonical_units>m s-2</canonical_units><staggering>v</staggering>
			<description>
			Diffusion term for :math:`v` along *x* axis in momentum equation:
			:math:`\ \frac{\partial}{\partial x}\left(\kappa_h\frac{\partial v}{\partial x}\right)`
			</description>
		</entry>
		<entry id="y_diffusion_of_sea_water_y_velocity">
			<canonical_units>m s-2</canonical_units><staggering>v</staggering>
			<description>
			Diffusion term for :math:`v` along *y* axis in momentum equation:
			:math:`\ \frac{\partial}{\partial y}\left(\kappa_h\frac{\partial v}{\partial y}\right)`
			</description>
		</entry>
		<entry id="upward_diffusion_of_sea_water_y_velocity">
			<canonical_units>m s-2</canonical_units><staggering>v</staggering>
			<description>
			Vertical mixing term for :math:`v` in momentum equation:
			:math:`\ \frac{\partial}{\partial z}\left(\kappa_v\frac{\partial v}{\partial z}\right)`
			</description>
		</entry>
	</category>

	<category id='coriolis'>
		<entry id="x_coriolis_acceleration">
			<canonical_units>m s-2</canonical_units><staggering>u</staggering>
			<description>
			Coriolis term: :math:`\ -f\cdot v`
			</description>
		</entry>
		<entry id="y_coriolis_acceleration">
			<canonical_units>m s-2</canonical_units><staggering>v</staggering>
			<description>
			Coriolis term: :math:`\ f\cdot u`
			</description>
		</entry>
	</category>

	<category id='other'>
		<entry id="sea_water_buoyancy">
			<canonical_units>m s-2</canonical_units><staggering>t</staggering>
			<description>
			buoyancy of sea water: :math:`\ -\frac{g}{\rho_0}\rho`
			</description>
		</entry>
	</category>

</standard_name_table>
