#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
TODO: Documentation of `standard_name_parser` module.
"""

import sys
from pycomodo.compat import OrderedDict

from xml.etree import ElementTree


class Entry:
    """
    TODO: Documentation of ``Entry``.
    """
    def __init__(self, element):

        self.name = element.get('id')

        for child in iter(element):

            child_name    = child.tag.strip()
            child_content = "" if child.text is None else child.text.strip()

            self.__dict__[child_name] = child_content

    @property
    def units(self):
        return self.canonical_units

    def __repr__(self):
        return "'%s' (%s)" % (self.name, self.units)

    def __str__(self):
        return self.__repr__()


class GenericParser:

    def __init__(self, filename=None):
        """
        Runs the parser on the file `filename`.
        """
        self.entries = OrderedDict()
        self.aliases = OrderedDict()

        if filename is not None:
            self.parse_file(filename)

    def __contains__(self, std_name):
        return (std_name in self.entries) or (std_name in self.aliases)

    def __getitem__(self, std_name):
        if std_name in self.entries:
            return self.entries[std_name]
        if std_name in self.aliases:
            return self.aliases[std_name]
        return None

    def parse_file(self, filename):

        self._init_parse(filename)
        self.parse_content()

    def _init_parse(self, filename):

        self.tree = ElementTree.parse(filename)
        self.version       = self.tree.find('version_number').text
        self.contact       = self.tree.find('contact').text
        self.institution   = self.tree.find('institution').text
        self.last_modified = self.tree.find('last_modified').text

    def parse_content(self):
        pass

    def get_aliases_of_name(self, name):
        return [alias_name for (alias_name, alias) in self.aliases.iteritems() if alias.name == name]

    def get_iterunits(self):
        return (entry.units for entry in self.entries.itervalues())


class StandardNameParser(GenericParser):
    """
    Parser for XML files compatible with cf-standard-name-table-1.1.xsd
    """
    def parse_content(self):

        for entry in self.tree.findall('entry'):
            entry_name = entry.get('id')
            self.entries[entry_name] = Entry(entry)

        for alias in self.tree.findall('alias'):
            alias_name = alias.get('id')
            entry_name = alias.find('entry_id').text.strip()
            self.aliases[alias_name] = self.entries[entry_name]


class BudgetTermsParser(GenericParser):
    """
    Parser for 'comodo-budget-terms.xml' XML file.
    """
    def parse_content(self):

        self.categories = OrderedDict()

        for category in self.tree.findall('category'):

            category_name = category.get('id')
            if category_name not in self.categories:
                self.categories[category_name] = []

            for element in category.findall('entry'):
                entry_name = element.get('id')
                entry      = Entry(element)
                entry.category = category_name
                self.entries[entry_name] = entry
                self.categories[category_name].append(entry)

            for alias in category.findall('alias'):
                alias_name = alias.get('id')
                entry_name = alias.find('entry_id').text.strip()
                self.aliases[alias_name] = self.entries[entry_name]


def test_main( filename ):

    # Set up dictionary of standard_names and their assoc. units
    parser = StandardNameParser()
    parser.parse_file(filename)
    for name in parser.entries:
        entry = parser.entries[name]
        aliases = parser.get_aliases_of_name(name)
        print "%s :" % ( name )
        if entry.units:
            print " - units = \"%s\"" % entry.units
        if entry.description:
            print " - desc = \"%s\"" % entry.description
        if aliases:
            print " - aliases:  %s" % (', '.join(str(alias) for alias in aliases))


if __name__ == "__main__":

    test_main(sys.argv[1])
