#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Defines the class :class:`.Variable` that represent a NetCDF variable with dimensions, data and attributes.
"""

import numpy as np

from pycomodo import log
from pycomodo.compat import OrderedDict, DefaultOrderedDict
from pycomodo.core.exceptions import UnknownVariable, UnknownCoordinate
from pycomodo.core.checker import CheckWarehouse
from pycomodo.core.netcdf_variable import NetcdfVariable
import pycomodo.operators as op
import pycomodo.util.vertical as puz


class Variable(object):
    """
    This class represents a NetCDF variable with its dimensions, data and attributes.

    In the following, most code samples will be based on a SSH variable from an archive built with a
    ROMS model output file :

    >>> import pycomodo
    >>> archive = pycomodo.Archive('examples/nc_files/roms_vortex.nc')
    >>> ssh  = archive.get_variable(stdname='sea_surface_height')
    >>> xvel = archive.get_variable(stdname='sea_water_x_velocity_at_u_location')

    A detailed output is given :doc:`[here]<../examples/archive_roms>`.

    .. py:attribute:: axes

        Dictionary of axes, which map a letter (``'T'``, ``'Z'``, ``'Y'`` or ``'X'``) to a coordinate
        variable. The latter is the :class:`.Variable` instance associated with the dimension of the
        corresponding axis.

        >>> xaxis = ssh.axes['X']
        >>> print xaxis.info()
            float32 xi_rho(xi_rho)
                xi_rho:long_name = "x-dimension of the grid"
                xi_rho:standard_name = "x_grid_index"
                xi_rho:axis = "X"
                xi_rho:c_grid_dynamic_range = "2:61"
    """
    def __init__(self, ncvar, from_archive):

        self.set_name(ncvar._name)
        self.__dict__["_var"]    = ncvar            # use this for to bypass Variable().__setattr__()
        self.__dict__["archive"] = from_archive
        self.__dict__["axes"] = DefaultOrderedDict()

    def __getattr__(self, name):
        # If the user calls Variable(var).some_attribute, it is deferred to var.some_attribute
        try:
            return self.__getattribute__(name)
        except AttributeError:
            try:
                return self._var.__getattr__(name)
            except (AttributeError, KeyError):
                return self._var.__getattribute__(name)

    def __setattr__(self, attr, value):
        # Store user defined attributes in _var's dict.
        setattr(self._var, attr, value)

    def __getitem__(self, index):
        # When called directly, Variable(var)[index] scales the data.
        # However, Variable(var).data[index] returns the raw data
        data = np.ma.asarray(self._var[index])
        if not data.shape:
            return np.asscalar(data)
        else:
            return data

    def __setitem__(self, index, data):
        self._var[index] = data

    def __hash__(self):
        """
        Compute a hash value based on the definition of the Variable (not on its data).
        """
        var = self._var
        hstr = "{0}{1}".format(var._name, var.dtype)
        hstr += "|".join(str(i) for i in var.shape)
        hstr += "|".join(str(i) for i in var.dimensions)
        try:
            hstr += var.standard_name
        except AttributeError:
            pass
        return hash(hstr)

    def __add__(self, other):
        return op.add(self, other)

    def __sub__(self, other):
        return op.sub(self, other)

    def __div__(self, other):
        return op.div(self, other)

    def __mul__(self, other):
        return op.mul(self, other)

    def set_name(self, new_name):
        """
        Define the name of the variable.

        >>> print str(ssh)
        zeta
        >>> ssh.set_name("my_ssh")
        >>> print str(ssh)
        my_ssh
        """
        # It should not be visible as a public attribute, so we have this special treatment:
        # bypass Variable.__setattr__ and and netcdf_variable.__setattr__ routines (GROS HACKK!)
        self.__dict__["_ncname"] = new_name

    def __str__(self):
        return self._ncname

    def info(self, verbose=2):
        """
        Prints some information about the variable.

        :param verbose: Amount of information. 0: name only, 2: full record.

        >>> print ssh.info(0)
        zeta

        >>> print ssh.info(1)
        float32 zeta(time, eta_rho, xi_rho)

        >>> print ssh.info(2)
            float32 zeta(time, eta_rho, xi_rho)
                zeta:long_name = "free-surface"
                zeta:units = "meter"
                zeta:field = "free-surface, scalar, series"
                zeta:standard_name = "sea_surface_height"
                zeta:coordinates = "x_rho y_rho"
        """
        if verbose == 0:
            return str(self)

        dimensions = ", ".join(self.dimensions)
        header = "{type} {name}({dimensions})"  \
                 .format(type=self._var.dtype, name=str(self), dimensions=dimensions)
        if verbose == 1:
            return header

        infostr = "    " + header
        attrs_dict = self.get_attributes()

        # Build string list of attributes
        for attrname, attr in attrs_dict.iteritems():
            if isinstance(attr, basestring):
                attr = u"\"{0}\"".format(attr)
            infostr += "\n        {0}:{1} = {2}".format(str(self), attrname, attr)

        return infostr

    def build_axes_dict(self):
        """
        Build the :obj:`.axes` dictionary for the Variable.
        """
        # let's document the variable's axes
        for dimname in self.dimensions:
            try:
                coordvar = self.archive.variables[dimname]
                axis = coordvar.axis
                self.axes[axis] = coordvar
            except (AttributeError, KeyError):
                pass

    @property
    def axes_str(self):
        """
        Returns the axes as a string.

        >>> ssh.axes_str
        u'TYX'
        >>> ssh.axes['X'].axes_str
        u'X'
        """
        return "".join(self.axes.keys())

    def has_recdim(self):
        """
        Return True if any dimension of the variable is a record dimension.

        >>> ssh.has_recdim()
        True
        >>> ssh.axes['X'].has_recdim()
        False
        """
        return self._var._nunlimdim > 0

    @property
    def shape(self):
        """
        Return the shape of the variable as a tuple.

        >>> ssh.shape
        (3, 62, 62)
        """
        return self._var.shape

    @property
    def position(self):
        """
        Return the position of the variable on the C grid, as a letter or a couple
        of letters ('t','u','v','f','w','uw','vw','fw').

        >>> ssh.position
        't'
        >>> xvel.position
        'u'
        """
        return op.var_position_on_c_grid(self)

    @property
    def standard_name(self):
        """
        Return the standard_name attribute. Always in lower case.

        >>> ssh.standard_name
        u'sea_surface_height'
        """
        try:
            stdname = self._var.standard_name.lower()
        except KeyError as e:
            raise AttributeError(str(e))

        return stdname

    @property
    def c_grid_axis_shift(self):
        """
        Return the value of the shift attribute (cf. :ref:`Comodo norm<norm-c_grid_axis_shift>`).
        Only works for coordinate variables.

        .. code-block:: python

            >>> # Sea-water x velcity is staggered along the X axis (U-point)
            >>> xvel.axes['X'].c_grid_axis_shift
            0.5
            >>> xvel.axes['Y'].c_grid_axis_shift
            0.0
        """
        if not self.is_coordinate_variable():
            raise Exception("You should not call c_grid_axis_shift on a regular variable "
                            "(only coordinate variables)")
        try:
            shift = self._var.c_grid_axis_shift
        except (AttributeError, KeyError):
            shift = 0.

        return shift

    @property
    def c_grid_dynamic_range(self):
        """
        Return the value of the dynamic range (cf. :ref:`Comodo norm<norm-c_grid_dynamic_range>`).
        Only works for coordinate variables.

        >>> ssh.axes['X'].c_grid_dynamic_range
        u'2:61'
        >>> xvel.axes['X'].c_grid_dynamic_range
        u'2:60'
        """
        if not self.is_coordinate_variable():
            raise Exception("You should not call c_grid_dynamic_range on a regular variable "
                            "(only coordinate variables)")
        try:
            drange = self._var.c_grid_dynamic_range
        except AttributeError:
            drange = "1:{0}".format(len(self.archive.dimensions[str(self)]))

        return drange

    def get_global_range(self, axis):
        """
        Returns the global range of the variable's *axis* as a tuple of integers,
        with C/Python notation (start with 0).

        >>> ssh.get_global_range('X')
        (0, 61)
        """
        axisvar = self.axes[axis]
        if axisvar is None:
            raise UnknownCoordinate("Unable to find '{0}' axis for '{1}'.".format(axis, self))

        return self.archive.get_dimension_global_range(axisvar)

    def get_dynamic_range(self, axis):
        """
        Returns the dynamic range of the variable's *axis* as a tuple of integers,
        with C/Python notation (start with 0).

        >>> ssh.get_dynamic_range('X')
        (1, 60)
        """
        axisvar = self.axes[axis]
        if axisvar is None:
            raise UnknownCoordinate("Unable to find '{0}' axis for '{1}'.".format(axis, self))

        return self.archive.get_dimension_dynamic_range(axisvar)

    def get_dynamic_slice(self, *args):
        """
        Returns a tuple of numpy slices to match the dynamic range of the variable axes.

        >>> u_slice = xvel.get_dynamic_slice("Y", "X")
        >>> print u_slice
        (Ellipsis, slice(1, 61, None), slice(1, 60, None))

        We can use this slice to access the dynamic part of the data array :

        >>> u_dynamic_data = xvel[u_slice]
        >>> print type(u_dynamic_data), u_dynamic_data.shape
        <class 'numpy.ma.core.MaskedArray'> (3, 10, 60, 59)

        If a requested axis is not present, the method returns a generic slice.

        >>> xvel.get_dynamic_slice("M")  # This axis does not exist
        (Ellipsis,)
        """
        slc = np.index_exp[...]
        for dim_name in self.dimensions:
            covar = self.archive.coordinate_variables[dim_name]
            for axis in args:
                try:
                    if covar.axis == axis:
                        ibeg, iend = self.get_dynamic_range(axis)
                        slc = slc + np.index_exp[ibeg:iend+1]
                        continue
                except AttributeError:
                    pass
        return slc

    def get_fill_value(self):
        """
        Returns a valid fill value, based on `_FillValue` or `missing_value` attributes if one of them
        if present. Otherwise returns None.
        """
        if hasattr(self, "_FillValue"):
            return self._FillValue
        if hasattr(self, "missing_value"):
            return self.missing_value
        return None

    def set_fill_value(self, fill_value):
        """
        Try to set the fill value. Note: this will not work for Variables connected to a NetCDF
        file in a read-only way.

        >>> ssh.set_fill_value(-9999.)
        [WARNING]: Unable to change fill_value for variable 'zeta'.
        """
        try:
            self._var.set_fill_value(fill_value)
        except AttributeError:
            log.warning("Unable to change fill_value for variable '{0}'.".format(self))

    def get_attributes(self):
        """
        Returns a dictionary containing the variable's attributes.

        >>> ssh.get_attributes()
        OrderedDict([
            (u'standard_name', u'sea_surface_height'),
            (u'long_name', u'free-surface'),
            (u'units', u'meter'),
            (u'coordinates', u'x_rho y_rho')])
        """
        return OrderedDict((attrname, getattr(self, attrname)) for attrname in self.ncattrs())

    def is_writeable(self):
        """
        Return True if we can modify the content of the Variable

        >>> ssh.is_writeable()
        False
        """
        return isinstance(self._var, NetcdfVariable)

    def is_coordinate_variable(self):
        """
        Returns True if we are a coordinate variable.

        >>> ssh.is_coordinate_variable()
        False
        >>> ssh.axes['T'].is_coordinate_variable()
        True
        """
        return str(self) in self.archive.coordinate_variables

    def is_latlon_variable(self):
        """
        Returns True if the variable's name is present the ``coordinates`` attribute of at
        least one variable of the archive.

        >>> ssh.is_latlon_variable()
        False
        >>> archive.get_variable('x_rho').is_latlon_variable()
        True
        """
        for var in self.archive.variables.itervalues():
            try:
                if str(self) in var.coordinates.split(' '):
                    return True
            except AttributeError:
                continue
        return False

    def get_auxiliary_coordinates(self):
        """
        Returns a dictionary mapping grid axis names (either 'X' or 'Y') to variables that contain
        auxiliary coordinates, according to the attribute ``coordinates`` and the content of the
        corresponding variables.

        >>> coords = ssh.get_auxiliary_coordinates()
        >>> print coords
        {'Y': <pycomodo.core.variable.Variable object at 0x1052e4310>,
         'X': <pycomodo.core.variable.Variable object at 0x1052e4210>}
        >>> print coords['X'].info()
            float32 x_rho(eta_rho, xi_rho)
                x_rho:standard_name = "plane_x_coordinate"
                x_rho:long_name = "x-locations of RHO-points"
                x_rho:units = "meter"
        """
        coordinates = {}

        if not hasattr(self, 'coordinates'):
            return coordinates

        for coord_name in self.coordinates.split(' '):

            vdir = None
            try:
                tmp_var = self.archive.get_variable(coord_name)
            except UnknownVariable:
                # Unable to find the coordinate variable: we skip this name
                continue

            # First, we try with the 'standard_name' attribute
            if hasattr(tmp_var, 'standard_name'):
                if (   tmp_var.standard_name.startswith("plane_x_coordinate") |
                       tmp_var.standard_name.startswith("longitude") ):
                    vdir = 'X'
                elif ( tmp_var.standard_name.startswith("plane_y_coordinate") |
                       tmp_var.standard_name.startswith("latitude") ):
                    vdir = 'Y'

            # Next, try with the 'long_name' attribute
            if vdir is None and hasattr(tmp_var, 'long_name'):
                if (   tmp_var.long_name.lower().startswith("latitude") ):
                    vdir = 'Y'
                elif ( tmp_var.long_name.lower().startswith("longitude") ):
                    vdir = 'X'

            # Finally, try with the variable's name.
            if vdir is None:
                if (   coord_name.lower().startswith("lon") ):
                    vdir = 'X'
                elif ( coord_name.lower().startswith("lat") ):
                    vdir = 'Y'
                elif ( "lon" in coord_name.lower() ):
                    vdir = 'X'
                elif ( "lat" in coord_name.lower() ):
                    vdir = 'Y'

            if vdir is not None:
                coordinates[vdir] = tmp_var

        return coordinates

    def get_instant_value(self, tindex, chunck_xy=None, dtype=None):
        """
        Returns a numpy masked array filled with the content of the variable at time index *tindex*.
        The horizontal slice can be restricted by defining *chunk_xy*.

        :param int tindex: Requested time index.
        :param chunck_xy: Horizontal restriction. Default: whole horizontal domain.
        :param dtype: Numpy type for output data array.

        >>> ssh.get_instant_value(0).shape
        (62, 62)
        >>> ssh.get_instant_value(-1, np.index_exp[30:32, 30:32], dtype=np.float64)
        masked_array(data =
         [[-0.01967009 -0.05502243]
          [-0.06833246 -0.07881874]],
                     mask = False,
               fill_value = 1e+20)
        """
        dtype    = self.dtype if (dtype is None) else dtype
        t_slice  = np.index_exp[tindex] if 'T' in self.axes      else ()
        t_slice += chunck_xy            if chunck_xy is not None else np.index_exp[...]

        return np.ma.array(self[t_slice], dtype=dtype)

    def get_zlevels(self, stag_axis=None, name=None, stdname=None, axes="TZYX", tpoint=None, hpoint=None):
        """
        Returns a :class:`.Variable` instance for the depth, using the axes of the current variable.
        If *stag_axis* is specified (either *'X'*, *'Y'* or *'Z'*), returns the depth for the staggered grid.

        :param stag_axis: axis to stagger (either *'X'*, *'Y'* or *'Z'*).
        :param name: name for the output variable.
        :param stdname: value of the ``standard_name`` attribute for the output variable.
        :param axes: list of axes for the output variable. Axis ``'Z'`` must be present.
        :param tpoint: time index where to get the data if 'T' axis is not requested. Default: 0.
        :param hpoint: horizontal point index where to get the data if either 'X' or 'Y' axis is not requested.
                       Default: (0, 0).

        >>> print ssh.get_zlevels(name="z").info()
            float32 z(time, s_rho, eta_rho, xi_rho)
                z:units = "m"
                z:standard_name = "depth"
        >>> print ssh.get_zlevels(stag_axis='Z', name="zw").info()
            float32 zw(time, s_w, eta_rho, xi_rho)
                zw:units = "m"
                zw:standard_name = "depth_at_w_location"

        It is possible to restrict the variable to a subset of the original axes. In this case,
        you may want to specify the value of the indices for the missing dimensions :

        >>> zw_at_center = ssh.get_zlevels(stag_axis='Z', name="zw_at_center",
                                           axes="Z", tpoint=0, hpoint=(31, 31))
        >>> print zw_at_center[:]
        [-5000.000 -4499.914 -3999.828 -3499.743 -2999.657 -2499.571
         -1999.485 -1499.399  -999.314  -499.228     0.858]
        """
        if self.is_coordinate_variable():
            raise Exception("get_zlevels: cannot be called for a coordinate variable")

        log.debug("# Variable.get_zlevels from variable '{0}' (stag_axis='{1}')".format(self, stag_axis))

        location = op.shift_location(self.position, stag_axis)

        return puz.get_zlevels(self.archive, location, axes, name, stdname, tpoint, hpoint)

    def diff_x(self, name=None, stdname=None, dummy=False):
        """
        Calls :func:`pycomodo.operators.differentiate.diff_x()` on the variable.
        """
        return op.diff_x(self, name, stdname, dummy)

    def diff_y(self, name=None, stdname=None, dummy=False):
        """
        Calls :func:`pycomodo.operators.differentiate.diff_y()` on the variable.
        """
        return op.diff_y(self, name, stdname, dummy)

    def diff_z(self, name=None, stdname=None, dummy=False):
        """
        Calls :func:`pycomodo.operators.differentiate.diff_z()` on the variable.
        """
        return op.diff_z(self, name, stdname, dummy)

    def interpolate(self, loc_dest, name=None, std_name=None, method='arithmetic'):
        """
        Calls :func:`pycomodo.operators.interpolation.interpolate` on the variable.
        """
        return op.interpolate(self, loc_dest, name, std_name, method)

    def check(self, verbose=False, log=None):
        """
        Prints a colored report on the conformity of the variable to the Comodo File Format norm.
        """
        cwh = CheckWarehouse(log)
        cwh.add(self.archive.checker.check_var(self))
        cwh.dump(verbose)
