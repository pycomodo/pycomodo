#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Defines some exceptions for particular errors in pycomodo.
"""


__all__ = [
    "UnknownCoordinate", "UnknownVariable",
    "InterpolationError", "ErrorCoordVarNonConformal",
    "WarnCoordVarAlmostConformal", "ErrorMalformedFormula" ]


class UnknownCoordinate(Exception):
    """
    Raised when an unknown coordinate variable is requested.
    """
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class UnknownVariable(Exception):
    """
    Raised when an unknown variable is requested.
    """
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class InterpolationError(Exception):
    """
    Raised when an interpolation failed.
    """
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return self.value


# ===================================================================================
#  Errors in ComodoChecker
# ===================================================================================

class ErrorCoordVarNonConformal(Exception):
    """
    Raised when a coordinate variable is non-conform.
    """
    pass


class WarnCoordVarAlmostConformal(Exception):
    """
    Raised when a coordinate variable is non-conform, but error reporting is not activated.
    """
    pass


class ErrorMalformedFormula(Exception):
    """
    Raised when a ``formula_terms`` attribute for vertical coordinate variable is malformed.
    """
    pass
