#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Defines the class :class:`.NetcdfVariable` that mimick a :class:`netCDF4.Variable`.
"""

import numpy as np
import operator

from pycomodo.compat import OrderedDict


class NetcdfVariable(object):
    """
    In-memory equivalent of :class:`netCDF4.Variable`. The latter maps an on-disk NetCDF variable object.
    """
    def __init__(self, varname, dimensions, dtype, shape, has_recdim, fill_value=None, attrdict=None):
        self._name = varname if varname is not None else ""
        self._dimensions = dimensions
        self._shape = shape
        self._maskandscale = True
        self._data = np.ma.empty(shape, dtype)
        self._data[:] = np.ma.masked
        self._nunlimdim = 1 if has_recdim else 0
        self._attributes = OrderedDict()
        if fill_value is not None:
            self.set_fill_value(fill_value)
        if attrdict is None:
            attrdict = {}
        for attrname, attrvalue in attrdict.iteritems():
            self._attributes[attrname] = attrvalue

    def __str__(self):
        return self._name

    def __getattr__(self, name):
        try:
            return self.__getattribute__(name)
        except:
            return self._attributes[name]

    def __setattr__(self, name, value):
        if name.startswith('_') and name != '_FillValue':
            self.__dict__[name] = value
        else:
            self._attributes[name] = value

    def __getitem__(self, item):
        # When called directly, fake_var[index] scales the data if maskandscale is True.
        data = self._data[item]
        if self._maskandscale:
            if (hasattr(self, 'scale_factor') and hasattr(self, 'add_offset') and
               (self.add_offset != 0.0 or self.scale_factor != 1.0)):
                data = self.scale_factor*data + self.add_offset
            # else if variable has only scale_factor attributes, rescale.
            elif hasattr(self, 'scale_factor') and self.scale_factor != 1.0:
                data = self.scale_factor*data
            # else if variable has only add_offset attributes, rescale.
            elif hasattr(self, 'add_offset') and self.add_offset != 0.0:
                data += self.add_offset
        return data

    def __setitem__(self, item, value):

        if self._maskandscale:
            # pack non-masked values using scale_factor and add_offset
            if hasattr(self, 'scale_factor') and hasattr(self, 'add_offset'):
                value = (value - self.add_offset)/self.scale_factor
                if self._data.dtype.kind == 'i':
                    value = np.around(value)
            elif hasattr(self, 'scale_factor'):
                value /= self.scale_factor
                if self._data.dtype.kind == 'i':
                    value = np.around(value)
            elif hasattr(self, 'add_offset'):
                value -= self.add_offset
                if self._data.dtype.kind == 'i':
                    value = np.around(value)
        self._data[item] = value

    @property
    def shape(self):
        """
        Shape of the data array.

        >>> ssh.shape
        (3, 62, 62)
        """
        return self._shape

    @property
    def size(self):
        """
        Number of array elements. Equals :math:`\Pi_i d_i` where :math:`d_i` are the dimensions.

        >>> ssh.size
        11532
        """
        return reduce(operator.mul, self.shape)

    @property
    def dimensions(self):
        """
        Tuple of the variable's dimensions names.

        >>> ssh.dimensions
        (u'time', u'eta_rho', u'xi_rho')
        """
        return self._dimensions

    @property
    def dtype(self):
        """
        Numpy *dtype* of data.

        >>> ssh.dtype
        dtype('float32')
        """
        return self._data.dtype

    @property
    def mask(self):
        return self._data.mask

    def ncattrs(self):
        """
        List of the variable's attributes.

        >>> ssh.ncattrs()
        [u'standard_name', u'long_name', u'units', u'field', u'coordinates']
        """
        return self._attributes

    def set_auto_maskandscale(self, maskandscale):
        self._maskandscale = bool(maskandscale)

    def set_fill_value(self, fill_value):
        """
        Define a new fill value for the variable (``_FillValue`` attribute).
        """
        self._data.set_fill_value(fill_value)
        self._attributes["_FillValue"] = fill_value
