#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Defines the class :class:`Dimension` designed to represent a NetCDF dimension.
"""

from collections import Sized


class Dimension(Sized):
    """
    Represents a dimension in a NetCDF file.
    """
    def __init__(self, name, size, unlimited=False):
        self.name = name                #: Name of the dimension.
        self.size = size                #: Length of the dimension.

        #: True if the dimension is *unlimited*.
        #: See `NetCDF documentation <http://www.unidata.ucar.edu/software/netcdf/docs/html_tutorial/unlimited_dims.html>`_.
        self.unlimited = unlimited

    def __str__(self):
        dimstr = "{0} = {1}".format(self.name, self.size)
        if self.unlimited:
            dimstr += " [unlimited]"
        return dimstr

    def __len__(self):
        return self.size

    def isunlimited(self):
        """
        Returns True if the dimension is unlimited.
        """
        return bool(self.unlimited)
