#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Defines the class :class:`Archive` designed to gather all data and metadata from a set of NetCDF files.

In the following, most code samples will be based on the archive built with a ROMS model output file :

>>> import pycomodo
>>> archive = pycomodo.Archive('examples/nc_files/roms_vortex.nc')

A detailed output is given :doc:`[here]<../examples/archive_roms>`.
"""

from collections import Iterable

import os
import sys
import numpy as np

from pycomodo import log, cdf, USE_NETCDF4
from pycomodo.compat import OrderedDict, DefaultOrderedDict
from pycomodo.core.exceptions import UnknownCoordinate, UnknownVariable, InterpolationError
from pycomodo.core.dimension import Dimension
from pycomodo.core.netcdf_variable import NetcdfVariable
from pycomodo.core.variable import Variable
from pycomodo.core.checker import ArchiveChecker
from pycomodo.io.termcolor import bolded

import pycomodo.operators as op
import pycomodo.util.stdnames as stdnames
import pycomodo.util.vertical as puz
import pycomodo.util.variables as puv


class Archive:
    """
    This class gathers all data and metadata from a set of NetCDF files.
    The variables and dimensions must be consistent across all files, except for the record
    dimension.

    For a single file, an :class:`Archive` is simply created as follows

    >>> archive = Archive("single_ncfile.nc")

    When variables are dispatched across multiple files, we can pass either a python list
    or a space-separated string of file names. If some files contain only grid data, with no
    record dimension, they can be passed separately with the *grids* argument.

    >>> archive = Archive(["grid_T.nc", "grid_U.nc", "grid_V.nc"], grids="meshgrid.nc")

    If the data is scattered along the record dimension in multiple files, you will achieve much
    better performance if you set the argument *multifile* to **True**. In this case, grid data files
    _must_ be passed with the *grid* argument.

    >>> archive = Archive("2000-01-01.nc 2000-01-02.nc", grids="grid.nc", multifile=True)
    """

    def __init__(self, inputs, grids=None, multifile=False):

        self.file_list  = OrderedDict()
        self.dimensions = OrderedDict()
        self.variables  = OrderedDict()
        self.standard_names = {}
        self.coordinate_variables = {}
        self.metrics = OrderedDict()
        self.ntimes  = 0
        self.checker = ArchiveChecker(self)
        self.multifile = multifile
        self.concat_file = {}
        self.grid_names = None
        self.data_names = None

        self._init_files(inputs, grids)
        self._parse_files()
        self.complete_init()

    def complete_init(self):

        # Compute the size of the record dimension.
        for dim in self.dimensions.values():
            if dim.isunlimited():
                self.ntimes = len(dim)
                break

        # Build 'axes' dictionary for all vars
        for var in self.variables.itervalues():
            var.build_axes_dict()

        # Try to build metrics variables
        self._build_metrics()

    def __str__(self):

        if self.grid_names:
            return "Archive[ grids=({0}) ; data=({1}) ]".format(self.grid_names, self.data_names)
        else:
            return "Archive[ ({0}) ]".format(self.data_names)

    def info(self, verbose=0):
        """
        Return a string with a description of the archive's content.

        - verbose == 0 : just the name of the input files.
        - verbose == 1 : compact information with list of dimensions and variables.
        - verbose == 2 : detailed information for each variable, much like 'ncdump -h'.
        """
        if verbose == 0:
            info_str = str(self)

        elif verbose == 1:
            info_str  = "{0}:\n".format(self)
            info_str += " - Dimensions : {0}\n".format(", ".join(self.dimensions))
            info_str += " - Variables  : {0}\n".format(", ".join(self.variables))

        elif verbose > 1:
            info_str  = "{0}:\n".format(self)
            info_str += "Dimensions :\n"
            for dim in self.dimensions.itervalues():
                info_str += "    {0}\n".format(dim)
            info_str += "Variables :\n"
            info_str_coord = ""
            info_str_other = ""
            for varname in sorted(self.variables):
                var = self.variables[varname]
                vinfo = "{0}\n".format(var.info())
                # Separate coordinate variables and regular ones
                if var.is_coordinate_variable():
                    info_str_coord += vinfo
                else:
                    info_str_other += vinfo
            # Display coordinate variables first.
            info_str += info_str_coord
            info_str += info_str_other

        return info_str

    def _init_files(self, inputs, grids):

        # The first files to be parsed should be the grids, if provided
        if isinstance(grids, str):
            gridfile_list = grids.split()
        elif isinstance(grids, Iterable):
            gridfile_list = grids
        else:
            gridfile_list = []

        # Then the data files
        if isinstance(inputs, str):
            datafile_list = inputs.split()
        elif isinstance(inputs, Iterable):
            datafile_list = inputs
        else:
            raise Exception("unknown input")

        # Build names for __str__()
        self.grid_names = ", ".join("'{0}'".format(g) for g in gridfile_list)
        self.data_names = ", ".join("'{0}'".format(f) for f in datafile_list)

        # Create Datasets from file names
        for ncfile in gridfile_list:    # ... first the grid files
            fname = os.path.basename(ncfile)
            self.file_list[fname] = cdf.Dataset(ncfile, mode="r")

        if self.multifile:              # ... then the data files
            if not USE_NETCDF4:
                log.error("Sorry, 'multifile' option only works with 'netcdf4-python'. "
                          "Please install 'https://code.google.com/p/netcdf4-python/'.")
                sys.exit(1)

            # We have been asked to use a 'mutlifile' view of the data file list.
            # To do this, we rely on netCDF4.MFDataset, which offer a view on this list as if it
            # was a single file. This won't work if the second or subsequent file in the list
            # contain variables that are not present in the first one.
            data_file_names = ":".join("{0}".format(f) for f in datafile_list)
            try:
                self.file_list[data_file_names] = cdf.MFDataset(datafile_list, check=False)
            except KeyError as ke:
                log.error("Sorry, the variable {0} has not been found in all files. ".format(ke) +
                          "Are you sure you correctly use 'multifile' option ?")
                raise ke

        else:
            # No mutlifile view was requested, we'll handle all the files one after the another.
            # The routines _handle_dimensions() and _handle_variables() have their own
            # recipe to decide whether concatenation should occur or not. If it should, it will
            # be done with in-memory data copy. Therefore in this case with large datasets, you
            # should seriously consider the *multifile* option during the Archive creation.
            for ncfile in datafile_list:
                fname = os.path.basename(ncfile)
                self.file_list[fname] = cdf.Dataset(ncfile, mode="r")

    def _parse_files(self):

        # Look over all input NetCDF files
        for fname in self.file_list:

            log.debug("_parse_files: == Parsing {0}...".format(fname))
            log.debug("_parse_files: "+"="*40)

            self._handle_dimensions(fname)
            self._handle_variables(fname)

    def register_variable(self, var):
        """
        Keeps track of *var*, and some of its features, depending on whether it is a coordinate variable
        or possesses a standard_name.

        This routine is used internally in pycomodo, but can occasionally be called to register
        variables created manually. The registration is required to later retrieve the reference
        of the variable, via :func:`get_variable` for example.
        """
        varname = str(var)

        try:
            stdname = var.standard_name
        except AttributeError:
            stdname = None

        log.debug("register_variable: '{0}' ({1}) with dimensions [{2}]"
                  .format(varname, stdname, ", ".join(var.dimensions)))

        # Register the variable
        self.variables[varname] = var

        # Is it a coordinate variable ?
        if len(var.dimensions) == 1 and var.dimensions[0] == varname:
            self.coordinate_variables[varname] = var

        # Keep track of standard_name
        if stdname is not None:
            self.standard_names[stdname] = var

    def unregister_variable(self, var):
        """
        Removes *var* from the various dictionaries.
        """
        varname = str(var)

        if varname in self.variables:
            del self.variables[varname]

        if varname in self.coordinate_variables:
            del self.coordinate_variables[varname]

        if hasattr(var, 'standard_name') and var.standard_name in self.standard_names:
            del self.standard_names[var.standard_name]

    def _handle_dimensions(self, fname):
        """
        Keep track of dimensions present in the file.
        """
        ncfile = self.file_list[fname]
        # First we have to know about the time axis (unlimited dimension).
        taxisname = None
        oldtaxisvar = newtaxisvar = None
        for dimname, dim in ncfile.dimensions.iteritems():
            if dim.isunlimited():
                taxisname = dimname
                try:
                    newtaxisvar = ncfile.variables[taxisname]
                except KeyError:
                    log.warning("dimension '{0}'".format(dimname) +
                                " has no associated coordinate variable.")
                try:
                    oldtaxisvar = self.variables[taxisname]
                except KeyError:
                    pass
                break

        # we should concatenate the dimensions if the new time axis is not already
        # included in what we have seen so far
        self.concat_file[fname] = (
            (all(t is not None for t in (oldtaxisvar, newtaxisvar))
             and not set(newtaxisvar[:]).issubset(oldtaxisvar[:]))
            or (oldtaxisvar is None and newtaxisvar is not None) )

        log.debug("_handle_dimensions: We will {0}concatenate the data"
                  .format("" if self.concat_file[fname] else "NOT "))

        # Keep track of dimensions across files
        for dimname in ncfile.dimensions:
            newdim = ncfile.dimensions[dimname]
            new_length = len(newdim)
            new_is_unlim = newdim.isunlimited()
            if dimname in self.dimensions:
                # This dimension has already been found in a previous file
                olddim = self.dimensions[dimname]
                old_is_unlim = olddim.isunlimited()
                if old_is_unlim ^ new_is_unlim:
                    # One dimension is unlimited, but not the other. We shall not tolerate this.
                    raise Exception("_handle_dimensions: inconsistency between files " +
                                    "for dimension {0}.".format(dimname))
                if old_is_unlim and new_is_unlim:
                    # Both dimensions are unlimited, should we concatenate ?
                    if self.concat_file[fname]:
                        del self.dimensions[dimname]
                        new_length = len(olddim) + len(newdim)
                        log.debug("updating unlimited dimension '{0}': {1} -> {2}"
                                  .format(dimname, len(olddim), new_length))
                    else:
                        continue
                else:
                    # Neither version is unlimited, so...
                    if len(newdim) == len(olddim):
                        # Both dimensions are the same. We skip this name.
                        continue
                    else:
                        # These two guys have the same name but do not have the same size. Call the police.
                        raise Exception("_handle_dimensions: inconsitency between files " +
                                        "for dimension {0}.".format(dimname))

            # Ok, we have a new or updated dimension. We keep track of that
            self.dimensions[dimname] = Dimension(dimname, new_length, newdim.isunlimited())
            log.debug("_handle_dimensions: register new dimension: '{0}'".format(self.dimensions[dimname]))

    def _handle_variables(self, fname):
        """
        Create a pycomodo.Variable instance for each variable found in the NetCDF files.
        """
        ncfile = self.file_list[fname]

        # Let's inspect every variable from 'ncfile'.
        for varname in ncfile.variables:

            newvar = Variable(ncfile.variables[varname], self)

            if varname not in self.variables:
                # We've never seen this name...
                try:
                    # Have we seen the standard_name ?
                    stdname = newvar.standard_name
                except AttributeError:
                    pass
                else:
                    if stdname in self.standard_names:
                        # if yes, we keep only the new variable
                        oldvar = self.standard_names[stdname]
                        self.unregister_variable(oldvar)
                        log.warning("Variable '{0}' is replaced by '{1}' because they have"
                                    " the same standard_name '{2}'".format(oldvar, newvar, stdname))

                # We keep track of this new variable and go to the next.
                self.register_variable(newvar)
                continue

            # We've already met 'varname' before.
            # We now have to decide what to do with the new version
            oldvar = self.variables[varname]

            if newvar.has_recdim():
                # 'newvar' has a record dimension
                if oldvar.has_recdim():
                    # so has the old version
                    log.debug("_handle_variables: '{0}' has a record dimension.".format(varname))

                    # we have to decide whether to concatenate the data or not.
                    try:
                        # if we deal with a time coordinate variable, we concatenate only if it is the
                        # first time we meet the new time values.
                        concatenate  = oldvar.axis == "T" and newvar.axis == "T"
                        concatenate &= not set(newvar[:]).issubset(oldvar[:])
                    except AttributeError:
                        # if it's not a time coordinate variable, we concatenate if necessary
                        concatenate = self.concat_file[fname]

                    if concatenate:
                        log.debug("_handle_variables: let's concatenate the data.")
                        concatvar = self.new_variable(model=newvar, copy_attrs=True)
                        concatvar[:] = np.ma.concatenate((oldvar[:], newvar[:]))
                        self.unregister_variable(newvar)
                        self.register_variable(concatvar)
                else:
                    log.warning("First occurrence of variable '{0}' had no unlimited dimension. "
                                "We will only keep the last one.".format(varname))
                    self.register_variable(newvar)
            else:
                # 'newvar' has no record dimension...
                if hash(newvar) == hash(oldvar):
                    # ...but has already been seen, so it's a duplicate and we skip it.
                    log.debug("_handle_variables: '{0}' has already been read.".format(newvar))
                    continue
                else:
                    # ...its name has already been seen, but the new version is different.
                    # This shall be not accepted.
                    raise Exception("Different versions of '{0}'".format(varname) +
                                    " have been found across files.")

    def _build_metrics(self):
        """
        # self.metrics["dx_t"] = self.standard_names["cell_x_size"]
        # self.metrics["dy_u"] = self.standard_names["cell_y_size_at_u_location"]
        # self.metrics["dz_t"] = self.standard_names["cell_thickness"]
        # self.metrics["dz_w"] = self.standard_names["cell_thickness_at_w_location"]
        """
        # This piece of code will be used more than once later
        def _try_to_set_metric(metric_name, std_name):
            try:
                metric = self.standard_names[std_name]
                full_name = "{0}_{1}".format(metric_name, "".join(metric.axes))
                self.metrics[full_name] = metric
                return True
            except KeyError:
                return False

        # Horizontal metrics
        for direction in ('x', 'y'):
            for location in ('t', 'u', 'v', 'w', 'f', 'uw', 'vw', 'fw'):
                m_name = "d{0}_{1}".format(direction, location)
                v_name = "cell_{0}_size".format(direction)
                if location != 't':
                    v_name += "_at_{0}_location".format(location)
                if _try_to_set_metric(m_name, v_name):
                    continue

                # Try inverse sizes (for ROMS for instance)
                inv_stdname = "inverse_of_cell_{0}_size".format(direction)
                if location != 't':
                    inv_stdname += "_at_{0}_location".format(location)

                try:
                    inv_var  = self.standard_names[inv_stdname]
                    inv_name = "inverse_of_{0}".format(inv_var)
                    self.metrics[m_name] = \
                        op.operation("1./inv_var", inv_var=inv_var, name=inv_name, stdname=v_name)
                    continue
                except KeyError:
                    pass

        # Vertical metrics
        _try_to_set_metric("dz_t", "cell_thickness")
        _try_to_set_metric("dz_w", "cell_thickness_at_w_location")

        log.debug("_build_metrics : {0}".format(self.metrics.keys()))

    def get_metric(self, direction, location, axes="TZYX",
                         tpoint=None, zpoint=None, hpoint=None, direct_call=True):
        """
        Returns a variable corresponding to a the cell dimensions, in a given *direction* and for
        a specific *location* on the Arakawa-C grid.

        :param direction: either 'x', 'y' or 'z'.
        :param location: one of the points on the Arakawa-C grid where the metric is required. See also
                         :py:mod:`.operators.indices` and the :ref:`documentation <arakawa-label>`.
        :param axes: list of axes required for the metric. By default, the returned metric is
                     fully-dimensioned, but we can restrict it to a given subset.
        :param tpoint: if 'T' axis is not requested, specifies the time index for which the metric is
                       computed. Default: 0.
        :param zpoint: if 'Z' axis is not requested, specifies the vertical index for which the metric is
                       computed. Default: 1.
        :param hpoint: if either 'X' or 'Y' axis is not requested, specifies the horizontal point index for
                       which the metric is computed. Default: (0, 0).

        Examples for horizontal metrics:

        >>> dx = archive.get_metric("x", "t")
        >>> print dx.info()
            float32 inverse_of_pm(eta_rho, xi_rho)
                inverse_of_pm:standard_name = "cell_x_size"

        >>> dy_f = archive.get_metric("y", "f")
        >>> print dy_f.info()
            float32 dy_f_YX(eta_v, xi_u)
                dy_f_YX:standard_name = "cell_y_size_at_f_location"

        For a vertical metric at vorticity points on cell interfaces:

        >>> dz_fw = archive.get_metric("z", "fw")
        >>> print dz_fw.info()
            float32 dz_fw_TZYX(time, s_w, eta_v, xi_u)
                dz_fw_TZYX:standard_name = "derivative_of_depth_at_f_location_wrt_z"
        >>> print dz_fw.position, dz_fw.shape
        fw (3, 11, 61, 61)

        For better performance, it may be useful to get a reduced metric ; for example just on the
        dimensions we need for a plot. The following returns a vertical metric at u-velocity points,
        but only for the "ZX" plane. We use the last time index (-1) and focus on the plane defined
        by ``j=5``:

        >>> dz_u = archive.get_metric("z", "u", axes="ZX", tpoint=-1, hpoint=(5, None))
        >>> print dz_u.info()
            float32 dz_u_ZX(s_rho, xi_u)
                dz_u_ZX:standard_name = "derivative_of_depth_at_uw_location_wrt_z"
        >>> print dz_u.position, dz_u.shape
        u (10, 61)
        """
        location  = location.lower()
        direction = direction.lower()
        axes      = "".join(axes)    # to make sure that it is a string.

        if direction in "xy":
            # Horizontal metrics should not be Time- or Z-dependent
            axes = axes.replace("T", "").replace("Z", "")

        m_name = "d{0}_{1}_{2}".format(direction, location, axes)

        if m_name in self.metrics:

            log.debug("# get_metric: found an existing metric: {0}".format(m_name))
            metric = self.metrics[m_name]

            if set(axes) != set(metric.axes):
                # The requested axes are not the same as those present in 'metric'.
                m_name_new = "{0}_{1}".format(m_name, axes)

                # Create a dictionary {k: v} where 'k' is the axis and 'v' the coordinate variable
                cvars  = self.get_coordinate_system(location, axes)
                axes_d = DefaultOrderedDict()
                for dim_name in cvars:
                    cvar = self.get_variable(dim_name)
                    axes_d[cvar.axis] = cvar
                log.debug('    metric.axes   : {0}'.format(["{0}: {1}".format(k, v)
                                                            for k, v in metric.axes.iteritems()]))
                log.debug('    required axes : {0}'.format(["{0}: {1}".format(k, v)
                                                            for k, v in axes_d.iteritems()]))

                def _add_index_exp(axis, def_loc):
                    if axis in metric.axes:
                        return np.index_exp[:] if axis in axes else np.index_exp[def_loc]
                    elif axis in axes:
                        raise Exception("Metric '{0}' has not the required dimensions '{1}'."
                                        .format(metric, axis))

                tpoint = 0 if tpoint is None else tpoint
                zpoint = 1 if zpoint is None else zpoint
                jpoint, ipoint = (0, 0) if (hpoint is None) else hpoint
                s_m = ()
                s_m += _add_index_exp("T", tpoint)
                s_m += _add_index_exp("Z", zpoint)
                s_m += _add_index_exp("Y", jpoint)
                s_m += _add_index_exp("X", ipoint)
                log.debug("  - s_m = {0}".format(s_m))

                log.debug("# get_metric: create new metric variable: {0}".format(m_name_new))
                dims = [ str(axis) for axis in axes_d.itervalues() ]
                new_m = self.new_variable(m_name_new, dims, model=metric, copy_attrs=True)
                new_m.standard_name = "{0}_{1}".format(metric.standard_name, axes)
                new_m[:] = metric[s_m]
                try:
                    new_m.units = metric.units
                except AttributeError:
                    pass
                self.register_variable(new_m)
                metric = new_m

            return metric

        log.debug("# get_metric '{0}': not found in dict. ".format(m_name) +
                  "We'll try to derive it from other metrics.")

        # This piece of code will be used more than once later
        def _try_to_interpolate_metric(in_dir, in_location):
            _name = "d{0}_{1}".format(in_dir, in_location)
            if _name in self.metrics:
                log.debug("# get_metric '{0}': try from '{1}' location with '{2}'"
                          .format(m_name, in_location, _name))
                try:
                    self.metrics[m_name] = self.metrics[_name].interpolate(location, m_name)
                    return self.metrics[m_name]
                except InterpolationError as error:
                    log.error(error)
                    return None

        # For vertical metrics, we'll have to reconstruct from z levels
        if direction == 'z':
            stag_location = op.shift_location(location, 'z')
            log.debug("# get_metric '{0}': try from difference of z levels with 'z_{1}'"
                      .format(m_name, stag_location))
            zvar = self.get_zlevels(stag_location, axes, tpoint=tpoint, hpoint=hpoint)
            self.metrics[m_name] = zvar.diff_z(m_name, dummy=True)
            return self.metrics[m_name]

        # Special case 1: interpolate to 'f' location.
        if (location[0] == 'f') and direct_call:
            # Try to interpolate from 'u' location.
            metric = _try_to_interpolate_metric(direction, 'u')
            if metric:
                return metric
            # Try to interpolate from 'v' location.
            metric = _try_to_interpolate_metric(direction, 'v')
            if metric:
                return metric

        # Special case 2: interpolate to 'u' or 'v' location. Try to interpolate from 'f' location.
        if (location[0] == 'u' or location[0] == 'v') and direct_call:
            metric = _try_to_interpolate_metric(direction, 'f')
            if metric:
                return metric

        # Special case 3: interpolate to 't' location.
        if (location[0] == 't') and direct_call:
            # Try to interpolate from 'u' location.
            metric = _try_to_interpolate_metric(direction, 'u')
            if metric:
                return metric
            # Then try to interpolate from 'v' location.
            metric = _try_to_interpolate_metric(direction, 'v')
            if metric:
                return metric

        # Try to interpolate from 't' location.
        metric = _try_to_interpolate_metric(direction, 't')
        if metric:
            return metric

        raise UnknownVariable("get_metric: unable to create metric ({0})".format(m_name))

    def get_variable(self, name=None, stdname=None, location=None, nasty=True):
        """
        Returns a variable stored in the archive.

        The variable can be selected either from its *name* or its *standard_name* attribute,
        but not both.

        :param name: name of the variable.
        :param stdname: value of the ``standard_name`` attribute.
        :param location: requested position of the variable on the Arakawa-C grid.
        :param nasty: if False, returns gently None in case of failure.

        Selection by name:

        >>> u = archive.get_variable(name="ubar")
        >>> print u.info()
            float32 ubar(time, eta_rho, xi_u)
                ubar:long_name = "vertically integrated u-momentum component"
                ubar:units = "meter second-1"
                ubar:field = "ubar-velocity, scalar, series"
                ubar:standard_name = "barotropic_sea_water_x_velocity_at_u_location"

        Selection by standard name:

        >>> v = archive.get_variable(stdname="sea_water_y_velocity", location="v")
        >>> print v.info()
            float32 v(time, s_rho, eta_v, xi_rho)
                v:long_name = "v-momentum component"
                v:units = "meter second-1"
                v:field = "v-velocity, scalar, series"
                v:standard_name = "sea_water_y_velocity_at_v_location"

        Selection by standard name with interpolation on cell centers:

        >>> vt = archive.get_variable(stdname="sea_water_y_velocity", location="t")
        >>> print vt.info()
            float32 v_at_t_location(time, s_rho, eta_rho, xi_rho)
                v_at_t_location:long_name = "v-momentum component"
                v_at_t_location:units = "meter second-1"
                v_at_t_location:field = "v-velocity, scalar, series"
                v_at_t_location:standard_name = "sea_water_y_velocity"

        Selection by an unknown standard_name:

        >>> print archive.get_variable(stdname="sea_water_z_velocity")
        pycomodo.core.exceptions.UnknownVariable: 'sea_water_z_velocity'
        >>> print archive.get_variable(stdname="sea_water_z_velocity", nasty=False)
        None
        """
        if (name is None and stdname is None) or (name is not None and stdname is not None):
            raise Exception("Archive.get_variable(): You have to choose either "
                            "a variable 'name' or a 'standard_name'.")

        var = None

        if name is not None:        # Search variable by its name
            try:
                var = self.variables[name]
            except KeyError:
                if nasty:
                    raise UnknownVariable(name)
        elif location is None:      # Search variable by its standard_name
            try:
                var = self.standard_names[stdname]
            except KeyError:
                if nasty:
                    raise UnknownVariable(stdname)
        else:                       # Search variable by its standard_name and location
            try:
                var = puv.get_variable_at_location(self, stdname, location, interpolate=True)
            except UnknownVariable as error:
                if nasty:
                    raise error

        return var

    def get_time_index(self, req_time, units):
        """
        Returns the array index of the time coordinate variable (the one with ``standard_name``
        equal to ``'time'``) that correspond to *req_time* expressed in *units*.

        :param float req_time: required time
        :param str   units:    string describing the unit used for *req_time*.

        >>> timevar = archive.get_variable(stdname="time")
        >>> print timevar[:], timevar.units
        [       0.  2160000.  4320000.] second
        >>> print archive.get_time_index(2160000, "seconds")
        1
        >>> print archive.get_time_index(50, "days")
        2
        """
        var_time = self.get_variable(stdname='time')
        time_0   = self.get_time(0, units)
        for k in range(var_time.shape[0]):
            time_k = self.get_time(k, units) - time_0
            if np.abs(req_time-time_k) < 0.0001:
                return k

        raise Exception("Unable to find variable index for time value {0} {1}.".format(req_time, units))

    def get_time(self, index, req_units='s'):
        """
        Returns the value of the time, expressed in the unit specified by *req_unit*, stored
        in the time coordinate variable at index array *index*.

        :param index: array index for which time is requested. Can be an integer or any slice object.
        :param str req_units: string describing the output units.

        >>> print archive.get_time(-1)                      # Time in seconds for last index
        4320000.0
        >>> print archive.get_time(Ellipsis, 'days')        # Time in days for all indices
        [  0.  25.  50.]
        >>> print archive.get_time(np.s_[1:], 'hours')      # Time in hours for all but first index
        [  600.  1200.]
        """
        time = self.get_variable(stdname='time')
        tunits = time.units
        tvalue = time[index]
        if req_units.startswith('s'):      # seconds where required
            if 'year' in tunits:
                tvalue *= 3600.*24.*365.
            if 'day' in tunits:
                tvalue *= 3600.*24.
            if 'hour' in tunits:
                tvalue *= 3600.
        elif req_units.startswith('h'):    # hours where required
            if 'year' in tunits:
                tvalue *= 24.*365.
            if 'day' in tunits:
                tvalue *= 24.
            if 'second' in tunits:
                tvalue /= 3600.
        elif req_units.startswith('d'):    # days where required
            if 'year' in tunits:
                tvalue *= 365.
            if 'hour' in tunits:
                tvalue /= 24.
            if 'second' in tunits:
                tvalue /= 3600.*24.
        elif req_units.startswith('y'):    # years where required
            if 'day' in tunits:
                tvalue /= 365.
            if 'hour' in tunits:
                tvalue /= 24.*365.
            if 'second' in tunits:
                tvalue /= 3600.*24.*365.

        return tvalue

    def find_coordinate_variable(self, axis, location='t'):
        """
        Returns the instance of the coordinate variable for the required *axis* with
        staggering described by *location*.

        :param axis: required axis type (either ``'T'``, ``'Z'``, ``'Y'``, or ``'X'``).
        :param str location: staggering type (position on the Arakawa grid, either ``'t'``, ``'u'``,
                             ``'v'``, ``'w'``, ``'f'``, ``'uw'``, ``'vw'`` or ``'fw'``.
                             Default: ``'t'``, cell-centered variable).

        >>> zaxis = archive.find_coordinate_variable('Z')
        >>> print zaxis, type(zaxis)
        s_rho <class 'pycomodo.core.variable.Variable'>
        >>> print archive.find_coordinate_variable('X', 'u').info()
            float32 xi_u(xi_u)
                xi_u:long_name = "x-dimension of the grid at u location"
                xi_u:standard_name = "x_grid_index_at_u_location"
                xi_u:axis = "X"
                xi_u:c_grid_axis_shift = 0.5
                xi_u:c_grid_dynamic_range = "2:60"
        """
        out_cvar = []
        location = op.expand_location(location)

        # Parse all coordinate variables and keep the ones that correspond
        for cvar in self.coordinate_variables.itervalues():
            try:
                cvar_axis     = cvar.axis
                cvar_position = op.expand_location(cvar.position)
            except AttributeError:
                # Skip malformed coordinate variables.
                continue

            # Skip bad axis
            if cvar_axis != axis:
                continue

            if ( (axis == 'T') or (cvar_position == location) ):
                out_cvar.append(cvar)
                continue

            if ( (axis == 'Z') and ( cvar_position[1] == location[1] ) ):
                out_cvar.append(cvar)
                continue

        if out_cvar:

            if len(out_cvar) == 1:
                # Only one coordinate was found ; everything should be all right.
                return out_cvar[0]

            # If we're here, it means that we have found more than one candidate.
            # We'll have to choose.
            log.debug("# find_coordinate_variable : multiple candidates for axis '{0}'".format(axis) +
                      " at location '{0}': {1}.".format(location, [str(cv) for cv in out_cvar]))

            # For each candidate, look at the 'standard_name' attribute
            for cvar in out_cvar:
                try:
                    cvar_stdpos = stdnames.get_at_location(cvar.standard_name)
                except AttributeError:
                    continue
                if cvar_stdpos == location:
                    log.debug("# find_coordinate_variable : OK, we choose '{0}' ({1})."
                              .format(cvar, cvar.standard_name))
                    return cvar

        raise UnknownCoordinate("Unable to find a valid coordinate variable for axis '{0}'.".format(axis))

    def get_coordinate_system(self, location='t', axes="TZYX", mock=None):
        """
        Returns a list of dimension names that can construct a coordinate system for a variable
        positioned on stagger point *location*.

        :param location: a letter (either ``'t'``, ``'u'``, ``'v'``, ``'w'``, ``'f'``, ``'uw'``,
                         ``'vw'`` or ``'fw'``) describing variable staggering.
                         Default: ``'t'``, cell-centered variable.
        :param axes: ordered list of axes names required. Can be any iterable, by really should be a string.
        :param mock: existing variable to take as a model for dimensions ordering.

        For example, if ``archive`` is obtained as in the :ref:`tutorial <archive_roms-label>`
        then we get:

        >>> archive.get_coordinate_system('t')
        ['time', 's_rho', 'eta_rho', 'xi_rho']
        >>> archive.get_coordinate_system('f', axes="YX")
        ['eta_v', 'xi_u']
        >>> archive.get_coordinate_system('uw', axes="XYZ")
        ['xi_u', 'eta_rho', 's_w']

        In the last line, the dimensions are ordered as requested in *axes*.
        To be certain to get the same order as in the file, we can use the *mock* argument:

        >>> u = archive.get_variable('u')
        >>> u.dimensions
        ['time', 's_rho', 'eta_rho', 'xi_u']
        >>> archive.get_coordinate_system('uw', axes="XYZ", mock=u)
        ['s_w', 'eta_rho', 'xi_u']
        # Here, the dimensions are ordered like in variable 'u'.
        """
        dims = { "X": None, "Y": None, "Z": None, "T": None,
                 "X_at_t": None, "Y_at_t": None, "Z_at_t": None }

        # Rewrite 'location' with 2 characters
        _location = op.expand_location(location)

        # Restrict requested axes to those present in mock.
        if mock is not None:
            _axis_name = lambda dim_name: self.variables[dim_name].axis
            axes_restr = [ _axis_name(dim_name) for dim_name in mock.dimensions
                           if _axis_name(dim_name) in axes]
            axes = axes_restr

        log.debug("# get_coordinate_system at location '{0}' for '{1}'"
                  .format(location, [str(axis) for axis in axes]))

        # Look for coordinate variables that have the requested 'axis' attribute.
        for coord_var in self.coordinate_variables.itervalues():

            # Skip malformed coordinate variables.
            try:
                cv_name    = str(coord_var)
                cv_axis    = coord_var.axis
                cv_stdname = coord_var.standard_name
            except AttributeError:
                continue

            var_location = op.expand_location(stdnames.get_at_location(cv_stdname))

            if var_location == 'tt':
                # This dimension should be for a cell-centered variable.
                if cv_axis == 'T':    # Time axis
                    dims["T"] = cv_name
                else:                 # Space axis on a "t" location
                    dims[cv_axis+"_at_t"] = cv_name

            # Searched position and variable's position match exactly
            if var_location == _location:
                dims[cv_axis] = cv_name

            # Searched position and variable's position match only for vertical
            if (var_location[1] == _location[1]) and (cv_axis == 'Z'):
                dims[cv_axis] = dims[cv_axis] or cv_name

            # Searched position and variable's position match only for horizontal
            if (var_location[0] == _location[0]) and (cv_axis in ('X', 'Y')):
                dims[cv_axis] = dims[cv_axis] or cv_name

        if _location[0] == 't':
            dims["X"] = dims["X"] or dims["X_at_t"]
            dims["Y"] = dims["Y"] or dims["Y_at_t"]

        if _location[1] == 't':
            dims["Z"] = dims["Z"] or dims["Z_at_t"]

        # OK, we should have all dimension names now...
        dims_array = [ dims[a] for a in axes ]

        if None in dims_array:
            # At least one requested axis has not been found.
            # Let's try some recipes.
            if _location[0] == 'f':
                # Here, we try with coordinate variables with standard_name that correspond
                # to "*x*_at_u_location" and "*y*_at_v_location"
                for coord_var in self.coordinate_variables.itervalues():
                    try:
                        cv_name    = str(coord_var)
                        cv_axis    = coord_var.axis
                        cv_stdname = coord_var.standard_name
                    except AttributeError:
                        continue
                    var_location = op.expand_location(stdnames.get_at_location(cv_stdname))
                    if (var_location[0] == 'u') and (cv_axis == 'X'):
                        dims["X"] = cv_name
                    if (var_location[0] == 'v') and (cv_axis == 'Y'):
                        dims["Y"] = cv_name
            if (dims["Y"] is None) and (_location[0] == 'u'):
                # Here, we try the Y axis of the 't' location
                dims["Y"] = dims["Y_at_t"]
            if (dims["X"] is None) and (_location[0] == 'v'):
                # Here, we try the X axis of the 't' location
                dims["X"] = dims["X_at_t"]

        # OK, let's retry
        dims_array = [ dims[a] for a in axes ]

        # Fail...
        if None in dims_array:
            raise UnknownCoordinate("Unable to find all requested coordinates: " +
                                    "{0} for '{1}' location".format(axes, location))

        log.debug("  -> result coordinate system: {0}".format(dims_array))

        # Dimensions are found, we return their names.
        return dims_array

    def get_dimension_dynamic_range(self, dimension):
        """
        Returns a dimension's dynamic range, with C/Python notation (start with 0).

        >>> xaxis = archive.find_coordinate_variable('X')
        >>> print xaxis, xaxis.c_grid_dynamic_range
        xi_rho 2:61
        >>> archive.get_dimension_dynamic_range('xi_rho')
        (1, 60)

        >>> zaxis = archive.find_coordinate_variable('Z', 'w')
        >>> print str(zaxis), archive.get_dimension_dynamic_range(zaxis)
        s_w (0, 10)
        """
        if dimension is None:
            return (None, None)
        else:
            covar = self.coordinate_variables[str(dimension)]
            min_f, max_f = covar.c_grid_dynamic_range.split(":")
            return (int(min_f)-1, int(max_f)-1)

    def get_dimension_global_range(self, dimension):
        """
        Returns a dimension's global range, with C/Python notation (start with 0).

        >>> xaxis = archive.find_coordinate_variable('X')
        >>> print xaxis, xaxis.shape
        xi_rho (62,)
        >>> archive.get_dimension_global_range(xaxis)
        (0, 61)
        """
        if dimension is None:
            return (None, None)
        else:
            return (0, len(self.dimensions[str(dimension)])-1)

    def get_zlevels(self, location="t", axes="TZYX", name=None, stdname=None, tpoint=None, hpoint=None):
        """
        Returns depth variable.

        :param location: point on the Arakawa grid where the depth is required (either ``'t'``, ``'u'``,
                         ``'v'``, ``'w'``, ``'f'``, ``'uw'``, ``'vw'`` or ``'fw'``.
                         Default: ``'t'``, cell-centered variable).
        :param axes: list of axes. Axis ``'Z'`` must be present.
        :param name: name for the new variable.
        :param stdname: value for the ``standard_name`` attribute.
        :param tpoint: if 'T' axis is not requested, specifies the time index for which the levels are
                       computed. Default: 0.
        :param hpoint: if either 'X' or 'Y' axis is not requested, specifies the horizontal point index for
                       which the levels are computed. Default: (0, 0).

        >>> print archive.get_zlevels().info()
            float32 z_at_t(time, s_rho, eta_rho, xi_rho)
                z_at_t:units = "m"
                z_at_t:standard_name = "depth"

        >>> z_fw = archive.get_zlevels("fw", axes="ZXY", tpoint=0)
        >>> print z_fw.dimensions, '\\n', z_fw[:, 30, 30]
        ['s_w', 'xi_u', 'eta_v']
        [-5000.0000 -4499.9141 -3999.8284 -3499.7427 -2999.6567 -2499.5710
         -1999.4852 -1499.3994  -999.3136  -499.2278     0.8580]
        """
        log.debug("# Archive.get_zlevels location='{0}' (axes='{1}')".format(location, axes))

        # Use get_zlevels() routine from pycomodo.util.vertical
        return puz.get_zlevels(self, location, axes, name, stdname, tpoint, hpoint)

    def new_dimension(self, name=None, size=None, model=None, dtype=None, attrs=None, unlimited=False):
        """
        Creates a new dimension and returns the associated coordinate variable.

        :param name: name for the new dimension
        :param size: length of the dimension (Default: same as *model*)
        :param model: coordinate variable of an existing dimension used as a model for the new one
        :param dtype: data type (e.g. float, int, np.float32, etc. Default: *model.dtype*)
        :param attrs: dictionary of NetCDF attributes for the coordinate variable (Default: same as *model*
                      if the latter is provided).
        :param unlimited: True if this is a record dimension. If *model* is provided, this argument
                          is ignored ; the new dimension will be as the *model*.
        """
        if (model is None) and (size is None or name is None or dtype is None):
            raise Exception("Archive.new_dimension(): if you do not specify 'model', you have "
                            "to specify 'name, 'size' and 'dtype' when calling this function.")

        if name is None:
            name = str(model)

        if size is None:
            size = model.size

        if dtype is None:
            dtype = model.dtype

        if model is not None:
            unlimited = model.has_recdim()
            if attrs is None:
                attrs = model.get_attributes()

        # Create and register the new dimension.
        new_dim = self.dimensions[name] = Dimension(name, size, unlimited)

        # Create and register the associated coordinate variable.
        var_dim = self.new_variable(name, (name,), dtype=dtype, attrs=attrs)
        self.register_variable(var_dim)

        return var_dim

    def new_variable(self, name=None, dimensions=None, model=None, dtype=None,
                           fill_value=None, attrs=None, copy_attrs=False):
        """
        Creates and returns a new variable, instance of :class:`.Variable`.

        :param name: name for the new variable
        :param dimensions: list of dimension names (Default: those of *model*)
        :param model: existing variable used as a model for the new one
        :param dtype: data type (e.g. float, int, np.float32, etc. Default: *model.dtype*)
        :param fill_value: value for the ``_FillValue`` attribute
        :param attrs: dictionary of NetCDF attributes
        :param copy_attrs: if **True** and *model* is provided, its NetCDF attributes will
                           be copied in the new variables

        The newly created Variable contains no data.
        As an illustration, let's create a new variable to store the magnitude of the barotropic velocity :

        .. code-block:: python
            :emphasize-lines: 9,10

            >>> # Interpolate 'ubar' and 'vbar' at cell centers
            >>> ubt = archive.get_variable("ubar").interpolate('t')
            >>> vbt = archive.get_variable("vbar").interpolate('t')

            >>> # Get a coordinate system for time + horizontal axes
            >>> dims_2d = archive.get_coordinate_system("t", axes="TYX")

            >>> # Create the new variable
            >>> speed = archive.new_variable("speed", dims_2d, fill_value=999., dtype=ubt.dtype,
                                             attrs={'standard_name': 'sea_water_speed'})

            >>> # We now have to fill the variable with data
            >>> speed[:] = np.sqrt(ubt[:]**2+vbt[:]**2)

            >>> # If we register the new variable, its standard_name is stored in the archive
            >>> # for later use.
            >>> archive.register_variable(speed)
            >>> velo = archive.get_variable(stdname="sea_water_speed")
            >>> print velo.info()
                float32 speed(time, eta_rho, xi_rho)
                    speed:_FillValue = 999.0
                    speed:standard_name = "sea_water_speed"

            >>> # We can write this new variable in a NetCDF file
            >>> pycomodo.write_netcdf("speed.nc", velo)
        """
        if (model is None) and (dimensions is None ):
            raise Exception("Archive.new_variable(): You have to specify either 'model' or "
                            "'dimensions' when calling this function.")
        if (model is None) and (dtype is None ):
            raise Exception("Archive.new_variable(): You have to specify either 'model' or "
                            "'dtype' when calling this function.")

        if dimensions is None:
            dimensions = model.dimensions

        if dtype is None:
            dtype = model.dtype

        shape      = tuple([len(self.dimensions[dim]) for dim in dimensions])
        has_recdim = any(self.dimensions[dim].isunlimited() for dim in dimensions)

        log.debug("# new_variable: '{0}' with dimensions [{1}]".format(bolded(name), ", ".join(dimensions)) +
                  "\n\t   - dtype = {0}\n\t   - shape = {1}".format(dtype, shape))

        if (fill_value is None) and (model is not None):
            fill_value = model.get_fill_value()

        if attrs is None:
            attrdict = OrderedDict()
        else:
            attrdict = OrderedDict(attrs)

        if copy_attrs and (model is not None):
            for attrname in model.ncattrs():
                attrdict[attrname] = getattr(model, attrname)

        # Create new variable
        _newvar = NetcdfVariable(name, dimensions, dtype, shape, has_recdim, fill_value, attrdict)

        newvar = Variable(_newvar, self)
        newvar.build_axes_dict()

        return newvar

    def check(self, verbose=True):
        """
        Use :mod:`pycomodo.core.checker` to print some hints about the conformity of the archive
        to the COMODO norm.
        """
        self.checker.check(verbose)
