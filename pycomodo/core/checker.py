#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Contains some tools to check if an :class:`.Archive` complies to the :ref:`COMODO norm <norm-comodo>`.

The classes defined here are used by :meth:`.Variable.check` and :meth:`.Archive.check`, which are
used in turn by the tool ``bin/check_comodo.py``.
"""

import os.path
import re
import pycomodo
import pycomodo.stdnames.standard_name_parser as snp
import pycomodo.util.stdnames as stdnames

from pycomodo.core.exceptions import *
from pycomodo.io.termcolor import colored, bolded
from pycomodo.util.vertical import parse_formula_terms, get_formula

__all__ = ['CheckWarehouse', 'ArchiveChecker']

NEW_COMMENT = lambda message: _Item(message, "[OK]    ", "green")
NEW_ERROR   = lambda message: _Item(message, "[FAILED]", "red")
NEW_WARNING = lambda message: _Item(message, "[WARN]  ", "yellow")


class _Item(object):
    """
    Generic message with a colored header (keyword).
    """
    def __init__(self, message, keyword, color):
        self.message = message
        self.keyword = keyword
        self.color   = color

    def __repr__(self):
        return "{0} - {1}".format(self.colored(self.keyword), self.message)

    def colored(self, msg):
        """
        Returns of version of *msg* colored with the item's color.
        """
        return colored(msg, self.color)


class CheckResult:
    """
    Stores the messages (Comment, Warning or Error) associated to a Dimension or Variable.
    """
    def __init__(self, name):

        self.name = name
        self.errors = []
        self.warnings = []
        self.comments = []
        self.items = []

    def add_error(self, error_msg):
        """
        Adds an 'Error' message.
        """
        error = NEW_ERROR(error_msg)
        self.errors.append(error)
        self.items.append(error)

    def add_warning(self, warning_msg):
        """
        Adds a 'Warning' message.
        """
        warning = NEW_WARNING(warning_msg)
        self.warnings.append(warning)
        self.items.append(warning)

    def add_comment(self, comment_msg):
        """
        Adds a 'Comment' message.
        """
        comment = NEW_COMMENT(comment_msg)
        self.comments.append(comment)
        self.items.append(comment)

    def is_ok(self):
        """
        Return True if the check has no error.
        """
        return len(self.errors) == 0 and len(self.warnings) == 0


class CheckWarehouse:
    """
    Defines a container to store the verification results for Variables or Dimensions.
    """
    def __init__(self, in_log=None):
        """
        Create a new ``CheckWarehouse`` instance.
        """
        #: Logging system. Default: :obj:`pycomodo.io.customlogger.log`
        self.log = pycomodo.log if (in_log is None) else in_log
        self.results = []

    def add(self, result):
        """
        Append *result* to the internal list.
        """
        self.results.append(result)

    def dump(self, verbose=False, lines=False):
        """
        Write a report of the stored results in :obj:`.log`.

        :param verbose: Prints complete report if True, or only the first error/warning/message if False.
        :param lines: Prints a line of '---' after the report.
        """
        # Cosmetics: compute the largest possible name for a dimension/variable.
        max_length = max(len(res.name) for res in self.results)

        for res in self.results:
            self._dump_result(res, lines, max_length, verbose)

        if verbose and lines:
            self.log("-"*120)

    def _dump_result(self, res, lines, max_length, verbose):
        """
        Actually writes the report for *res*.
        """
        res_name = "{0:<{wlen}}".format(res.name, wlen=max_length)
        blanks = "{0:{wlen}}".format("", wlen=len(res_name))

        if len(res.items) == 0:
            self.log("{0} nothing to say.".format(res_name))
            return

        if verbose:
            if lines:
                self.log("-"*120)
            if res.is_ok():
                item = res.items[0]
            else:
                try:
                    item = res.errors[0]
                except IndexError:
                    item = res.warnings[0]

            res_name = item.colored(res_name)

            # Print first item with its name
            self._put_item(res_name, res.items[0])
            # For the following items, replace name with blanks
            for item in res.items[1:]:
                self._put_item(blanks, item)
        else:
            if res.is_ok():
                self._put_item_nocomment(res_name, res.items[0])
            else:
                try:
                    self._put_item(res_name, res.errors.pop(0))
                except IndexError:
                    self._put_item(res_name, res.warnings.pop(0))

    def _put_item(self, name, item):
        """
        Writes *item* with keyword and complete comment.
        """
        self.log("{0} | {1}".format(name, item))

    def _put_item_nocomment(self, name, item):
        """
        Writes *item* with keyword but no other comment.
        """
        self.log("{0} | {1}".format(name, item.colored(item.keyword)))


class ArchiveChecker:
    """
    Class used to parse and check the content of an :class:`.Archive` by means of
    :class:`.CheckWarehouse` objects.

    The definition of valid ``standard_names`` attribute values are fetched in XML configuration
    files managed in :mod:`pycomodo.stdnames.standard_name_parser`.
    """
    def __init__(self, archive, cf_xml=None, comodo_xml=None, budget_xml=None):
        """
        Create an new ArchiveChecker object.
        """
        self.archive = archive
        self.log = pycomodo.log

        _thisdir    = os.path.dirname(__file__)
        _dir_tables = os.path.join(_thisdir, os.pardir, "stdnames", "standard_name_tables")

        if cf_xml is None:
            cf_xml = os.path.join(_dir_tables, "cf-standard-name-table.xml")
        if comodo_xml is None:
            comodo_xml = os.path.join(_dir_tables, "comodo-standard-name-table.xml")
        if budget_xml is None:
            budget_xml = os.path.join(_dir_tables, "comodo-budget-terms.xml")

        self.cf_parser     = snp.StandardNameParser(cf_xml)
        self.comodo_parser = snp.StandardNameParser(comodo_xml)
        self.budget_parser = snp.BudgetTermsParser(budget_xml)

    def check(self, verbose=False):
        """
        Check if the *archive* instance does correctly follow the COMODO norm.
        `archive` should be an instance of :class:`.Archive`.
        """
        dim_checks = CheckWarehouse(self.log)
        var_checks = CheckWarehouse(self.log)

        # Check dimensions
        for dim_name in self.archive.dimensions:
            result = self.check_dim(dim_name)
            dim_checks.add(result)

        # Sort variables by lower-case name, then check every one of them
        for var in sorted(self.archive.variables.itervalues(), key=lambda x: str.lower(str(x))):
            result = self.check_var(var)
            var_checks.add(result)

        self.log("")
        self.log(bolded("Dimensions :"))
        dim_checks.dump(verbose)

        self.log("")
        self.log(bolded("Variables :"))
        var_checks.dump(verbose, lines=True)

    def check_dim(self, dim_name):
        """
        Check if a given dimensions *dim_name* is correctly associated with a coordinate variable.
        """
        result = CheckResult(dim_name)
        try:
            self.archive.get_variable(dim_name)
        except UnknownVariable:
            result.add_error("dimension '{0}' has no coordinate variable.".format(bolded(dim_name)))
        else:
            dim = self.archive.dimensions[dim_name]
            comment = "size: {0}".format(len(dim))
            if dim.isunlimited():
                comment += " [RECORD DIMENSION]"
            result.add_comment(comment)
        return result

    def check_var(self, var):
        """
        Check various elements of the :ref:`Comodo norm <norm-comodo>` for the variable *var*.

        :param var: instance of :class:`.Variable`.
        """
        result = CheckResult(str(var))
        self._check_standard_name(var, result)
        self._check_position(var, result)
        self._check_coord_variables(var, result)
        self._check_coordinates(var, result)
        return result

    def get_canonical_std_name(self, std_name):
        """
        For a given variable standard name *std_name*, return its canonical version. If *std_name* is an
        alias of another name, the canonical version is the latter.
        """
        entry = None
        if std_name in self.cf_parser:
            entry = self.cf_parser[std_name]
        if std_name in self.comodo_parser:
            entry = self.comodo_parser[std_name]
        if entry is None:
            raise UnknownVariable(std_name)

        return entry.name

    def _check_standard_name(self, var, result):
        """
        Check the attribute *standard_name* for variable *var*. If this attribute is present,
        try to determine how it is formed and keep track of various modifiers.
        """
        # Check if 'standard_name' attribute is present
        try:
            std_name = var.standard_name
        except AttributeError:
            result.add_error("variable '{0}' has no standard_name.".format(bolded(var)))
            return

        # If standard_name begin with 'inverse_of', isolate the generic name.
        inverse_of = stdnames.get_inverse_of(std_name)
        if inverse_of:
            std_name = inverse_of

        # If standard_name ends with 'at_[uvwf]_location', isolate the root name.
        generic_std_name = stdnames.get_root(std_name)
        std_location = stdnames.get_at_location(std_name)

        # Look for the "generic" standard name in the CF database...
        if generic_std_name in self.cf_parser:
            std_entry  = self.cf_parser[generic_std_name]
            std_source = "CF v.%s" % self.cf_parser.version
        # ...or in the Comodo database
        elif generic_std_name in self.comodo_parser:
            std_entry  = self.comodo_parser[generic_std_name]
            std_source = "COMODO v.%s" % self.comodo_parser.version
        elif generic_std_name in self.budget_parser:
            std_entry  = self.budget_parser[generic_std_name]
            std_source = "BUDGET v.%s" % self.budget_parser.version
        else:
            result.add_error("standard_name '{0}' not found in databases.".format(bolded(std_name)))
            std_entry = None

        if std_entry is not None:
            comment_pattern = "standard_name: {inverse}{std_entry}{at_location} {units} [{std_source}]"
            comment_values = {
                "std_entry":   bolded(std_entry.name),
                "std_source":  std_source,
                "units":       "({0})".format(std_entry.units),
                "inverse":     inverse_of and "[inverse of] " or "",
                "at_location": (std_location != 't') and " [at {0} location]".format(std_location) or ""
            }
            result.add_comment(comment_pattern.format(**comment_values))

        # Check if standard_name was given to another variable:
        for other_var in self.archive.variables.itervalues():
            if var is other_var:
                continue
            try:
                other_std_name = other_var.standard_name
            except AttributeError:
                continue
            if other_std_name == var.standard_name:
                result.add_error("standard_name '{0}' was also given to variable '{1}'.".format(
                    bolded(other_std_name), bolded(other_var)))

    def _check_coord_variables(self, var, result):
        """
        For each dimensions of variable ``var``, check if a coordinate variable is correctly
        associated, and if it carries ``axis`` and ``c_grid_dynamic_range`` attributes.
        """
        if var.is_coordinate_variable():
            # 'var' is a coordinate variable, so we report all errors.
            self._check_one_coord_variable(var, result, report_errors=True)
        else:
            # 'var' is a regular variable, so for each dimension...
            for dim_name in var.dimensions:
                # ... we look for the corresponding variable (so-called 'coordinate variable')
                try:
                    coord_var = self.archive.get_variable(dim_name)
                except UnknownVariable:
                    result.add_error("dimension '{0}' has no coordinate variable.".format(bolded(dim_name)))
                    continue
                # Now we just check if this coordinate variable is valid or not.
                # We don't care about details.
                try:
                    self._check_one_coord_variable(coord_var, result, report_errors=False)
                except WarnCoordVarAlmostConformal:
                    result.add_warning("has an almost-conformal coordinate variable '{0}'"
                                       .format(bolded(coord_var)))
                except (ErrorCoordVarNonConformal, ErrorMalformedFormula):
                    result.add_error("has a non-conformal coordinate variable '{0}'"
                                     .format(bolded(coord_var)))
                else:
                    result.add_comment("has a checked coordinate variable '{0}' for axis '{1}'"
                                       .format(bolded(coord_var), bolded(coord_var.axis)))

    def _check_one_coord_variable(self, var, result, report_errors=True):
        """
        If *var* is a coordinate variable, check if it is correctly formed: it must carry
        valid ``axis``, ``c_grid_dynamic_range`` and ``c_grid_axis_shift`` attributes.
        """
        if not var.is_coordinate_variable():
            return

        coord_axis = None
        dyn_range  = None
        axis_shift = None
        formula_terms = None

        # Look for 'axis' attribute
        try:
            coord_axis = var.axis
        except AttributeError:
            if report_errors:
                result.add_error("coordinate variable '{0}' has no 'axis' attribute.".format(bolded(var)))
            else:
                raise ErrorCoordVarNonConformal()

        # Look for 'positive' attribute if vertical axis
        if (coord_axis == "Z"):
            try:
                positive = var.positive
                if positive in ("up", "down"):
                    result.add_comment("is a correctly directed (positive='{0}')".format(positive))
                else:
                    result.add_error("has an invalid 'positive' attribute ('{0}').".format(positive))
            except AttributeError:
                if report_errors:
                    result.add_error("coordinate variable '{0}' has no 'positive' attribute.".format(bolded(var)))
                else:
                    raise ErrorCoordVarNonConformal()

        # Look for 'c_grid_dynamic_range' attribute (but not for Z and T axis)
        if ((coord_axis is not None) and (coord_axis in "XY")) or (coord_axis is None):
            dyn_range  = self._check_c_grid_dynamic_range(var, result, report_errors)

        # Look for 'c_grid_shift_axis' attributes (but not for T axis)
        if ((coord_axis is not None) and (coord_axis in "XYZ")) or (coord_axis is None):
            axis_shift = self._check_c_grid_shift_axis(var, result, report_errors)

        # It's now time to report on our investigations on 'c_grid_dynamic_range'.
        if report_errors:
            if (coord_axis is not None):
                result.add_comment("is a coordinate variable with axis '{0}'".format(bolded(coord_axis)))

            if (dyn_range is not None) and self._is_dynamic_range_valid(var, dyn_range):
                result.add_comment("is a coordinate variable with dynamic range: {0}".format(dyn_range))

            if (axis_shift is not None):
                result.add_comment("is a coordinate variable with axis shift: {0}".format(axis_shift))

        # Look for 'formula_terms' attributes (for Z axis)
        if (coord_axis == "Z"):
            formula_terms = self._check_formula_terms(var, result, report_errors)

        # It's now time to report on our investigations on 'formula_terms'.
        if report_errors and (formula_terms is not None):
            result.add_comment("has a validated dimensional coordinate vertical"
                               " formula: {0}".format(formula_terms))

    @staticmethod
    def _check_c_grid_dynamic_range(var, result, report_errors):
        """
        Check if ``c_grid_dynamic_range`` attribute is present and well-formed.
        Returns actual dynamic range.
        """
        try:
            # we look in original _attributes because a default value has been given
            # to 'c_grid_dynamic_range' in Archive._enhance_variable().
            dyn_range = getattr(var, "c_grid_dynamic_range")
        except AttributeError:
            if report_errors:
                result.add_warning("coordinate variable '{0}' ".format(bolded(var)) +
                                   "has no 'c_grid_dynamic_range' attribute (this is recommanded)")
                return None
            else:
                raise WarnCoordVarAlmostConformal()

        # Is it well-formed ?
        if not ArchiveChecker._is_dynamic_range_valid(var, dyn_range):
            if report_errors:
                result.add_error("malformed 'c_grid_dynamic_range' attribute: \"{0}\"".format(dyn_range))
                return None
            else:
                raise ErrorCoordVarNonConformal()

        # Let's return the dynamic_range
        return dyn_range

    @staticmethod
    def _is_dynamic_range_valid(var, dyn_range):
        """
        Returns True if ``c_grid_dynamic_range`` attribute is well-formed.
        """
        try:
            re_dyn_range = re.match(r"([0-9]+):([0-9]+)", dyn_range)
            dr_min, dr_max = (int(drm) for drm in re_dyn_range.groups())
        except AttributeError:
            return False

        if (dr_min < 1      or dr_min > len(var.archive.dimensions[str(var)])) or \
           (dr_max < dr_min or dr_max > len(var.archive.dimensions[str(var)])):
            return False

        return True

    @staticmethod
    def _check_c_grid_shift_axis(var, result, report_errors):
        """
        Check if ``c_grid_axis_shift`` attribute is present and well-typed.
        Returns its actual value.
        """
        try:
            # we look in original _attributes because a default value has been given
            # to 'c_grid_shift_axis' in Archive._enhance_variable().
            axis_shift = var.getncattr("c_grid_axis_shift")
        except AttributeError:
            # This attribute is not mandatory (default: 0.)
            return None

        if "float" not in str(type(axis_shift)):
            if report_errors:
                result.add_error("attribute 'c_grid_axis_shift' should be of type float" +
                                 " ({0})".format(axis_shift))
                return None
            else:
                raise ErrorCoordVarNonConformal()

        # Let's return the axis shift
        return axis_shift

    @staticmethod
    def _check_formula_terms(var, result, report_errors):
        """
        Check if ``formula_terms`` attribute is present and well-typed.
        Returns its actual value.
        """
        try:
            formula_terms = var.getncattr("formula_terms")
        except AttributeError:
            # This attribute is not mandatory (default: Z vertical coordinate)
            return None

        try:
            terms = parse_formula_terms(formula_terms)
        except ErrorMalformedFormula as error:
            if report_errors:
                result.add_error("malformed 'formula_terms' attribute: \"{0}\".".format(formula_terms))
                return None
            else:
                raise error

        try:
            std_name = stdnames.get_root(var.standard_name)
        except AttributeError:
            if report_errors:
                result.add_warning("defines a 'formula_terms' attribute but has no 'standard_name'.")
            return None

        formula = get_formula(std_name)

        if formula is None:
            message = "defines an unknown dimensional coordinate vertical formula."
            if report_errors:
                result.add_error(message)
                return None
            else:
                raise ErrorMalformedFormula(message)

        return_val = bolded(formula.name)
        for param in formula.parameters:
            try:
                param_var = var.archive.get_variable(terms[param])
            except UnknownVariable as error:
                message = "formula term '{0}' is not mapped to any known variable.".format(param)
                if report_errors:
                    result.add_error(message)
                    return_val = None
                else:
                    raise ErrorMalformedFormula(message)
            else:
                if report_errors:
                    result.add_comment("formula term '{0}' is mapped ".format(param) +
                                       "to variable '{0}'.".format(param_var))

        return return_val

    def _check_coordinates(self, var, result):
        """
        Check if ``coordinates`` attribute of variable *var* is well formed if present.
        """
        if var.is_latlon_variable():
            result.add_comment("is used as an auxiliary coordinate variable")

        try:
            coordinates = var.coordinates
        except AttributeError:
            return

        coord_names = coordinates.split(' ')
        unknowns = len(coord_names)

        for name in coord_names:
            try:
                self.archive.get_variable(name)
                unknowns -= 1
            except UnknownVariable:
                result.add_error("auxiliary coordinate variable '{0}' ".format(bolded(name)) +
                                 "not found in file.")
                continue

        if unknowns == 0:
            result.add_comment("has checked auxiliary coordinate variables: " +
                               ", ".join("'"+bolded(v)+"'" for v in coord_names))

    def _check_position(self, var, result):
        """
        Check position of variable *var* on the C grid.
        """
        result.add_comment("has position '{0}' on C grid.".format(var.position))
