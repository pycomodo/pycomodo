#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This module provides some functions to compute baroclinic modes.

>>> import pycomodo.util.internal as pui
"""

import numpy as np
import pycomodo.operators as op

__all__ = [ "compute_instant_bn2", "compute_modes" ]

def compute_profiles_fallback( bn2, idzt, idzw, cst_eps ):

    nz = bn2.shape[0]-1
    ny = bn2.shape[1]
    nx = bn2.shape[2]

    profiles = np.ma.empty([3, nz, ny, nx], dtype=np.float64)
    mask3d   = np.ma.empty([   nz, ny, nx], dtype=np.uint8)

    ak = np.ma.empty([nz], dtype=np.float64)
    bk = np.ma.empty([nz], dtype=np.float64)
    ck = np.ma.empty([nz], dtype=np.float64)
    mk = np.ma.empty([nz], dtype=np.bool)

    for j in range(ny):
        for i in range(nx):

            mk[0]  = (bn2[1, j, i] > cst_eps) and (idzt[0, j, i] > cst_eps) and (idzw[1, j, i] > cst_eps)
            bk[0]  = idzw[1, j, i]*idzt[1, j, i] / bn2[1, j, i]
            ck[0]  = idzw[1, j, i]*idzt[0, j, i] / bn2[1, j, i]
            ak[0]  = -ck[0]

            for k in range(1, nz-1):
                mk[k] = (bn2[k+1, j, i] > cst_eps) and (idzt[k, j, i] > cst_eps) and (idzw[k+1, j, i] > cst_eps)
                bk[k] = idzw[k+1, j, i]*idzt[k+1, j, i] / bn2[k+1, j, i]
                ck[k] = idzw[k+1, j, i]*idzt[k,   j, i] / bn2[k+1, j, i]
                ak[k] = -( bk[k-1] + ck[k] )

            mk[nz-1] = (bn2[nz,   j, i] > cst_eps) and (idzt[nz-1, j, i] > cst_eps) and (idzw[nz-1, j, i] > cst_eps)
            ak[nz-1] = idzt[nz-1, j, i] / ( 9.81 - 0.5*bn2[nz, j, i]/idzw[nz-1, j, i] ) - bk[nz-2]

            profiles[0, :, j, i] = ak[:]
            profiles[1, :, j, i] = bk[:]
            profiles[2, :, j, i] = ck[:]
            mask3d[:, j, i] = mk[:]

    return profiles, mask3d


try:
    from pycomodo.util._internal import compute_profiles
except ImportError:
    from pycomodo.io.customlogger import log
    log.warning("Unable to load Cython-optimized 'pycomodo.util._internal' library.")
    log.warning("You should install 'cython' package and run './build_pyx.py' script "
                "in the pycomodo main directory.")
    compute_profiles = compute_profiles_fallback


def compute_instant_bn2(var_rho, var_dzw, time_index, cst_g=9.81, cst_rho0=1000.):
    """
    Returns the instantaneous Brunt-Väisälä frequency at time index *time_index*.

    :param var_rho: model potential density (instance of :class:`.Variable`).
    :param var_dzw: cell thickness value at vertical interfaces (instance of :class:`.Variable`).
    :param int time_index: index of the time dimension for the requested value.
    :param float cst_g: gravity constant in m/s^2 (Default: 9.81).
    :param float cst_rho0: base density in kg/m^3 (Default: 1000).

    >>> var_rho = puv.get_potential_density(archive)
    >>> var_dzw = archive.get_metric("z", "w", axes="ZYX", tpoint=0)
    >>> n2 = pui.compute_instant_bn2(var_rho, var_dzw, time_index=0)
    >>> n2[:, 30, 30]
    masked_array(data = [-- 9.21961e-06 9.21958e-06 9.21962e-06 9.21958e-06 1.61166e-05
                            9.21962e-06 9.21962e-06 9.21958e-06 9.21962e-06 --],
                 mask = [ True False False False False False False False False False  True],
                 fill_value = 1e+20)
    """
    # Get instant values for rho and dzw.
    rho_n = var_rho.get_instant_value(time_index)
    dzw_n = var_dzw.get_instant_value(time_index, dtype=np.float64)

    # Compute n2
    up = 1. if var_rho.axes['Z'].positive == "up" else -1.
    s_to, s_in1, s_in2 = op.get_interp_indices_1d(var_rho, var_rho.position, var_dzw.position, 'z')

    bn2_n = np.ma.masked_all(dzw_n.shape, dtype=np.float)
    bn2_n[s_to] = -up*(cst_g/cst_rho0)*( rho_n[s_in2] - rho_n[s_in1] ) / dzw_n[s_to]

    return bn2_n


def create_chunks_i( ni, ntiles ):
    """
    Returns an array of numpy slices corresponding to the tiling of the domain in *ntiles*
    subdomains in the x dimension.
    """
    chunk_i_int = ni // ntiles
    chunk_i_res = ni %  ntiles

    chunks  = []
    ie_array = np.arange(0, ni, chunk_i_int) + chunk_i_int

    for k in range(ntiles):

        if chunk_i_res > 0:
            chunk_i_res -= 1
            ie_array[k:] += 1

        ib = 0 if k == 0 else ie_array[k-1]
        ie = ie_array[k]

        chunks.append(np.index_exp[..., ib:ie])

    return chunks


def create_chunks_j( nj, ntiles ):
    """
    Returns an array of numpy slices corresponding to the tiling of the domain in *ntiles*
    subdomains in the y dimension.
    """
    chunk_j_int = nj // ntiles
    chunk_j_res = nj %  ntiles

    chunks  = []
    je_array = np.arange(0, nj, chunk_j_int) + chunk_j_int

    for k in range(ntiles):

        if chunk_j_res > 0:
            chunk_j_res -= 1
            je_array[k:] += 1

        jb = 0 if k == 0 else je_array[k-1]
        je = je_array[k]

        chunks.append(np.index_exp[..., jb:je, :])

    return chunks


def solve_eigen_tridiag(pp, mask2d, mask3d=None, n_modes=None):
    """
    Solves the eigenproblem for each non-masked horizontal point.

    Let N be the number of non-masked Z levels : we solve the following system :

    |a_0  c_0   0    0   0  ...  0  |   | M^p_0   |           | M^p_0   |
    |b_0  a_1  c_1   0   0       0  |   | M^p_1   |           | M^p_1   |
    | 0   b_1  a_2  c_2  0  ...  0  |   | M^p_2   |           | M^p_2   |
    |               ...             |   |  ...    |           |  ...    |
    | 0  ...  b_k-1 a_k c_k ...  0  | x | M^p_k   | = lamda^p | M^p_k   |
    |               ...             |   |  ...    |           |  ...    |
    | 0  ...    0  b_N-3 a_N-2 b_N-2|   | M^p_N-2 |           | M^p_N-2 |
    | 0  ...    0    0   b_N-2 a_N-1|   | M^p_N-1 |           | M^p_N-1 |

    where the coefficients of the tridiagonal matrix are computed by 'compute_profiles'
    and stored in *pp*.

    The first index corresponds to the bottom and the last on to the surface (positive=="up").
    Therefore must ensure that c_0 = -a_0 (bottom boundary condition) when there are masked cells
    like in Z vertical coordinates.

    Only the first *n_modes* pairs of eigenvalues and eigenvectors returned, sorted by decreasing magnitude.
    """
    nz = pp.shape[1]
    ny = mask2d.shape[-2]
    nx = mask2d.shape[-1]

    if n_modes is None:
        n_modes = nz-2

    shape_vals = [n_modes] + list(mask2d.shape)
    shape_vecs = [n_modes] + list(mask2d.shape) + [nz]

    valps = np.ma.masked_all(shape_vals, dtype=np.float64)
    vecps = np.ma.masked_all(shape_vecs, dtype=np.float64)

    for j in range(ny):
        for i in range(nx):

            if mask2d[j, i]:
                continue

            if mask3d is None:
                izb = 0
            else:
                try:
                    izb = mask3d[..., j, i].nonzero()[0][0]
                except IndexError:
                    continue

            if nz-izb <= n_modes + 1:
                continue

            # Extract coefficients of the tridiagonal matrix
            ak = pp[0, izb:,   j, i]
            bk = pp[1, izb:-1, j, i]
            ck = pp[2, izb:-1, j, i]

            # Ensure bottom boundary condition
            ak[0] = -ck[0]  # Necessary if izb != 0 (Z-coordinate system)

            # Solve eigensystem
            matrix     = np.diag(ak) + np.diag(bk, -1) + np.diag(ck, 1)
            valp, vecp = np.linalg.eig(matrix)

            # Sort the eigenvalues by decreasing magnitude
            valp       = np.abs(valp)
            indices    = np.argsort(valp, kind='mergesort', axis=0)
            valps[:, j, i]       = valp[indices][:n_modes]
            vecps[:, j, i, izb:] = vecp[:, indices].T[:n_modes]

    return valps, vecps


def ensure_3d_contiguous(array):
    """
    Returns a 3d masked array with the same data as in *array*. Make sure that the output is 3d and
    contiguous in memory for the last dimension. The overhead should be minimal.
    """
    ndims = len(array.shape)
    if ndims == 3:
        array_c = np.ma.asarray(np.ascontiguousarray(array))
    elif ndims == 2:
        array_c = np.ma.asarray(np.ascontiguousarray(array[:, None]))
    elif ndims == 1:
        array_c = np.ma.asarray(np.ascontiguousarray(array[:, None, None]))
    else:
        raise Exception("Bad shape for this array : {0}".format(array.shape))

    array_c[array.mask] = np.ma.masked

    return array_c


def compute_modes(n_modes, bn2_n, var_dzt, var_dzw, time_index=0, chunk_xy=None, pool=None, cst_eps=1e-10):
    """
    Returns a tuple (valp, vecp) of numpy arrays corresponding to internal modes. *valp* contains
    the eigenvalues and *vecp* contains the eigenvectors of the following problem:

    .. code-block:: python

        (d/dz)[ 1/N^2 (d/dz)[ vecp_n ] ] + valp_n vecp_n = 0

        # with boundary conditions :
        (d/dz)[ vecp_n ] = 0                  at bottom
        (d/dz)[ vecp_n ] + N^2/g vecp_n = 0   at free surface

    :param int n_modes: numbers of modes to return.
    :param bn2_n: instantaneous Brunt-Väisälä frequency (Numpy array).
    :param var_dzt: cell thickness (instance of :class:`.Variable`).
    :param var_dzw: cell thickness value at vertical interfaces (instance of :class:`.Variable`).
    :param int time_index: index of the time dimension for the requested value.
    :param chunk_xy: horizontal slice for data restriction (tiling, etc.).
    :param pool: pool of worker for parallel processing (instance of :class:`.multiprocessing.Pool`).
    :param float cst_eps: threshold value for bn2 values (Default: 1e-10).

    >>> var_dzt = archive.get_metric("z", "t", axes="ZYX", tpoint=0)
    >>> var_dzw = archive.get_metric("z", "w", axes="ZYX", tpoint=0)
    >>> var_rho = puv.get_potential_density(archive)
    >>> chunk_xy = np.index_exp[..., 1, 1]
    >>> n2       = pui.compute_instant_bn2(var_rho, var_dzw, time_index=0)[chunk_xy]
    >>> valps, vecps = pui.compute_modes(2, n2, var_dzt, var_dzw, time_index=0, chunk_xy=chunk_xy)

    We can now extract mode velocities from eigenvalues:

    >>> celerite = 1. / np.sqrt(np.abs(valps))
    >>> print "c0 = {0}\\nc1 = {1}".format(celerite[0], celerite[1])
    c0 = 221.298011009
    c1 = 4.85475286635

    And get the associated eigenvectors:

    >>> v0 = vecps[0]
    >>> v1 = vecps[1]
    """
    if chunk_xy is None:
        chunk_xy = np.index_exp[...]

    # Get instantanous values for cell thickness
    dzt_n  = var_dzt.get_instant_value(time_index, chunk_xy, dtype=np.float64)
    dzw_n  = var_dzw.get_instant_value(time_index, chunk_xy, dtype=np.float64)

    # Make sure that data arrays are 3D and contiguous in memory.
    bn2_c = ensure_3d_contiguous(bn2_n)
    dzt_c = ensure_3d_contiguous(dzt_n)
    dzw_c = ensure_3d_contiguous(dzw_n)

    # Ensure data is oriented from bottom to up
    up = 1 if var_dzt.axes["Z"].positive == "up" else -1
    if up < 0:
        dzt_c[:] = dzt_c[::-1]
        dzw_c[:] = dzw_c[::-1]
        bn2_c[:] = bn2_c[::-1]

    mask_w  = bn2_c.mask | (bn2_c < cst_eps)

    # Fix boundary conditions for n2
    bn2_c[mask_w] = cst_eps
    bn2_c[ 0] = bn2_c[ 1]   # at bottom
    bn2_c[-1] = bn2_c[-2]   # at surface

    idzt_c = np.ma.divide(1., dzt_c)
    idzw_c = np.ma.divide(1., dzw_c)

    idzt_c[idzt_c.mask] = 0.5*cst_eps
    idzw_c[idzw_c.mask] = 0.5*cst_eps

    mask_n = np.asarray(bn2_c[-1].mask, dtype=np.uint8)

    # Compute components of tridiagonal matrix and 3d mask before solving eigenproblem.
    pp, mask3d = compute_profiles( bn2_c, idzt_c, idzw_c, cst_eps)

    if pool is not None:

        # Prepare data for multiprocessing
        valp = np.ma.masked_all( (n_modes,) + mask3d.shape[1:] )
        vecp = np.ma.masked_all( (n_modes,) + mask3d.shape[1:] + (mask3d.shape[0],))

        # Make the workers call 'solve_eigen_tridiag' in an asynchrone way.
        results = [ ( chunk_i,
                      pool.apply_async(solve_eigen_tridiag, (pp[chunk_i], mask_n[chunk_i], mask3d[chunk_i], n_modes)) )
                    for chunk_i in create_chunks_i( mask_n.shape[-1], len(pool._pool) ) ]

        # Gather data from workers
        for chunk_i, data_array in results:
            _valp, _vecp  = data_array.get()
            chunk_iz = chunk_i + np.index_exp[:]
            valp[chunk_i]  = _valp
            vecp[chunk_iz] = _vecp
    else:

        # Call 'solve_eigen_tridiag' serially.
        valp, vecp = solve_eigen_tridiag(pp, mask_n, mask3d, n_modes)

    return np.ma.squeeze(valp), np.ma.squeeze(vecp)
