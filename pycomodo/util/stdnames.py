#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This module provides some utility functions to deal with NetCDF variable attributes.

>>> import pycomodo.util.stdnames as pus
"""
import re

RE_INVERSE_OF     = re.compile(r"inverse_of_(\w+)")
RE_AT_LOCATION    = re.compile(r"(\w+)(_at_[tuvfw]+_location)$")
RE_WHICH_LOCATION = re.compile(r"_at_([tuvfw]+)_location")


def get_root(name):
    """
    Returns the root of the variable *name*, by removing the suffix ``_at_[uvfw]_location`` if present.

    >>> pus.get_root("varname_at_w_location")
    'varname'

    >>> pus.get_root("stress_at_bottom")
    'stress_at_bottom'
    """
    m_at = RE_AT_LOCATION.match(name)
    if m_at:
        # in this case, read the first part of the name ("generic" standard name)...
        root = m_at.group(1)
    else:
        root = name

    return root


def get_at_location(name):
    """
    If the pattern ``_at_[uvfw]_location`` is found in *name*, this function returns the corresponding
    location (either ``u``, ``v``, ``f`` or ``w``).

    >>> pus.get_at_location("varname_at_w_location")
    'w'
    >>> pus.get_at_location("varname_at_uw_location")
    'uw'

    Otherwise, it returns ``t``:

    >>> pus.get_at_location("stress_at_bottom")
    't'
    """
    m_at = RE_AT_LOCATION.match(name)
    if m_at:
        return RE_WHICH_LOCATION.match(m_at.group(2)).group(1)
    else:
        return 't'


def get_inverse_of(name):
    """
    If the pattern ``inverse_of_`` is found at the beginning of *name*, this function
    returns the remainder.

    >>> pus.get_inverse_of("inverse_of_cell_x_size")
    'cell_x_size'

    Otherwise, it returns False:

    >>> pus.get_inverse_of("cell_x_size")
    False
    """
    m_inv = RE_INVERSE_OF.match(name)
    if m_inv:
        return m_inv.group(1)
    else:
        return False


def get_positioned(root_name, position=None):
    """
    Returns the full ``standard_name`` made of *root_name* followed by the *position* suffix if
    it is not *t*.

    >>> pus.get_positioned('sea_water_x_velocity', 'u')
    'sea_water_x_velocity_at_u_location'
    """
    if position is None:
        position = 't'
    else:
        position = position.lower()

    if position == 't' or position == 'tt':
        return root_name
    else:
        return "{0}_at_{1}_location".format(root_name, position)
