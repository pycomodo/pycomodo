#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This module provides some functions to compute Courant numbers for various terms of the
primitive equations, based on model output files.

>>> import pycomodo.util.courant as puc
"""

import pycomodo.operators as op
import pycomodo.util.variables as puv
import pycomodo.util.internal as pui

from pycomodo.compat import evaluate, USE_NUMEXPR

import multiprocessing as mp
import time
import signal

import numpy as np

DEBUG = True


def _init_worker():
    signal.signal(signal.SIGINT, signal.SIG_IGN)


def get_courant_internal(archive, dt, var_dzt=None, var_dzw=None,
                         cst_g=9.81, cst_rho0=1000., cst_alpha=0.20, cst_beta=0.77,
                         ncpus=None, ntiles=1):
    """
    Computes Courant number associated with the first baroclinic mode. Actually returns a tuple
    made of the first baroclinic mode *c1* and the CFL.

    :param archive: instance of :class:`.Archive` to work with.
    :param float dt: baroclinic time step
    :param var_dzt: cell thickness (instance of :class:`.Variable`)
    :param var_dzw: cell thickness value at vertical interfaces (instance of :class:`.Variable`)
    :param float cst_g: gravity constant in m/s^2 (Default: 9.81)
    :param float cst_rho0: base density in kg/m^3 (Default: 1000)
    :param float cst_alpha: thermal expansion coefficient (linear equation of state)
    :param float cst_beta: saline  expansion coefficient (linear equation of state)
    :param int ncpus: number of CPUs to use for mode computation.
    :param int ntiles: number of vertical chunks for domain tiling.

    Cell thickness can be provided if it has been previously computed. This will prevent a
    re-computation (*var_dzt* at 't' points, *var_dzw* at 'w' points).

    If the model does not provide potential density, the latter will be reconstituted using a
    linear EOS using *cst_alpha* and *cst_beta* for thermal & saline expansion coefficients.

    In order to improve performance on large datasets, it is very advantageous to slice the domain
    in several tiles ; use *ntiles* option for this. It may also be interesting to dispatch the computation
    on several CPUS on a multi-core machine. If *ncpus* is larger than 1, a pool of workers will be set
    up for this purpose. To many workers may also decrease performance ; use with care.

    >>> var_dzt = archive.get_metric("z", "t", axes="ZYX")
    >>> var_dzw = archive.get_metric("z", "w", axes="ZYX")
    >>> c1, cfl_i = puc.get_courant_internal(archive, dt, var_dzt, var_dzw, ntiles=8)
    """
    var_dxt = archive.get_metric("x", "t")
    var_dyt = archive.get_metric("y", "t")
    var_dzw = var_dzw if (var_dzw is not None) else archive.get_metric("z", "w")
    var_dzt = var_dzt if (var_dzt is not None) else archive.get_metric("z", "t")

    var_rho = puv.get_potential_density(archive, cst_rho0=cst_rho0, cst_alpha=cst_alpha, cst_beta=cst_beta)
    var_bn2 = puv.get_bn2(var_rho, cst_g, cst_rho0)

    # dimensions for c1 are dimensions of rho minus the "Z" axis
    new_dims = [str(dim) for dim in var_rho.axes.values() if dim.axes["Z"] is None]
    c1    = archive.new_variable("c1", dimensions=new_dims, dtype=var_rho.dtype)
    cfl_i = archive.new_variable("cfl_i", dimensions=new_dims, dtype=var_rho.dtype)

    c1[:]    = np.ma.masked
    cfl_i[:] = np.ma.masked

    dx_t = var_dxt.get_instant_value(0)
    dy_t = var_dyt.get_instant_value(0)

    nj, ni = dx_t.shape[-2:]
    chunks = pui.create_chunks_j(nj, ntiles)

    # Setup a pool of workers if we were asked to work on more than one cpu
    ncpus = ncpus if (ncpus is not None) else 1
    pool  = None  if (ncpus == 1) else mp.Pool(ncpus, _init_worker)

    if DEBUG:
        t_start = time.time()

    for n in range(0, archive.ntimes):

        for chunk_j in chunks:

            chunk_t  = np.index_exp[n] + chunk_j
            bn2_n    = var_bn2[chunk_t]
            cvals, _ = pui.compute_modes(2, bn2_n, var_dzt, var_dzw, time_index=n, chunk_xy=chunk_j, pool=pool)

            with np.errstate(all='ignore'):
                c1n = np.ma.divide(1., np.ma.sqrt(cvals[1]))
                c1[chunk_t] = c1n
                cfl_i[chunk_t] = np.ma.divide(dt*c1n, np.sqrt(dx_t[chunk_j]**2+dy_t[chunk_j]**2))

    # If we have a pool of workers, it is time to close it.
    if pool is not None:
        pool.close()
        pool.join()

    if DEBUG:
        t_end = time.time()
        print "NCPU = {} ; Time spent : {}".format(ncpus, t_end-t_start)

    archive.register_variable(c1)
    archive.register_variable(cfl_i)

    return c1, cfl_i


def get_courant_advection(archive, dt, component, name=None, volume=None):
    """
    Computes Courant number associated with advective terms. Returns an instance of :class:`.Variable`.

    :param archive: instance of :class:`.Archive` to work with.
    :param float dt: baroclinic time step
    :param char component: velocity component (either 'u', 'v', 'w')
    :param str name: name of the new variable
    :param volume: cell volume (instance of :class:`.Variable`)

    Cell volume can be provided if it has been previously computed. This will prevent a re-computation.

    >>> dx_t = archive.get_metric("x","t")
    >>> dy_t = archive.get_metric("y","t")
    >>> dz_t = archive.get_metric("z","t", axes="ZYX")
    >>> volume = op.operation("1/(dx*dy*dz)", name="volume", dx=dx_t, dy=dy_t, dz=dz_t)
    >>> cu_adv = puc.get_courant_advection(archive, dt, 'u', name='cfl_adv_u', volume=volume)
    >>> cv_adv = puc.get_courant_advection(archive, dt, 'v', name='cfl_adv_v', volume=volume)
    >>> cw_adv = puc.get_courant_advection(archive, dt, 'w', name='cfl_adv_w', volume=volume)
    """
    component = component.lower()
    w_stag = 'Z' if component in ('u', 'v') else None
    name = name or "c_adv"

    stdname = {'u': 'sea_water_x_velocity',
               'v': 'sea_water_y_velocity',
               'w': 'upward_sea_water_velocity'}

    if volume is None:
        dx_t = archive.get_metric("x", "t")
        dy_t = archive.get_metric("y", "t")
        dz_t = archive.get_metric("z", "t")
        volume = op.operation("1./abs(dx*dy*dz)", name='cell_volume', dx=dx_t, dy=dy_t, dz=dz_t)

    axes = "".join(volume.axes)

    velocity = puv.get_variable_at_location(archive, stdname[component], location=component, interpolate=True)

    if component == 'u':
        var_z = velocity.get_zlevels(stag_axis=w_stag, axes=axes)
        dx = archive.get_metric("y", "u")
        dy = var_z.diff_z(dummy=True)
        del var_z
    elif component == 'v':
        var_z = velocity.get_zlevels(stag_axis=w_stag, axes=axes)
        dx = archive.get_metric("x", "v")
        dy = var_z.diff_z(dummy=True)
        del var_z
    elif component == 'w':
        dx = archive.get_metric("x", "t")
        dy = archive.get_metric("y", "t")

    vel_flux = op.operation("dt*vel*dx*dy", dt=dt, vel=velocity, dx=dx, dy=dy)

    courant_adv = vel_flux.interpolate('T', name=name, method="flux")
    if USE_NUMEXPR:
        mask_adv = np.ma.copy(courant_adv.mask)
        data_adv = courant_adv[:]
        data_vol = volume[:]
        courant_adv[:] = evaluate("data_adv*data_vol")
        courant_adv._var.mask[:] = mask_adv[:]
    else:
        courant_adv[:] *= volume[:]

    return courant_adv


def get_courant_coriolis( archive, dt, var_coriolis=None):
    """
    Computes Courant number associated with Coriolis term. Returns an instance of :class:`.Variable`.

    :param archive: instance of :class:`.Archive` to work with.
    :param float dt: baroclinic time step
    :param var_coriolis: Coriolis parameter at vorticity points.

    If *var_coriolis* is not provided, the *archive* will try to rebuild it from model data.

    >>> c_cor = puc.get_courant_coriolis(archive, dt)
    """
    if var_coriolis is None:
        var_coriolis = puv.get_coriolis(archive, name="f")

    c_cor = archive.new_variable("fdt", model=var_coriolis)
    c_cor[:] = var_coriolis[:] * dt
    archive.register_variable(c_cor)

    if hasattr(c_cor, 'valid_min'):
        c_cor.valid_min *= dt
    if hasattr(c_cor, 'valid_max'):
        c_cor.valid_max *= dt

    return c_cor
