#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This package contains various utility functions to compute diagnostics.
"""
