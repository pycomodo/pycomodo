#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
cimport numpy as np

cimport cython

@cython.boundscheck(False)  # turn off bounds-checking for entire function
@cython.wraparound(False)
@cython.nonecheck(False)
def compute_profiles (
     np.float64_t[:,:,::1] bn2,
     np.float64_t[:,:,::1] idzt,
     np.float64_t[:,:,::1] idzw,
     np.float_t            cst_eps ):

    cdef int nz = bn2.shape[0]-1
    cdef int ny = bn2.shape[1]
    cdef int nx = bn2.shape[2]

    cdef np.ndarray[np.float64_t, ndim=4] profiles = np.empty([3, nz, ny, nx], dtype=np.float64)
    cdef np.ndarray[np.uint8_t,   ndim=3] mask3d   = np.empty([   nz, ny, nx], dtype=np.uint8)
    cdef np.ndarray[np.float64_t, ndim=1] ak = np.empty([nz], dtype=np.float64)
    cdef np.ndarray[np.float64_t, ndim=1] bk = np.empty([nz], dtype=np.float64)
    cdef np.ndarray[np.float64_t, ndim=1] ck = np.empty([nz], dtype=np.float64)
    cdef np.ndarray[np.uint8_t,   ndim=1] mk = np.empty([nz], dtype=np.uint8)
    cdef unsigned int i, j, k

    assert (idzt.shape[1] == ny) and (idzw.shape[1] == ny)
    assert (idzt.shape[2] == nx) and (idzw.shape[2] == nx)

    for i in range(nx):
        for j in range(ny):

            mk[0] = (bn2[1,j,i] > cst_eps) * (idzt[0,j,i] > cst_eps) * (idzw[1,j,i] > cst_eps)
            bk[0] = idzw[1,j,i]*idzt[1,j,i] / bn2[1,j,i]
            ck[0] = idzw[1,j,i]*idzt[0,j,i] / bn2[1,j,i]
            ak[0] = -ck[0]

            for k in range(1, nz-1):
                mk[k] = (bn2[k+1,j,i] > cst_eps) * (idzt[k,j,i] > cst_eps) * (idzw[k+1,j,i] > cst_eps)
                bk[k] = idzw[k+1,j,i]*idzt[k+1,j,i] / bn2[k+1,j,i]
                ck[k] = idzw[k+1,j,i]*idzt[k,  j,i] / bn2[k+1,j,i]
                ak[k] = -( bk[k-1] + ck[k] )

            mk[nz-1] = (bn2[nz,j,i] > cst_eps) * (idzt[nz-1,j,i] > cst_eps) * (idzw[nz-1,j,i] > cst_eps)
            ak[nz-1] = idzt[nz-1,j,i] / ( 9.81 - 0.5*bn2[nz,j,i]/idzw[nz-1,j,i] ) - bk[nz-2]

            profiles[0,:,j,i] = ak[:]
            profiles[1,:,j,i] = bk[:]
            profiles[2,:,j,i] = ck[:]
            mask3d[:,j,i] = mk[:]

    return profiles, mask3d
