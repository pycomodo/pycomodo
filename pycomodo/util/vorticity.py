#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Defines various functions ...
"""
import numpy as np
from pycomodo.io.customlogger import log

__all__ = [ "compute_z_iso", "compute_pv" ]

def compute_z_iso_fallback(rho_iso, sigma, zt, seafl, ssh ):
    """
    Compute the depth of interfaces for layers of iso-sigma.

    :param rho_iso: values of sigma-theta that define the layers (1d array: Z)
    :param sigma: model sigma-theta (3d array: ZYX)
    :param zt: model cell-center depth (3d array: ZYX)
    :param seafl: sea floor depth (2d array: YX)
    :param ssh: sea-surface height (2d array: YX)
    """
    nlev       = len(rho_iso)       # Number of iso-sigma layers
    nz, ny, nx = sigma.shape[-3:]   # Model grid dimensions

    # depth of layers interfaces
    z_int = np.empty((nlev+1, ny, nx), dtype=sigma.dtype)
    z_int[0]    = -ssh
    z_int[nlev] = seafl

    for k in range(nlev-1):

        sig_crit = 0.5 * ( rho_iso[k] + rho_iso[k+1] )

        for j in range(ny):
            for i in range(nx):

                # Find layer whose 't' point is upper than the isopycnal interface
                smask = sigma[:, j, i] <= sig_crit
                ind = max(np.argmin(smask)-1, 0)    # cap with surface
                if np.all(smask):                   # cap with sea floor depth
                    ind = nz-2

                sig_kp0 = sigma[ind,   j, i]
                sig_kp1 = sigma[ind+1, j, i]
                zt_kp0 = zt[ind,   j, i]
                zt_kp1 = zt[ind+1, j, i]

                # Interpolate zt at layer interfaces
                _zint = ( (sig_kp1-sig_crit) * zt_kp0 + (sig_crit-sig_kp0) * zt_kp1 ) / (sig_kp1-sig_kp0)
                _zint = max(_zint, -ssh[j, i])      # cap with surface
                _zint = min(_zint, seafl[j, i])     # cap with sea floor depth

                z_int[k+1, j, i] = _zint

    return z_int

def compute_pv_fallback(z_int_f, zf, totvort_z):
    """
    Compute potential vorticity for layers of iso-density.

    :param z_int_f: depth of layers interfaces at vorticity points (3d array: ZYX)
    :param zf: model depth at vorticity points (3d array: ZYX)
    :param totvort_z: total vorticity at model levels (3d array: ZYX)
    """
    nlev       = z_int_f.shape[0]-1
    nz, ny, nx = zf.shape[-3:]

    tvor = np.zeros((nlev, ny, nx), dtype=totvort_z.dtype)

    # We compute the mean vorticity between z_int_f(kk) and z_int_f(kk+1)
    for kk in range(nlev):
        for k in range(nz-1):
            for j in range(ny):
                for i in range(nx):

                    if k == 0:
                        rup = max(z_int_f[0, j, i], z_int_f[kk, j, i])
                    else:
                        rup = max(zf[k, j, i], z_int_f[kk, j, i])

                    if k == nz-1:
                        rdn = min(z_int_f[nlev, j, i], z_int_f[kk+1, j, i])
                    else:
                        rdn = min(zf[k+1,j,i],         z_int_f[kk+1, j, i])

                    rkp0 = zf[k,   j, i]
                    rkp1 = zf[k+1, j, i]

                    if rdn > rup:
                        tvor[kk, j, i] += 0.5*(rdn-rup)/(rkp1-rkp0) * \
                                              ( (rup+rdn-2.*rkp0)*totvort_z[k+1, j, i] -
                                                (rup+rdn-2.*rkp1)*totvort_z[k,   j, i] )

    return tvor / (z_int_f[:-1]-z_int_f[1:])**2

try:
    from pycomodo.util._vorticity import compute_z_iso
except ImportError:
    log.warning("You should install 'cython' package and run './build_pyx.py' script "
                "in the pycomodo main directory.")
    log.warning("Unable to load Cython-optimized 'pycomodo.util._vorticity.compute_z_iso' routine.")
    compute_z_iso = compute_z_iso_fallback

try:
    from pycomodo.util._vorticity import compute_pv
except ImportError:
    log.warning("Unable to load Cython-optimized 'pycomodo.util._vorticity.compute_pv' routine.")
    compute_pv    = compute_pv_fallback

