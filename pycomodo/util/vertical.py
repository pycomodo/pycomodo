#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This module provides functions to compute vertical slices or reconstruct vertical levels.

>>> import pycomodo.util.vertical  as puz
"""

import numpy as np

import pycomodo.util.stdnames as stdnames
import pycomodo.util.variables as puv
import pycomodo.operators as op
from pycomodo.compat import DefaultOrderedDict
from pycomodo.core.exceptions import ErrorMalformedFormula, UnknownVariable
from pycomodo.io.customlogger import log

__all__ = [ "get_zlevels", "vinterp" ]


class VerticalFormula:
    """
    Container class for operations in :func:`.get_formula`.
    """
    def __init__(self, name, description, parameters, zfunc=None):
        self.name = name
        self.description = description
        self.parameters  = parameters
        self.zfunc       = zfunc

    def __repr__(self):
        return "{0}:\n{1}".format(self.name, self.description)


def calc_sigma(archive, axes, name, position, tpoint, jpoint, ipoint, **kwargs):
    """
    :code:`z(n,k,j,i) = eta(n,j,i) + sigma(k)*(depth(j,i)+eta(n,j,i))`
    """
    # Horizontal range for variables
    s_t  = np.index_exp[:] if ('T' in axes) else ()
    s_h  = np.index_exp[...]
    s_h += np.index_exp[:] if ('Y' in axes) else np.index_exp[jpoint]
    s_h += np.index_exp[:] if ('X' in axes) else np.index_exp[ipoint]

    if ("T" not in axes) and (tpoint is None):
        # We assume a free surface at rest
        eta = 0.
    else:
        # We fetch free surface at requested points
        var_eta = kwargs['eta'].interpolate(position)
        tpoint  = slice(None) if (tpoint is None) else tpoint
        eta     = var_eta[np.index_exp[tpoint] + s_h]

    var_depth = kwargs['depth'].interpolate(position)

    depth = var_depth[s_h]
    sigma = kwargs['sigma'][:]
    nlevs = sigma.shape[0]
    dims  = [ str(axis) for axis in axes.itervalues() ]

    z = archive.new_variable(name, dims, dtype=np.result_type(var_depth))

    for k in range(nlevs):
        s_tk = s_t + np.index_exp[k]
        z[s_tk] = eta + sigma[k]*(eta+depth)

    return z


def calc_s(archive, axes, name, position, tpoint, jpoint, ipoint, **kwargs):
    """
    :code:`z(n,k,j,i) = eta(n,j,i)*(1+s(k)) + depth_c*s(k) + (depth(j,i)-depth_c)*C(k)`

    with: :code:`C(k) = (1-b)*sinh(a*s(k))/sinh(a) + b*[tanh(a*(s(k)+0.5))/(2*tanh(0.5*a)) - 0.5]`
    """
    # Horizontal range for variables
    s_t  = np.index_exp[:] if ('T' in axes) else ()
    s_h  = np.index_exp[...]
    s_h += np.index_exp[:] if ('Y' in axes) else np.index_exp[jpoint]
    s_h += np.index_exp[:] if ('X' in axes) else np.index_exp[ipoint]

    if ("T" not in axes) and (tpoint is None):
        # We assume a free surface at rest
        eta = 0.
    else:
        # We fetch free surface at requested points
        var_eta = kwargs['eta'].interpolate(position)
        tpoint  = slice(None) if (tpoint is None) else tpoint
        eta     = var_eta[np.index_exp[tpoint] + s_h]

    var_depth   = kwargs['depth'].interpolate(position)
    var_depth_c = kwargs['depth_c'].interpolate(position)

    depth   = var_depth[s_h]
    depth_c = var_depth_c[s_h]
    s       = kwargs['s'][:]
    a       = kwargs['a'][:]
    b       = kwargs['b'][:]
    nlevs   = s.shape[0]
    dims    = [ str(axis) for axis in axes.itervalues() ]

    z    = archive.new_variable(name, dims, dtype=np.result_type(var_depth))
    C    = np.ndarray(nlevs, dtype=s.dtype)
    C[:] = (1-b)*np.sinh(a*s)/np.sinh(a) + b*(np.tanh(a*(s+0.5))/(2*np.tanh(0.5*a)) - 0.5)

    for k in range(nlevs):
        s_tk = s_t + np.index_exp[k]
        z[s_tk] = eta*(1+s[k]) + depth_c*s[k] + (depth-depth_c)*C[k]

    return z


def calc_s_g1(archive, axes, name, position, tpoint, jpoint, ipoint, **kwargs):
    """
    :code:`z(n,k,j,i) = S1(k,j,i) + eta(n,j,i)*(1+S1(k,j,i)/depth(j,i))`

    with: :code:`S1(k,j,i) = depth_c*s(k) + (depth(j,i)-depth_c) * C(k)`
    """
    # Horizontal range for variables
    s_t  = np.index_exp[:] if ('T' in axes) else ()
    s_h  = np.index_exp[...]
    s_h += np.index_exp[:] if ('Y' in axes) else np.index_exp[jpoint]
    s_h += np.index_exp[:] if ('X' in axes) else np.index_exp[ipoint]

    if ("T" not in axes) and (tpoint is None):
        # We assume a free surface at rest
        eta = 0.
    else:
        # We fetch free surface at requested points
        var_eta = kwargs['eta'].interpolate(position)
        tpoint  = slice(None) if (tpoint is None) else tpoint
        eta     = var_eta[np.index_exp[tpoint] + s_h]

    var_depth = kwargs['depth'].interpolate(position)
    depth     = var_depth[s_h]
    depth_c   = kwargs['depth_c'][:]
    C         = kwargs['C'][:]
    s         = kwargs['s'][:]
    nlevs     = s.shape[0]
    dims      = [ str(axis) for axis in axes.itervalues() ]
    ones      = np.ones_like(depth)

    z = archive.new_variable(name, dims, dtype=np.result_type(var_depth))

    for k in range(nlevs):
        s_tk = s_t + np.index_exp[k]
        S1   = depth_c*s[k] + (depth-depth_c)*C[k]
        z[s_tk] = S1 + eta*(ones+S1/depth)

    return z


def calc_s_g2(archive, axes, name, position, tpoint, jpoint, ipoint, **kwargs):
    """
    :code:`z(n,k,j,i) = eta(n,j,i) + (eta(n,j,i)+depth(j,i))*S2(k,j,i)`

    with: :code:`S2(k,j,i) = ( depth_c*s(k) + depth(j,i)*C(k) ) / ( depth_c + depth(j,i) )`
    """
    # Horizontal range for variables
    s_t  = np.index_exp[:] if ('T' in axes) else ()
    s_h  = np.index_exp[...]
    s_h += np.index_exp[:] if ('Y' in axes) else np.index_exp[jpoint]
    s_h += np.index_exp[:] if ('X' in axes) else np.index_exp[ipoint]

    if ("T" not in axes) and (tpoint is None):
        # We assume a free surface at rest
        eta = 0.
    else:
        # We fetch free surface at requested points
        var_eta = kwargs['eta'].interpolate(position)
        tpoint  = slice(None) if (tpoint is None) else tpoint
        eta     = var_eta[np.index_exp[tpoint] + s_h]

    var_depth = kwargs['depth'].interpolate(position)
    depth     = var_depth[s_h]
    depth_c   = kwargs['depth_c'][:]
    C         = kwargs['C'][:]
    s         = kwargs['s'][:]
    nlevs     = s.shape[0]
    dims      = [ str(axis) for axis in axes.itervalues() ]

    z = archive.new_variable(name, dims, dtype=np.result_type(var_depth))

    for k in range(nlevs):
        s_tk = s_t + np.index_exp[k]
        z[s_tk] = eta + (eta+depth)*(depth_c*s[k] + depth*C[k]) / ( depth_c + depth )

    return z


VERTICAL_FORMULAS = {
    "ocean_sigma_coordinate": VerticalFormula(
        name="Ocean sigma coordinate",
        description=""" z(n,k,j,i) = eta(n,j,i) + sigma(k)*(depth(j,i)+eta(n,j,i))
with:
  - z(n,k,j,i) : grid point elevation, increasing upwards, wrt. the reference level (eg. the mean sea level).
  - eta(n,j,i) : water level above the reference level (positive upwards).
  - sigma(k)   : adimensional vertical coordinate (0 <= k <= 1).
  - depth(j,i) : bottom depth (positive value) wrt. the reference level.""",
        parameters=["eta", "sigma", "depth"],
        zfunc=calc_sigma),

    "ocean_s_coordinate": VerticalFormula(
        name="Ocean s coordinate",
        description=""" z(n,k,j,i) = eta(n,j,i)*(1+s(k)) + depth_c*s(k) + (depth(j,i)-depth_c)*C(k)
with: C(k) = (1-b)*sinh(a*s(k))/sinh(a) + b*[tanh(a*(s(k)+0.5))/(2*tanh(0.5*a)) - 0.5]
and:
  - z(n,k,j,i) : grid point elevation, increasing upwards, wrt. the reference level (eg. the mean sea level).
  - eta(n,j,i) : water level above the reference level (positive upwards).
  - s(k)       : adimensional vertical coordinate (0 <= k <= 1).
  - depth(j,i) : bottom depth (positive value) wrt. the reference level.
  - a, b, depth_c : stretching parameters.""",
        parameters=["eta", "s", "depth", "depth_c", "a", "b"],
        zfunc=calc_s),

    "ocean_s_coordinate_g1": VerticalFormula(
        name="Ocean s-coordinate (ROMS original)",
        description=""" z(n,k,j,i) = S1(k,j,i) + eta(n,j,i)*(1+S1(k,j,i)/depth(j,i))
with: S1(k,j,i) = depth_c*s(k) + (depth(j,i)-depth_c) * C(k)
and:
  - z(n,k,j,i) : grid point elevation, increasing upwards, wrt. the reference level (eg. the mean sea level).
  - eta(n,j,i) : water level above the reference level (positive upwards).
  - s(k)       : adimensional vertical coordinate (0 <= k <= 1).
  - depth(j,i) : bottom depth (positive value) wrt. the reference level.
  - depth_c    : a stretching parameter.
  - C(k)       : vertical stretching function.""",
        parameters=["eta", "s", "depth", "depth_c", "C"],
        zfunc=calc_s_g1),

    "ocean_s_coordinate_g2": VerticalFormula(
        name="Ocean s-coordinate (UCLA-ROMS 2005)",
        description=""" z(n,k,j,i) = eta(n,j,i) + (eta(n,j,i)+depth(j,i))*S2(k,j,i)
with: S2(k,j,i) = ( depth_c*s(k) + depth(j,i)*C(k) ) / ( depth_c + depth(j,i) )
and:
  - z(n,k,j,i) : grid point elevation, increasing upwards, wrt. the reference level (eg. the mean sea level).
  - eta(n,j,i) : water level above the reference level (positive upwards).
  - s(k)       : adimensional vertical coordinate (0 <= k <= 1).
  - depth(j,i) : bottom depth (positive value) wrt. the reference level.
  - depth_c    : a stretching parameter.
  - C(k)       : vertical stretching function.""",
        parameters=["eta", "s", "depth", "depth_c", "C"],
        zfunc=calc_s_g2)
}


def parse_formula_terms( formula ):
    """
    Returns a dictionary with the correspondance between formula terms and variables.
    """
    terms = {}
    term_g = (t for t in formula.split(" "))

    try:
        while True:
            param = term_g.next()
            if param[-1] != ":":
                raise ErrorMalformedFormula("Malformed 'formula_terms' attribute: {0}".format(formula))
            param = param[:-1]
            try:
                variable = term_g.next()
            except StopIteration:
                raise ErrorMalformedFormula("Malformed 'formula_terms' attribute: {0}".format(formula))
            terms[param] = variable
    except StopIteration:
        pass

    return terms


def get_formula( standard_name ):
    """
    Returns the instance of VerticalFormula corresponding to ``standard_name``.
    """
    if standard_name in VERTICAL_FORMULAS:
        return VERTICAL_FORMULAS[standard_name]


def get_zlevels( archive, location='t', axes="TZYX", name=None, stdname=None, tpoint=None, hpoint=None ):
    """
    Returns depth variable.

    :param archive: instance of :class:`.Archive` where to look for the variable.
    :param location: point on the Arakawa grid where the depth is required (either ``'t'``, ``'u'``,
                     ``'v'``, ``'w'``, ``'f'``, ``'uw'``, ``'vw'`` or ``'fw'``.
                     Default: ``'t'``, cell-centered variable).
    :param axes: list of axes. Axis ``'Z'`` must be present. Default: 4D variable "TZXY".
    :param name: name for the new variable.
    :param stdname: value for the ``standard_name`` attribute.
    :param tpoint: if 'T' axis is not requested, specifies the time index for which the levels are
                   computed. Default: 0.
    :param hpoint: if either 'X' or 'Y' axis is not requested, specifies the horizontal point index for
                   which the levels are computed. Default: (0, 0).

    >>> print puz.get_zlevels(archive).info()
        float32 z_at_t(time, s_rho, eta_rho, xi_rho)
            z_at_t:units = "m"
            z_at_t:standard_name = "depth"

    It is possible to restrict the variable to a subset of the original axes. In this case,
    you may want to specify the value of the indices for the missing dimensions :

    >>> z_fw = puz.get_zlevels(archive, "fw", axes="ZXY", tpoint=0)
    >>> print z_fw.dimensions, "size = {0}\\n".format(z_fw.size), z_fw[:, 30, 30]
    ['s_w', 'xi_u', 'eta_v'] size = 40931
    [-5000.0000 -4499.9141 -3999.8284 -3499.7427 -2999.6567 -2499.5710
     -1999.4852 -1499.3994  -999.3136  -499.2278     0.8580]

    >>> zw_at_center = puz.get_zlevels(archive, "w", name="zw_at_center",
                                       axes="Z", tpoint=0, hpoint=(30, 30))
    >>> print zw_at_center.dimensions, "size = {0}\\n".format(zw_at_center.size), zw_at_center[:]
    ['s_w'] size = 11
    [-5000.0000 -4499.9141 -3999.8284 -3499.7427 -2999.6567 -2499.5710
     -1999.4852 -1499.3994  -999.3136  -499.2278     0.8580]
    """
    # Check that a vertical axis has been given
    if 'Z' not in axes:
        raise Exception("get_zlevels: Please provide a vertical coordinate.")

    # Build ordered dictionary mapping axes to coordinate variables.
    cvars  = archive.get_coordinate_system(location, axes)
    coords = DefaultOrderedDict()
    for dim_name in cvars:
        cvar = archive.get_variable(dim_name)
        coords[cvar.axis] = cvar

    position3d = op.axes_position_on_c_grid(coords)
    position2d = 't' if position3d == 'w' else position3d.replace('w', '')  # horizontal position on C grid
    v_staggered = 'w' in position3d

    z_axis = coords['Z']
    z_root = stdnames.get_root(z_axis.standard_name)
    z_name = name or "z_at_{0}".format(position3d)

    log.debug("# get_zlevels for " +
              " ".join(["{0}:{1}".format(k, v.position) for k, v in coords.iteritems()]) +
              " ({0} on {1})".format(z_root, position3d))

    if stdname is None:
        stdname = stdnames.get_positioned("depth", position3d)

    jpoint, ipoint = (0, 0) if (hpoint is None) else hpoint
    tpoint         =  0     if (tpoint is None) else tpoint

    if tpoint == -1:
        # Fix netcdf4-python bug with '-1' last index.
        tpoint = archive.ntimes-1

    # First check if 4D depth variable is not already present in archive.
    depth = _get_zlevels_from_depth_variable(archive, coords, z_name, position3d, tpoint, jpoint, ipoint)
    if depth is not None:
        return depth

    # Then try to recompute depth from formula...
    if z_root.startswith("ocean_") and ("coordinate" in z_root) and hasattr(z_axis, "formula_terms"):
        depth = _get_zlevels_from_formula(archive, coords, z_name, position2d, tpoint, jpoint, ipoint)

    # ...or from cell_thickness
    elif z_axis.positive == "down":
        depth = _get_zlevels_from_cell_thickness(archive, coords, z_name, position2d, v_staggered,
                                                 tpoint, jpoint, ipoint)

    # Finally, try to recompute from z_axis values
    if depth is None:
        depth = _get_zlevels_from_depth_axis(archive, coords, z_name, position2d)

    if depth is None:
        raise Exception("unable to get vertical levels from {0} for axes '{1}' at location '{2}'."
                        .format(archive, "".join(coords.keys()), position3d))

    # set 'standard_name' attribute
    depth.standard_name = stdname

    return depth


def _get_zlevels_from_cell_thickness( archive, axes, z_name, h_position, v_staggered, tpoint, jpoint, ipoint ):
    """
    Try to reconstruct depth from 'cell_thickness' values. The returned variable has the
    same axes as *axes*.
    """
    log.debug("# get_zlevels_from_cell_thickness : try from standard_name 'cell_thickness'.")
    try:
        var_dz = puv.get_variable_at_location(archive, "cell_thickness", h_position, True)
    except UnknownVariable:
        log.debug("_get_zlevels_from_cell_thickness failed: unable to find standard name "
                  "'cell_thickness' for location '{0}'".format(h_position))
        return None

    dimensions = [str(dim) for dim in axes.itervalues()]
    var_z = archive.new_variable(z_name, dimensions, model=var_dz)

    s_x  = np.index_exp[:] if ("X" in axes) else np.index_exp[ipoint]
    s_y  = np.index_exp[:] if ("Y" in axes) else np.index_exp[jpoint]
    s_z  = np.index_exp[:] if ("T" in axes and "T" in var_dz.axes)              \
                           else np.index_exp[tpoint] if ("T" in var_dz.axes)    \
                           else ()
    s_z += np.index_exp[:] + s_y + s_x
    log.debug("   s_z = {0}".format(s_z))

    dz = var_dz[s_z]

    # Pour HYCOM, nous avons ceci (cf. mail de Cyril LATHUILIERE):
    # > Pour ce qui est des profondeurs, on a bien z_{k+1} = z_k + (h_{k+1}+h_k)/2.
    # > Si on veut z{k+1/2}, le plus simple est de recalculer à partir du fond :
    # > z{k+1/2}= somme( i=k+1..kdm, h{i})
    if 'T' in var_z.axes:
        if v_staggered:
            var_z[:,  0] = 0.
            if "T" in var_dz.axes:
                var_z[:, 1:] = np.cumsum(dz, axis=1)[:, :-1]
            else:
                var_z[:, 1:] = np.cumsum(dz, axis=0)[:-1]
        else:
            if "T" in var_dz.axes:
                for k in range(archive.ntimes):
                    var_z[k,  0] = 0.5*dz[k, 0]
                    var_z[k, 1:] = 0.5*dz[k, 0] + np.cumsum(0.5*(dz[k, :-1]+dz[k, 1:]), axis=0)
            else:
                for k in range(archive.ntimes):
                    var_z[k,  0] = 0.5*dz[0]
                    var_z[k, 1:] = 0.5*dz[0] + np.cumsum(0.5*(dz[:-1]+dz[1:]), axis=0)
    else:
        if v_staggered:
            var_z[ 0] = 0.
            var_z[1:] = np.cumsum(dz, axis=0)[:-1]
        else:
            var_z[ 0] = 0.5*dz[0]
            var_z[1:] = 0.5*dz[0] + np.cumsum(0.5*(dz[:-1]+dz[1:]), axis=0)

    try:
        var_z.units = var_dz.axes["Z"].units
    except AttributeError:
        pass

    return var_z


def _get_zlevels_from_depth_variable( archive, axes, z_name, position, tpoint, jpoint, ipoint ):
    """
    TODO: Documentation of _get_zlevels_from_depth_variable.
    """
    try:
        z = puv.get_variable_from_stdnames(archive, ("depth", "depth_below_geoid"), position, interpolate=False)
    except UnknownVariable:
        return None

    if z.is_coordinate_variable():      # This case will be handled by '_get_zlevels_from_depth_axis'.
        return None

    if not set(axes).issubset(z.axes):  # We do not have all the required axes in 'z'.
        return None

    log.debug("# get_zlevels_from_depth_variable: found a depth variable: {0}".format(z))
    log.debug('    z.axes        : {0}'.format(["{0}: {1}".format(k, v) for k, v in z.axes.iteritems()]))
    log.debug('    required axes : {0}'.format(["{0}: {1}".format(k, v) for k, v in axes.iteritems()]))

    if set(z.axes.keys()) != set(axes.keys()):
        log.debug("# get_zlevels_from_depth_variable: create new depth variable: {0}".format(z_name))
        dims = [ str(axis) for axis in axes.itervalues() ]
        new_z = archive.new_variable(z_name, dims, model=z, copy_attrs=True)

        if "T" in z.axes:
            s_z  = np.index_exp[:] if "T" in axes else np.index_exp[tpoint]
        elif "T" not in axes:
            s_z  = ()
        else:
            raise Exception("Depth variable '{0}' has no time dimensions.".format(z))

        s_z += np.index_exp[:]
        s_z += np.index_exp[:] if "Y" in axes else np.index_exp[jpoint] if "Y" in z.axes else ()
        s_z += np.index_exp[:] if "X" in axes else np.index_exp[ipoint] if "X" in z.axes else ()
        log.debug("  - s_z = {0}".format(s_z))
        new_z[:] = z[s_z]
        try:
            new_z.units = z.units
            new_z.standard_name = "{0}_{1}".format(z.standard_name, "".join(axes.keys()))
        except AttributeError:
            pass
    else:
        new_z = z

    return new_z


def _get_zlevels_from_depth_axis( archive, axes, z_name, position ):
    """
    TODO: Documentation of _get_zlevels_from_depth_axis.
    """
    z_axis = axes['Z']

    if ((not hasattr(z_axis, 'standard_name')) or
        (not ( z_axis.standard_name.startswith('model_level_number')
         or z_axis.standard_name.startswith('depth')))):
        return None

    dims   = [ str(axis) for axis in axes.itervalues() ]

    log.debug("# get_zlevels_from_depth_axis: try Z axis values from '{0}'".format(z_axis))

    z = archive.new_variable(z_name, dims, dtype=z_axis.dtype)

    s_z  = (np.newaxis,) if "T" in axes else np.index_exp[...]
    s_z += np.index_exp[...]
    s_z += (np.newaxis,) if "Y" in axes else ()
    s_z += (np.newaxis,) if "X" in axes else ()

    z_tmp = z_axis[:]   # avoid NetCDF4-python limitations on fancy slicing
    z[:] = z_tmp[s_z]
    try:
        z.units = z_axis.units
    except AttributeError:
        pass

    return z


def _get_zlevels_from_formula( archive, axes, z_name, position, tpoint, jpoint, ipoint ):
    """
    TODO: Documentation of _get_zlevels_from_formula.

    - axes : ordered dictionary of coordinate variables.
    """
    z_axis = axes['Z']
    root_stdname = stdnames.get_root(z_axis.standard_name)

    log.debug("# get_zlevels_from_formula: '{0}' at position '{1}'".format(root_stdname, position))

    # get the variables for the formula
    vars = {}
    terms = parse_formula_terms(z_axis.formula_terms)

    for term in terms:
        vars[term] = archive.get_variable(terms[term])

    # build z from formula
    formula = get_formula(root_stdname)
    z = formula.zfunc(archive, axes, z_name, position, tpoint, jpoint, ipoint, **vars)
    z.units = "m"

    return z


def vinterp( var, depth, name, z=None ):
    """
    This function interpolate a 4D variable on a horizontal level of constant depth.

    :param var: 4D variable to slice.
    :param depth: slice depth (scalar; meters, negative).
    :param name: name for the new variable
    :param z: 4D depth array (m).

    Returns an horizontal slice.
    """
    archive = var.archive

    # If it has not been provided, recompute z array.
    z = z or var.get_zlevels()

    # Find the grid position of the nearest vertical levels
    if z.axes["Z"].positive == "down":
        zdata   = -z[:, ::-1]
        vardata = var[:, ::-1]
    else:
        zdata   = z[:]
        vardata = var[:]

    levs = np.sum(zdata <= depth, 1)-1

    # Mask levels that are above ssh
    levs[levs == zdata.shape[1]-1] = -1

    # Create a new horizontal variable
    dims = archive.get_coordinate_system(var.position, axes="TYX", mock=var)
    hvar = archive.new_variable(name, dims, dtype=var.dtype)

    z1 = np.ma.masked_all(levs.shape)
    z2 = np.ma.masked_all(levs.shape)
    v1 = np.ma.masked_all(levs.shape)
    v2 = np.ma.masked_all(levs.shape)

    ny = levs.shape[-2]
    nx = levs.shape[-1]

    for n in range(levs.shape[0]):
        for j in range(ny):
            for i in range(nx):
                k1 = levs[n, j, i]
                k2 = k1+1
                if k1 == -1 or k1 is np.ma.masked:
                    continue
                v1[n, j, i] = vardata[n, k1, j, i]
                v2[n, j, i] = vardata[n, k2, j, i]
                z1[n, j, i] = zdata[n, k1, j, i]
                z2[n, j, i] = zdata[n, k2, j, i]

    with np.errstate(all='ignore'):
        hvar[:] = v1 - (v2-v1)*(z1-depth)/(z2-z1)

    return hvar
