#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
cimport numpy as np

cimport cython

@cython.boundscheck(False)  # turn off bounds-checking for entire function
@cython.wraparound(False)
@cython.nonecheck(False)
def compute_z_iso(
        np.float64_t[::1]       rho_iso,
        np.float64_t[:, :, ::1] sigma,
        np.float64_t[:, :, ::1] zt,
        np.float32_t[:, ::1]    sfd,
        np.float32_t[:, ::1]    ssh ):

    cdef int nlev = len(rho_iso)     # Number of iso-sigma layers
    cdef int nz   = sigma.shape[0]   # Model grid dimensions
    cdef int ny   = sigma.shape[1]   # Model grid dimensions
    cdef int nx   = sigma.shape[2]   # Model grid dimensions

    # depth of layers interfaces
    cdef np.ndarray[np.float64_t, ndim=3] z_int = np.empty([nlev+1, ny, nx], dtype=np.float64)
    cdef np.ndarray[np.uint8_t,   ndim=1] smask = np.empty([nz],             dtype=np.uint8)
    cdef np.float64_t sig_crit
    cdef np.float64_t sig_kp0, sig_kp1
    cdef np.float64_t zt_kp0,  zt_kp1, _zint
    cdef unsigned int i, j, k
    cdef unsigned int ind

    for j in range(ny):
        for i in range(nx):
            z_int[0,    j, i] = -ssh[j, i]
            z_int[nlev, j, i] =  sfd[j, i]

    for k in range(nlev-1):

        sig_crit = 0.5 * ( rho_iso[k] + rho_iso[k+1] )

        for j in range(ny):
            for i in range(nx):

                # Find layer whose 't' point is upper than the isopycnal interface
                for kk in range(nz):
                    smask[kk] = sigma[kk, j, i] <= sig_crit
                ind = max(np.argmin(smask)-1, 0)    # cap with surface
                if np.all(smask):                   # cap with sea floor depth
                    ind = nz-2

                sig_kp0 = sigma[ind,   j, i]
                sig_kp1 = sigma[ind+1, j, i]
                zt_kp0 = zt[ind,   j, i]
                zt_kp1 = zt[ind+1, j, i]

                # Interpolate zt at layer interfaces
                if sig_kp1-sig_kp0 > 1e-15:
                    _zint = ( (sig_kp1-sig_crit) * zt_kp0 + (sig_crit-sig_kp0) * zt_kp1 ) / (sig_kp1-sig_kp0)
                else:
                    _zint = -9999.
                _zint = max(_zint, -ssh[j, i])     # cap with surface
                _zint = min(_zint,  sfd[j, i])     # cap with sea floor depth

                z_int[k+1, j, i] = _zint

    return z_int

@cython.boundscheck(False)  # turn off bounds-checking for entire function
@cython.wraparound(False)
@cython.nonecheck(False)
def compute_pv(
        np.float64_t[:, :, ::1] z_int_f,
        np.float64_t[:, :, ::1] zf,
        np.float32_t[:, :, ::1] totvort_z ):

    cdef int nlev = z_int_f.shape[0]-1  # Number of iso-sigma layers
    cdef int nz   = zf.shape[0]         # Model grid dimensions
    cdef int ny   = zf.shape[1]         # Model grid dimensions
    cdef int nx   = zf.shape[2]         # Model grid dimensions

    # depth of layers interfaces
    cdef np.ndarray[np.float64_t, ndim=3] tvor = np.zeros([nlev, ny, nx], dtype=np.float64)
    cdef np.ndarray[np.uint8_t,   ndim=1] smask = np.empty([nz],             dtype=np.uint8)
    cdef np.float64_t rup,  rdn
    cdef np.float64_t rkp0, rkp1
    cdef unsigned int i, j, k, kk

    # We compute the mean vorticity between z_int_f(kk) and z_int_f(kk+1)
    for kk in range(nlev):
        for k in range(nz-1):
            for j in range(ny):
                for i in range(nx):

                    if k == 0:
                        rup = max(z_int_f[0, j, i], z_int_f[kk, j, i])
                    else:
                        rup = max(zf[k, j, i], z_int_f[kk, j, i])

                    if k == nz-1:
                        rdn = min(z_int_f[nlev, j, i], z_int_f[kk+1, j, i])
                    else:
                        rdn = min(zf[k+1,j,i],         z_int_f[kk+1, j, i])

                    rkp0 = zf[k,   j, i]
                    rkp1 = zf[k+1, j, i]

                    if rdn > rup:
                        tvor[kk, j, i] += 0.5*(rdn-rup)/(rkp1-rkp0) * \
                                              ( (rup+rdn-2.*rkp0)*totvort_z[k+1, j, i] -
                                                (rup+rdn-2.*rkp1)*totvort_z[k,   j, i] )

    for kk in range(nlev):
        for j in range(ny):
            for i in range(nx):
                tvor[kk, j, i] /= (z_int_f[kk+1, j, i]-z_int_f[kk, j, i])**2

    return tvor
