#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This module provides some functions that can be useful when one wants to deal with plots.

>>> import pycomodo.util.plots as pup
"""

import numpy as np
import math

__all__ = ["ceil_to_digit", "floor_to_digit", "enlarge_range", "ticks_from_range"]


def ceil_to_digit(value, ndigits):
    """
    Rounds off *value* upwards up to *ndigits* digits.

    >>> pup.ceil_to_digit(1.234567,4)
    1.2346
    >>> pup.ceil_to_digit(1.234567,2)
    1.24
    >>> pup.ceil_to_digit(1.234567,0)
    2.0
    """
    r_factor = 10 ** ndigits
    return math.ceil(value * r_factor) / r_factor


def floor_to_digit(value, ndigits):
    """
    Rounds off *value* downward up to *ndigits* digits.

    >>> pup.floor_to_digit(1.234567,4)
    1.2345
    >>> pup.floor_to_digit(1.234567,2)
    1.23
    >>> pup.floor_to_digit(1.234567,0)
    1.0
    """
    r_factor = 10 ** ndigits
    return math.floor(value * r_factor) / r_factor


def enlarge_range(range_in, stretch_pct=0, digits=None):
    """
    Return an object of type `numpy.ndarray <http://docs.scipy.org/doc/numpy/reference/arrays.ndarray.html>`_
    derived from *range_in*.

    :param range_in: sorted real-valued one-dimensional array (or list).
    :param stretch_pct: the range is stretched by this percentage.
    :param digits: number of decimals for the output.

    >>> print pup.enlarge_range([-1.562, 0.675], digits=2) # doctest: +NORMALIZE_WHITESPACE
    [-1.57  0.68]

    >>> print pup.enlarge_range([0., 1.], stretch_pct=20)  # doctest: +NORMALIZE_WHITESPACE
    [-0.1  1.1]

    >>> print pup.enlarge_range([-0.721, 0., 1.432], stretch_pct=10, digits=2) # doctest: +NORMALIZE_WHITESPACE
    [-0.83  0.  1.54]
    """
    range_out = np.copy(np.asarray(range_in))
    r_min = range_out[0]
    r_max = range_out[-1]
    r_range = r_max - r_min

    # Enlarge range by stretch_pct %
    r_min = r_min - r_range * float(stretch_pct)/200.
    r_max = r_max + r_range * float(stretch_pct)/200.

    # If digits is given, then clip r_min and r_max values to the desired accuracy
    if digits is not None:
        r_min = floor_to_digit(r_min, digits )
        r_max = ceil_to_digit( r_max, digits )
        range_out = np.round(range_out, digits)

    range_out[0]  = r_min
    range_out[-1] = r_max

    return range_out


def ticks_from_range(bounds, nticks=3, digits=None):
    """
    Returns a list of *nticks* equally spaced values bounded by *bounds*.

    :param bounds: initial range
    :param nticks: required number of ticks, including bounds.
    :param digits: number of decimals for the output.

    >>> pup.ticks_from_range([1, 2])
    array([ 1. ,  1.5,  2. ])

    >>> pup.ticks_from_range([1, 1.5, 2], nticks=4)             # doctest: +NORMALIZE_WHITESPACE
    array([ 1. ,  1.33333333,  1.66666667,  2. ])

    >>> pup.ticks_from_range([1, 1.5, 2], nticks=4, digits=2)   # doctest: +NORMALIZE_WHITESPACE
    array([ 1. ,  1.33,  1.67,  2.  ])
    """
    r_min = float(bounds[0])
    r_max = float(bounds[-1])
    r_dx = (r_max - r_min) / (nticks-1)

    if digits is None:
        new_range = [ r_min + i*r_dx for i in range(nticks) ]
    else:
        new_range = [ round(r_min + i*r_dx, digits) for i in range(nticks) ]

    return np.array(new_range)


def choose(index, choices):
    """
    Pure-python implementation of numpy.choose, which has a bug when the
    size of *index* is greater than 32.
    Cf. `Numpy discussion <http://mail.scipy.org/pipermail/numpy-discussion/2011-June/056881.html>`_.
    """
    igrd = np.mgrid[[np.s_[:k] for k in index.shape]]
    cindices = [index] + [igrd[k] for k in range(igrd.shape[0])]
    return choices[cindices]


def _test():

    initial_range = ( np.random.rand(4) - np.random.rand(4) ) * 10
    initial_range.sort()

    print " Initial range    = %s" % str(initial_range)

    enlarged_range = enlarge_range(initial_range, stretch_pct=10)
    print " Enlarged range   = %s" % str(enlarged_range)
    print " Ticks from range = %s" % str(ticks_from_range( enlarged_range, nticks=5 ))

    enlarged_range = enlarge_range(initial_range, digits=3, stretch_pct=10)
    print " Enlarged range   = %s" % str(enlarged_range)
    print " Ticks from range = %s" % str(ticks_from_range( enlarged_range, nticks=5, digits=3 ))


if __name__ == "__main__":

    _test()
