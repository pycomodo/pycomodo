#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Defines various functions to fetch common variables or ease interpolation on standard names.

>>> import pycomodo
>>> import pycomodo.util.variables as puv
>>> archive = pycomodo.Archive('examples/nc_files/roms_vortex.nc')
"""
import numpy as np
import pycomodo.operators as op
import pycomodo.util.stdnames as pus
from pycomodo.core.exceptions import UnknownVariable
from pycomodo.io.customlogger import log
from pycomodo.compat import OrderedDict
from pycomodo.util.plots import choose


def _interpolate_stdname_at_t_location(archive, stdname):
    """
    Returns a variable defined on 't' points whose attribute ``standard_name`` is *stdname*.

    #. First try to interpolate *stdname* from 'u' to 't'.
    #. Otherwise try to interpolate *stdname* from 'v' to 't'.
    #. Otherwise give up.
    """
    std_dict = archive.standard_names

    # If 'stdname' is present at 'u' location, interpolate at 't' location
    stdname_at_u = pus.get_positioned(stdname, 'u')
    if (stdname_at_u in std_dict):
        var_at_t = op.interpolate(std_dict[stdname_at_u], 't')
        var_at_t.standard_name = stdname
        return var_at_t

    # If 'stdname' is present at 'v' location, interpolate at 't' location
    stdname_at_v = pus.get_positioned(stdname, 'v')
    if (stdname_at_v in std_dict):
        var_at_t = op.interpolate(std_dict[stdname_at_v], 't')
        var_at_t.standard_name = stdname
        return var_at_t

    # Otherwise, we give up.
    raise UnknownVariable(stdname)


def _interpolate_stdname_at_u_location(archive, stdname):
    """
    Returns a variable defined on 'u' points whose attribute ``standard_name`` is *stdname*.

    #. First try to interpolate *stdname* from 't' to 'u'.
    #. Otherwise try to interpolate *stdname* from 'f' to 'u'.
    #. Otherwise give up.
    """
    std_dict = archive.standard_names

    stdname_at_u = pus.get_positioned(stdname, 'u')
    stdname_at_f = pus.get_positioned(stdname, 'f')

    # If 'stdname' is present at 't' location, interpolate at 'u' location
    if (stdname in std_dict):
        var_at_u = op.interpolate(std_dict[stdname], 'u')
        var_at_u.standard_name = stdname_at_u
        return var_at_u

    # If 'stdname' is present at 'f' location, interpolate at 'u' location
    if (stdname_at_f in std_dict):
        var_at_u = op.interpolate(std_dict[stdname_at_f], 'u')
        var_at_u.standard_name = stdname_at_u
        return var_at_u

    # Otherwise, we give up.
    raise UnknownVariable(stdname)


def _interpolate_stdname_at_v_location(archive, stdname):
    """
    Returns a variable defined on 'v' points whose attribute ``standard_name`` is *stdname*.

    #. First try to interpolate *stdname* from 't' to 'v'.
    #. Otherwise try to interpolate *stdname* from 'f' to 'v'.
    #. Otherwise give up.
    """
    std_dict = archive.standard_names

    stdname_at_v = pus.get_positioned(stdname, 'v')
    stdname_at_f = pus.get_positioned(stdname, 'f')

    # If 'stdname' is present at 't' location, interpolate at 'v' location
    if (stdname in std_dict):
        var_at_v = op.interpolate(std_dict[stdname], 'v')
        var_at_v.standard_name = stdname_at_v
        return var_at_v

    # If 'stdname' is present at 'f' location, interpolate at 'v' location
    if (stdname_at_f in std_dict):
        var_at_v = op.interpolate(std_dict[stdname_at_f], 'v')
        var_at_v.standard_name = stdname_at_v
        return var_at_v

    # Otherwise, we give up.
    raise UnknownVariable(stdname)


def _interpolate_stdname_at_f_location(archive, stdname):
    """
    Returns a variable defined on 'f' points whose attribute ``standard_name`` is *stdname*.

    #. First try to interpolate *stdname* from 'u' and 'v' to 'f' and take the mean.
    #. Otherwise try to interpolate *stdname* from 'u' to 'f'.
    #. Otherwise try to interpolate *stdname* from 'v' to 'f'.
    #. Otherwise try to interpolate *stdname* from 't' to 'f' (it will actually do 't' -> 'u' -> 'f').
    #. Otherwise give up.
    """
    std_dict = archive.standard_names

    # If 'stdname' is present at 'u' and 'v' locations, interpolate each one
    # at 'f' location and return the mean
    stdname_at_u = pus.get_positioned(stdname, 'u')
    stdname_at_v = pus.get_positioned(stdname, 'v')
    stdname_at_f = pus.get_positioned(stdname, 'f')

    if ( (stdname_at_u in std_dict) and (stdname_at_v in std_dict) ):
        var_at_u = std_dict[stdname_at_u]
        var_at_v = std_dict[stdname_at_v]
        varu_at_f = op.interpolate(var_at_u, 'f')
        varv_at_f = op.interpolate(var_at_v, 'f')
        var_at_f = (varu_at_f + varv_at_f) / 2.
        fill_at_u = var_at_u.get_fill_value()
        fill_at_v = var_at_v.get_fill_value()
        if (fill_at_u is not None) and (fill_at_v is not None):
            var_at_f._FillValue = (fill_at_u + fill_at_v) / 2.
        var_at_f.standard_name = stdname_at_f
        return var_at_f

    if (stdname_at_u in std_dict):
        var_at_f = op.interpolate(std_dict[stdname_at_u], 'f')
        var_at_f.standard_name = stdname_at_f
        return var_at_f

    if (stdname_at_v in std_dict):
        var_at_f = op.interpolate(std_dict[stdname_at_v], 'f')
        var_at_f.standard_name = stdname_at_f
        return var_at_f

    # If 'stdname' is present at 't' location, interpolate at 'f' location
    if (stdname in std_dict):
        var_at_f = op.interpolate(std_dict[stdname], 'f')
        var_at_f.standard_name = stdname_at_f
        return var_at_f

    # Otherwise, we give up.
    raise UnknownVariable(stdname)


def _interpolate_stdname_at_w_location(archive, stdname):
    """
    Returns a variable defined on 'w' points whose attribute ``standard_name`` is *stdname*.

    #. Try to interpolate *stdname* from 't' and 'w'.
    #. Otherwise give up.
    """
    std_dict = archive.standard_names

    # If 'stdname' is present at 't' location, interpolate at 'w' location
    if (stdname in std_dict):
        return op.interpolate(std_dict[stdname], 'w')

    # Otherwise, we give up.
    raise UnknownVariable(stdname)


def get_variable_at_location(archive, stdname, location, interpolate=False):
    """
    Returns a variable from *archive* whose attribute ``standard_name`` is *stdname*.
    It will be defined on points specified by *location*.

    :param archive: instance of :class:`.Archive` where to look for the variable.
    :param stdname: should be the short version of the standard_name
                    (*ie.* without ``at_[uvfw]_location``).
    :param location: can be one of 't', 'u', 'v', 'f', 'w', 'uw', 'vw' or 'fw'.
    :param interpolate: if True, try to interpolate from a variable with the same
                        ``standard_name`` root to fulfill *location* requirement.

    .. code-block:: python

        >>> ut = puv.get_variable_at_location(archive, 'sea_water_x_velocity', 't')
        pycomodo.core.exceptions.UnknownVariable: 'sea_water_x_velocity'

        >>> # Sea water velocity only exists on 'u' points. We should try interpolation.
        >>> ut = puv.get_variable_at_location(archive, 'sea_water_x_velocity', 't', interpolate=True)
        >>> print ut.dimensions, ut.position, ut.standard_name
        ['time', 's_rho', 'eta_rho', 'xi_rho'] t sea_water_x_velocity

    .. code-block:: python

        >>> corio_f = puv.get_variable_at_location(archive, 'coriolis_parameter', 'f', interpolate=True)
        >>> print corio_f.dimensions, corio_f.position
        ['eta_v', 'xi_u'] f
    """
    loc = location.lower()
    var = None

    std_dict = archive.standard_names

    # If 'stdname' is present at requested location, just return this variable
    stdname_at_loc = pus.get_positioned(stdname, location)
    if (stdname_at_loc in std_dict):
        return std_dict[stdname_at_loc]

    if not interpolate:
        raise UnknownVariable(stdname_at_loc)

    if loc == 't':
        var = _interpolate_stdname_at_t_location(archive, stdname)
    elif loc == 'u':
        var = _interpolate_stdname_at_u_location(archive, stdname)
    elif loc == 'v':
        var = _interpolate_stdname_at_v_location(archive, stdname)
    elif loc == 'f':
        var = _interpolate_stdname_at_f_location(archive, stdname)
    elif loc == 'w':
        var = _interpolate_stdname_at_w_location(archive, stdname)
    elif loc in ('uw', 'vw', 'fw'):
        tmpvar = get_variable_at_location(archive, stdname, loc[0], interpolate)
        var = op.interpolate(tmpvar, loc)

    if var is None:
        raise NotImplementedError("# get_variable_at_location: location {0}".format(location))

    log.debug("# get_variable_at_location: {0} at '{1}' (interpolate:{2}) : "
              .format(stdname, location, interpolate) +
              "\n\t   -> found variable '{0}' ({1}).".format(var, var.standard_name))

    return var


def get_variable_from_stdnames(archive, stdnames, location=None, interpolate=True):
    """
    Returns the first variable in *archive* whose attribute ``standard_name`` is in the list
    *stdnames* and is defined on the grid specified by *location*.

    :param archive: instance of :class:`.Archive` where to look for the variable.
    :param stdnames: list of the ``standard_name`` attribute candidates.
    :param location: can be one of 't', 'u', 'v', 'f', 'w', 'uw', 'vw' or 'fw'.
    :param interpolate: if True, try to interpolate from a variable with the same
                        ``standard_name`` root to fulfill *location* requirement.

    The routine looks for all candidates in *stdname*, as well as possible aliases. In the following
    example, the ``standard_name`` attribute of the winner is not part of the requested list:

    >>> sea_floor = puv.get_variable_from_stdnames(archive, ('sea_floor_depth_below_geoid', 'sea_floor_depth'))
    >>> sea_floor.standard_name
    u'model_sea_floor_depth_below_geoid'
    """
    if isinstance(stdnames, str):
        stdnames = [stdnames]

    if location is not None:
        location = location.lower()

    log.debug("# get_variable_from_stdnames: {0} at '{1}' (interpolate: {2})"
              .format(stdnames, location, interpolate))

    keys = archive.standard_names.keys()

    for searched_stdname in stdnames:

        searched_atpoint = location or pus.get_at_location(searched_stdname)
        searched_stdroot = pus.get_root(searched_stdname)
        try:
            searched_canroot = archive.checker.get_canonical_std_name(searched_stdroot)
        except UnknownVariable:
            searched_canroot = searched_stdroot

        searched_stdatloc = pus.get_positioned(searched_stdroot, searched_atpoint)
        searched_canatloc = pus.get_positioned(searched_canroot, searched_atpoint)

        if searched_stdatloc in keys:
            return archive.standard_names[searched_stdatloc]

        if searched_canatloc in keys:
            return archive.standard_names[searched_canatloc]

        if interpolate:
            # Find a variable to interpolate. Its 'standard_name' attribute must share
            # the same root as 'searched_stdname'.
            stdroot = None

            if searched_canroot in keys:
                stdroot = searched_canroot

            if searched_stdroot in keys:
                stdroot = searched_stdroot

            if stdroot is None:
                for stdname in keys:
                    _stdroot = pus.get_root(stdname)
                    if _stdroot == searched_stdroot:
                        stdroot = _stdroot
                        break
                    if _stdroot == searched_canroot:
                        stdroot = _stdroot
                        break

            if stdroot is not None:
                return get_variable_at_location(archive, stdroot, searched_atpoint, interpolate)

    raise UnknownVariable(stdnames)


def get_ssh(archive):
    """
    Returns the variable defining sea surface elevation from *archive*.

    :param archive: instance of :class:`.Archive` where to look for the variable.
    """
    ssh_varnames = ('sea_surface_elevation',
                    'sea_surface_height')

    return get_variable_from_stdnames(archive, ssh_varnames)


def get_sea_floor_depth(archive, location=None):
    """
    Returns the variable defining sea floor depth from *archive*.

    :param archive: instance of :class:`.Archive` where to look for the variable.
    """
    depth_varnames = ('model_sea_floor_depth_below_geoid', 'sea_floor_depth_below_geoid', 'sea_floor_depth')

    try:
        sfd = get_variable_from_stdnames(archive, depth_varnames, location)
    except UnknownVariable:
        sfd = None

    if sfd is None:     # Not found. Try to recompute it from depth
        log.warning("get_sea_floor_depth: Unable to find a suitable variable. "
                    "We'll try to recompute sea floor depth from 'depth' variable.")

        location = 't' if (location is None) else location
        depth = archive.get_zlevels(location, "ZYX")
        var_name = pus.get_positioned('sea_floor_depth', location)
        var_dims = depth.dimensions[1:]
        var_attrs = { "standard_name": var_name, "units": depth.units }

        sfd = archive.new_variable(var_name, var_dims, dtype=depth.dtype, attrs=var_attrs)

        if depth.axes["Z"].positive == "down":
            # NEMO-like: Sea floor depth is depth at level number 'model_level_number_at_sea_floor'.
            mlnasf = get_variable_from_stdnames(archive, 'model_level_number_at_sea_floor', location)
            sfd[:] = choose(mlnasf[:]-1, depth)
        else:
            # Sea floor depth is the first level of 3D depth.
            sfd[:] = depth[0]

    return sfd


def get_vorticity_2d_roms(ubar, vbar, name=None, stdname=None, dtype=None):
    """
    Returns the 2d vorticity computed from barotropic velocities *ubar* and *vbar* using ROMS formula.
    Will not work with model outputs from other models.
    """
    archive = ubar.archive
    name = name or "curl2d"
    pm   = archive.get_variable('pm')
    pn   = archive.get_variable('pn')
    mnof = op.interpolate(pm*pn, "F")
    pmou = pm.interpolate("U", method='harmonic')
    pnov = pn.interpolate("V", method='harmonic')

    uom = op.operation("u/pm", u=ubar, pm=pmou)
    von = op.operation("v/pn", v=vbar, pn=pnov)

    dvon_dx = von.diff_x(dummy=True)
    duom_dy = uom.diff_y(dummy=True)

    vort = op.operation("mnof*(dvdx-dudy)", name, stdname, dtype, mnof=mnof, dvdx=dvon_dx, dudy=duom_dy)

    return vort


def get_vorticity_2d(ubar, vbar, name=None, stdname=None, dtype=None):
    """
    Returns the 2d vorticity computed from barotropic velocities *ubar* and *vbar*.
    """
    name = name or "curl2d"
    dvdx = vbar.diff_x()
    dudy = ubar.diff_y()
    curl = op.operation("dvdx-dudy", name, stdname, dtype, dvdx=dvdx, dudy=dudy)

    return curl


def _build_coriolis_from_lat(var_lat, corio_name):
    """
    Creates and returns a Coriolis parameter field computed from latitude variable *var_lat*.
    The new variable is named *corio_name*.
    """
    cst_omega   = 7.292115083046061e-5    # earth rotation rate (s-1)
    cst_deg2rad = np.pi / 180.

    archive = var_lat.archive
    corio = archive.new_variable(corio_name, model=var_lat)
    corio[:] = 2.*cst_omega*np.sin(var_lat[:]*cst_deg2rad)

    return corio


def get_coriolis(archive, name=None, interpolate=True):
    """
    Returns the Coriolis parameter on an 'f' grid. This may be the result of an interpolation.
    """
    log.debug("# get_coriolis")

    if name is None:
        name = 'coriolis_at_f'

    try:
        corio_at_f = get_variable_at_location(archive, 'coriolis_parameter', 'f', interpolate)
    except UnknownVariable:
        # We were unable to find or interpolate Coriolis parameter from the archive,
        # so we try to compute it from latitude.
        try:
            lat_at_f = get_variable_at_location(archive, 'latitude', 'f', interpolate)
        except UnknownVariable:
            raise UnknownVariable("either coriolis or latitude")
        else:
            corio_at_f = _build_coriolis_from_lat(lat_at_f, name)

    if name is not None:
        corio_at_f.set_name(name)

    if corio_at_f.is_writeable():
        corio_at_f.standard_name = "coriolis_parameter_at_f_location"

    return corio_at_f


def _compute_potential_density_from_linear_eos(temp, salt, cst_alpha, cst_beta, cst_rho0):
    """
    Returns a potential density variable computed using a linear equation of state from
    from potential temperature *temp* and salinity *salt*.
    """
    if salt is None:
        log.debug("_compute_potential_density_from_linear_eos: rho = rho0 - alpha*T")
        rho = op.operation("rho0-alpha*temp", name="rho", stdname="sea_water_potential_density",
                           rho0=cst_rho0, alpha=cst_alpha, temp=temp )
    else:
        log.debug("_compute_potential_density_from_linear_eos: rho = beta*S - alpha*T")
        rho = op.operation("beta*salt-alpha*temp", name="rho", stdname="sea_water_potential_density",
                           alpha=cst_alpha, beta=cst_beta, temp=temp, salt=salt )

    if rho is not None:
        rho._FillValue = 0.
        rho.units = "kg m-3"

    return rho


def get_potential_density(archive, eos='linear', cst_alpha=2e-1, cst_beta=7.7e-1, cst_rho0=1024.):
    """
    Returns potential density variable from *archive*.

    In no corresponding variable is found, the routine tries to compute potential density
    using an EOS from potential temperature and salinity if available.

    :param eos: type of Equation of State to use as a fallback. Only 'linear' currently.

    :WARNING: it looks for potential density AND sigma-theta. Check the standard_name attribute
             of the result.
    :TODO: check if potential temperature is in C or K.
    :TODO: implement other EOS formulas.
    """
    try:
        rho = get_variable_from_stdnames(archive, ('sea_water_potential_density', 'sea_water_sigma_theta',
                                                   'sea_water_sigma_t'))
        log.debug("get_potential_density: found variable '{0}' ({1}) in archive.".format(rho, rho.standard_name))

        return rho

    except UnknownVariable:
        rho = None
        log.debug("get_potential_density: potential density not found. We'll compute it from T and S.")

    if eos != 'linear':
        raise NotImplementedError("get_potential_density: onyl 'linear' EOS is currently "
                                  "implemented. Sorry.")

    # Get potential temperature for EOS.
    try:
        temp = archive.get_variable(stdname='sea_water_potential_temperature')
    except UnknownVariable:
        log.error("get_potential_density: unable to find potential temperature to compute potential density.")
        raise

    # Get salinity for EOS.
    try:
        salt = archive.get_variable(stdname='sea_water_salinity')
    except UnknownVariable:
        salt = None
        log.warning("get_potential_density: unable to find salinity to compute potential density.")

    return _compute_potential_density_from_linear_eos(temp, salt, cst_alpha, cst_beta, cst_rho0)


def get_bn2(var_rho, cst_g=9.81, cst_rho0=1000.):
    """
    Returns Brunt–Väisälä frequency from potential density (or sigma-theta) variable 'var_rho'.
    """
    up = 1 if var_rho.axes['Z'].positive == "up" else -1
    dzrho = var_rho.diff_z()

    var_bn2 = op.operation("-up*(g/rho0)*dzrho", name="bn2", stdname="brunt_vaisala_frequency",
                           g=cst_g, rho0=cst_rho0, dzrho=dzrho, up=up, dtype=np.float )
    var_bn2._FillValue = -1.

    return var_bn2
