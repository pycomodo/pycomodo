#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This module defines the :obj:`log <.customlogger.log>` object. It is the main logging facility for the library, as an
instance of the :class:`LogWrapper` class.
"""

import sys
import re
import logging

from pycomodo.io.termcolor import colored


class _RewritableHandler:
    """
    Interface that rewrite logging messages according to their actual level.
    """
    @property
    def istty(self):
        """
        Returns True if the current object is of console type.
        """
        # Beware, there is some magic in the following lines.
        isatty = getattr(self.stream, 'isatty', None)
        return isatty and isatty()

    def rewrite_msg(self, record):
        """
        Rewrite the record with a colored label according to the message level.
        """
        if record.levelno in (logging.ERROR, logging.CRITICAL):
            record.msg = "{0} {1}".format(colored('[{0}]:'.format(record.levelname), 'red'), record.msg)
        elif record.levelno in (logging.WARNING, logging.DEBUG):
            record.msg = "{0} {1}".format(colored('[{0}]:'.format(record.levelname), 'yellow'), record.msg)

        if not self.istty:  # Remove all colors
            record.msg = re.sub(r"\033\[[0-9;]*m", "", str(record.msg))


class ColoredStreamHandler(logging.StreamHandler, _RewritableHandler):
    """
    A handler class which writes formatted logging records to data stream.
    """
    def format(self, record):
        self.rewrite_msg(record)
        return logging.StreamHandler.format(self, record)


class ColoredFileHandler(logging.FileHandler, _RewritableHandler):
    """
    A handler class which writes formatted logging records to disk files.
    """
    def format(self, record):
        self.rewrite_msg(record)
        return logging.FileHandler.format(self, record)


class LogWrapper(object):
    """
    Wrapper class for logging.Logger.
    Make it possible to call the instance directly ; in this case, the message is passed
    at level INFO.
    """
    def __init__(self, log):
        self.log = log

    def __getattr__(self, name):
        # If the user calls LogWrapper.info(), it is deferred to LogWrapper.log.info()
        try:
            return self.__getattribute__(name)
        except:
            return getattr(self.log, name)

    def __call__(self, message):
        self.log.info(message)


_MAIN_LOGGER = logging.getLogger("pycomodo")

if len(_MAIN_LOGGER.handlers) == 0:
    _CSH = ColoredStreamHandler(sys.stdout)
    _MAIN_LOGGER.setLevel(logging.INFO)
    _MAIN_LOGGER.addHandler(_CSH)

#: Default main Logger for pycomodo library. Instance of :class:`.LogWrapper`.
#:
#: >>> from pycomodo import log
#: >>> log("message")
#: message
#: >>> log.error("this is an error")
#: [ERROR]: this is an error
log = LogWrapper(_MAIN_LOGGER)


################################################################################
#
#  TEST
#
def _test_module():

    def _logit(logger, level=None):

        if level is not None:
            logger.setLevel(level)
        logger.critical("="*50)
        logger.critical("=  LEVEL = %s" % logging.getLevelName(logger.level))
        logger.critical("="*50)
        logger.debug("Ceci est un message de niveau 'debug'.")
        logger.info("Ceci est un message de niveau 'info'.")
        logger.warn("Ceci est un message de niveau 'warn'.")
        logger.error("Ceci est un message de niveau 'error'.")
        logger.fatal("Ceci est un message de niveau 'fatal'.")
        try:
            print 1/0.
        except ZeroDivisionError:
            logger.exception("Ceci est un message lancé lors d'une exception.")
            print ""

    _logit(log)
    _logit(log, logging.NOTSET)
    _logit(log, logging.ERROR)
    _logit(log, logging.INFO)
    _logit(log, logging.DEBUG)
    _logit(log, logging.CRITICAL)

if __name__ == "__main__":

    _test_module()
