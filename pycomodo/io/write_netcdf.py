#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Defines the function :func:`.write_netcdf` to write instances of :class:`.Variable` into
a new NetCDF file.
"""

from pycomodo.io.customlogger import log
from pycomodo.compat import OrderedDict
from pycomodo import cdf
from pycomodo.core.exceptions import UnknownVariable

from collections import Sequence, Iterable


def _write_var(ncfile, var):
    """
    Actually creates the variable in the file
    """
    # Declare new variable in output file
    ncvar = ncfile.createVariable(str(var), var.dtype, var.dimensions, fill_value=var.get_fill_value())

    # Copy the data
    var.set_auto_maskandscale(False)  # Bypass offset/scaling
    ncvar[:] = var[:]
    var.set_auto_maskandscale(True)

    # Copy the attributes in the same order as in the source
    # (_FillValue has a special treatment) by netCDF4 package.
    attrdict = OrderedDict()
    for attr in var.ncattrs():
        if attr != "_FillValue":
            attrdict[attr] = getattr(var, attr)
    ncvar.setncatts(attrdict)


def write_netcdf( filename, variables ):
    """
    Write a new NetCDF file with variables contained in the list *variables*.

    :param str filename: Name of the new NetCDF file.
    :param variables: instance or list of instances of :class:`.Variable`.

    >>> z = archive.get_zlevels()
    >>> pycomodo.write_netcdf("z.nc", z)

    You can check what was written in the new file (spoiler: the 'z' variable and its associated
    coordinate variables):

    >>> archive_z = pycomodo.Archive("z.nc")
    >>> print archive_z.info(1)
    Archive[ ('z.nc') ]:
     - Dimensions : time, s_rho, eta_rho, xi_rho
     - Variables  : time, s_rho, eta_rho, xi_rho, z_at_t
    """
    # Convert the variable(s) into a sequence
    if not isinstance(variables, Sequence):
        if isinstance(variables, Iterable):
            variables = list(variables)
        else:
            variables = [variables]

    if not variables:
        log.warning("write_netcdf: empty variable list. '{0}' is not written.".format(filename))
        return

    # Get output NetCDF format
    archive = variables[0].archive
    files = archive.file_list.values()
    if files:
        ncformat = files[0].file_format
    else:
        ncformat = 'NETCDF3_CLASSIC'

    # Create new file for writing
    ncfile = cdf.Dataset(filename, mode="w", format=ncformat)

    # Look for dimensions to write
    allvars = []
    dims = OrderedDict()
    for var in variables:
        for dimname in var.dimensions:
            if dimname not in dims:
                # Add the corresponding coordinate variables to the list of output variables.
                dims[dimname] = archive.dimensions[dimname]
                try:
                    allvars.append(archive.get_variable(dimname))
                except UnknownVariable:
                    log.debug("write_netcdf: variable '{0}' not found...".format(dimname))

    for var in variables:
        if var not in allvars:
            allvars.append(var)

    allvar_names = [str(x) for x in allvars]
    log.debug("# write_netcdf: writing {0}...".format(allvar_names))

    # Write dimensions
    for dim_name, dim in dims.iteritems():
        dimlen = None if dim.isunlimited() else len(dim)
        ncfile.createDimension(dim_name, dimlen)

    # Write variables
    for var in allvars:
        _write_var(ncfile, var)

    # We are done
    ncfile.close()
