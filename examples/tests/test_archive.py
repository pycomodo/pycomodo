#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import traceback

from optparse import OptionParser
from logging import DEBUG

import pycomodo
import pycomodo.util.variables as puv
from pycomodo import log, UnknownVariable

class GetCS_Test:
    def __init__(self, arch, location="t", axes="TZYX", mock=None, err=None):
        self.arch = arch
        self.location = location
        self.axes = axes
        self.mock = mock
        self.err  = err

    def test(self):
        if self.err is not None:
            log.error(self.err)
            log("")
            return

        log("= Calling arch.get_coordinate_system({0}, {1}, {2})".format(self.location, self.axes, self.mock))
        if self.mock is not None:
            self.mock.check(True)
        try:
            cs = self.arch.get_coordinate_system(self.location, self.axes, self.mock)
            log("   -> {0}".format(cs))
        except Exception as e:
            log.error("   -> FAILED: {0}".format(e))
            traceback.print_exc()
        finally:
            log("")

def main(arch):

    ts = []
    ts.append(GetCS_Test(arch, 't'))
    ts.append(GetCS_Test(arch, 'f', axes=['X', 'Y']))

    try:
        u = puv.get_variable_from_stdnames(arch, "sea_water_x_velocity_at_u_location")
    except UnknownVariable as e:
        ts.append(GetCS_Test(arch, err="unable to find {0}".format(e)))
    else:
        ts.append(GetCS_Test(arch, 'f', axes='XY', mock=u))

    ts.append(GetCS_Test(arch, 'u', axes='XYZ'))
    try:
        v = puv.get_variable_from_stdnames(arch, "sea_water_y_velocity", "V")
    except UnknownVariable as e:
        ts.append(GetCS_Test(arch, err="unable to find {0}".format(e)))
    else:
        ts.append(GetCS_Test(arch, 'v', axes='XYZ', mock=v))

    ts.append(GetCS_Test(arch, 'w'))
    ts.append(GetCS_Test(arch, 'uw'))
    ts.append(GetCS_Test(arch, 'vw'))
    ts.append(GetCS_Test(arch, 'fw'))

    for test in ts:
        test.test()

if __name__ == '__main__':

    usage = """Usage: %prog NETCDF_FILE\n  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage, version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    main(pycomodo.Archive(args))
