#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import traceback
import logging
from optparse import OptionParser

import pycomodo
import pycomodo.operators as op
import pycomodo.util.variables as puv

from pycomodo import log
from pycomodo.io.termcolor import bolded

class GetMetric_Test:
    def __init__(self, arch, direction, location, axes="TZYX"):
        self.arch = arch
        self.direction = direction
        self.location  = location
        self.axes      = axes

    def test(self):

        log(" - d{0}_{1} :".format(self.direction, self.location))

        try:
            metric = self.arch.get_metric(self.direction, self.location, self.axes)
            log(metric.info())
        except Exception as e:
            metric = None
            log.error("   -> FAILED: {0}".format(e))
            traceback.print_exc()

        return metric


def main(archive):

    var_metrics = []
    log(bolded("\nRegistered metrics so far :"))
    log("  {0}\n".format(archive.metrics.keys()))

    log(bolded("Get some horizontal metrics..."))

    dx_t = GetMetric_Test(archive, "x", "t").test()
    dx_u = GetMetric_Test(archive, "x", "u").test()
    dx_v = GetMetric_Test(archive, "x", "v").test()
    dx_f = GetMetric_Test(archive, "x", "f").test()
    dy_t = GetMetric_Test(archive, "y", "t").test()
    dy_u = GetMetric_Test(archive, "y", "u").test()
    dy_v = GetMetric_Test(archive, "y", "v").test()
    dy_f = GetMetric_Test(archive, "y", "f").test()

    if dx_t: var_metrics.append(dx_t)
    if dx_u: var_metrics.append(dx_u)
    if dx_v: var_metrics.append(dx_v)
    if dx_f: var_metrics.append(dx_f)
    if dy_t: var_metrics.append(dy_t)
    if dy_u: var_metrics.append(dy_u)
    if dy_v: var_metrics.append(dy_v)
    if dy_f: var_metrics.append(dy_f)

    log(bolded("\nRegistered metrics so far :"))
    log("  {0}\n".format(archive.metrics.keys()))

    log(bolded("Get standard vertical metrics..."))
    dz_t = GetMetric_Test(archive, "z", "t").test()
    dz_w = GetMetric_Test(archive, "z", "w").test()

    if dz_t: var_metrics.append(dz_t)
    if dz_w: var_metrics.append(dz_w)

    log(bolded("\nRegistered metrics so far :"))
    log("  {0}\n".format(archive.metrics.keys()))

    log(bolded("Get complex staggered metrics..."))
    dz_u  = GetMetric_Test(archive, "z", "u").test()
    dz_v  = GetMetric_Test(archive, "z", "v").test()
    dx_uw = GetMetric_Test(archive, "x", "uw").test()
    dx_vw = GetMetric_Test(archive, "x", "vw").test()
    dx_fw = GetMetric_Test(archive, "x", "fw").test()
    dy_uw = GetMetric_Test(archive, "y", "uw").test()
    dy_vw = GetMetric_Test(archive, "y", "vw").test()
    dy_fw = GetMetric_Test(archive, "y", "fw").test()
    dz_uw = GetMetric_Test(archive, "z", "uw").test()
    dz_vw = GetMetric_Test(archive, "z", "vw").test()
    dz_fw = GetMetric_Test(archive, "z", "fw").test()

    if dz_u:  var_metrics.append(dz_u)
    if dz_v:  var_metrics.append(dz_v)
    if dx_uw: var_metrics.append(dx_uw)
    if dx_vw: var_metrics.append(dx_vw)
    if dx_fw: var_metrics.append(dx_fw)
    if dy_uw: var_metrics.append(dy_uw)
    if dy_vw: var_metrics.append(dy_vw)
    if dy_fw: var_metrics.append(dy_fw)
    if dz_uw: var_metrics.append(dz_uw)
    if dz_vw: var_metrics.append(dz_vw)
    if dz_fw: var_metrics.append(dz_fw)

    log(bolded("\nRegistered metrics so far :"))
    log("  {0}\n".format(archive.metrics.keys()))

    log(bolded("Get vertical metrics on restricted axes..."))
    dz_t_ZX = GetMetric_Test(archive, "z", "t", "ZX").test()
    dz_t_ZY = GetMetric_Test(archive, "z", "t", "ZY").test()

    if dz_t_ZX:  var_metrics.append(dz_t_ZX)
    if dz_t_ZY:  var_metrics.append(dz_t_ZY)

    log(bolded("Compute z_w with get_zlevels from upward velocity..."))
    wz = puv.get_variable_at_location(archive, 'upward_sea_water_velocity', 'w', interpolate=True)
    zw  = wz.get_zlevels()

    log(bolded("Compute dz_t from z_w..."))
    dz_t1 = zw.diff_z(dummy=True)
    log(dz_t1.info())

    log(bolded("Writing 'metrics.nc'..."))
    print [str(v) for v in var_metrics]
    pycomodo.write_netcdf("metrics.nc", var_metrics)

if __name__ == '__main__':

    usage = """Usage: %prog NETCDF_FILE\n  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage, version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    main(pycomodo.Archive(args))
