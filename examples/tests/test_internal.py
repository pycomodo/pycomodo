#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import logging
from optparse import OptionParser

import pycomodo
import pycomodo.util.variables as puv
import pycomodo.util.vertical  as puz
import pycomodo.util.internal  as pui
import pycomodo.util.plots     as pup
import pycomodo.operators as op

from pycomodo import log

import numpy as np

import matplotlib.pyplot as plt

cst_rho0 = 1000

def main( fic_in, t0, j0, i0 ):

    archive = pycomodo.Archive(fic_in)
    log("Reading : {}".format(archive))
    log(" - t0 = {0}".format(t0))
    log(" - j0 = {0}".format(j0))
    log(" - i0 = {0}".format(i0))

    tm  = archive.ntimes
    jm, = archive.get_metric("x", "t").axes["Y"].shape
    im, = archive.get_metric("x", "t").axes["X"].shape

    if t0 >= tm:
        log.error("t index t0 must be inferior to {0}.".format(tm))
        return
    if j0 >= jm:
        log.error("j index j0 must be inferior to {0}.".format(jm))
        return
    if i0 >= im:
        log.error("i index i0 must be inferior to {0}.".format(im))
        return

    var_dzw = archive.get_metric("z", "w", axes="ZYX", tpoint=t0)
    var_dzt = archive.get_metric("z", "t", axes="ZYX", tpoint=t0)
    var_zw = archive.get_zlevels("w", "TZ", name="zw_ij", stdname="zw_ij", hpoint=(j0, i0))
    var_zt = archive.get_zlevels("t", "TZ", name="zt_ij", stdname="zt_ij", hpoint=(j0, i0))

#     cst_alpha = 0.28
#     cst_beta  = 0.
    cst_alpha = 0.20
    cst_beta  = 0.77

    var_rho = puv.get_potential_density(archive, cst_alpha=cst_alpha, cst_beta=cst_beta)

    chunk_xy = np.index_exp[..., j0, i0]
    n2_n     = pui.compute_instant_bn2(var_rho, var_dzw, t0, cst_rho0=cst_rho0)
    n2       = n2_n[chunk_xy]
    valps, vecps = pui.compute_modes(3, n2, var_dzt, var_dzw, time_index=t0, chunk_xy=chunk_xy)

    celerite = 1. / np.sqrt(np.abs(valps))

    c0 = celerite[0]
    c1 = celerite[1]
    c2 = celerite[2]

    print "c0 =", c0
    print "c1 =", c1
    print "c2 =", c2

    v0 = vecps[0]
    v1 = vecps[1]
    v2 = vecps[2]

    v0 *= np.sign(v0[-1])
    v1 *= np.sign(v1[-1])
    v2 *= np.sign(v2[-1])

    def print_v(v):
        return "[\n " + "\n ".join("{0:.6e}".format(vi) for vi in v) + "\n]"

#     print "v0 = ", print_v(v0)
#     print "v1 = ", print_v(v1)
#     print "v2 = ", print_v(v2)

    zt = var_zt.get_instant_value(t0)
    zw = var_zw.get_instant_value(t0)

    if zt.shape == zw.shape:
        zt = zt[:-1]

    # Pour le plot, il faut que l'axe vertical soit orienté vers le haut.
    # Si ce n'est pas le cas, on le renverse.
    up  = 1 if var_rho.axes['Z'].positive == "up" else -1
    if up < 0:
        zt = -zt[::-1]
        zw = -zw[::-1]

    zt.mask[:]   = v0.mask[:]
    zw.mask[:-1] = zt.mask[:]

    print "Zt   :", zt
    print "Zw   :", zw
    print "N2   :", n2

    np.seterr(all='ignore')

    n2_max = n2.max()
    n2_min = n2.min()
    ndigits = np.floor(-np.log10(n2_max-n2_min))+1
    if ndigits > 9:
        ndigits -= 1
    n2_max = pup.ceil_to_digit (n2_max, ndigits)
    n2_min = pup.floor_to_digit(n2_min, ndigits)

    filename = "N2.png"
    ax = plt.subplot()
    ax.set_xlim(n2_min, n2_max)
    ax.plot(n2, -zw, label='$N^2$')
    ax.invert_yaxis()
    ax.xaxis.get_major_formatter().set_powerlimits((0,0))
    ax.legend()
    ax.ticklabel_format(useOffset=False)
    ax.minorticks_on()
    log("Plotting {}...".format(filename))
    plt.savefig(filename, dpi=150)
    plt.close()

    # Plot des 3 premiers modes, interpolation linéaire, valeurs aux point T
    filename = "Mode_barocline_linear.png"
    ax = plt.subplot()
    ax.plot(v2, -zt, label='C2={0:1.3f}'.format(c2))
    ax.plot(v1, -zt, label='C1={0:1.3f}'.format(c1))
    ax.step(v0, -zt, label='Barotrope')
    ax.plot([0,0], [0, -zw[0]], 'k--')
    ax.invert_yaxis()
    ax.legend(loc='best')
    ax.set_ylabel('$Depth$ $(m)$',fontsize=20)
    ax.set_title('$Mode Barotrope/Barocline$',fontsize=20)
    ax.tick_params(axis='both', which='major', labelsize=20)
    log("Plotting {}...".format(filename))
    plt.savefig(filename, dpi=150)
    plt.close()

    # Plot des 3 premiers modes, constant par morceaux, valeurs aux point T
    filename = "Mode_barocline_piecewise.png"
    import matplotlib.transforms as mt
    ax = plt.subplot()
    trans_data2 = mt.Affine2D().rotate_deg(-90) + ax.transData
    ax.step(zt, v2, where='mid', label='C2={0:1.3f}'.format(c2), transform=trans_data2)
    ax.step(zt, v1, where='mid', label='C1={0:1.3f}'.format(c1), transform=trans_data2)
    ax.step(zt, v0, where='mid', label='Barotrope', transform=trans_data2)
    ax.plot([0,0], [0, -zw[0]], 'k--')
    ax.invert_yaxis()
    ax.legend(loc='best')
    ax.set_ylabel('$Depth$ $(m)$',fontsize=20)
    ax.set_title('$Mode Barotrope/Barocline$',fontsize=20)
    ax.tick_params(axis='both', which='major', labelsize=20)
    log("Plotting {}...".format(filename))
    plt.savefig(filename, dpi=150)
    plt.close()


if __name__ == "__main__":

    parser = OptionParser(version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")
    parser.add_option("-t", "--time", dest="t0", default=0,  type="int",
                      help="time index to extract")
    parser.add_option("-j", "--j0",   dest="j0", default=1, type="int",
                      help="j index to extract")
    parser.add_option("-i", "--i0",   dest="i0", default=1, type="int",
                      help="i index to extract")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    main(args, options.t0, options.j0, options.i0)

