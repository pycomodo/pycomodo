#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import logging
import copy
import numpy as np
from optparse import OptionParser

import pycomodo
from pycomodo import log
from pycomodo.core.archive import Dimension
from pycomodo.compat import evaluate
from pycomodo import write_netcdf
import pycomodo.operators as op

def report_var(varname, var, idx=4):
    def format_num(num):
        kind = np.result_type(num).kind
        if num is np.ma.masked:
            return "--"
        elif kind == 'f':     # *num* is a float
            return "{0:+.4e}".format(num)
        elif kind == 'i':     # *num* is an int
            return "{0:3}".format(num)

    if len(var.shape) == 3:
        ib = np.index_exp[:idx,1,1]
        ie = np.index_exp[-idx:,1,1]
    else:
        ib = np.index_exp[1,:idx]
        ie = np.index_exp[1,-idx:]

    vshape = "("+",".join([format_num(v) for v in var.shape])+")"
    vmin = format_num(var.min())
    vmax = format_num(var.max())
    vib = "b:["+", ".join([format_num(v) for v in var[ib]])+"]"
    vie = "e:["+", ".join([format_num(v) for v in var[ie]])+"]"
    log.debug("{} {} min: {}, max: {}, {} {}".format(varname, vshape, vmin, vmax, vib, vie))

def choose(index, choices):
    """
    Pure-python implementation of numpy.choose, which has a bug when the
    size of *index* is greater than 32.
    Cf. http://mail.scipy.org/pipermail/numpy-discussion/2011-June/056881.html
    """
    igrd = np.mgrid[[np.s_[:k] for k in index.shape]]
    cindices = [index] + [igrd[k] for k in range(igrd.shape[0])]
    return choices[cindices]

def minmod(a, b):
    """
    For two scalars:
        minmod(a, b) = sign(a,b) * min(abs(a), abs(b))
    Here the implementation is for two numpy arrays.
    """
    minv = a*b <= 0.0
    mneg = ~minv & (a<0.0)

    sign = np.ones(a.shape)
    sign[minv] = 0
    sign[mneg] = -1
    return sign * np.minimum(np.absolute(a), np.absolute(b))

def remap(rho, z_win, z_wout, dpthin=1e-6):
    """
    Les notations D2009 font référence à:
    - D2009 : Debreu 2009, "Schémas de remapping pour Hybgen" (rapportinterweno)
    - S2001 : Shchepetkin and McWilliams, March 2001, "A Family of Finite-Volume Methods for
                  Computing Pressure Gradient Force in an Ocean Model with a Topography-Following
                  Vertical Coordinate" (unpublished)
    """
    dpi = z_win[1:] - z_win[:-1]

    ic  = np.index_exp[0,1]
    ick = np.index_exp[:] + ic

    report_var("rho    :", rho)
    report_var("z_win  :", z_win)
    report_var("z_wout :", z_wout)
    report_var("dpi    :", dpi)

    qtmp = dpi[:-2] + dpi[1:-1] + dpi[2:]
    lap_dpi = qtmp + dpi[1:-1]          # lap_dpi[k] <=> ∆ in D2009-(21c) <=> ∆₄  in S2001-(4.76)
    inv_dpi = np.reciprocal(qtmp)       # inv_dpi[k] <=> 1/(H[k-1]+H[k]+H[k+1]) in S2001

    # Compute half slopes : d⁰[k+1/2] = (ρ[k+1]−ρ[k])/(H[k+1]+H[k])
    #   half_slope[k] <=> d[k-1/2]  in D2009:(12)
    #                 <=> d⁰[k-1/2] in S2001:(4.68)
    half_slope = (rho[1:]-rho[:-1]) * np.reciprocal(dpi[1:]+dpi[:-1])

    # Compute interfacial deviations δ^{R,L} D2009:(20)-(22) or S2001:(4.75)-(4.77)
    #   δ^R′[k] = minmod(H[k]d⁰[k+1/2], ∆₄d⁰[k-1/2])
    #   δ^L′[k] = minmod(H[k]d⁰[k-1/2], ∆₄d⁰[k+1/2])
#     dR = dpi[1:-1] * half_slope[1:]    # dR[k] <=> dprs[k]*d[k+1/2] in D2009:(21a)
#     dL = dpi[1:-1] * half_slope[:-1]   # dL[k] <=> dprs[k]*d[k-1/2] in D2009:(21b)
#     dR2 = lap_dpi * half_slope[:-1]    # dR2[k] <=> ∆*d[k-1/2] in D2009:(21a)
#     dL2 = lap_dpi * half_slope[1:]     # dL2[k] <=> ∆*d[k+1/2] in D2009:(21b)
# #
#     # dR = minmod(dR, dR2)
#     # dL = minmod(dL, dL2)
#     with np.errstate(over='ignore'):
#         m = np.where(dR*dL < 0)
#         dR[m] = 0.0
#         dL[m] = 0.0
#     m1 = np.where(abs(dR) > abs(dR2))
#     m2 = np.where(abs(dL) > abs(dL2))
#     dR[m1] = dR2[m1]
#     dL[m2] = dL2[m2]
#
    dR = minmod(dpi[1:-1]*half_slope[1:],  lap_dpi*half_slope[:-1])
    dL = minmod(dpi[1:-1]*half_slope[:-1], lap_dpi*half_slope[1:])

    # Compute ɣ'[k] D2009:(22) or in S2001:(4.77)
    #   ɣ'[k] = (δ^R′[k]-δ^L′[k]) / (H[k-1]+H[k]+H[k+1])
    gamk = (dR - dL) * inv_dpi

    # Compute interfacial deviation D2009:(20) or S2001:(4.77)
    #   δ^R[k] = δ^R′[k] - H[k+1]γ′[k]
    #   δ^L[k] = δ^L′[k] + H[k-1]γ′[k]
    dR -= gamk * dpi[2:]       # /!\ Inversion entre dpi(k+1) et dpi(k-1) dans le manuscript D2009-(20)
    dL += gamk * dpi[:-2]      #            et entre H[k+1]   et H[k-1] dans S2001-(4.77)
    report_var("dR  :", dR)
    report_var("dL  :", dL)

    # Compute facial values and derivatives S2001:(4.70)
    rhoR = np.empty_like(dpi)
    rhoL = np.empty_like(dpi)
    rhoR[1:-1] = rho[1:-1] + dR
    rhoL[1:-1] = rho[1:-1] - dL
    wR = np.empty_like(dpi)
    wL = np.empty_like(dpi)
    wR[1:-1] = (2.0*dR - dL)**2  # wR(k) <=> WR[k]/(2/dprs[k])**2 ???
    wL[1:-1] = (2.0*dL - dR)**2  # wL(k) <=> WL[k]/(2/dprs[k])**2
    #
    m_extr = np.index_exp[[0,-1]]
    rhoL[m_extr] = rho[m_extr]
    rhoR[m_extr] = rho[m_extr]
    wR[m_extr] = 0.0
    wL[m_extr] = 0.0
    #
    m_thin = dpi <= dpthin
    rhoL[m_thin] = rho[m_thin]
    rhoR[m_thin] = rho[m_thin]
    wR[m_thin] = 0.0
    wL[m_thin] = 0.0

    report_var("rhoL :", rhoL)
    report_var("rhoR :", rhoR)
#     print rhoL[ick]
#     print rhoR[ick]
    report_var("wR :", wR)
    report_var("wL :", wL)
#     print wR[ick]
#     print wL[ick]

    # WENO reconciliation
    q001 = np.maximum(wR[1:],  1e-8)    # q001 <=> WR[k]  /(2/dprs[k])**2
    q002 = np.maximum(wL[:-1], 1e-8)    # q002 <=> WL[k-1]/(2/dprs[k-1])**2

    zw_shape = list(rho.shape)
    zw_shape[0] += 1
    zwork23 = np.empty(zw_shape)
    zwork23[1:-1] = ( q001*rhoR[:-1] + q002*rhoL[1:] ) / (q001+q002)
    zwork23[ 0] = 2.0*rho[ 0] - zwork23[ 1]
    zwork23[-1] = 2.0*rho[-1] - zwork23[-2]
    report_var("zw23 :", zwork23)
#     print zwork23[ick]

    # Reconstruction monotone          D2009:2.1.1  : zwork23(k) <-> rhoL[k]
    # internal monotonicity constraint S2001:(4.58) : zwork23(k) <-> ρ[k-1/2]
    # - assign
    dR = zwork23[1:] - rho[:]    # δ^R[k] = T[k+1/2] - T[k]
    dL = rho[:] - zwork23[:-1]   # δ^L[k] = T[k] - T[k-1/2]
    # - reject
    m = dR*dL < 0
    dR[m] = 0.0
    dL[m] = 0.0
    # - restrict
    dRm = 2.0 * dL
    dLm = 2.0 * dR
    mR = abs(dR) > abs(dRm)
    mL = abs(dL) > abs(dLm)
    dR[mR] = dRm[mR]
    dL[mL] = dLm[mL]
    report_var("dR  :", dR)
    report_var("dL  :", dL)
    dR = minmod(dR, 2*dL)
    dL = minmod(dL, 2*dR)
    report_var("dR  :", dR)
    report_var("dL  :", dL)

    # - assign
    rhoL[:] = rho[:] - dL
    rhoR[:] = rho[:] + dR
    report_var("rhoL :", rhoL)
    report_var("rhoR :", rhoR)
#     print "rhoL[ick] =", rhoL[ick]
#     print "rhoR[ick] =", rhoR[ick]

    _ = """
        zbot = 0.0
        kbot = 1
        """
    zbot = np.zeros_like(rho[0], dtype=np.float)
    zbot = z_wout[0]
    kbot = np.zeros_like(rho[0], dtype=np.int)
    report_var("zbot :", zbot)
    report_var("kbot :", kbot)

    so_shape = list(z_wout.shape)
    so_shape[0] -= 1
    so = np.zeros(so_shape)

    Ni = rho.shape[0]
    No = so.shape[0]

    _ = """
        do k = 1,No
    """
    for k in range(No):

#         print "--", k
        _ = """
            ztop = zbot  ! top is bottom of previous layer
            ktop = kbot
            if (ztop >= z_win(ktop+1)) then
                ktop = ktop + 1
            endif
            zbot = z_wout(k+1)
            zthk = zbot - ztop
        """
        ztop = zbot  # top is bottom of previous layer
        ktop = kbot
        ## ??? ktop[ztop >= z_win[ktop+1]] += 1
#        zwin_ktop = choose(ktop, z_win)
#        ktop[ztop < zwin_ktop] = -1 # TODO : traiter cette valeur avec un np.ma.masked
        zbot = z_wout[k+1]
        zthk = zbot - ztop
        report_var("ktop :", ktop)
        report_var("ztop :", ztop)
        report_var("zbot :", zbot)
        report_var("zthk :", zthk)

        _ = """
            if (zthk <= dpthin .or. ztop >= z_wout(No+1)) then
                if (k==1) then
                    so(k) = T_k(k)  ! from HYCOM_2.2
                else
                    so(k) = so(k-1)
                endif
                continue
            endif ! thin layer
        """
        mask_thin = (zthk <= dpthin) | (ztop >= z_wout[-1])
        if np.any(mask_thin):
            if k == 0:
                so[k, mask_thin] = rho[k,   mask_thin]
            else:
                so[k, mask_thin] = so[k-1, mask_thin]


        _ = """
            kbot = ktop
            do while (z_win(kbot+1) < zbot .and. kbot < Ni)
                kbot = kbot + 1
            enddo
        """
        kbot = np.argmin(z_win < zbot, 0)-1   # kbot contains the last index k where z_win[k] < zbot
        kbot = np.minimum(kbot, Ni-1)

        report_var("kbot :", kbot)

        _ = """
            zbox = zbot
            do k1 = k+1,No
                if (z_wout(k1+1)-z_wout(k1) > dpthin) then
                    exit  ! thick layer
                else
                    zbox = z_wout(k1+1)  !include thin adjacent layers
                    if (zbox == z_wout(No+1)) then
                        exit  ! at bottom
                    endif
                endif
            enddo
        """
        zbox = zbot
        report_var("zbox :", zbox)
        # TODO : include thin adjacent layers

        _ = """
            zthk = zbox - ztop
            kbox = ktop
            do while (z_win(kbox+1) < zbox .and. kbox < Ni)
                kbox = kbox + 1
            enddo
        """
        zthk = zbox - ztop
        kbox = np.argmin(z_win < zbox, 0)-1  # kbox contains the last index k where z_win[k] < zbox
        kbox = np.minimum(kbox, Ni-1)

        report_var("kbox :", kbox)
#         print "ktop[ic] =", ktop[ic]
#         print "kbox[ic] =", kbox[ic]
#         print "zwinktop[ic] =", z_win[ick][ktop[ic]]
#         print "zwinkbot[ic] =", z_win[ick][kbot[ic]]
#         print "zwinkpls[ic] =", z_win[ick][kbot[ic]+1]
#         print "ztop[ic]     =", ztop[ic]
#         print "zbox[ic]     =", zbox[ic]

        _ = """
            if (ktop == kbox) then
                if (z_wout(k) /= z_win(kbox) .or. z_wout(k+1) /= z_win(kbox+1) ) then
                    if (dpi(kbox) > dpthin) then
                        q001 = (zbox-z_win(kbox))/dpi(kbox)     !# zbox'
                        q002 = (ztop-z_win(kbox))/dpi(kbox)     !# ztop'
                        aL = q001**2 + q002**2 + q001*q002  &   !# alpha_L
                             + 1.0 - 2.0*(q001+q002)
                        aR = aL - 1.0 + (q001+q002)           !# alpha_R
                    else
                        aL = 0.0
                        aR = 0.0
                    endif
                    a0 = 1.0 - aL - aR  !# 1-alpha_L-alpha_R
                    so(k) = a0 * T_k(kbox)  + &       !# ^T[k]
                            aL * T_kL(kbox) + &
                            aR * T_kR(kbox)
                else
                    so(k) = si(kbox)
                endif
        """
        mask_single = (ktop == kbox)
        mask_outside = (ktop == -1) | (kbox == -1)

#         print "mask_single  : ", np.where(mask_single)
#         print "mask_outside : ", np.where(mask_outside)
        m = mask_single & mask_outside
#         print "m_outside  : ", np.where(m)[0].shape

        if np.any(m):
            so[k, m] = np.ma.masked
#             print so[k, m]

        # Reconstruction for a single layer ( kbot == kbox )
        m = ~mask_thin & mask_single
#         print "m_single   : ", np.where(m)[0].shape

        if np.any(m):
            dpi_kbox  = choose(kbox, dpi)[m]
            zwin_kbox = choose(kbox, z_win)[m]
            rho_kbox  = choose(kbox, rho)[m]
            rhoL_kbox = choose(kbox, rhoL)[m]
            rhoR_kbox = choose(kbox, rhoR)[m]
            zpR = ( zbox[m] - zwin_kbox ) / dpi_kbox
            zpL = ( ztop[m] - zwin_kbox ) / dpi_kbox

            aL = evaluate("zpR**2 + zpL**2 + zpR*zpL + 1.0 - 2.0*(zpR+zpL)")
            aR = evaluate("aL - 1.0 + (zpR+zpL)")
            aL[dpi_kbox < dpthin] = 0.0
            aR[dpi_kbox < dpthin] = 0.0
            a0 = 1.0 - aL - aR

            so[k, m] = evaluate("a0 * rho_kbox + aL * rhoL_kbox + aR * rhoR_kbox") # / zthk[m]

#         print "so[ic](1) = ", so[k][ic]
#         if m[ic]:
#             print " . dpi[kbox][ic] = ", dpi[ick][kbox[ic]]
#             print " . zin[kbox][ic] = ", z_win[ick][kbox[ic]]
#             print "   -> zpR[ic] = ", (zbox[ic]-z_win[ick][kbox[ic]]) / dpi[ick][kbox[ic]]
#             print "   -> zpL[ic] = ", (ztop[ic]-z_win[ick][kbox[ic]]) / dpi[ick][kbox[ic]]
#             print " . rho[kbox][ic] = ", rho[ick][kbox[ic]]
#             print " . rhoL[kbox][ic]= ", rhoL[ick][kbox[ic]]
#             print " . rhoR[kbox][ic]= ", rhoR[ick][kbox[ic]]

        _ = """
            else
                if     (ktop <= k .and. kbox >= k) then
                    ka = k
                elseif (kbox-ktop >= 3) then
                    ka = (kbox+ktop)/2
                elseif (dpi(ktop) >= dpi(kbox)) then
                    ka = ktop
                else
                    ka = kbox
                endif
                offset = T_k(ka)
        """
        # Reconstruction for a multiple layers ( kbot > kbox )
        m = ~mask_thin & ~mask_single
#         print "m_notsingle: ", np.where(m)[0].shape
        if not np.any(m):
            continue

        zwin_ktop   = choose(ktop,   z_win)[m]
        zwin_ktopp1 = choose(ktop+1, z_win)[m]
        zwin_kbox   = choose(kbox,   z_win)[m]
        dpi_ktop    = choose(ktop, dpi)[m]
        dpi_kbox    = choose(kbox, dpi)[m]
        rho_kbox    = choose(kbox, rho)[m]
        rho_ktop    = choose(ktop, rho)[m]
        rhoL_kbox   = choose(kbox, rhoL)[m]
        rhoL_ktop   = choose(ktop, rhoL)[m]
        rhoR_kbox   = choose(kbox, rhoR)[m]
        rhoR_ktop   = choose(ktop, rhoR)[m]

#        offset = 0.5*(T_ktop+T_kbox)

        _ = """
                qtop = z_win(ktop+1) - ztop  ! partial layer thickness
                if (dpi(ktop) > dpthin) then
                    q = (ztop-z_win(ktop)) / dpi(ktop)
                    aL = q*(q - 1.0)
                    aR = aL + q
                else
                    aL = 0.0
                    aR = 0.0
                endif
                a0 = 1.0 - aL - aR
                tsum = ( a0 * T_k(ktop)  + &
                         aL * T_kL(ktop) + &
                         aR * T_kR(ktop) - offset ) * qtop
        """
        qtop = zwin_ktopp1 - ztop[m]  # partial layer thickness
        q = (ztop[m]-zwin_ktop) / dpi_ktop
        aL = q*(q - 1.0)
        aR = aL + q

        aL[dpi_ktop < dpthin] = 0.0
        aR[dpi_ktop < dpthin] = 0.0

        a0 = 1.0 - aL - aR
        so[k, m]  += ( a0 * rho_ktop  + aL * rhoL_ktop + aR * rhoR_ktop ) * qtop / zthk[m]
#         print "so[ic](2) = ", so[k][ic]
#         if m[ic]:
#             print " . dpi[ktop][ic] = ", dpi[ick][ktop[ic]]
#             print " . zin[ktop][ic] = ", z_win[ick][ktop[ic]]
#             print " -> qtop[ic] = ", z_win[ick][ktop[ic]+1]-ztop[ic]
#             print " -> q[ic]    = ", (ztop[ic]-z_win[ick][ktop[ic]]) / dpi[ick][ktop[ic]]
#             print " . rho[ktop][ic] = ", rho[ick][ktop[ic]]
#             print " . rhoL[ktop][ic]= ", rhoL[ick][ktop[ic]]
#             print " . rhoR[ktop][ic]= ", rhoR[ick][ktop[ic]]

        _ = """
                qbot = zbox - z_win(kbox)  ! partial layer thickness
                if (dpi(kbox) > dpthin) then
                  q   = qbot / dpi(kbox)
                  aL = (q-1.0)**2
                  aR = aL - 1.0 + q
                else
                  aL = 0.0
                  aR = 0.0
                endif

                a0 = 1.0 - aL - aR
                tsum = tsum + ( a0 * T_k(kbox)  + &
                                aL * T_kL(kbox) + &
                                aR * T_kR(kbox) - offset ) * qbot
        """
        qbot = zbox[m] - zwin_kbox
        q = qbot / dpi_kbox
        aL = (q - 1.0)**2
        aR = aL - 1.0 + q

        aL[dpi_kbox < dpthin] = 0.0
        aR[dpi_kbox < dpthin] = 0.0
        a0 = 1.0 - aL - aR

        so[k, m] += ( a0 * rho_kbox  + aL * rhoL_kbox + aR * rhoR_kbox ) * qbot / zthk[m]
#         print "so[ic](3) = ", so[k][ic]
#         if m[ic]:
#             print " . dpi[kbox][ic] = ", dpi[ick][kbox[ic]]
#             print " . zin[kbox][ic] = ", z_win[ick][kbox[ic]]
#             print " -> qbot[ic] = ", (zbox[ic]-z_win[ick][kbox[ic]]) / dpi[ick][kbox[ic]]
#             print " -> q[ic] = ", (kbox[ic]-z_win[ick][kbox[ic]]) / dpi[ick][kbox[ic]]
#             print " . rho[kbox][ic] = ", rho[ick][kbox[ic]]
#             print " . rhoL[kbox][ic]= ", rhoL[ick][kbox[ic]]
#             print " . rhoR[kbox][ic]= ", rhoR[ick][kbox[ic]]

        _ = """
                do k1 = ktop+1,kbox-1
                    tsum = tsum + dpi(k1)*(T_k(k1) - offset)
                enddo
        """
        mask_inter = ((kbox-1) - (ktop+1)) > 0
        mask_inter = (kbox-ktop) > 1

        m = ~mask_thin & mask_inter
#         print "m_inter    : ", np.where(m)[0].shape
#         print "mask_thin  : ", np.where(mask_thin)[0].shape
#         print "mask_single: ", np.where(mask_single)[0].shape
#         print "mask_inter : ", np.where(mask_inter)[0].shape

        if np.any(m):

            kboxm1_inter = kbox[m]-1
            ktopp1_inter = ktop[m]+1

#             print "kbox[m]:", kbox[m].shape, kbox[m][:10]
#             print "kbot[m]:", kbot[m].shape, kbot[m][:10]
#             print "ktop[m]:", ktop[m].shape, ktop[m][:10]

            it = np.nditer([ktop[m]+1, kbox[m], so[k, m]],
                           flags=['c_index'], op_flags=['readwrite'])
            while not it.finished:
                ii = it.index
                kb, ke = it[0], it[1]
                it[2] += np.sum(dpi[kb:ke, m]*rho[kb:ke, m], 0)[ii] / zthk[m][ii]
                it.iternext()

            so[k, m] = it.operands[2][:]

        _ = """
                rpsum = 1.0 / zthk
                so(k) = offset + tsum * rpsum
            endif
        """

    # end loop: for k in range(No)

    return so

def remap_var(old_var, new_zaxis, new_zaxis_w):
    """
    Interpolates the content of *old_var* on the vertical axis *new_zaxis*.
    """
    old_archive = old_var.archive
    new_archive = new_zaxis.archive

    old_zaxis   = old_archive.find_coordinate_variable(axis="Z", location="t")
    old_zaxis_w = old_archive.find_coordinate_variable(axis="Z", location="w")

    if old_zaxis is None:
        raise Exception("Unable to find Z coordinate variable for 't' location")

    if old_zaxis_w is None:
        raise Exception("Unable to find Z coordinate variable for 'w' location")

    log("# {} ({}) -> {} ({})".format(old_zaxis, old_zaxis.position, old_zaxis_w, old_zaxis_w.position))

    # Add dimensions of 'old_var' to 'new_archive' (all but 'old_zaxis')"
    for dim_name in old_var.dimensions:
        if dim_name == str(old_zaxis):
            continue
        if dim_name not in new_archive.dimensions:
            log.debug("# remap_var: add {0} dimension & coordinate to new Archive".format(dim_name))
            # Copy dimension
            old_cvar = old_archive.variables[dim_name]
            new_cvar = new_archive.new_dimension(model=old_cvar)
            new_cvar[:] = old_cvar[:]
            new_archive.register_variable(new_cvar)

    log("# Finalize the initialization of 'new_archive'.")
    new_archive.complete_init()

    log("# Create new variable with new Z axis")
    new_name = "{0}_on_{1}".format(old_var, new_zaxis)
    new_dims = [str(new_zaxis) if dimname == str(old_zaxis) else dimname for dimname in old_var.dimensions]
    new_var  = new_archive.new_variable(new_name, new_dims, dtype=old_var.dtype, attrs=old_var.get_attributes())
    log('# old:\n' + old_var.info())
    log('# new:\n' + new_var.info())

    log("# Get staggered zlevels of ({0})".format(old_var))
    old_depth_w = old_var.get_zlevels(stag_axis='Z', name='old_depth_w')
    zmin, zmax = old_depth_w[:].min(), old_depth_w[:].max()
    if old_zaxis.positive == "up":
        zmin, zmax = -zmax, -zmin
    print "zmin, zmax:", zmin, zmax, " | position: ", op.axes_position_on_c_grid(old_depth_w.axes)

    log("# Fill values for new Z axis")
    new_zaxis_w[:] = np.linspace(zmin, zmax, new_zaxis_w.shape[0])
    new_zaxis[:]   = 0.5*(new_zaxis_w[1:] + new_zaxis_w[:-1])
    print "new_zaxis_w = ", new_zaxis_w[:]
    print "new_zaxis   = ", new_zaxis[:]

    log("# Compute staggered zlevels of new_var ({0})".format(new_var))
    new_depth_w = new_var.get_zlevels(stag_axis='Z', name='new_depth_w')

    for l in range(old_var.shape[0]):
        if old_zaxis.positive == "up":
            rho    = old_var[l,::-1]
            z_win = -old_depth_w[l,::-1]
        else:
            rho    = old_var[l]
            z_win = old_depth_w[l]
        new_var[l,:] = remap(rho, z_win, new_depth_w[l,:])

    return new_var

def test_remap(archive_in, nblevels, velocity):

    if nblevels is None:
        nblevels = 1

    log("# Create new empty archive")
    new_archive = pycomodo.Archive([])

    log("# Create new vertical dimensions")
    new_zdim_t_attrs = {"axis": "Z", "positive":"down", "standard_name":"depth", }
    new_zdim_w_attrs = {"axis": "Z", "positive":"down", "standard_name":"depth_at_w_location", "c_grid_axis_shift": -0.5}
    new_zaxis_t = new_archive.new_dimension("zdim_t", nblevels,   dtype=np.dtype('f'), attrs=new_zdim_t_attrs)
    new_zaxis_w = new_archive.new_dimension("zdim_w", nblevels+1, dtype=np.dtype('f'), attrs=new_zdim_w_attrs)

    log("# Compute old depths...")
    depth_t = archive_in.get_zlevels("t", name="depth_t")
    depth_w = archive_in.get_zlevels("w", name="depth_w")
    depths = (depth_t, depth_w)

    log("# Build list of vars to remap...")
    oldvars   = []
    variables = []
    for var in archive_in.variables.values():
        var_axes = "".join(var.axes.keys())
        if var_axes == "TZYX" and var not in depths:
            oldvars.append(var)
            log(" - {0} ;".format(var.info(1)))

    log("# Write old depths in 'depth_orig.nc'")
    write_netcdf("depth_orig.nc", depths)

    log("# Remaps original variables on new z axis")
    for var in oldvars:
        log("-"*32+"\n# remap '{0:20}' :".format(var))
        newvar = remap_var(var, new_zaxis_t, new_zaxis_w)
        new_archive.register_variable(newvar)
        variables.append(newvar)

    newdepth_t = new_archive.get_zlevels("t", name="new_depth_t")
    newdepth_w = new_archive.get_zlevels("w", name="new_depth_w")

    variables.append(newdepth_t)
    variables.append(newdepth_w)

    log("# Archive log :")
    log(new_archive.info(1))
    write_netcdf("remap.nc", variables)

if __name__ == "__main__":

    usage = """Usage: %prog NETCDF_FILE\n  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage, version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                        help="outputs debug messages")
    parser.add_option("-n", "--nblevels", dest="nblevels", default=None, type="int",
                        help="number of output levels")
    parser.add_option("-u", "--velocity", dest="velocity", action="store_true",
                        help="interpolate velocity instead of temperature")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    test_remap(pycomodo.Archive(args), options.nblevels, options.velocity)
