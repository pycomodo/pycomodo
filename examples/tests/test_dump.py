#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import logging
from optparse import OptionParser

import pycomodo
from pycomodo import log

def main(arch):

    log("--  Dump with verbose = 2 :")
    log("---------------------------")
    log(arch.info(2))
    log("--  Dump with verbose = 1 :")
    log("---------------------------")
    log(arch.info(1))
    log("--  Dump with verbose = 0 :")
    log("---------------------------")
    log(arch.info(0))

if __name__ == '__main__':

    usage = """Usage: %prog NETCDF_FILE\n  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage, version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    main(pycomodo.Archive(args))
