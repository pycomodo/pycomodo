#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import logging
import numpy as np
from optparse import OptionParser

import pycomodo
import pycomodo.util.vertical as puz
import pycomodo.operators as op
from pycomodo import log, UnknownVariable

def test_vinterp(archive, depth):

    try:
        log("=== Get u :")
        u = archive.get_variable(stdname='sea_water_x_velocity', location='t')
        u.check(True)
        log("=== Get v :")
        v = archive.get_variable(stdname='sea_water_y_velocity', location='t')
        v.check(True)
    except UnknownVariable as err:
        log.error("variable {0} not found in '{1}'".format(err, archive))
        return

    log("=== Compute horizontal velocity :")
    spd = op.operation("sqrt(u**2+v**2)", "hspeed", u=u, v=v)
    spd.check(True)

    log("=== Write speed.nc")
    pycomodo.write_netcdf("speed.nc", (u, v, spd))

    log("=== Compute Z levels :")
    z = spd.get_zlevels()
    z.check(True)

    log("=== Interpolate velocity on depth={0} :".format(depth))
    vslice = puz.vinterp( spd, depth, name="slice" )
    vslice.check(True)

    archive.register_variable(z)
    archive.register_variable(vslice)

    log("=== Write vinterp.nc")
    pycomodo.write_netcdf("vinterp.nc", (u, v, z, spd, vslice))
    log(" Done !\n")

if __name__ == "__main__":

    usage = """Usage: %prog NETCDF_FILE\n  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage, version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")
    parser.add_option("-v", "--depth", dest="depth", default=-1000,
                      help="cut depth in meters (default: -1000)")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    test_vinterp(pycomodo.Archive(args), float(options.depth))
