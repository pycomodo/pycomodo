#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import pycomodo

a = pycomodo.Archive("nc_files/masked.nc")

t = a.variables['t_var']
s = a.variables['s_var']

print "t : ", t
print t.info()
print t[:]
print "-"*50

print "s : ", s
print s.info()
print s[:]
print "-"*50

