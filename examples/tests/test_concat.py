#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import logging
import numpy as np
from optparse import OptionParser

import pycomodo
from pycomodo import log, write_netcdf

if __name__ == "__main__":

    usage = """Usage: %prog [options] NETCDF_FILE NETCDF_FILE """

    parser = OptionParser(usage, version="%prog {0}".format(pycomodo.__version__))

    (options, args) = parser.parse_args()

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    log.setLevel(logging.DEBUG)

    arch = pycomodo.Archive(args)

    log("= Dimensions :")
    for dim in arch.dimensions.values():
        print " - ", dim

    log("= Variables :")
    for varname, var in arch.variables.iteritems():
        print " - {0:15s}:".format(varname), type(var._var)

    write_netcdf("concat.nc", arch.variables.values())
