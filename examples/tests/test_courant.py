#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This module provides some functions to compute Courant numbers for various terms of the
primitive equations, based on model output files.
"""

import sys
import numpy as np

import pycomodo
import pycomodo.util.courant   as puc
import pycomodo.util.variables as puv
import pycomodo.operators as op

from pycomodo import write_netcdf
from pycomodo import log, UnknownVariable
from pycomodo.compat import evaluate


def write_courant_internal ( archive, dt ):
    """
    Computes Courant number for first baroclinic mode.
    """
    log("=== Get metrics...")
    var_dxt = archive.get_metric("x", "t")
    var_dyt = archive.get_metric("y", "t")
    var_dzt = archive.get_metric("z", "t", axes="ZYX")
    var_dzw = archive.get_metric("z", "w", axes="ZYX")

    try:
        # Do we have a NEMO ORCA output ?
        depthw = archive.get_variable("hdepw")
        cst_g     = 9.806
        cst_rho   = 1000.
        cst_alpha = 2.0e-1
        cst_beta  = 7.7e-1
    except:
        # Something went wrong, assume it is ROMS baroclinic vortex
        cst_g     = 9.81
        cst_rho   = 1024.4
        cst_alpha = 2.8e-1
        cst_beta  = 0.
        pass

    log("=== Compute internal Courant number...")
    c1, cfl_i = puc.get_courant_internal(archive, dt, var_dzt, var_dzw,
                                          cst_g, cst_rho, cst_alpha, cst_beta,
                                          ncpus=1, ntiles=1)

    log("=== Writing 'courant_internal.nc'...")
    write_netcdf("courant_internal.nc", (var_dxt, var_dyt, c1, cfl_i))


def write_courant_advection ( archive, dt ):
    """
    Computes Courant number for advective terms.
    """

    log("=== Get metrics...")
    dx_t = archive.get_metric("x","t")
    dy_t = archive.get_metric("y","t")
    dz_t = archive.get_metric("z","t", axes="ZYX")

    vars_out = []

    log("=== Compute Courant numbers...")
    volume = op.operation("1/(dx*dy*dz)", name="volume", dx=dx_t, dy=dy_t, dz=dz_t)

    try:
        cw_adv = puc.get_courant_advection( archive, dt, 'w', name='cfl_adv_w', volume=volume )
        cw_adv.set_fill_value(999.)
        vars_out.append(cw_adv)
    except UnknownVariable as e:
        log.error("Unable to compute 'cw_adv' because we couldn't find standard name {0}".format(e))

    try:
        cu_adv = puc.get_courant_advection( archive, dt, 'u', name='cfl_adv_u', volume=volume )
        cu_adv.set_fill_value(999.)
        vars_out.append(cu_adv)
    except UnknownVariable as e:
        log.error("Unable to compute 'cu_adv' because we couldn't find standard name {0}".format(e))

    try:
        cv_adv = puc.get_courant_advection( archive, dt, 'v', name='cfl_adv_v', volume=volume )
        cv_adv.set_fill_value(999.)
        vars_out.append(cv_adv)
    except UnknownVariable as e:
        log.error("Unable to compute 'cv_adv' because we couldn't find standard name {0}".format(e))

    dims_2d = [str(dim) for dim in cu_adv.axes.values() if dim.axes["Z"] is None]
#     cfl_adv_1_3d  = archive.new_variable("cfl_adv_L1_3d",   model=cu_adv, fill_value=999)
#     cfl_adv_2_3d  = archive.new_variable("cfl_adv_Lmax_3d", model=cu_adv, fill_value=999)
    cfl_adv_1_max = archive.new_variable("cfl_adv_L1_max",   dimensions=dims_2d, dtype=cu_adv.dtype, fill_value=999)
    cfl_adv_2_max = archive.new_variable("cfl_adv_Lmax_max", dimensions=dims_2d, dtype=cu_adv.dtype, fill_value=999)
    kmax_adv_1 = archive.new_variable("kmax_adv_L1",   dimensions=dims_2d, dtype=np.int16, fill_value=9999)
    kmax_adv_2 = archive.new_variable("kmax_adv_Lmax", dimensions=dims_2d, dtype=np.int16, fill_value=9999)

    data_u = cu_adv[:]
    data_v = cv_adv[:]
    data_w = cw_adv[:]

    mask_u = data_u.mask
    mask_v = data_v.mask
    mask_w = data_w.mask

    # L^1 norm
    cadv_1 = np.ma.empty_like(data_u)
    cadv_1     [:] = evaluate("data_u + data_v + data_w")
    cadv_1.mask[:] = evaluate("mask_u | mask_v | mask_w")

    # L^inf norm ; we cannot use evaluate because numexpr does not support numpy.maximum yet.
    cadv_2 = np.maximum(np.maximum(data_u, data_v), data_w)

#     cfl_adv_1_3d[:]  = cadv_1
#     cfl_adv_2_3d[:]  = cadv_2
    cfl_adv_1_max[:] = np.max(cadv_1, axis=1)
    cfl_adv_2_max[:] = np.max(cadv_2, axis=1)
    kmax_adv_1[:]    = np.argmax(cadv_1, axis=1)
    kmax_adv_2[:]    = np.argmax(cadv_2, axis=1)
    kmax_adv_1[cfl_adv_1_max.mask] = np.ma.masked   # Because np.argmax does not propagate masked values
    kmax_adv_2[cfl_adv_2_max.mask] = np.ma.masked

#     vars_out.append(cfl_adv_1_3d)  ; archive.register_variable(cfl_adv_1_3d)
#     vars_out.append(cfl_adv_2_3d)  ; archive.register_variable(cfl_adv_2_3d)
    vars_out.append(cfl_adv_1_max) ; archive.register_variable(cfl_adv_1_max)
    vars_out.append(cfl_adv_2_max) ; archive.register_variable(cfl_adv_2_max)
    vars_out.append(kmax_adv_1)    ; archive.register_variable(kmax_adv_1)
    vars_out.append(kmax_adv_2)    ; archive.register_variable(kmax_adv_2)

    log("=== Writing 'courant_adv.nc'...")
    write_netcdf("courant_adv.nc", vars_out)


def write_courant_coriolis ( archive, dt ):
    """
    Computes Courant number for Coriolis term.
    """
    log("=== Compute Coriolis Courant number...")
    var_f = puv.get_coriolis(archive, name="f")
    var_f.check(True)

    c_cor = puc.get_courant_coriolis(archive, dt, var_f)

    log("=== Writing 'courant_coriolis.nc'...")
    write_netcdf("courant_coriolis.nc", c_cor)


def main():

    from optparse import OptionParser
    from logging import DEBUG

    usage = """Usage: %prog -t DT NETCDF_FILE [NETCDF_FILE]...
    NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm"""

    parser = OptionParser(usage, version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")
    parser.add_option("-t", "--dt", dest="dt", default=None,
                      help="baroclinic time step used in simulation [REQUIRED]")
    (options, args) = parser.parse_args()

    if options.dt is None:
        log.error("Option '-t'/'--dt' is required.")
        parser.print_help()
        sys.exit(-1)

    else:
        dt = float(options.dt)

    if options.debug:
        log.setLevel(DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    a = pycomodo.Archive(args)

    write_courant_advection(a, dt)
    write_courant_coriolis(a, dt)
    write_courant_internal(a, dt)

if __name__ == "__main__":

    main()
