#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import logging
import numpy as np
from optparse import OptionParser

import pycomodo
import pycomodo.util.variables as puv
import pycomodo.util.vorticity as pux
import pycomodo.operators as op
from pycomodo import log, UnknownVariable

from pycomodo.core.dimension import Dimension

RHO_ISO = np.asarray([
    24.260719299316406,
    24.730625152587891,
    25.200534820556641,
    25.670442581176758,
    26.140661239624023,
    26.984518051147461,
    27.454427719116211,
    27.924335479736328,
    28.394243240356445,
    28.864152908325195 ])


def get_pv(archive_iso, rho_iso, var_zt, var_zf, var_sigma, var_ssh, var_sfd, var_totvort, tindex):

    # extract data from NetCDF file
    ssh   = var_ssh.get_instant_value(tindex, dtype=np.float32)
    sfd   = var_sfd.get_instant_value(tindex, dtype=np.float32)

    # In the following, we suppose that Zindex==0 means sea surface.
    if var_zt.axes["Z"].positive == "up":
        zt      =   -var_zt.get_instant_value(tindex, dtype=np.float64)[::-1]
        zf      =   -var_zf.get_instant_value(tindex, dtype=np.float64)[::-1]
        sigma   = var_sigma.get_instant_value(tindex, dtype=np.float64)[::-1]
        totvort = var_totvort.get_instant_value(tindex, dtype=np.float32)[::-1]
        rho_iso = rho_iso[::-1]
    else:
        zt      =    var_zt.get_instant_value(tindex, dtype=np.float64)
        zf      =    var_zf.get_instant_value(tindex, dtype=np.float64)
        sigma   = var_sigma.get_instant_value(tindex, dtype=np.float64)
        totvort = var_totvort.get_instant_value(tindex, dtype=np.float32)

    # Create new variable to store depth of isodensity layers interfaces
    z_int_dims = archive_iso.get_coordinate_system('w', axes="ZYX")
    var_z_int  = archive_iso.new_variable("z_int", z_int_dims, dtype=np.float64)
    var_z_int.set_fill_value(-9999)

    # Compute depth of isodensity layers interfaces
    var_z_int[:] = pux.compute_z_iso(rho_iso, sigma, zt, sfd, ssh)

    # Interpolate depth of isodensity layers interfaces at 'f' points
    var_z_int_f = var_z_int.interpolate('fw')

    # Compute potential vorticity
    return pux.compute_pv(var_z_int_f[:], zf, totvort)

def test_pv(archive_z, tindex):

    ###
    ### Gather data from input archive
    ###
    try:
        var_u     = archive_z.get_variable(stdname='sea_water_x_velocity', location='u')
        var_v     = archive_z.get_variable(stdname='sea_water_y_velocity', location='v')
        var_sigma = archive_z.get_variable(stdname='sea_water_sigma_theta')
        var_corio = archive_z.get_variable(stdname='coriolis_parameter',   location='f')
        var_ssh   = puv.get_ssh(archive_z)
        var_sfd   = puv.get_sea_floor_depth(archive_z)
        var_zt    = archive_z.get_zlevels('t', axes="ZYX", tpoint=tindex)
        var_zw    = archive_z.get_zlevels('w', axes="ZYX", tpoint=tindex)
        var_zf    = archive_z.get_zlevels('f', axes="ZYX", tpoint=tindex)

    except UnknownVariable as err:
        log.error("variable {0} not found in '{1}'".format(err, archive_z))
        return

    # Compute z-level (total) vorticity
    var_vort_z  = puv.get_vorticity_2d(var_u, var_v)
    var_totvort = op.operation("vort+corio", name="totvort_z", vort=var_vort_z, corio=var_corio)

    ##
    ## Create new empty archive for iso levels
    ##
    archive_iso = pycomodo.Archive([])

    # Create new dimension & associated coordinate variable for nlev_iso
    dim_iso_t_name = "nlev_iso"
    dim_iso_w_name = "nlev_iso_w"
    dim_iso_t_attr = var_zt.axes['Z'].get_attributes()
    dim_iso_w_attr = var_zw.axes['Z'].get_attributes()

    nlev = len(RHO_ISO)
    var_iso_t = archive_iso.new_dimension(dim_iso_t_name, nlev,   dtype=np.float32, attrs=dim_iso_t_attr)
    var_iso_w = archive_iso.new_dimension(dim_iso_w_name, nlev+1, dtype=np.float32, attrs=dim_iso_w_attr)

    # Add horizontal axes to new Archive
    for cvar in archive_z.coordinate_variables.itervalues():
        if cvar.axes['Z'] is None:
            archive_iso.new_dimension(model=cvar)

    # Complete new archive initialization
    archive_iso.complete_init()

    ###
    ### Create new variable for potential vorticity
    ###
    dims_pv = archive_iso.get_coordinate_system('f', axes='ZYX')
    var_pv  = archive_iso.new_variable("pv", dims_pv, dtype=var_sigma.dtype)
    var_pv.set_fill_value(-1.)
    var_pv[:] = get_pv(archive_iso, RHO_ISO, var_zt, var_zf, var_sigma, var_ssh, var_sfd, var_totvort, tindex)

    log("=== Write z_old.nc")
    pycomodo.write_netcdf("z_old.nc", (var_zt, var_zf, var_vort_z, var_totvort))

    log("=== Write z_iso.nc")
    pycomodo.write_netcdf("z_iso.nc", (var_pv,))

    log(" Done !\n")

if __name__ == "__main__":

    usage = """Usage: %prog NETCDF_FILE\n  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage, version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")
    parser.add_option("-t", "--tindex", type=int, default=0,
                      help="Time index")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    test_pv(pycomodo.Archive(args), options.tindex)
