#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import logging
sys.path.insert(0, os.path.abspath('..'))

import pycomodo
import pycomodo.operators as op

from pycomodo import log
from optparse import OptionParser

def mini_grids(archive):

    t = archive.get_variable('t_var')
    u = archive.get_variable('u_var')
    v = archive.get_variable('v_var')
    f = archive.get_variable('f_var')

    log("="*30)
    log("= Calculate d(t)/dx")
    dtdx = t.diff_x()

    log("="*30)
    log("= Calculate d(u)/dx")
    dudx = u.diff_x()

    log("="*30)
    log("= Calculate d(v)/dx")
    dvdx = v.diff_x()

    log("="*30)
    log("= Calculate d(f)/dx")
    dfdx = f.diff_x()

    log("="*30)
    log("= Calculate d(t)/dy")
    dtdy = t.diff_y()

    log("="*30)
    log("= Calculate d(u)/dy")
    dudy = u.diff_y()

    log("="*30)
    log("= Calculate d(v)/dy")
    dvdy = v.diff_y()

    log("="*30)
    log("= Calculate d(f)/dy")
    dfdy = f.diff_y()

    log("="*30)
    log("= Calculate div(u,v)")
    div2d = op.add(dudx,dvdy,"div2d")

    log("="*30)
    log("= Calculate curl2d(u,v)")
    curl2d = op.sub(dvdx,dudy,"curl2d")

    log("="*30)
    log("= Write du.nc...")
    pycomodo.write_netcdf("du.nc", (t,u,v,f,dtdx,dtdy,dudx,dudy,dvdx,dvdy,dfdx,dfdy,div2d,curl2d))

if __name__ == "__main__":

    usage = """Usage: %prog NETCDF_FILE\n  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage, version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    a = pycomodo.Archive(args)
    mini_grids(a)
