#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import matplotlib.pyplot as plt

from optparse import OptionParser
from logging import DEBUG

import pycomodo
from pycomodo import log, UnknownVariable
from pycomodo import write_netcdf
import pycomodo.util.variables as puv
import pycomodo.operators as op

def calc_pvort_2d(archive):

    try:
        log("=== Call get_ssh")
        ssh   = puv.get_ssh(archive)
        ssh.check(True)
        log("=== Call get_coriolis :")
        corio = puv.get_coriolis(archive)
        corio.check(True)
        log("=== Get ubar :")
        ubar  = archive.get_variable(stdname='barotropic_sea_water_x_velocity_at_u_location')
        ubar.check(True)
        log("=== Get vbar :")
        vbar  = archive.get_variable(stdname='barotropic_sea_water_y_velocity_at_v_location')
        vbar.check(True)
        log("=== Get depth :")
        depth = puv.get_sea_floor_depth(archive)
        depth.check(True)

    except UnknownVariable as err:
        log.error("variable {0} not found in '{1}'".format(err, archive))

    else:
        log("== Compute htot = ssh + depth at 'f' points)")
        htot_t = op.operation("ssh+depth", name="htot_t", ssh=ssh, depth=depth)
        htot_f = htot_t.interpolate('f')

        log("== Compute 2d vorticity curl = dv/dx-du/dy")
        curl2d = puv.get_vorticity_2d(ubar, vbar, "curl2d")
        curl2d.set_fill_value(999.)

        log("== Compute pv2d = (f+curl)/h")
        pv2d = op.operation("(f+curl)/h", name="pv2d", curl=curl2d, f=corio, h=htot_f)
        pv2d.set_fill_value(999.)

        log("== Write pvort2d.nc")
        write_netcdf("pvort2d.nc", (curl2d, pv2d))

    try:
        log("=== Get u :")
        u = archive.get_variable(stdname='sea_water_x_velocity_at_u_location')
        u.check(True)
        log("=== Get v :")
        v = archive.get_variable(stdname='sea_water_y_velocity_at_v_location')
        v.check(True)
        log("=== Get dz :")
        dz = archive.get_metric("z", "f")
        dz.check(True)

    except UnknownVariable as err:
        log.error("variable {0} not found in '{1}'".format(err, archive))

    else:
        log("== Compute 2d vorticity curl = dv/dx-du/dy")
        curl = puv.get_vorticity_2d(u, v, "curl", dtype=float)
        curl.set_fill_value(999.)

        log("== Compute pv3d = (f+curl)/dz")
        pv3d = op.operation("(f+curl)/dz", name="pv3d", curl=curl, f=corio, dz=dz)
        pv3d.set_fill_value(999.)

        log("== Write pvort3d.nc")
        write_netcdf("pvort3d.nc", (curl, pv3d))

    log(" Done !\n")


if __name__ == "__main__":

    usage = """Usage: %prog NETCDF_FILE\n  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage, version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    a = pycomodo.Archive(args)
    calc_pvort_2d(a)
