#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
sys.path.insert(0, os.path.abspath('..'))

import matplotlib.pyplot as plt
import numpy as np
try:
    from mpl_toolkits.basemap import Basemap
    _basemap_present = True
except ImportError:
    _basemap_present = False

from optparse import OptionParser
from logging import DEBUG

import pycomodo
import pycomodo.util.variables as puv
import pycomodo.util.plots as pup
from pycomodo import log
from pycomodo import UnknownVariable

def plot_ssh(archive, draw_map=False):

    try:
        ssh = puv.get_ssh(archive)
    except UnknownVariable as err:
        log.error("variable {0} not found in '{1}'".format(err, archive))
        return

    log("=== Checking ssh variable : ")
    ssh.check(True)

    log("=== Plotting variable '%s' :" % ssh.standard_name)
    coords = ssh.get_auxiliary_coordinates()
    try:
        lons, lats = coords['X'], coords['Y']
    except KeyError:
        pycomodo.log.warning("Error when computing lats/lons. I am not using geographical data.")
        dims_map = {}
        for dim_name in ssh.dimensions:
            coord_var = archive.get_variable(dim_name)
            dims_map[coord_var.axis] = coord_var[:]
        lons, lats = np.meshgrid(dims_map['X'], dims_map['Y'])
        draw_map = False

    fig = plt.figure(1)
    ax = fig.add_axes([0.1,0.1,0.8,0.8])

    latlon_slice = ssh.get_dynamic_slice("X", "Y")

    # If we have a time coordinate, let's plot just the last chunk
    if len(ssh.shape) > 2:
        lasti = ssh.shape[0]-1  # Fix for NetCDF4 bug with '-1' indexing
        ssh_slice = np.index_exp[lasti] + latlon_slice
    else:
        ssh_slice = latlon_slice

    lons = lons[latlon_slice]
    lats = lats[latlon_slice]

    try:
        Z = np.ma.masked_values(ssh[ssh_slice], ssh._FillValue)
    except AttributeError:
        Z = ssh[ssh_slice]

    Z_range = [ Z.min(), Z.max() ]
    Z_range = pup.enlarge_range(Z_range, digits=2)
    Z_ticks = pup.ticks_from_range(Z_range, digits=2, nticks=5)

    if draw_map and _basemap_present:
        bmap = Basemap(projection='merc',resolution='i',
                      llcrnrlat=lats.min(), urcrnrlat=lats.max(),
                      llcrnrlon=lons.min(), urcrnrlon=lons.max())
        x,y = bmap(lons,lats)

        cs = bmap.pcolormesh(x,y,Z,vmin=Z_ticks.min(),vmax=Z_ticks.max())

        bmap.fillcontinents(color='0.8',lake_color='aqua')
        bmap.drawcoastlines(linewidth=0.25)
        bmap.drawcountries(linewidth=0.25)
        bmap.drawmapboundary()
        bmap.drawrivers(linewidth=0.1)
        bmap.drawmeridians(np.arange(-180,180,2),linewidth=0.25,labels=[0,0,0,1],fontsize=8)
        bmap.drawparallels(np.arange(-90,90,2),linewidth=0.25,labels=[1,0,0,0],fontsize=8)

    else:
        cs = plt.pcolormesh(lons,lats,Z,vmin=Z_ticks.min(),vmax=Z_ticks.max())
        ax = cs.get_axes()
        ax.set_xlim(lons.min(), lons.max())
        ax.set_ylim(lats.min(), lats.max())

    cbar = fig.colorbar(cs, orientation='horizontal', ticks=Z_ticks)
    cbar.ax.set_xticklabels(Z_ticks)

    try:
        var_name = ssh.long_name.capitalize()
    except AttributeError:
        var_name = str(ssh)
    try:
        var_unit = ssh.units
        plot_title = u'{0} — [{1}]'.format(var_name, var_unit)
    except:
        plot_title = u'{0}'.format(var_name)

    plt.title(plot_title, fontsize='large', family='serif')

    log("=== Write output to ssh.png.")
    plt.savefig('ssh', dpi=200)
    log(" Done !\n")

if __name__ == "__main__":

    usage = """Usage: %prog NETCDF_FILE\n  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage, version="%prog {0}".format(pycomodo.__version__))

    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                        help="outputs debug messages")
    parser.add_option("-m", "--draw_map", action="store_true", dest="draw_map", default=False,
                        help="plot field on a geographical map using Basemap")


    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    a = pycomodo.Archive(args)
    plot_ssh(a, options.draw_map)
