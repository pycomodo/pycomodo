#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
sys.path.insert(0, os.path.abspath('..'))

import matplotlib.pyplot as plt
import numpy as np

from optparse import OptionParser
from logging import DEBUG

import pycomodo
from pycomodo import log
from pycomodo.exceptions import UnknownVariable
import pycomodo.util.variables as puv

def compute_vortex_trace(archive, out_filename, use_latlon):

    try:
        log("=== Get time :")
        time = archive.var_by_standard_name('time')
        time.check(True)
        log("=== Get ubar :")
        ubar = archive.var_by_standard_name('barotropic_sea_water_x_velocity_at_u_location')
        ubar.check(True)
        log("=== Get vbar :")
        vbar = archive.var_by_standard_name('barotropic_sea_water_y_velocity_at_v_location')
        vbar.check(True)

    except UnknownVariable as err:
        log.error("variable {0} not found in '{1}'".format(err, archive))
        return

    log("== Compute curl2d = get_vorticity_2d(ubar, vbar)")
    curl2d = puv.get_vorticity_2d(ubar, vbar)
    cdata = np.ma.masked_values(curl2d[:], curl2d.get_fill_value())

    ntimes = len(time[:])

    if use_latlon:
        try:
            latf = puv.get_variable_at_f_location(archive, 'latitude')
            lonf = puv.get_variable_at_f_location(archive, 'longitude')
        except UnknownVariable:
            use_latlon = False
            log.warning("Unable to find correct latitude or longitude. We will instead output grid indices.")

    with open(out_filename,'w') as fout:
        for k in range(ntimes):

            trace_str = "{0} ; {1}".format(time[k], np.min(cdata[k,:,:]))

            min_idx = np.argmin(cdata[k,:,:])
            min_ij  = np.unravel_index(min_idx, cdata[k,:,:].shape)
            if use_latlon:
                latmin = latf[...,min_ij[0],min_ij[1]]
                lonmin = lonf[...,min_ij[0],min_ij[1]]
                trace_str += " ; {0} ; {1}".format(lonmin, latmin)
            else:
                trace_str += " ; {0} ; {1}".format(min_ij[0], min_ij[1])

            fout.write(trace_str+"\n")


if __name__ == "__main__":

    usage = """Usage: %prog NETCDF_FILE\n  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage, version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")
    parser.add_option("-l", "--latlon", action="store_true", dest="use_latlon", default=False,
                      help="use latitude/longitude if available in data source")
    parser.add_option("-o", "--output", dest="trace_name", metavar="OUTPUT",
                      help="name of output file")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(DEBUG)

    if options.trace_name is None:
        trace_base = os.path.basename(args[0])
        trace_name = "trace_{0}.txt".format(os.path.splitext(trace_base)[0])
    else:
        trace_name = options.trace_name

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    archive = pycomodo.Archive(args)

    compute_vortex_trace(archive, trace_name, options.use_latlon)
