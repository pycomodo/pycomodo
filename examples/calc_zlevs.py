#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import logging
import numpy as np

from optparse import OptionParser

import pycomodo
import pycomodo.operators as op
import pycomodo.util.variables as puv
from pycomodo import log

if __name__ == "__main__":

    usage = """Usage: %prog NETCDF_FILE\n  NETCDF_FILE: Name of a NetCDF file that complies (roughly) the Comodo norm."""

    parser = OptionParser(usage, version="%prog {0}".format(pycomodo.__version__))
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="outputs debug messages")

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)

    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)

    archive = pycomodo.Archive(args)
    ssh = puv.get_ssh(archive)

    ny, nx = ssh.shape[-2:]

    def print_z(z, name):
        for n in range(0,max(z.shape[0]/20,1)):
            print "{0}, t={1}".format(name, n),
            for zl in z[n,:,ny/2,nx/2]:
                if zl is np.ma.masked:
                    print "{0:10}".format(zl),
                else:
                    print "{0:.4e}".format(zl),
            print "|ssh: {0:.4e}".format(ssh[n,ny/2,nx/2])

    def print_dz(dz, name):
        for n in range(0,max(z.shape[0]/20,1)):
            print "{0}, t={1}".format(name, n),
            for zl in z[n,:,ny/2,nx/2]:
                if zl is np.ma.masked:
                    print "{0:10}".format(zl),
                else:
                    print "{0:.4e}".format(zl),
            print ""

    dx = archive.metrics['dx_t']

    for stag in (None, 'Z'):

        print "Staggering : {0}".format(stag)
        print "="*20

        # Compute cell elevation
        z = dx.get_zlevels(stag_axis=stag)
        z.check(True)
        print_z(z, str(z))

        # Compute cell thickness
        dz = z[:,1:,:,:] - z[:,:-1,:,:]
        print_dz(dz, "dz_numpy ")

        dz = pycomodo.operators.differentiate.diff_z(z, dummy=True)
        print_dz(dz[:], "dz_comodo")

        dz = archive.get_metric("z", "w")
        print_dz(dz[:], "dz_metric")
