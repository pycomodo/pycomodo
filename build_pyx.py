#!/usr/bin/env python
# -*- coding: utf-8 -*-

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

import numpy

def main():

    include_dirs = [ numpy.get_include() ]

    extensions = [
        Extension("util._internal",     ["pycomodo/util/pyx_internal.pyx"],  include_dirs=include_dirs),
        Extension("util._vorticity",    ["pycomodo/util/pyx_vorticity.pyx"], include_dirs=include_dirs),
    ]

    setup(
        ext_package = "pycomodo",
        ext_modules = extensions,
        cmdclass    = {"build_ext": build_ext},
        script_args = ["build_ext", "-i"]
    )

if __name__ == "__main__":

    main()
